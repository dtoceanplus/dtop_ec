FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt .
COPY setup.py .
COPY manage.py .

COPY src/instance/dtop_tidal_wakes_db_red.msgpack ./wake_database/
COPY src/dtop_energycapt/ ./src/dtop_energycapt/

RUN apt update && \
    apt install --yes --no-install-recommends fort77 gfortran && \
    pip install --requirement requirements.txt && \
    pip install --editable . && \
    pip install dredd-hooks && \
    pip install pytest pytest-cov python-dotenv && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes fort77 gfortran

ENV FLASK_APP dtop_energycapt.service

EXPOSE 5000
ENV DATABASE_URL sqlite://///db/energycapt.db
# ENTRYPOINT ["tail", "-f", "/dev/null"]







# FROM continuumio/miniconda3

# WORKDIR /app

# COPY dependency.yml .
# RUN conda config --set always_yes True
# RUN conda env create --name dtop_energycapt_env --file dependency.yml
# RUN echo "source activate dtop_energycapt_env" >> ~/.bashrc
# ENV PATH /opt/conda/envs/dtop_energycapt_env/bin:$PATH

# RUN conda install -c conda-forge nodejs
# RUN npm install dredd
# RUN pip install dredd-hooks

# COPY setup.py .
# RUN mkdir src
# RUN pip install -e .

# ENV FLASK_APP dtop_energycapt.service
# EXPOSE 5000

# ENV DATABASE_URL sqlite://///db/energycapt.db

# CMD make all
