# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

# # manual config
import os
from logging.handlers import RotatingFileHandler
if not os.path.isdir('./logs'):
    os.mkdir('./logs')
# logging.basicConfig(filename='/logs/mc.log', filemode='w',
#                      format='%(asctime)s :: %(name)s - %(levelname)s - %(message)s')
# logger = logging.getLogger()
# logger.setLevel(logging.INFO)
# file_handler = RotatingFileHandler('/logs/mc.log','w', 1000000,1)
# formatter = logging.Formatter('%(asctime)s :: %(name)s - %(levelname)s :: %(message)s', '%Y-%m-%d %H:%M:%S')
# file_handler.setFormatter(formatter)
# logger.addHandler(file_handler)

# log file config
from dtop_energycapt.service.logsetup import LOGGING_CONFIG
import logging.config
logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger()

logger.warning('Started Main Module')

logger.info('+'*50)
logger.info('+'*50)
logger.info('DTOcean+: Energy Capture Module v1.1.0')
logger.info('+'*50)
logger.info('+'*50)
