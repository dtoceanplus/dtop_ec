# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, url_for, make_response, request, jsonify
from flask import current_app as app
from datetime import datetime as dt

from dtop_energycapt.service.models import db, Project, Farm, Device
from dtop_energycapt.service.schemas import ProjectSchema

import numpy as np


bp = Blueprint('routes', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

# @bp.route('/', methods=['GET'])
# def get_all_routes():
#     result = []
#     for el in app.url_map.iter_rules():
#         print(el)
#         result.append(str(el))

#     return make_response(jsonify(result), 200, headers)

# @bp.route('/test', methods=['GET'])
# def create_user():
#     """Create a user."""
#     username = request.args.get('user')
#     email = request.args.get('email')
#     if username and email:
#         existing_project = Project.query.filter(Project.username == username or Project.email == email).first()
#         if existing_project:
#             return make_response(f'{username} ({email}) already created!')
#         new_project = Project(username=username,
#                                 email=email,
#                                 created=dt.now(),
#                                 bio="In West Philadelphia born and raised, on the playground is where I spent most of my days",
#                                 admin=False)  # Create an instance of the User class
#         db.session.add(new_project)  # Adds new User record to database
#         db.session.commit()  # Commits all changes
    
#     all_projects = Project.query.all()
#     projects_schema = ProjectSchema(many=True)
#     return make_response(jsonify(projects_schema.dump(all_projects)), 200, headers)


# @bp.route('/clear', methods=['GET'])
# def clear_db():
#     try:
#         num_rows_deleted = db.session.query(Project).delete()
#         num_rows_deleted = db.session.query(Farm).delete()
#         num_rows_deleted = db.session.query(Device).delete()
#         db.session.commit()
#     except:
#         db.session.rollback()
#         return make_response(jsonify({'message': 'DB NOT cleared'}), 200, headers)

#     return make_response(jsonify({'message': 'DB cleared'}), 404, headers)
    


# @bp.route('/add', methods=['GET'])
# def add_db_project():
#     prj = Project(title='test',
#                 desc='project description',
#                 complexity=1,
#                 type='WEC',
#                 date='09/12/2019',
#                 status=20,tags='wec')
#     db.session.add(prj)
#     db.session.commit()

#     farm = Farm(name='farm test',
#             numberDevice=20,
#             qFactor=0.98,
#             aep=10300.3,
#             capturedPower=102304e3,
#             resourceReduction=0.97,
#             _capturedPowerEachSiteCondition='',
#             project=prj)
    
#     db.session.add(farm)
#     db.session.commit()

#     for dev in range(2):
#         device = Device(name=f'device {dev}',
#                             qFactor=0.98,
#                             aep=10300.3,
#                             capturedPower=102304e3,
#                             northing=0,
#                             easting=0,
#                             farm=farm,
#                             capturedPowerPerCondition={'siteConditionID': [0],
#                                                         'capturedPower': [12354.313110241861]},
#                             project=prj)
       
#         db.session.add(device)
#     db.session.commit()

#     device = Device.query.first()
#     print(device.capturedPowerPerCondition)


#     return make_response(jsonify({'message': 'project added to the DB'}), 200, headers)