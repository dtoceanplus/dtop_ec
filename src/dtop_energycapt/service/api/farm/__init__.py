# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/env python3
from flask import Blueprint, current_app, jsonify, make_response, request
import requests
from dtop_energycapt import business

from dtop_energycapt.service.models import db, Project, Farm, Device, EnergyCaptureInputs
from dtop_energycapt.service.schemas import ProjectSchema, FarmSchema, DeviceSchema, EnergyCaptureInputsSchema

from dtop_energycapt.service import config

bp = Blueprint('api_farm', __name__)

headers = {'Access-Control-Allow-Headers': 'Content-Type'}

# @bp.route('/ec/<id>/tec/1/calculate', methods=['GET'])
@bp.route('/ec/<id>/results', methods=['GET'])
def ec_farm_design(id):
    try:
        # TODO: move this check on the frontend
        project = Project.query.get(id)
        if project.farm:
            return jsonify({
                'message':
                'farm already exist in the project. Delete the resource first!'
            }), 409
    except:
        return jsonify({'message': 'project does not exist in the DB'}), 404

    try:
        project_inputs = project.inputs
        input_schema = EnergyCaptureInputsSchema()

        inputs = input_schema.dump(project_inputs)
        site_inputs = input_schema.dump(inputs).pop('site_url', None)
        machine_inputs = input_schema.dump(inputs).pop(
            'machine_url', None)
        machine = requests.get(f'{request.url_root}{machine_inputs[1:]}').json()
        site = requests.get(f'{request.url_root}{site_inputs[1:]}').json()
        inputs.update(machine['data'])
        inputs.update(site['data'])
    except:
        return jsonify({'message': 'cannot fetch the input data'}), 404

    try:
        obj = business.ArrayFactory.get_array(
                            project.type.upper(), str(project.complexity))
        # obj = config.PROJECT_OBJECTS[str(id)]

        obj.get_inputs('test', int(id), **inputs)
        obj.calc_powprod()
    
        farm, devices = obj.send_output()
    except:
        return jsonify({'message': 'calculation failed'}), 404


    # TODO: Add a check request validation
    # TODO: check if the project object is
    # obj = config.PROJECT_OBJECTS[str(id)]
    # obj.get_inputs('test', int(id), **request.get_json())
    # obj.calc_powprod()
    # farm, devices = obj.send_output()

    # print(farm)
    # print(devices)
    try:
        farm_db = Farm(project=project)
        farm_db.update(**farm)
        db.session.add(farm_db)
        db.session.commit()

        # insert devices into the DB table
        for dev in devices:
            device = Device(farm=farm_db, project=project)
            device.update(**dev)
            db.session.add(device)

        db.session.commit()

        project.status = 100
        db.session.commit()

        farm_schema = FarmSchema()
        devices_schema = DeviceSchema(many=True)
 
        response = {'farm': farm_schema.dump(farm_db),
                    'devices': devices_schema.dump(project.devices)}
        
        return jsonify(response), 200
    except:
        return jsonify({'message': 'a problem occured while saving the data in the DB'}), 404
        
# TODO: remove this route, the POST action is not delegated to the DB push in the calculation
@bp.route('/ec/<id>/farm', methods=['POST'])
def post_ec_farm(id):
    try:
        project = Project.query.get(id)
        # if project.farm:
        #     # 409 Conflict
        #     """
        #     The request could not be completed due to a conflict with the current state of the resource.
        #     This code is only allowed in situations where it is expected that the user might be 
        #     able to resolve the conflict and resubmit the request. 
        #     The response body SHOULD include enough information for the user to recognize the source of the conflict. 
        #     Ideally, the response entity would include enough information for the user or user agent to fix the problem; 
        #     however, that might not be possible and is not required.

        #     Conflicts are most likely to occur in response to a PUT request. For example, 
        #     if versioning were being used and the entity being PUT included changes to a resource which conflict 
        #     with those made by an earlier (third-party) request, the server might use the 409 response to 
        #     indicate that it can't complete the request. In this case, the response entity would likely 
        #     contain a list of the differences between the two versions in a format defined by the response 
        #     Content-Type.
        #     """
        #     return jsonify({
        #         'message':
        #         'farm already exist in the project. Use the PUT method instead'
        #     }), 409

        farm = Farm(project=project)
        farm.update(**request.get_json())

        db.session.add(farm)
        db.session.commit()
        return jsonify({'message': 'farm added'}), 201
    except:
        return jsonify({'message': 'no farm added'}), 400


@bp.route('/ec/<id>/farm', methods=['GET'])
def get_ec_farm(id):
    try:
        project = Project.query.get(id)
        if project is None:
            return jsonify({'message': f'Project ID-{id} not found'}), 404
        farm = project.farm
        
        # farm = Farm.query.filter_by(project_id=id).first()
        farm_schema = FarmSchema()
        try:
            dictionary = farm_schema.dump(farm)
            ids = dictionary['layout']['deviceID']
            dictionary['layout']['deviceID'] = [el-min(ids)+1 for el in ids]
            if project.type.lower() == 'tec':
                nb_rotors = project.inputs.machine_model.get('number_rotor', 1)
                if nb_rotors > 1:
                    nb_turbines = len(ids)
                    nb_machines = nb_turbines // nb_rotors
                    
                    dictionary['layout']['deviceID'] = list(range(1, nb_machines+1))
                    easting = dictionary['layout']['easting']
                    dictionary['layout']['easting'] = [sum(easting[i::nb_machines])/nb_rotors for i in range(len(easting) // nb_rotors)]
                    northing = dictionary['layout']['northing']
                    dictionary['layout']['northing'] = [sum(northing[i::nb_machines])/nb_rotors for i in range(len(northing) // nb_rotors)]
                    dictionary['number_devices'] = nb_machines

        except Exception as e:
            print(f'error: {str(e)}')
            
        return jsonify(dictionary), 200

    except Exception as e:
        return jsonify({"status": "error", 'message': f"farm not found: {str(e)}"}), 404

@bp.route('/ec/<id>/farm', methods=['DELETE'])
def delete_ec_farm(id):
    try:
        project = Project.query.get(id)
        if project is None:
            return jsonify({'message': f'Project ID-{id} not found'}), 404
        farm = project.farm
        if not farm:
            raise IOError(f'Farm result is not available for project {id}')
        db.session.delete(farm)
        db.session.commit()

        return jsonify({"status": "success", 'message': 'Farm deleted from the project'}), 200

    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404


@bp.route('/ec/<id>/farm', methods=['PUT'])
def put_ec_farm(id):

    try:
        farm = Farm.query.filter_by(project_id=id).first()
        if not farm:
            return jsonify({
                'message':
                'The request could not be completed due to a conflict with the current state of the resource.'
            }), 409
        farm_body = request.get_json()

        farm.update(**farm_body)
        db.session.commit()

        return jsonify({'message': 'Farm updated'}), 200

    except:
        return jsonify({'message': 'Farm not updated'}), 404


# @bp.route('/ec/<id>/farm', methods=['DELETE'])
# def delete_ec_farm(id):

#     try:
#         farm = Farm.query.filter_by(project_id=id).first()
#         if not farm:
#             raise IOError(f'Farm result is not available for project {id}')

#         db.session.delete(farm)
#         db.session.commit()

#         return jsonify({'message': 'Farm deleted'}), 200

#     except:
#         return jsonify({'message': 'Farm not deleted'}), 404