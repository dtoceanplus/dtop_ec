# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/env python3
from flask import Blueprint, current_app, jsonify, make_response, request
import requests
from dtop_energycapt import business

from dtop_energycapt.service.models import db, Project, Farm, Device, EnergyCaptureInputs
from dtop_energycapt.service.schemas import ProjectSchema, FarmSchema, DeviceSchema, EnergyCaptureInputsSchema

from dtop_energycapt.service import config
import numpy as np

bp = Blueprint('api_devices', __name__)

headers = {'Access-Control-Allow-Headers': 'Content-Type'}

@bp.route('/ec/<id>/devices', methods=['POST'])
def post_ec_devices(id):
    try:
        device_list = request.get_json()
        project = Project.query.get(id)
        farm = project.farm
        if not farm:
            return jsonify({'message': 'The request could not be completed due to a conflict with the current state of the resource. Device depends on Farm and the Farm is not present yet'}), 409

        for dev in device_list:
            device = Device(farm=farm, project=project)
            device.update(**dev)
            db.session.add(device)

        db.session.commit()
        return jsonify({'result': {'message': 'farm added'}}), 201
    except:
        return jsonify({'result': {'message': 'no farm added'}}), 400

@bp.route('/ec/<id>/devices', methods=['GET'])
def get_ec_machine_assemblies(id):
    # after the discussion with ET the machine_assemblies route
    # is added. The device will be used to define the rotor, 
    # while the machine assembly is used to define the complete machine unit, 
    # potentially with multiple rotors.
    # assuming a case with 3 machines each with two rotors (devices),
    # the convesion assumes that the devices are related to the rotor as follow:
    # m1, m2, m3 -> m1r1, m2r1, m3r1, m1r2, m2r2, m3r2
    # and so on, so the additional rotors will be appended at the the end of the machines lists.
    try:
        project = Project.query.get(id)
        devices = project.devices
        if not devices:
            return jsonify({
                'message':
                'The request could not be completed due to a conflict with the current state of the resource. No Device has been added to the project yet'
            }), 409

        devices_schema = DeviceSchema(many=True)
        devices = devices_schema.dump(project.devices)

        # find first ID for this particular farm
        ids = [dev["id"] for dev in devices]
        start_id = min(ids)
        for d in devices:
            d['id'] -= start_id - 1  # number the devices starting from 1 for the given project ID

        if project.type.lower() == 'tec':
            nb_rotors = project.inputs.machine_model.get('number_rotor', 1)
            # check if the number of rotors is larger then 1
            if nb_rotors > 1:
                # assemble the machines based on the number or rotors and the device list
                nb_turbines = len(devices)
                nb_machines = nb_turbines // nb_rotors
                _devices = []
                for im in range(1, 1+nb_machines):  # the devices where renumbered with 1 base
                    # find each devices and each associated rotor
                    _dev = []
                    for ir in range(nb_rotors):
                        _dev.append(next((item for item in devices if item["id"] == im+ir*nb_machines), False))
                    _devices.append(makeAssembly(_dev))
                devices = _devices
        
        return jsonify(devices), 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': f'bad request: {str(e)}'}), 404

def makeAssembly( turbines ):
    """make_assmbly is a method to sum two devices stored in a dictionary
    """
    # we used the first element as the base one
    c = turbines[0]

    c['aep'] = np.sum([o['aep'] for o in turbines])
    c['captured_power'] = np.sum([o['captured_power'] for o in turbines])
    c['easting'] = np.mean([o['easting'] for o in turbines])
    c['northing'] = np.mean([o['northing'] for o in turbines])
    c['q_factor'] = np.mean([o['q_factor'] for o in turbines])
    c['captured_power_per_condition']['capturedPower'] = np.sum([o['captured_power_per_condition']['capturedPower'] for o in turbines], 0).tolist()

    return c

@bp.route('/ec/<id>/rotors', methods=['GET'])
def get_ec_devices(id):
    try:
        project = Project.query.get(id)
        devices = project.devices
        if not devices:
            return jsonify({
                'message':
                'The request could not be completed due to a conflict with the current state of the resource. No Device has been added to the project yet'
            }), 409

        devices_schema = DeviceSchema(many=True)
        rotors = devices_schema.dump(devices)

        # reference data structure
        # r1d1 r1d2 r1d3 r2d1 r2d2 r2d3
        # find first ID for this particular farm
        ids = [dev["id"] for dev in rotors]
        start_id = min(ids)
        for d in rotors:
            d['id'] -= start_id - 1  # number the devices starting from 1 for the given project ID

        if project.type.lower() == 'tec':
            nb_rotors = project.inputs.machine_model.get('number_rotor', 1)
            if nb_rotors > 1:
                nb_turbines = len(rotors)
                nb_machines = nb_turbines // nb_rotors
                
                for im in range(1, 1+nb_machines):
                    # find each devices and each associated rotor
                    for ir in range(nb_rotors):
                        rotor = [item for item in rotors if item["id"] == im+ir*nb_machines][0]
                        rotor['deviceID'] = im

                return jsonify(rotors), 200

        for rotor in rotors:
            rotor['deviceID'] = rotor['id']

        return jsonify(rotors), 200

    except:
        return jsonify({'message': 'bad request'}), 404

@bp.route('/ec/<id>/devices', methods=['DELETE'])
def delete_ec_devices(id):
    try:
        project = Project.query.get(id)
        devices = project.devices
        if not devices:
            return jsonify({
                'message':
                'The request could not be completed due to a conflict with the current state of the resource. No Device has been added to the project yet'
            }), 404

        for device in devices:
            db.session.delete(device)
        db.session.commit()


        return jsonify({"status": "success", 'message': 'Devices deleted from the project'}), 200

    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404


@bp.route('/ec/<id>/devices/<dev_id>', methods=['GET'])
def get_ec_device(id, dev_id):
    # try:
    #     device = Device.query.filter_by(project_id=id, id=dev_id).one()
    #     device_schema = DeviceSchema()
    #     # print(device.captured_power_per_condition)
    #     # print(device_schema.dump(device))
    #     return jsonify(device_schema.dump(device)), 200
    try:
        project = Project.query.get(id)
        devices = project.devices
        if not devices:
            return jsonify({
                'message':
                'The request could not be completed due to a conflict with the current state of the resource. No Device has been added to the project yet'
            }), 409

        devices_schema = DeviceSchema(many=True)
        devices = devices_schema.dump(project.devices)
        if project.type.lower() == 'tec':
            nb_rotors = project.inputs.machine_model.get('number_rotor', 1)
            # check if the number of rotors is larger then 1
            if nb_rotors > 1:
                # assemble the machines based on the number or rotors and the device list
                nb_turbines = len(devices)
                nb_machines = nb_turbines // nb_rotors
                # find first ID for this particular farm
                ids = [dev["id"] for dev in devices]
                start_id = min(ids)
                _devices = []
                for im in range(start_id, start_id+nb_machines):
                    # find each devices and each associated rotor
                    _dev = []
                    for ir in range(nb_rotors):
                        _dev.append(next((item for item in devices if item["id"] == im+ir*nb_machines), False))
                    _devices.append(makeAssembly(_dev))
                devices = _devices
        
        device = list(filter(lambda x: x['id'] == int(dev_id), devices))
        if not device:
            raise RuntimeError(f'Invalid device ID: device{dev_id} not saved into the DB')
                
        return jsonify(device), 200
    except Exception as e:
        return jsonify({'message': f'bad request. Error message: {str(e)}'}), 404


@bp.route('/ec/<id>/devices/<dev_id>', methods=['DELETE'])
def delete_ec_device(id, dev_id):
    try:
        device = Device.query.filter_by(project_id=id, id=dev_id).one()
        db.session.delete(device)
        db.session.commit()

        return jsonify({'message': 'device deleted'}), 200
    except:
        return jsonify({'message': 'bad request'}), 404
