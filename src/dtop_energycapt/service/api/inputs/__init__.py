# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/env python3
from flask import Blueprint, current_app, jsonify, make_response, request
import requests
import os

from dtop_energycapt import business

from dtop_energycapt.service.models import db, Project, Farm, Device, EnergyCaptureInputs
from dtop_energycapt.service.schemas import *
from marshmallow import ValidationError

from dtop_energycapt.service import config
import json 

import utm
import re
from scipy.interpolate import NearestNDInterpolator
import numpy as np

import logging

LOG = logging.getLogger(__name__)

bp = Blueprint('api_inputs', __name__)

headers = {'Access-Control-Allow-Headers': 'Content-Type'}

import requests
import os

# Common function.
# to get DTOP basic module URL and headers
# module - DTOP module nichname
def get_url_and_headers(module):    
    protocol = os.getenv('DTOP_PROTOCOL', 'http')
    domain = os.environ['DTOP_DOMAIN']
    auth = os.getenv('DTOP_AUTH','')    

    header_auth = f'{auth}'
    headers = {}
    headers['Authorization'] = header_auth
    header_host = f'{module + "." + domain}'

    server_url = f'{protocol + "://" + header_host}'
    try:
        response = requests.get(server_url, headers=headers)
        module_api_url = f'{server_url}'
    except requests.ConnectionError as exception:
        docker_ws_ip = "http://172.17.0.1:80"  # why this is hardcoded?
        module_api_url = f'{docker_ws_ip}'
        headers['Host'] = header_host

    return module_api_url, headers

def interp2grid(request_data, x, y):
    """ interpolate data from [latitude,longitude,value] into the grid data specified in the x, y axis
    """
    x = np.array(x)
    y = np.array(y)
    lat, lon = request_data['latitude'], request_data['longitude']
    points = np.array([list(utm.from_latlon(*el)[:2]) for el in zip(lat, lon)])

    interp = NearestNDInterpolator(list(zip(points[:,0], points[:,1])), np.array(request_data['value']))
    X, Y = np.meshgrid(x, y)
    Z = interp(X, Y)
    del X, Y, interp, lat, lon, points
    return Z.tolist()

def binString2Center(bins):
    """ converts a list of string representing the min and max value of a bin from an histogram to a
    float that represents the center value of the bin
    """
    if bins is None:
        raise RuntimeError('The data fetched from the SC module contians an invalid bin defintion for the EJDP (Hs, Tp, Dir)')
    if len(bins) == 0:
        raise RuntimeError('The data fetched from the SC module contians an invalid bin defintion for the EJDP (Hs, Tp, Dir)')
    
    bin_centers = []
    for bin in bins:
        try:
            bin_centers.append(np.mean([float(el) for el in bin.split('-')]))
        except Exception as e:
            raise RuntimeError(f'Invalid bins format for the EJDP (Hs, Tp, Dir). Error: {str(e)}')

    return bin_centers

@bp.route('/ec/<id>/inputs', methods=['DELETE'])
def ec_inputs_delete(id):
    try:
        project = Project.query.get(id)
        if project is None:
            raise IOError('Wrong project ID')
        inputs = project.inputs
        if inputs is None:
            raise IOError(f'Project ID {id} does not have any input to DELETE')
        else:
            db.session.delete(inputs)
            db.session.commit()

        return jsonify({'message': f'Inputs deleted for project {id}'}), 200
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400

@bp.route('/ec/<id>/inputs/farm', methods=['PUT'])
def ec_inputs_farm_put(id):

    try:
        project = Project.query.get(id)
        # TODO: check if project as a farm, cos we are changing the input while the ouput has already been calculated
        body = request.get_json()
        if not project.inputs is None:
            farm_inputs_db = project.inputs
        else:
            # push the Input and update the project status
            farm_inputs_db = EnergyCaptureInputs(project=project)
        farm_inputs_db.update(**body)
        db.session.add(farm_inputs_db)
        db.session.commit()

        # project.status = 25
        db.session.add(project)
        db.session.commit()

        input_schema = EnergyCaptureInputsSchema()
        return jsonify(input_schema.dump(farm_inputs_db)), 201
    except Exception as e:
        return jsonify({'status': 'error', 'message': 'Layout input could not be save to DB: str{e}'}), 400


@bp.route('/ec/<id>/inputs/farm', methods=['GET'])
def ec_inputs_farm_get(id):

    try:
        project = Project.query.get(id)
        project_inputs = project.inputs
        if not project_inputs is None:
            if project_inputs.layout_type is None:
                return jsonify(
                    {'message': 'Layout inputs has not been saved yet'}), 404
            input_schema = EnergyCaptureInputsSchema()
            farm_inputs_db = {
                k: input_schema.dump(project_inputs)[k]
                for k in input_schema.dump(project_inputs)
                if k not in ['site', 'machine']
            }
        else:
            return jsonify(
                {'message': 'Layout inputs has not been saved yet'}), 404

        return jsonify(farm_inputs_db), 200
    except Exception as e:
        return jsonify(
            {'status': 'error', 'message': f'Layout inputs schema cannot be returned: str{e}'}), 404

@bp.route('/project/<id>/inputs/site/upload', methods=['POST'])  # TODO: remove! This is due to a typo in the fron end
@bp.route('/ec/<id>/upload/inputs/site/tec/complexity1', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/site/tec/complexity2', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/site/tec/complexity3', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/site/wec/complexity1', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/site/wec/complexity2', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/site/wec/complexity3', methods=['POST'])
def ec_inputs_site_upload_post(id):
    try:
        project = Project.query.get(id)
        if project is None:
            raise IndexError(f'Project ID ({id}) not found')

        binary_json = request.files['file'].read()
        my_json = binary_json.decode('utf-8')
        body = json.loads(my_json)
        try:
            SiteSchemaValidator(project.type, 
                                    project.complexity,
                                    body['data'])
        except ValidationError as err:
            raise ValidationError(f"Input schema validation error: {err.messages}") 
        site_inputs_db = project.inputs
        if site_inputs_db is None:
            site_inputs_db = EnergyCaptureInputs(project=project)
        site_inputs_db.site_model=body['data']
        site_inputs_db.site_url=body['url']
        db.session.add(site_inputs_db)
        db.session.commit()
        return jsonify({"status": "success", 'message': "Site data uploaded to the DB"}), 201
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400

def findNearestNotNaN(data, X, Y):
    """cretes a Nearest Neiborough Interpolant with the not corrupted data
    and reinterpolate the data into the original regular grid.
    """
    indexNaN = np.isnan(data)
    dataWOnan = data[~indexNaN]
    xy = np.c_[X[~indexNaN].ravel(), Y[~indexNaN].ravel()]
    interp = NearestNDInterpolator(xy, dataWOnan.ravel())
    return interp(X, Y)

def checkNaN(data):
    """Ensures the farm/scenarii_matrices/current does not contain NaN
    The NaN are replaced by the closest value
    """
    keys = ['SSH', 'U', 'V', "TI"]

    if not np.any(np.isnan(data['SSH'])):
        return False
    LOG.warning('The data from the Site Characterization module contains NaN.')
    LOG.warning('The system will attempt to interpolate the data, but the results might be inconsistent.')
    # X and Y are in lat lon
    x = np.array(np.unique(data['X']))
    y = np.array(np.unique(data['Y']))
    X, Y = np.meshgrid(x, y, indexing='ij')

    for k in keys:
        value = np.array(data[k])
        accumulator = np.zeros(value.shape)
        for ip in range(value.shape[0]):
            accumulator[ip,:,:] = findNearestNotNaN(value[ip,:,:], X, Y)
        data[k] = accumulator.tolist()
    
    # this data is 2D not 3D so it must be considered differently
    for k in ["soil_characteristic", "bathymetry"]:
        accumulator = findNearestNotNaN(np.array(data[k]), X, Y)
        data[k] = accumulator.tolist()

    return True

def checkDataSize(data):
    u_shape = np.shape(data['U'])
    v_shape = np.shape(data['V'])
    x_shape = np.shape(np.unique(data['X']))
    y_shape = np.shape(np.unique(data['Y']))
    p_shape = np.shape(data['probability'])

    if len(p_shape) > 1:
        raise ValueError(f'Dimension mismatch. Probbility vector must be a vector (e.g. shape(9,)). Current given shape (p: {p_shape}).')

    if p_shape[0] != u_shape[0]:
        raise ValueError(f'Dimension mismatch. First size of the Easting (U: {u_shape}) velocity component do not match the shape of the probbility vector (p: {p_shape}).')
        
    if u_shape != v_shape:
        raise ValueError(f'Dimension mismatch. Shape of Easting (U: {u_shape}) and Northing: (V: {v_shape}) velocity components do not match.')

    if x_shape[0] not in u_shape or len(x_shape) > 1:
        raise ValueError(f'Dimension mismatch. Shape of easting axis (X: {x_shape}) does not match the shape of velocity components (U: {u_shape}).')
    
    if y_shape[0] not in u_shape or len(y_shape) > 1:
        raise ValueError(f'Dimension mismatch. Shape of northing axis (Y: {y_shape}) does not match the shape of velocity components (U: {u_shape}).')

@bp.route('/ec/<id>/inputs/site/fetch', methods=['POST'])
def ec_tech_inputs_site(id):
    # THE DEFINITION OF THE CONVENTION USED IN THE SITE DATA CAN BE FOUND HERE:
    # https://link.springer.com/referenceworkentry/10.1007%2F0-387-30843-1_506
    # TIDAL (GOING TO)
    # WAVE (COMING FROM)
    warning = False  # used to return Warning to the user if data contains NaN

    try:
        project = Project.query.get(id)
        if project is None:
            raise IndexError(f'Project ID ({id}) not found')
        complexity = project.complexity
        machine = project.type.lower()
    except IndexError as e:
        return jsonify({"status": "error", 'message': str(e)}), 400
    
    try:
        sc_api_url, headers = get_url_and_headers("sc")
        siteID = request.get_json()['entityID']
        base_site_url = f'{sc_api_url}/sc/{siteID}'
        body = {}
        # Basic Request shared for all the Cases
        farm = requests.get(f'{base_site_url}/farm/info', headers=headers).json()

        lat, lon = farm['boundary']['latitude'][:-1], farm['boundary']['longitude'][:-1]  # removed last point since it is a repetition of the first value
        lease = [list(utm.from_latlon(*el)[:2]) for el in zip(lat, lon)]
        body['loc_position'] = lease
        # Request specific for the different case
        if complexity == 1 and machine == 'tec':
            # /sc/1/point/statistics/currents/Basic/mag -> mean
            body['loc_resource'] = requests.get(f'{base_site_url}/point/statistics/currents/Basic/mag', headers=headers).json().get('mean')

        elif complexity == 2 and machine == 'tec':
            #  /sc/1/point/direct_values/bathymetry -> value
            body['bathymetry'] = requests.get(f'{base_site_url}/point/direct_values/bathymetry', headers=headers).json().get('value')
            #  /sc/1/point/statistics/currents/EJPD/mag_theta -> bin1, bin2, pdf
            raw_data = requests.get(f'{base_site_url}/point/statistics/currents/EJPD/mag_theta', headers=headers).json()
            # modify request data to create a velocity vector
            mag = np.array(raw_data.get('bins1'))
            mag = 0.5*(mag[1:]-mag[:-1]) + mag[:-1]
            theta = np.array(raw_data.get('bins2'))
            theta = 0.5*(theta[1:]-theta[:-1]) + theta[:-1]
            pdf = np.array(raw_data.get('pdf'), dtype=float)
            # Ensure the PDF is normalized
            np.divide(pdf, pdf.sum(), out=pdf, casting='unsafe')
            #print(pdf)

            m,t = np.meshgrid(mag, theta, indexing='ij')
            MAG = np.sum(m*pdf)
            TH = np.sum(t*pdf)  # GOING TO CONVENTION 0 TO NORTH - 90 TO EAST
            vel = MAG*np.exp(1j*np.deg2rad(-TH+90))  # ANGLE CONVERSION TO CARTESIAN

            u, v = np.real(vel), np.imag(vel)  # x-easting, v-northing
            body['velocity_field'] = [u, v]

        elif complexity == 3 and machine == 'tec':
            # /sc/1/farm/scenarii_matrices/currents -> x,y,U,V,p,TI,SSH,id
            base_data = requests.get(f'{base_site_url}/farm/scenarii_matrices/currents', headers=headers).json()
            base_data.pop('__type__', None)
            base_data.pop('name', None)
            base_data.pop('description', None)
            base_data['X'] = base_data.pop('x')
            base_data['Y'] = base_data.pop('y')
            base_data['probability'] = base_data.pop('p')
            checkDataSize(base_data)
            
            base_data.pop('id')
            body = {**body, **base_data}
            # /sc/1/farm/direct_values/bathymetry -> value, latitude, longitude
            bathy = requests.get(f'{base_site_url}/farm/direct_values/bathymetry', headers=headers).json()
            
            body['bathymetry'] = interp2grid(bathy, base_data['X'], base_data['Y'])

            # /sc/1/farm/direct_values/roughness_length -> value, latitude, longitude
            roughness_length = requests.get(f'{base_site_url}/farm/direct_values/roughness_length', headers=headers).json()
            body['soil_characteristic'] = interp2grid(roughness_length, base_data['X'], base_data['Y'])
            body['power_law_exponent'] = (np.zeros(np.shape(body['soil_characteristic'])) + 0.7).tolist()
            body['blockage_ratio'] = 1.0

            warning = checkNaN(body)

            # convert the base x and y axis from lat/lon to east/north
            xy = [list(utm.from_latlon(*el)[:2]) for el in zip(base_data['Y'], base_data['X'])]
            body['X'] = [el[0] for el in xy]
            body['Y'] = [el[1] for el in xy]           

        elif complexity == 1 and machine == 'wec':
            # /sc/1/point/statistics/waves/Basic -> gamma, spr, CgE
            body['loc_resource'] = requests.get(f'{base_site_url}/point/statistics/waves/Basic', headers=headers).json().get('CgE')['mean']*1000
        elif complexity == 2 and machine == 'wec':
            # /sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_flattened -> bin1, bin2, bin3, pdf, id
            raw_value = requests.get(f'{base_site_url}/point/statistics/waves/EJPD3v/hs_tp_dp_flattened', headers=headers).json()
            pdf = np.array(raw_value.get('pdf'), dtype=float)
            # Ensure the PDF is normalized
            np.divide(pdf, pdf.sum(), out=pdf, casting='unsafe')

            body = {**body, **{'sea_state_wave_height': binString2Center(raw_value.get('bins1')), 
                               'sea_state_wave_period': binString2Center(raw_value.get('bins2')),
                               'sea_state_directions': binString2Center(raw_value.get('bins3')),
                               'scatter_diagram': pdf.tolist()}}
        elif complexity == 3 and machine == 'wec':
            # /sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_flattened -> bin1, bin2, bin3, pdf, id
            raw_value = requests.get(f'{base_site_url}/point/statistics/waves/EJPD3v/hs_tp_dp_flattened', headers=headers).json()
            pdf = np.array(raw_value.get('pdf'), dtype=float)
            # Ensure the PDF is normalized
            np.divide(pdf, pdf.sum(), out=pdf, casting='unsafe')

            body = {**body, **{'sea_state_wave_height': binString2Center(raw_value.get('bins1')), 
                               'sea_state_wave_period': binString2Center(raw_value.get('bins2')),
                               'sea_state_directions': binString2Center(raw_value.get('bins3')),
                               'scatter_diagram': pdf.tolist()}}
            # /sc/1/point/direct_values/bathymetry -> value
            bathy = requests.get(f'{base_site_url}/farm/direct_values/bathymetry', headers=headers).json()
            lat, lon = bathy['latitude'], bathy['longitude']
            points = np.array([list(utm.from_latlon(*el)[:2]) for el in zip(lat, lon)])
            body['bathymetry'] = np.c_[points, np.array(bathy['value'])].tolist()
            # /sc/1/point/statistics/waves/Basic -> gamma, spr, CgE
            spec = requests.get(f'{base_site_url}/point/statistics/waves/Basic', headers=headers).json()
            body['spectral_type'] = {'model': 'Jonswap', 'gamma': spec['gamma']['mean'], 'directional_spreading': spec['spr']['mean']}
            body['nogo_areas'] = [[]]
        else:
            raise RuntimeError(f'Project Complexity ({complexity}) and Machine Type ({machine}) not understood')
    except ValueError as ve:
        return jsonify({"status": "error", 'message': f'Error while fetching the site data: {str(ve)}. Please check site characterisation entity inputs.'}), 400
    except Exception as e:
        return jsonify({"status": "error", 'message': f'Error while fetching the site data: {str(e)}. Request body received from SC: {body}'}), 400

    try:
        obj = SiteSchemaValidator(machine, 
                                complexity,
                                body)
    except ValidationError as err:
        return jsonify({"status": "error", 'message': f'ValidationError: {str(err)}'}), 400 

    try:  
        site_inputs_db = project.inputs
        if site_inputs_db is None:
            site_inputs_db = EnergyCaptureInputs(project=project)

        site_inputs_db.update(site_model=obj.schema, site_url=base_site_url)
        db.session.add(site_inputs_db)
        db.session.commit()
        
        if warning:
            message = "The data has been saved to the DB, but Site Data contained NaN. The system attempted to interpolate the data but the results accuracy is not guranteed"
            return jsonify({"status": "warning", "message": message}), 201

        return jsonify(site_inputs_db.site_model), 201
        
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400 


@bp.route('/ec/<id>/inputs/site', methods=['PUT'])
@bp.route('/ec/<id>/inputs/site/tec/complexity1', methods=['PUT'])
@bp.route('/ec/<id>/inputs/site/tec/complexity2', methods=['PUT'])
@bp.route('/ec/<id>/inputs/site/tec/complexity3', methods=['PUT'])
@bp.route('/ec/<id>/inputs/site/wec/complexity1', methods=['PUT'])
@bp.route('/ec/<id>/inputs/site/wec/complexity2', methods=['PUT'])
@bp.route('/ec/<id>/inputs/site/wec/complexity3', methods=['PUT'])
def ec_inputs_site_put(id):
    try:
        project = Project.query.get(id)
        if project is None:
            raise IndexError(f'Project ID ({id}) not found')

        body = request.get_json()
        try:
            obj = SiteSchemaValidator(project.type, 
                                    project.complexity,
                                    body)
        except ValidationError as err:
            raise ValidationError(f"Input schema validation error: {err.messages}") 
        
        site_inputs_db = project.inputs
        if site_inputs_db is None:
            site_inputs_db = EnergyCaptureInputs(project=project)

        site_inputs_db.update(site_model=obj.schema)
        db.session.add(site_inputs_db)
        db.session.commit()
        project.status = 50
        db.session.add(project)
        db.session.commit()
        input_schema = EnergyCaptureInputsSchema()        
        return jsonify(input_schema.dump(site_inputs_db)), 201
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400


@bp.route('/ec/<id>/inputs/site', methods=['GET'])
@bp.route('/ec/<id>/inputs/site/tec/complexity1', methods=['GET'])
@bp.route('/ec/<id>/inputs/site/tec/complexity2', methods=['GET'])
@bp.route('/ec/<id>/inputs/site/tec/complexity3', methods=['GET'])
@bp.route('/ec/<id>/inputs/site/wec/complexity1', methods=['GET'])
@bp.route('/ec/<id>/inputs/site/wec/complexity2', methods=['GET'])
@bp.route('/ec/<id>/inputs/site/wec/complexity3', methods=['GET'])
def ec_inputs_site_get(id):
    try:
        project = Project.query.get(id)
        project_inputs = project.inputs
        if project_inputs is None:
            raise IOError(f'Input not saved for project {id}')
        
        site_inputs_db = project_inputs.site_model
        body = SiteSchemaValidator.serialize(project.type, project.complexity, site_inputs_db)
        return jsonify(body), 200                       
    except:
        return jsonify(
            {'message': 'the site input schema cannot be returned'}), 404          

@bp.route('/ec/<id>/upload/inputs/machine/tec/complexity1', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/machine/tec/complexity2', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/machine/tec/complexity3', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/machine/wec/complexity1', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/machine/wec/complexity2', methods=['POST'])
@bp.route('/ec/<id>/upload/inputs/machine/wec/complexity3', methods=['POST'])
def ec_inputs_machine_upload_post(id):
    try:
        project = Project.query.get(id)
        if project is None:
            raise IndexError(f'Project ID ({id}) not found')

        binary_json = request.files['file'].read()
        my_json = binary_json.decode('utf-8')
        uploaded_data = json.loads(my_json)
        complexity = uploaded_data['project']['complexity']
        machine = uploaded_data['project']['type'].lower()
        general = uploaded_data['general']
        dimensions = uploaded_data['dimensions']

        ## upload data formatted as the fetch data
        body = {}
        body['rated_pow_device'] = general['rated_capacity']
        body['main_dim_device'] = dimensions['characteristic_dimension']
        body['sec_dim_device'] = max(general['min_interdistance_x'], general['min_interdistance_y'])
        if complexity == 1 and machine == 'tec':
            model = uploaded_data['model']
        elif complexity == 2 and machine == 'tec':
            model = uploaded_data['model']
            model['floating'] = general['floating']
            model['hub_height'] = dimensions['hub_height']
            model.pop('tip_speed_ratio')
            body['rotor_diameter'] = body.pop('main_dim_device')
        elif complexity == 3 and machine == 'tec':
            model = uploaded_data['model']
            model['floating'] = general['floating']
            model['hub_height'] = dimensions['hub_height']
            model.pop('tip_speed_ratio')
            body['rotor_diameter'] = body.pop('main_dim_device')
        elif complexity == 1 and machine == 'wec':
            model = uploaded_data['model']
            model.pop('pto_damping', None)
            model['device_capture_width_ratio'] = model.pop('capture_width_ratio')
        elif complexity == 2 and machine == 'wec':
            model = uploaded_data['model']
            model['device_capture_width_ratio'] = model.pop('capture_width_ratio')
            model.pop('pto_damping', None)
            model.pop('mooring_stiffness')
        elif complexity == 3 and machine == 'wec':
            model_full = uploaded_data['model']
            keys_model = ['pto_damping', 'mooring_stiffness', 'additional_damping', 'additional_stiffness',
            'heading_angle_span', 'water_depth', 'frequency', 'directions', 'power_matrix', 'hs_pm',
            'tp_pm', 'dir_pm', 'scatter_diagram']
            
            model_full.pop('power_matrix', None)
            model_full['frequency'] = model_full.pop('wave_frequency')
            model_full['directions'] = model_full.pop('wave_direction')
            model_full['power_matrix'] = model_full.pop('capture_width_ratio')
            model_full['hs_pm'] = model_full.pop('hs_capture_width')
            model_full['tp_pm'] = model_full.pop('tp_capture_width')
            model_full['dir_pm'] = model_full.pop('wave_angle_capture_width')
            model_full['scatter_diagram'] = model_full['hs_pm']  # why this is here

            # the BL still uses the 5D matrices
            # model_full['pto_damping'] = np.array(model_full['pto_damping'])[0,0,0,:,:].tolist()
            # model_full['mooring_stiffness'] = np.array(model_full['mooring_stiffness'])[0,0,0,:,:].tolist()
            # model_full['additional_damping'] = np.array(model_full['additional_damping'])[0,0,0,:,:].tolist()
            # model_full['additional_stiffness'] = np.array(model_full['additional_stiffness'])[0,0,0,:,:].tolist()


            model = {key: model_full[key] for key in keys_model}
            del model_full
            # hydro
            hydro = uploaded_data['hydro']
            hydro['order'] = hydro.pop('max_truncation_order')
            hydro['radiation_amplitude_coefficients_real'] = hydro.pop('amplitude_coefficient_radiation_real')
            hydro['radiation_amplitude_coefficients_imag'] = hydro.pop('amplitude_coefficient_radiation_imag')
            hydro.pop('wave_direction', None)
            hydro.pop('wave_frequency', None)
            hydro.pop('velocity_transformation_matrix', None)

            # the BL still uses the 5D matrices
            # hydro['fitting_damping'] = np.array(hydro['fitting_damping'])[0,0,0,:,:].tolist()
            # hydro['fitting_stiffness'] = np.array(hydro['fitting_stiffness'])[0,0,0,:,:].tolist()
            
            model = {**model, **hydro}
            # modify body
            body['minimum_interdistance'] = [general['min_interdistance_x'], general['min_interdistance_y']]
            body['installation_depth'] = [general['min_installation_water_depth'], general['max_installation_water_depth']]
            body['rated_power_device'] = body.pop('rated_pow_device')
        else:
            pass

        body = {**body, **model}

        try:
            MachineSchemaValidator(project.type, 
                                    project.complexity,
                                    body)
        except ValidationError as err:
            raise ValidationError(f"Input schema validation error: {err.messages}") 

        # machine, url, cpx, mac = verify_machine_upload(data)
        # TODO: use copx and mac to check with the actual request agreement
        machine_inputs_db = project.inputs
        if machine_inputs_db is None:
            machine_inputs_db = EnergyCaptureInputs(project=project)
        machine_inputs_db.machine_model = body
        machine_inputs_db.machine_url = ''
        db.session.add(machine_inputs_db)
        db.session.commit()
        return jsonify({"status": "success", 'message': "Machine data uploaded to the DB"}), 201
    except Exception as e:
        return jsonify({"status": "error", 'message': f'Error Encountered While Formatting the Uplooaded Data: {str(e)}'}), 400

@bp.route('/ec/<id>/inputs/machine/fetch', methods=['POST'])
def ec_tech_inputs_machine(id):
    try:
        project = Project.query.get(id)
        if project is None:
            raise IndexError(f'Project ID ({id}) not found')
        complexity = project.complexity
        machine = project.type.lower()
    except IndexError as e:
        return jsonify({"status": "error", 'message': str(e)}), 400

    try:
        mc_api_url, headers = get_url_and_headers("mc")
        machineID = request.get_json()['entityID']
        base_machine_url = f'{mc_api_url}/mc/{machineID}'
        # base_url = request.get_json()['url']
        # url = f'{base_url}/mc/{machineID}'  # request.get_json()['url']
        # base_machine_url = url if url.startswith('http') else ('http://' + url)

        body = {}
        # resp = requests.get(users_url, headers=headers)
        # Basic Request shared for all the Cases
        general = requests.get(f'{base_machine_url}/general', headers=headers).json()
        dimensions = requests.get(f'{base_machine_url}/dimensions', headers=headers).json()

        body['rated_pow_device'] = general['rated_capacity']
        body['main_dim_device'] = dimensions['characteristic_dimension']
        body['sec_dim_device'] = max(general['min_interdistance_x'], general['min_interdistance_y'])
        # Request specific for the different case
        if complexity == 1 and machine == 'tec':
            model = requests.get(f'{base_machine_url}/model/tec/complexity1', headers=headers).json()
        elif complexity == 2 and machine == 'tec':
            model = requests.get(f'{base_machine_url}/model/tec/complexity2', headers=headers).json()
            model['floating'] = general['floating']
            model['hub_height'] = dimensions['hub_height']
            model.pop('tip_speed_ratio')
            body['rotor_diameter'] = body.pop('main_dim_device')
        elif complexity == 3 and machine == 'tec':
            model = requests.get(f'{base_machine_url}/model/tec/complexity3', headers=headers).json()
            model['floating'] = general['floating']
            model['hub_height'] = dimensions['hub_height']
            model.pop('tip_speed_ratio')
            body['rotor_diameter'] = body.pop('main_dim_device')
        elif complexity == 1 and machine == 'wec':
            model = requests.get(f'{base_machine_url}/model/wec/complexity1', headers=headers).json()
            model.pop('pto_damping', None)
            model['device_capture_width_ratio'] = model.pop('capture_width_ratio')
        elif complexity == 2 and machine == 'wec':
            model = requests.get(f'{base_machine_url}/model/wec/complexity2', headers=headers).json()
            model['device_capture_width_ratio'] = model.pop('capture_width_ratio')
            model.pop('pto_damping', None)
            model.pop('mooring_stiffness')
        elif complexity == 3 and machine == 'wec':
            model_full = requests.get(f'{base_machine_url}/model/wec/complexity3', headers=headers).json()
            keys_model = ['pto_damping', 'mooring_stiffness', 'additional_damping', 'additional_stiffness',
            'heading_angle_span', 'water_depth', 'frequency', 'directions', 'power_matrix', 'hs_pm',
            'tp_pm', 'dir_pm', 'scatter_diagram']
            model_full['frequency'] = model_full.pop('wave_frequency')
            model_full['directions'] = model_full.pop('wave_direction')
            model_full['power_matrix'] = model_full.pop('capture_width_ratio')
            model_full['hs_pm'] = model_full.pop('hs_capture_width')
            model_full['tp_pm'] = model_full.pop('tp_capture_width')
            model_full['dir_pm'] = model_full.pop('wave_angle_capture_width')
            model_full['scatter_diagram'] = model_full['hs_pm']  # why this is here
            # the BL still uses the 5D matrices
            # model_full['pto_damping'] = np.array(model_full['pto_damping'])[0,0,0,:,:].tolist()
            # model_full['mooring_stiffness'] = np.array(model_full['mooring_stiffness'])[0,0,0,:,:].tolist()
            # model_full['additional_damping'] = np.array(model_full['additional_damping'])[0,0,0,:,:].tolist()
            # model_full['additional_stiffness'] = np.array(model_full['additional_stiffness'])[0,0,0,:,:].tolist()


            model = {key: model_full[key] for key in keys_model}
            del model_full
            # hydro
            hydro = requests.get(f'{base_machine_url}/single_machine_hydrodynamic/wec/complexity3', headers=headers).json()
            hydro['order'] = hydro.pop('max_truncation_order')
            hydro['radiation_amplitude_coefficients_real'] = hydro.pop('amplitude_coefficient_radiation_real')
            hydro['radiation_amplitude_coefficients_imag'] = hydro.pop('amplitude_coefficient_radiation_imag')
            hydro.pop('wave_direction', None)
            hydro.pop('wave_frequency', None)
            hydro.pop('velocity_transformation_matrix', None)

            # the BL still uses the 5D matrices
            # hydro['fitting_damping'] = np.array(hydro['fitting_damping'])[0,0,0,:,:].tolist()
            # hydro['fitting_stiffness'] = np.array(hydro['fitting_stiffness'])[0,0,0,:,:].tolist()
            
            model = {**model, **hydro}
            # modify body
            body['minimum_interdistance'] = [general['min_interdistance_x'], general['min_interdistance_y']]
            body['installation_depth'] = [general['min_installation_water_depth'], general['max_installation_water_depth']]
            body['rated_power_device'] = body.pop('rated_pow_device')
        else:
            pass

        body = {**body, **model}

    except Exception as e:
        return jsonify({"status": "error", 'message': f'Error while fetching the data: {str(e)}.'}), 400

    try:
        obj = MachineSchemaValidator(machine, 
                                complexity,
                                body)
    except ValidationError as err:
        return jsonify({"status": "error", 'message': f'ValidationError: {str(err)}'}), 400 

    try:  
        machine_inputs_db = project.inputs
        if machine_inputs_db is None:
            machine_inputs_db = EnergyCaptureInputs(project=project)

        machine_inputs_db.update(machine_model=obj.schema, machine_url=base_machine_url)
        db.session.add(machine_inputs_db)
        db.session.commit()
     
        return jsonify(machine_inputs_db.machine_model), 201
        
    except Exception as e:
        return jsonify({"status": "error", 'message': f'Error while saving the data into the DB: {str(e)}'}), 400 

@bp.route('/ec/<id>/inputs/machine', methods=['PUT'])
@bp.route('/ec/<id>/inputs/machine/tec/complexity1', methods=['PUT'])
@bp.route('/ec/<id>/inputs/machine/tec/complexity2', methods=['PUT'])
@bp.route('/ec/<id>/inputs/machine/tec/complexity3', methods=['PUT'])
@bp.route('/ec/<id>/inputs/machine/wec/complexity1', methods=['PUT'])
@bp.route('/ec/<id>/inputs/machine/wec/complexity2', methods=['PUT'])
@bp.route('/ec/<id>/inputs/machine/wec/complexity3', methods=['PUT'])
def ec_inputs_machine_put(id):
    try:
        project = Project.query.get(id)
        if project is None:
            raise IndexError(f'Project ID ({id}) not found')
        body = request.get_json()
        try:
            MachineSchemaValidator(project.type, 
                                        project.complexity,
                                        body)
        except ValidationError as err:
            raise ValidationError(f"Input schema validation error: {err.messages}") 
        
        machine_inputs_db = project.inputs
        if machine_inputs_db is None:
            machine_inputs_db = EnergyCaptureInputs(project=project)

        machine_inputs_db.update(machine_model=body)
        db.session.add(machine_inputs_db)
        db.session.commit()

        project.status = 75
        db.session.add(project)
        db.session.commit()

        input_schema = EnergyCaptureInputsSchema()
        return jsonify(input_schema.dump(machine_inputs_db)), 201
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400


@bp.route('/ec/<id>/inputs/machine', methods=['GET'])
@bp.route('/ec/<id>/inputs/machine/tec/complexity1', methods=['GET'])
@bp.route('/ec/<id>/inputs/machine/tec/complexity2', methods=['GET'])
@bp.route('/ec/<id>/inputs/machine/tec/complexity3', methods=['GET'])
@bp.route('/ec/<id>/inputs/machine/wec/complexity1', methods=['GET'])
@bp.route('/ec/<id>/inputs/machine/wec/complexity2', methods=['GET'])
@bp.route('/ec/<id>/inputs/machine/wec/complexity3', methods=['GET'])
def ec_inputs_machine_get(id):
    try:
        project = Project.query.get(id)
        project_inputs = project.inputs
        if project_inputs is None:
            raise IOError(f'Input not saved for project {id}')
        
        machine_inputs_db = project_inputs.machine_model

        return jsonify(machine_inputs_db), 200                       
    except:
        return jsonify(
            {'message': 'the machine input schema cannot be returned'}), 404   
    
