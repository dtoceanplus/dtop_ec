# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/env python3
from flask import Blueprint, current_app, jsonify, make_response, request

from dtop_energycapt import business

from dtop_energycapt.service.models import db, Project, Farm, Device, EnergyCaptureInputs
from dtop_energycapt.service.schemas import ProjectSchema, FarmSchema, DeviceSchema, EnergyCaptureInputsSchema

from dtop_energycapt.service import config
import json 
import ast


bp = Blueprint('api_results', __name__)

headers = {'Access-Control-Allow-Headers': 'Content-Type'}

@bp.route('/ec/<id>/results/tec/complexity1', methods=['GET'])
@bp.route('/ec/<id>/results/tec/complexity2', methods=['GET'])
@bp.route('/ec/<id>/results/tec/complexity3', methods=['GET'])
@bp.route('/ec/<id>/results/wec/complexity3', methods=['GET'])
def ec_results_wec3(id):

    try:
        project = Project.query.get(id)
        if project is None:
            raise IOError(f'Invalid project ID: {id}')
        
        devices = project.devices
        if not devices:
            raise IOError('No Device has been added to the project yet')

        if project.type.lower() != 'tec':
            return jsonify({"status": "error", 
                "message": "method not implemented for Wave Energy Converters"}), 404

        array_velocity_field = []
        for device in devices:
          array_velocity_field.append(
            {
              "rotorID": device.id,
              "siteID": device.velocity_field['siteConditionID'],
              "hub_velocity": device.velocity_field['hubVelocity']
            })

        return jsonify({"array_velocity_field": array_velocity_field}), 200
        
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 404


