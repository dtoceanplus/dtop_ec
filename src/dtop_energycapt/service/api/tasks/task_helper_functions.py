# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json

import redis
from flask import current_app
from rq import Connection, Queue, Worker
from rq.registry import FinishedJobRegistry
from rq.job import Job

from dtop_energycapt.service import create_app
from dtop_energycapt.service.models import Task, db, Farm, Project, Device

class RoundFloat(float):
    def __repr__(self):
        return '%.2f' % self
        
def round_floats(obj):
    if isinstance(obj, float):
        return RoundFloat(obj)
    elif isinstance(obj, dict):
        return dict((k, round_floats(v)) for k, v in obj.items())
    elif isinstance(obj, (list, tuple)):
        return [round_floats(el) for el in obj]
    return obj

def save_array_design(task_id):
    with Connection(redis.from_url(current_app.config["REDIS_URL"])):
        # check for failed jobs and clean the Failed job registry
        registry = Queue().finished_job_registry
        ind = 0
        for job_id in registry.get_job_ids():
            ind += 1
            job = Job.fetch(job_id, connection=redis.from_url(current_app.config["REDIS_URL"]))
            print(f'ITERATING FINISHED JOBS: finished number {ind}')
            # print(job_id, job.exc_info)
            print(job.func_name)

        registry = Queue().failed_job_registry
        ind = 0
        for job_id in registry.get_job_ids():
            ind += 1
            job = Job.fetch(job_id, connection=redis.from_url(current_app.config["REDIS_URL"]))
            print(f'ITERATING FAILED JOBS: failure number {ind}')
            # print(job_id, job.exc_info)
            print(job.func_name)
            try:
                registry.remove(job_id)
            except:
                print(f'CANNOT REMOVE JOB: {job_id}')
        
        print(f'SHOW CURRENT JOBS')
        job = Queue().fetch_job(task_id)
        print('JOB FETCHED')
        print(job)
        print(job.exc_info)
        print(job.get_status())
        print(job.origin)
        print('Job %s: %s' % (job.id, job.func_name))
        print(job.result)

        print('ATTEMPT TO SAVE RESULTS TO DB')
        print(job)
        # Save results to database
        task = Task.query.filter_by(rq_job_id=job.id).first()
        assert task is not None
        farm, devices = job.result
        print(job.result)
        if devices == -1:
            task.message = farm['message']
            task.status = farm['status']
            db.session.commit()

            return 0
        
        print('CORECT RESULTS....CONTINUE TO SAVE TO DB')
        outputs_farm = round_floats(farm)
        output_devices = round_floats(devices)

        project = Project.query.get(task.project_id)

        farm_db = Farm(project=project)
        farm_db.update(**farm)
        db.session.add(farm_db)
        db.session.commit()

        # insert devices into the DB table
        for dev in devices:
            device = Device(farm=farm_db, project=project)
            device.update(**dev)
            db.session.add(device)
        
        task.status = 'success'
        db.session.commit()