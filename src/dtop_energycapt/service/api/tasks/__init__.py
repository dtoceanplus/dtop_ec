# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import redis
import rq
from flask import Blueprint, current_app, jsonify, request

from dtop_energycapt import business
from dtop_energycapt.service.models import Project, Task, db, EnergyCaptureInputs
from dtop_energycapt.service.schemas import EnergyCaptureInputsSchema
from dtop_energycapt.service.api.tasks.task_helper_functions import *

bp = Blueprint('tasks', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/ec/<int:study_id>/task', methods=['GET'])
def get_task_status_from_project(study_id: int):
    task: Task = Task.query.filter_by(project_id=study_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)
        return jsonify({
            "task_id": task.id,
            "task_status": job.get_status(),
            "task_result": job.result,
            "worker_message": task.message,
            "worker_status": task.status,
            "job_tta": job.meta['tta']
        })
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@bp.route('/ec/tasks/<int:task_id>', methods=['GET'])
def get_project_task_status(task_id: int):
    task: Task = Task.query.filter_by(id=task_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)
        return jsonify({
            "task_id": task.id,
            "task_status": job.get_status(),
            "task_result": job.result,
        })
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@bp.route('/ec/tasks/<ecId>', methods=['DELETE'])
def remove_project_tasks(ecId):
    try:
        tasks = Task.query.filter_by(project_id=ecId)
        for task in tasks:
            db.session.delete(task)
        db.session.commit()
        return jsonify({"status": "success", 'message': 'Project results deleted'}), 200

    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404

@bp.route('/ec/tasks', methods=['POST'])
def add_project_task():
    try:
        project_id = request.json["id"]
        try:  # try to clear the previous task to avoid accumulation
            tasks = Task.query.filter_by(project_id=project_id)
            for task in tasks:
                db.session.delete(task)
            db.session.commit()
        except:
            pass
        project = Project.query.filter_by(id=project_id).outerjoin(EnergyCaptureInputs).first()
        if project is None:
            raise IOError(f'Invalid project ID: {project_id}')

        # Should the input data be checked again or not here?
        if project.inputs is None:
            raise IOError(f'Project ID {project_id} is missing the required input data')

        obj = business.ArrayFactory.get_array( project.type.upper(), 
                                               str(project.complexity))
        
    except Exception as e:
        return jsonify({"status": "error", 'message': f'An error occured while fetching the project inputs: {str(e)}'}), 400

    try:    
        # Do i need to check the data or not here?
        ID = project.id
        pj_name = project.title
        inputs = EnergyCaptureInputsSchema().dump(project.inputs)
        machine = project.inputs.machine_model
        site = project.inputs.site_model

        if not machine:
            raise IOError('Machine definition is missing')

        if not site:
            raise IOError('Site definition is missing')


        inputs.pop('machine_model')
        inputs.pop('site_model')

        inputs.update(machine)
        inputs.update(site)
        # obj.get_inputs(pj_name, ID, **inputs)
        # return jsonify(inputs), 200
        # data_quality = True
        # if not data_quality:
        #     raise IOError('The input data does not comply with the API format') 
    except Exception as e:
        return jsonify({"status": "error", 'message': f'An error occurred during the preparation of the data for the calculation: {str(e)}'}), 400
    # obj.get_inputs(pj_name, ID, **inputs)
    try:
        # import pickle 
        # pickle.dump(inputs, open('DEBUG_WEC.p','wb'))
        obj.get_inputs(pj_name, ID, **inputs)
    except Exception as e:
        return jsonify({"status": "error", 'message': f'An error occured while validating the input data: {str(e)}'}), 400
    # Sync calculation to DEBUG
    # obj.solve()
    # print(obj.send_output())
    
    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            queue = rq.Queue(is_async=True)
            # job = queue.enqueue(run_array_design, obj)
            # queue.enqueue(save_array_design, job.get_id(), depends_on=job)
            job = queue.enqueue(obj.solve, job_timeout=7200)
            queue.enqueue(save_array_design, job.get_id(), depends_on=job)
            # job = queue.enqueue('obj.calc_powprod')
            # job_output = queue.enqueue(obj.send_output, depends_on=job)
            # job = queue.enqueue(obj.send_output)
            # queue.enqueue(save_array_design, job_output.get_id(), depends_on=job_output)

            task = Task(project_id=project.id, rq_job_id=job.get_id())
            db.session.add(task)
            # task = Task(project_id=project.id, rq_job_id=job_output.get_id())
            # db.session.add(task)
            db.session.commit()
            response_object = {
                "status": "success",
                "data": {
                    "task_id": task.id,
                }
            }
        return jsonify(response_object), 202
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400
