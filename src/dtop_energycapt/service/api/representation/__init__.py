# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import redis
import rq
from flask import Blueprint, current_app, jsonify, request

from dtop_energycapt import business
from dtop_energycapt.service.models import Project, db, EnergyCaptureInputs
from dtop_energycapt.service.schemas import EnergyCaptureInputsSchema
from dtop_energycapt.service.api.tasks.task_helper_functions import *

bp = Blueprint('representation', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/representation/<int:study_id>', methods=['GET'])
def get_entity_representation(study_id: int):
    entity: Project = Project.query.filter_by(id=study_id).first()
    
    if entity is None:
        return jsonify({"status": "error", "message": "Entity not found."}), 404

    if entity.status!=100:
        return jsonify({"status": "error", "message": "Cannot return the digital representation if the status of the entity is not 100%"}), 404 

    try:
        entity.type
       
        dr = {
                "array": [
                    {
                        "id": "1",
                        "properties": {
                            "number_of_devices": {
                                "description": "Number of devices deployed in the farm",
                                "value": entity.farm.number_devices,
                                "unit": "-",
                                "origin": "EC"
                            },
                            "array_rated_power": {
                                "description": "Rated power of the array, obtained as the rated power of the device x the number of devices",
                                "value": entity.inputs.machine_model["rated_pow_device"]*entity.farm.number_devices,
                                "unit": "kW",
                                "origin": "EC"
                            }
                        }
                    }
                ],
                "device": [],
                "prime_mover": []
            }
        index = 1
        for dev in entity.devices:
            IDdev = f"1_{index}"
            IDprime = f"{IDdev}_1"
            if entity.type.lower() == "tec":
                type_tec = f"Horizontal {entity.inputs.machine_model['number_rotor']}-Axis Turbine"
            else:
                if entity.inputs.machine_model.get('machine_archetype', None) is None:
                    type_tec = "Wave Converter at Complexity 3"
                else:
                    type_tec = entity.inputs.machine_model.get('machine_archetype')

            device = {
                "id": IDdev,
                "location": [],
                "properties": {
                    "technology": {
                        "description": "Identifies either a Tidal or Wave energy converter",
                        "value": entity.type,
                        "unit": "-",
                        "origin": "EC"
                    },
                    "type_of_technology": {
                        "description": "Specify the machine type, for the given Technology type",
                        "value": type_tec,
                        "unit": "-",
                        "origin": "EC"
                    }
                }
            }

            prime_mover = {                
                "id": IDprime,
                "location": [dev.easting, dev.northing, 0],
                "properties": {
                    "technology": {
                        "description": "Identifies either a Tidal or Wave energy converter",
                        "value": entity.type,
                        "unit": "-",
                        "origin": "EC"
                    },
                    "type_of_technology": {
                        "description": "Specify the machine type, for the given Technology type",
                        "value": type_tec,
                        "unit": "-",
                        "origin": "EC"
                    }
                },
                "assessment": {
                    "captured_annual_energy_production":{
                        "description": "Gross Annual Energy Production converted, before transformation efficiency",
                        "value": dev.aep,
                        "unit": "kWh",
                        "origin": "EC"
                    },
                    "array_power_matrix_per_sea_state":{
                        "description": "Average Gross Power Absorbed, before transformation efficiency",
                        "value": dev.captured_power,
                        "unit": "kW",
                        "origin": "EC"
                    },
                    "hydrodynamic_ffficiency":{
                        "description": "Q-factor for the specific device, as the ratio between average produced power in the array over the produced power as isolated.",
                        "value": dev.q_factor,
                        "unit": "-",
                        "origin": "EC"
                    }
                }
            }

            dr["device"].append(device)
            dr["prime_mover"].append(prime_mover)

            index += 1
        
        return jsonify(dr), 200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


