# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# This file should contain create_app() function.
# This function is used by Flask.
import os
from contextlib import suppress

from flask import Flask, request, jsonify, render_template
from flask_babel import Babel
from flask_cors import CORS
# from flask_sqlalchemy import SQLAlchemy
# from flask_marshmallow import Marshmallow
from .config import Config

# db = SQLAlchemy()
# ma = Marshmallow()
babel = Babel()

import logging

import os
from logging.handlers import RotatingFileHandler
if not os.path.isdir('./logs'):
    os.mkdir('./logs')

from dtop_energycapt.service.logsetup import LOGGING_CONFIG
import logging.config
logging.config.dictConfig(LOGGING_CONFIG)
LOG = logging.getLogger(__name__)

LOG.warning('... starting the EC server')
    
def create_app(test_config=None):

    # create and configure the app
    app = Flask(__name__,
                instance_relative_config=False)
    
    # disable logging for the requests
    log = logging.getLogger('werkzeug')
    log.disabled = True

    app.config.from_object(Config)

    CORS(app)
    babel.init_app(app)
    # db.init_app(app)
    # ma.init_app(app)

    # ensure the instance folder exists
    with suppress(OSError):
        os.makedirs(app.instance_path)

    from . import db

    db.init_app(app)

    from .schemas import ma

    ma.init_app(app)

    from .api import inputs as inputs_api
    app.register_blueprint(inputs_api.bp)

    from .api import core as core_api
    app.register_blueprint(core_api.bp, url_prefix='/api')

    from .api import farm as farm_api
    app.register_blueprint(farm_api.bp)

    from .api import devices as devices_api
    app.register_blueprint(devices_api.bp)

    from .api import tasks as tasks_api
    app.register_blueprint(tasks_api.bp)

    from .api import results as results_api
    app.register_blueprint(results_api.bp)

    from .gui import core as core_gui
    app.register_blueprint(core_gui.bp, url_prefix='/tasks')

    from .gui import main
    app.register_blueprint(main.bp)

    from .gui import back_end as server_gui
    app.register_blueprint(server_gui.bp)

    from . import routes as routes
    app.register_blueprint(routes.bp, url_prefix='/routes')

    from .api import representation as dr
    app.register_blueprint(dr.bp)

    if os.environ.get('FLASK_ENV') == 'development':
        from .gui.back_end import provider_states
        app.register_blueprint(provider_states.bp)

    # This should be activated IF the front end is integrated without a separate port
    # @app.route('/', defaults={'path': ''})
    # @app.route('/<path:path>')
    # def render_vue(path):
    #     return render_template("index.html")
    LOG.warning('EC server started')

    return app

    # with app.app_context():
    #     # Registering Blueprints
    #     from .api import core as core_api
    #     app.register_blueprint(core_api.bp, url_prefix='/api')

    #     from .gui import core as core_gui
    #     app.register_blueprint(core_gui.bp, url_prefix='/tasks')

    #     from .gui import main
    #     app.register_blueprint(main.bp)

    #     from .gui import back_end as server_gui
    #     app.register_blueprint(server_gui.bp)

    #     from . import routes as routes
    #     app.register_blueprint(routes.bp, url_prefix='/routes')

    #     # This should be activated IF the front end is integrated without a separate port
    #     @app.route('/', defaults={'path': ''})
    #     @app.route('/<path:path>')
    #     def render_vue(path):
    #         return render_template("index.html")

    #     db.create_all()

    #     return app


# @babel.localeselector
# def get_locale():
#     return request.accept_languages.best_match(['en', 'fr'])
