This folder contains HTML templates for generating pages.

Template engine is [Jinja2](http://jinja.pocoo.org/).

Documenation:
- [Rendering Templates](http://flask.pocoo.org/docs/1.0/quickstart/#rendering-templates)
- [Jinja2](http://jinja.pocoo.org/)