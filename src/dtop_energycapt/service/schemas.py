# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energycapt.service import ma
from dtop_energycapt.service.models import Project, Farm, Device, EnergyCaptureInputs

from flask_marshmallow import Marshmallow
from marshmallow import post_dump, Schema, fields, ValidationError
import json
import ast
import numpy as np
ma = Marshmallow()

from cerberus import Validator
import numpy as np

class CustomValidator(Validator):
    def _validate_array(self, array, field, value):
        """ Test the oddity of a value.

        The rule's arguments are validated against this schema:
        """
        try:
            ## validate the schema first
            depth = array.get('depth', None)
            shape = array.get('shape', None)
            constraint = array.get('constraint', None)
            if depth is None:
                raise KeyError('Schema definition error: missing array key "depth"')
            if shape is None:
                raise KeyError('Schema definition error: missing array key "shape"')
            if constraint is None:
                raise KeyError('Schema definition error: missing array key "constraint"')
            
            if not len(shape) == depth:
                raise ValueError(f'Schema definition error: Shape key must have length {depth}')
            if not len(constraint) == depth:
                raise ValueError(f'Schema definition error: Constraint key must have length {depth}')

            value_shape = np.shape(value)
            if not len(value_shape) == depth:
                self._error(field, f"Must have depth of {depth}")
            else:
                for i in range(depth):
                    if not eval(f'{value_shape[i]}{constraint[i]}{shape[i]}'):
                        self._error(field, f"Must have shape at index [{i}]: {value_shape[i]}{constraint[i]}{shape[i]}")

        except Exception as e:
            self._error(field, f"Unexpected error: {str(e)}")
        return True  

class SiteSchemaValidator():
    tec1 = {"loc_resource": {'type': 'number', 'required': True},
            "loc_position": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [3, 2],
                                       'constraint': ['>=', '==']}, 'required': True}}
    tec2 = {"bathymetry": {'type': 'number', 'required': True},
            "loc_position": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [3, 2],
                                       'constraint': ['>=', '==']}, 'required': True},
            "velocity_field": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [2],
                                       'constraint': ['==']}, 'required': True}}
    tec3 = {"loc_position": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [3, 2],
                                       'constraint': ['>=', '==']}, 'required': True},
            "probability":  {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "TI":  {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1,1,1],
                                       'constraint': ['>=','>=','>=']}, 'required': True},
            "U":  {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1,1,1],
                                       'constraint': ['>=','>=','>=']}, 'required': True},
            "V":  {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1,1,1],
                                       'constraint': ['>=','>=','>=']}, 'required': True},
            "SSH":  {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1,1,1],
                                       'constraint': ['>=','>=','>=']}, 'required': True},
            "power_law_exponent":  {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1,1],
                                       'constraint': ['>=','>=']}, 'required': True},
            "soil_characteristic":  {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1,1],
                                       'constraint': ['>=','>=']}, 'required': True},
            "bathymetry":  {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1,1],
                                       'constraint': ['>=','>=']}, 'required': True},
            "X":  {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [2],
                                       'constraint': ['>=']}, 'required': True},
            "Y":  {'array': {'type': 'number',
                             'depth': 1,
                             'shape': [2],
                             'constraint': ['>=']}, 'required': True},
            "blockage_ratio":  {'type': 'number', 'required': True},
            }
    wec1 = {"loc_resource": {'type': 'number', 'required': True},
            "loc_position": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [3, 2],
                                       'constraint': ['>=', '==']}, 'required': True}}
    wec2 = {"loc_position": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [3, 2],
                                       'constraint': ['>=', '==']}, 'required': True},
            "scatter_diagram": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sea_state_wave_height": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sea_state_wave_period": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sea_state_directions": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True}}
    wec3 = {"loc_position": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [3, 2],
                                       'constraint': ['>=', '==']}, 'required': True},
            "scatter_diagram": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sea_state_wave_height": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sea_state_wave_period": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sea_state_directions": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            'bathymetry': {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1, 3],
                                       'constraint': ['>=', '==']}, 'required': True},                      
            "nogo_areas": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [0, 0],
                                       'constraint': ['>=', '>=']}, 'required': True},
            "spectral_type": {'type': 'dict', 'schema': {'model': {'type': 'string'},
                                                         'gamma': {'type': 'number'},
                                                         'directional_spreading': {'type': 'number'}}}                  
                                       
            }
    def __init__(self, machine, complexity, data):
        schema = getattr(self, f"{machine.lower()}{complexity}")
        v = CustomValidator(schema)
        
        self.valid = v.validate(data)
        self.errors = v.errors

        if not self.valid:
            raise ValidationError(f'Cannot validate the Site Data. Error(s): {self.errors}') 
        if machine.lower() == 'tec' and complexity == 3:
            data['PLE'] = data.pop('power_law_exponent')
            data['BR'] = data.pop('blockage_ratio')
            data['bathy'] = data.pop('bathymetry')
            data['lease'] = data.pop('loc_position')
            data['geophy'] = data.pop('soil_characteristic')
        elif machine.lower() == 'wec' and complexity == 3:
            spec_type = data.pop('spectral_type')
            data["spectral_type"] = (spec_type['model'], spec_type['gamma'], 
                                    spec_type['directional_spreading'])
            data['scat_diagram'] = data.pop('scatter_diagram')
            data['lease'] = data.pop('loc_position')
            data['water_depth'] = np.array(data.get('bathymetry')).mean()
        self.schema = data
    
    @staticmethod
    def serialize(machine, complexity, schema):
        if machine.lower() == 'tec' and complexity == 3:
            schema['power_law_exponent'] = schema.pop('PLE')
            schema['blockage_ratio'] = schema.pop('BR')
            schema['bathymetry'] = schema.pop('bathy')
            schema['loc_position'] = schema.pop('lease')
            schema['soil_characteristic'] = schema.pop('geophy')
        elif machine.lower() == 'wec' and complexity == 3:
            spec_type = schema.pop('spectral_type')
            schema["spectral_type"] = {'model': spec_type[0],
                                       'gamma': spec_type[1],
                                       'directional_spreading': spec_type[2]}
            schema['scatter_diagram'] = schema.pop('scat_diagram')
            schema['loc_position'] = schema.pop('lease')
            schema.pop('water_depth')
        return schema

class MachineSchemaValidator():
    tec1 = {"cp": {'type': 'number', 'required': True},
            "main_dim_device": {'type': 'number', 'required': True},
            "sec_dim_device": {'type': 'number', 'required': True},
            "number_rotor": {'type': 'number', 'required': True},
            "rated_pow_device": {'type': 'number', 'required': True}}
    tec2 = {
        "rotor_diameter": {'type': 'number', 'required': True},
        "hub_height": {'type': 'number', 'required': True},
        "floating": {'type': 'boolean', 'required': True},
        "cut_in_velocity": {'type': 'number', 'required': True},
        "cut_out_velocity": {'type': 'number', 'required': True},
        "cp": {'type': 'number', 'required': True},
        "ct": {'type': 'number', 'required': True},
        "rated_pow_device": {'type': 'number', 'required': True},
        "number_rotor": {'type': 'number', 'required': True},
        "rotor_interdistance": {'type': 'number', 'required': True},
        "sec_dim_device": {'type': 'number', 'required': False}}

    tec3 = {
        "rotor_diameter": {'type': 'number', 'required': True},
        "hub_height": {'type': 'number', 'required': True},
        "floating": {'type': 'boolean', 'required': True},
        "cut_in_velocity": {'type': 'number', 'required': True},
        "cut_out_velocity": {'type': 'number', 'required': True},
        "cp": {'array': {'type': 'number',
                                'depth': 1,
                                'shape': [1],
                                'constraint': ['>=']}, 'required': True},
        "ct": {'array': {'type': 'number',
                                'depth': 1,
                                'shape': [1],
                                'constraint': ['>=']}, 'required': True},
        "cp_ct_velocity": {'array': {'type': 'number',
                                'depth': 1,
                                'shape': [1],
                                'constraint': ['>=']}, 'required': True},
        "rated_pow_device": {'type': 'number', 'required': True},
        "number_rotor": {'type': 'number', 'required': True},
        "rotor_interdistance": {'type': 'number', 'required': True},
        "heading_angle_span": {'type': 'number', 'required': True},
        "bidirectional": {'type': 'boolean', 'required': True},
        "sec_dim_device": {'type': 'number', 'required': False}}

    wec1 = {"device_capture_width_ratio": {'type': 'number', 'required': True},
            "main_dim_device": {'type': 'number', 'required': True},
            "sec_dim_device": {'type': 'number', 'required': True},
            "rated_pow_device": {'type': 'number', 'required': True},
            "machine_archetype": {'type': 'string', 'required': True}}

    wec2 = {"device_capture_width_ratio": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "tp_capture_width": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "hs_capture_width": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "wave_angle_capture_width": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "main_dim_device": {'type': 'number', 'required': True},
            "sec_dim_device": {'type': 'number', 'required': True},
            "rated_pow_device": {'type': 'number', 'required': True},
            "machine_archetype": {'type': 'string', 'required': True}}

    wec3 = {"main_dim_device": {'type': 'number', 'required': True},
            "pto_damping": {'array': {'type': 'number',
                                       'depth': 5,
                                       'shape': [1, 1, 1, 1, 1],
                                       'constraint': ['>=', '>=','>=', '>=','>=']}, 'required': True},
            "mooring_stiffness": {'array': {'type': 'number',
                                       'depth': 5,
                                       'shape': [1, 1, 1, 1, 1],
                                       'constraint': ['>=', '>=','>=', '>=','>=']}, 'required': True},
            "additional_damping": {'array': {'type': 'number',
                                       'depth': 5,
                                       'shape': [1, 1, 1, 1, 1],
                                       'constraint': ['>=', '>=','>=', '>=','>=']}, 'required': True},
            "additional_stiffness": {'array': {'type': 'number',
                                       'depth': 5,
                                       'shape': [1, 1, 1, 1, 1],
                                       'constraint': ['>=', '>=','>=', '>=','>=']}, 'required': True},
            "fitting_damping": {'array': {'type': 'number',
                                       'depth': 5,
                                       'shape': [1, 1, 1, 1, 1],
                                       'constraint': ['>=', '>=','>=', '>=','>=']}, 'required': True},
            "fitting_stiffness": {'array': {'type': 'number',
                                       'depth': 5,
                                       'shape': [1, 1, 1, 1, 1],
                                       'constraint': ['>=', '>=','>=', '>=','>=']}, 'required': True},
            "mass_matrix": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1, 1],
                                       'constraint': ['>=', '>=']}, 'required': True},
            "added_mass": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "radiation_damping": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "excitation_force_real": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "excitation_force_imag": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "hydrostatic_stiffness": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1, 1],
                                       'constraint': ['>=', '>=']}, 'required': True},
            "diffraction_transfer_matrix_real": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "force_transfer_matrix_real": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "diffraction_transfer_matrix_imag": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "force_transfer_matrix_imag": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "radiation_amplitude_coefficients_real": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "radiation_amplitude_coefficients_imag": {'array': {'type': 'number',
                                       'depth': 3,
                                       'shape': [1, 1, 1],
                                       'constraint': ['>=', '>=', '>=']}, 'required': True},
            "inscribing_cylinder_radius": {'type': 'number', 'required': True},   
            "modes": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1, 7],
                                       'constraint': ['>=', '==']}, 'required': True},
            "frequency": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "directions": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "truncation_order": {'array': {'type': 'number',
                                       'depth': 2,
                                       'shape': [1, 1],
                                       'constraint': ['>=', '>=']}, 'required': True},
            "heading_angle_span": {'type': 'number', 'required': True},
            "water_depth": {'type': 'number', 'required': True},   
            "minimum_interdistance": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [2],
                                       'constraint': ['==']}, 'required': True},
            "installation_depth": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [2],
                                       'constraint': ['==']}, 'required': True},
            "order": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [2],
                                       'constraint': ['==']}, 'required': True},
            "power_matrix": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "hs_pm": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "tp_pm": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "dir_pm": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "scatter_diagram": {'array': {'type': 'number',
                                       'depth': 1,
                                       'shape': [1],
                                       'constraint': ['>=']}, 'required': True},
            "sec_dim_device": {'type': 'number', 'required': False},
            "rated_power_device": {'type': 'number', 'required': True}}
                                       
    def __init__(self, machine, complexity, data):
        schema = getattr(self, f"{machine.lower()}{complexity}")
        v = CustomValidator(schema)
        
        self.valid = v.validate(data)
        self.errors = v.errors
        if not self.valid:
            raise ValidationError(f'Cannot validate the Machine Data. Error(s): {self.errors}') 
        self.schema = data


class ProjectSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Project
        include_relationships = True


class EnergyCaptureInputsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = EnergyCaptureInputs

    @post_dump(pass_many=True)
    def modify_layout(self, data, many):
        if data['minimum_interdistance'] == "":
            data['minimum_interdistance'] = []
        else:
            data['minimum_interdistance'] = json.loads(data['minimum_interdistance'])

        if data['array_layout'] is None:
            data['array_layout'] = []
            return data
        data['array_layout'] = json.loads(data['array_layout'])

        if data['orientation_angle'] is None:
            data['orientation_angle'] = []
            return data
        data['orientation_angle'] = json.loads(data['orientation_angle'])
        
        return data

class DeviceSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Device

    @post_dump(pass_many=True)
    def modify_out(self, data, many):
        if many:
            for dev in data:
                decode = None
                try: 
                    decode = json.loads(dev['captured_power_per_condition'])
                except Exception as e1:
                    try:
                        decode = ast.literal_eval(dev['captured_power_per_condition'])
                    except Exception as e2:
                        raise RuntimeError(f"DeviceSchema: error while decoding using json.load or ast.literal: {str(e1)}-{str(e2)}")
            
                dev['captured_power_per_condition'] = decode
                dev.pop('velocity_field')
        else:
            decode = None
            try: 
                decode = json.loads(data['captured_power_per_condition'])
            except Exception as e1:
                try:
                    decode = ast.literal_eval(data['captured_power_per_condition'])
                except Exception as e2:
                    raise RuntimeError(f"DeviceSchema: error while decoding using json.load or ast.literal: {str(e1)}-{str(e2)}")


            data['captured_power_per_condition'] = decode
            data.pop('velocity_field')

        return data

class FarmSchema(ma.SQLAlchemyAutoSchema):
    layout = ma.Nested(DeviceSchema,
                       many=True)

    class Meta:
        model = Farm

    @post_dump(pass_many=True)
    def modify_layout(self, data, many):
        decode = None
        try: 
            decode = json.loads(data['captured_power_per_condition'])
        except Exception as e1:
            try:
                decode = ast.literal_eval(data['captured_power_per_condition'])
            except Exception as e2:
                raise RuntimeError(f"FarmSchema: error while decoding using json.load or ast.literal: {str(e1)}-{str(e2)}")
        data['captured_power_per_condition'] = decode
        layout = data['layout']
        ids = []
        easting = []
        northing = []
        for dat in layout:
            ids.append(dat['id'])
            easting.append(dat['easting'])
            northing.append(dat['northing'])

        data['layout'] = {
            'deviceID': ids,
            'easting': easting,
            'northing': northing
        }
        return data


from marshmallow import Schema, ValidationError


class OneOfSchema(Schema):
    """
    This is a special kind of schema that actually multiplexes other schemas
    based on object type. When serializing values, it uses get_obj_type() method
    to get object type name. Then it uses `type_schemas` name-to-Schema mapping
    to get schema for that particular object type, serializes object using that
    schema and adds an extra "type" field with name of object type.
    Deserialization is reverse.
    Example:
        class Foo(object):
            def __init__(self, foo):
                self.foo = foo
        class Bar(object):
            def __init__(self, bar):
                self.bar = bar
        class FooSchema(marshmallow.Schema):
            foo = marshmallow.fields.String(required=True)
            @marshmallow.post_load
            def make_foo(self, data, **kwargs):
                return Foo(**data)
        class BarSchema(marshmallow.Schema):
            bar = marshmallow.fields.Integer(required=True)
            @marshmallow.post_load
            def make_bar(self, data, **kwargs):
                return Bar(**data)
        class MyUberSchema(marshmallow.OneOfSchema):
            type_schemas = {
                'foo': FooSchema,
                'bar': BarSchema,
            }
            def get_obj_type(self, obj):
                if isinstance(obj, Foo):
                    return 'foo'
                elif isinstance(obj, Bar):
                    return 'bar'
                else:
                    raise Exception('Unknown object type: %s' % repr(obj))
        MyUberSchema().dump([Foo(foo='hello'), Bar(bar=123)], many=True)
        # => [{'type': 'foo', 'foo': 'hello'}, {'type': 'bar', 'bar': 123}]
    You can control type field name added to serialized object representation by
    setting `type_field` class property.
    """

    type_field = "type"
    type_field_remove = True
    type_schemas = []

    def get_obj_type(self, obj):
        """Returns name of object schema"""
        return obj.__class__.__name__

    def dump(self, obj, *, many=None, **kwargs):
        errors = {}
        result_data = []
        result_errors = {}
        many = self.many if many is None else bool(many)
        if not many:
            result = result_data = self._dump(obj, **kwargs)
        else:
            for idx, o in enumerate(obj):
                try:
                    result = self._dump(o, **kwargs)
                    result_data.append(result)
                except ValidationError as error:
                    result_errors[idx] = error.normalized_messages()
                    result_data.append(error.valid_data)

        result = result_data
        errors = result_errors

        if not errors:
            return result
        else:
            exc = ValidationError(errors, data=obj, valid_data=result)
            raise exc

    def _dump(self, obj, *, update_fields=True, **kwargs):
        obj_type = self.get_obj_type(obj)
        if not obj_type:
            return (
                None,
                {"_schema": "Unknown object class: %s" % obj.__class__.__name__},
            )

        type_schema = self.type_schemas.get(obj_type)
        if not type_schema:
            return None, {"_schema": "Unsupported object type: %s" % obj_type}

        schema = type_schema if isinstance(type_schema, Schema) else type_schema()

        schema.context.update(getattr(self, "context", {}))

        result = schema.dump(obj, many=False, **kwargs)
        if result is not None:
            result[self.type_field] = obj_type
        return result

    def load(self, data, *, many=None, partial=None, unknown=None):
        errors = {}
        result_data = []
        result_errors = {}
        many = self.many if many is None else bool(many)
        if partial is None:
            partial = self.partial
        if not many:
            try:
                result = result_data = self._load(
                    data, partial=partial, unknown=unknown
                )
                #  result_data.append(result)
            except ValidationError as error:
                result_errors = error.normalized_messages()
                result_data.append(error.valid_data)
        else:
            for idx, item in enumerate(data):
                try:
                    result = self._load(item, partial=partial)
                    result_data.append(result)
                except ValidationError as error:
                    result_errors[idx] = error.normalized_messages()
                    result_data.append(error.valid_data)

        result = result_data
        errors = result_errors

        if not errors:
            return result
        else:
            exc = ValidationError(errors, data=data, valid_data=result)
            raise exc

    def _load(self, data, *, partial=None, unknown=None):
        if not isinstance(data, dict):
            raise ValidationError({"_schema": "Invalid data type: %s" % data})

        data = dict(data)
        unknown = unknown or self.unknown

        data_type = data.get(self.type_field)
        if self.type_field in data and self.type_field_remove:
            data.pop(self.type_field)

        if not data_type:
            raise ValidationError(
                {self.type_field: ["Missing data for required field."]}
            )

        try:
            type_schema = self.type_schemas.get(data_type)
        except TypeError:
            # data_type could be unhashable
            raise ValidationError({self.type_field: ["Invalid value: %s" % data_type]})
        if not type_schema:
            raise ValidationError(
                {self.type_field: ["Unsupported value: %s" % data_type]}
            )

        schema = type_schema if isinstance(type_schema, Schema) else type_schema()

        schema.context.update(getattr(self, "context", {}))

        return schema.load(data, many=False, partial=partial, unknown=unknown)

    def validate(self, data, *, many=None, partial=None):
        try:
            self.load(data, many=many, partial=partial)
        except ValidationError as ve:
            return ve.messages
        return {}
