# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/env python3
from flask import Blueprint, current_app, jsonify, make_response, request

from dtop_energycapt import business

from dtop_energycapt.service.models import db, Project, Farm, Device, Task
from dtop_energycapt.service.schemas import ProjectSchema, FarmSchema, DeviceSchema

from dtop_energycapt.service import config

bp = Blueprint('gui_server', __name__)

headers = {'Access-Control-Allow-Headers': 'Content-Type'}

@bp.route('/ec/logs', methods=['GET'])
def get_logs():
    try:
        # Extract the Log File
        logfile = open("./logs/ec.log","r") 

        # Return the file and the date
        return logfile.read(),200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 400

@bp.route('/ec', methods=['GET'])
def get_all_ec_projects():
    
    try:
        all_projects = Project.query.all()
        if not all_projects:
            raise IOError('No project found')

        projects_schema = ProjectSchema(many=True)
        return make_response(jsonify(projects_schema.dump(all_projects)), 200,
                             headers)

    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404


@bp.route('/ec', methods=['POST'])
def add_ec_project():
    
    try:
        body = request.get_json()
        if body['type']=='':
            raise ValueError('Missing Machine Type Input.')
        prj = Project(**body)
        db.session.add(prj)
        db.session.commit()

        return make_response(
                            jsonify({
                                'message': f'Project {prj.id} added',
                                'ID': prj.id
                            }), 201)
    
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 400


@bp.route('/ec/<id>', methods=['GET'])
def get_ec_project(id):

    try:
        project = Project.query.get(id)
        project_schema = ProjectSchema()

        if project:
            return make_response(jsonify(project_schema.dump(project)), 200,
                                 headers)
        else:
            raise IndexError('The specified ID does not exist in the DB')
    except IndexError as e:
        return jsonify({'status': 'fail', 'message': str(e)}, ), 404
    except:
        return jsonify({'status': 'fail', 'message': 'Unkown fails'}, ), 404


@bp.route('/ec/<id>', methods=['PUT'])
def update_ec_project(id):
    
    try:
        project = Project.query.get(id)
        project.update(**request.get_json())
        # inputs = project.inputs
        # farm = project.farm
        # devices = project.devices
        # if not inputs is None:
        #     db.session.delete(inputs)
        # if not farm is None:
        #     db.session.delete(farm)
        # if not devices is None or not devices:
        #     for dev in devices:
        #         db.session.delete(dev)
        db.session.commit()

        return make_response(jsonify({'message': 'project updated'}), 201,
                             headers)
    except:
        return make_response(jsonify({'message': 'project updated failed'}),
                             400, headers)


@bp.route('/ec/<id>', methods=['DELETE'])
def delete_ec_project(id):

    try:
        project = Project.query.get(id)
        db.session.delete(project)
        db.session.commit()
        try:  # try to clear the previous task to avoid accumulation
            tasks = Task.query.filter_by(project_id=id)
            for task in tasks:
                db.session.delete(task)
            db.session.commit()
        except:
            pass

        return make_response(jsonify({'message': 'project deleted'}), 200,
                             headers)
    except:
        return make_response(jsonify({'message': 'project deleted failed'}),
                             404, headers)


# @bp.route('/ec/<id>/devices', methods=['POST'])
# def post_ec_devices(id):
#     try:
#         device_list = request.get_json()
#         project = Project.query.get(id)
#         farm = project.farm
#         if not farm:
#             return jsonify({
#                 'message':
#                 'The request could not be completed due to a conflict with the current state of the resource. Device depends on Farm and the Farm is not present yet'
#             }), 409

#         for dev in device_list:
#             device = Device(farm=farm, project=project)
#             device.update(**dev)
#             db.session.add(device)

#         db.session.commit()
#         return jsonify({'result': {'message': 'farm added'}}), 201
#     except:
#         return jsonify({'result': {'message': 'no farm added'}}), 400


# @bp.route('/ec/<id>/devices', methods=['GET'])
# def get_ec_devices(id):
#     try:
#         project = Project.query.get(id)
#         devices = project.devices
#         if not devices:
#             return jsonify({
#                 'message':
#                 'The request could not be completed due to a conflict with the current state of the resource. No Device has been added to the project yet'
#             }), 409

#         devices_schema = DeviceSchema(many=True)

#         return jsonify(devices_schema.dump(project.devices)), 200

#     except:
#         return jsonify({'message': 'bad request'}), 404


# @bp.route('/ec/<id>/devices/<dev_id>', methods=['GET'])
# def get_ec_device(id, dev_id):
#     try:
#         device = Device.query.filter_by(project_id=id, id=dev_id).one()
#         device_schema = DeviceSchema()
#         print(device_schema.dump(device))
#         return jsonify(device_schema.dump(device)), 200
#     except:
#         return jsonify({'message': 'bad request'}), 404


# @bp.route('/ec/<id>/devices/<dev_id>', methods=['DELETE'])
# def delete_ec_device(id, dev_id):
#     try:
#         device = Device.query.filter_by(project_id=id, id=dev_id).one()
#         db.session.delete(device)
#         db.session.commit()

#         return jsonify({'message': 'device deleted'}), 200
#     except:
#         return jsonify({'message': 'bad request'}), 404
