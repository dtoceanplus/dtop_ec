# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from contextlib import suppress

import sqlalchemy.exc
from flask import Blueprint, request

from dtop_energycapt.service.db import init_db
from dtop_energycapt.service.models import Device, Farm, Project, db


bp = Blueprint('provider_states', __name__)


@bp.route('/provider_states/setup', methods=['POST'])
def provider_states_setup():
    """
    Reset database and create data neccessary for contract testing.
    Should be available only with `FLASK_ENV=development`.
    """
    init_db()

    consumer = request.json['consumer']
    state = request.json['state']

    if state.startswith('ec 1 exists'):
        with suppress(sqlalchemy.exc.IntegrityError):
            with db.session.begin_nested():
                project = Project(id=1, complexity=1, type='TEC')
                db.session.add(project)

        if state == 'ec 1 exists and has device 2':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    device = Device(id=2, project_id=1, aep=1.11, q_factor=0.98,)
                    db.session.add(device)
        
        elif state == 'ec 1 exists and it has devices':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    device = Device(id=2, project_id=1, aep=1.11, q_factor=0.98,
                                    captured_power_per_condition={"capturedPower": [10001],"siteConditionID": [2]})
                    db.session.add(device)

        elif state == 'ec 1 exists and has farm':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    farm = Farm(project_id=1, aep=1.11, q_factor=0.98, number_devices=1)
                    db.session.add(farm)
                    device = Device(id=1, project_id=1, aep=1.11, q_factor=0.98, easting=10.1, northing=12.3, farm=farm)
                    db.session.add(device)
        
        elif state == 'ec 1 exists and it has farm':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    farm = Farm(project_id=1, aep=1.11, q_factor=0.98, number_devices=1)
                    db.session.add(farm)
                    device = Device(id=1, project_id=1, aep=1.11, q_factor=0.98, easting=10.1, northing=12.3, farm=farm)
                    db.session.add(device)
        
        elif state == 'ec 1 exists and it has tidal at complexity 1':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    device = Device(id=2, project_id=1, aep=1.11, q_factor=0.98,
                                    captured_power_per_condition={"capturedPower": [10001],"siteConditionID": [2]},
                                    velocity_field={"hubVelocity": [1.24],"siteConditionID": [2]})
                    db.session.add(device)
        
        elif state == 'ec 1 exists and it has tidal at complexity 2':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    device = Device(id=2, project_id=1, aep=1.11, q_factor=0.98,
                                    captured_power_per_condition={"capturedPower": [10001],"siteConditionID": [2]},
                                    velocity_field={"hubVelocity": [1.24],"siteConditionID": [2]})
                    db.session.add(device)
    
        elif state == 'ec 1 exists and it has tidal at complexity 3':
            with suppress(sqlalchemy.exc.IntegrityError):
                with db.session.begin_nested():
                    device = Device(id=2, project_id=1, aep=1.11, q_factor=0.98,
                                    captured_power_per_condition={"capturedPower": [10001],"siteConditionID": [2]},
                                    velocity_field={"hubVelocity": [1.24],"siteConditionID": [2]})
                    db.session.add(device)

    return ''
