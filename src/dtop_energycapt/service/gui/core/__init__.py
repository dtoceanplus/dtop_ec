# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, url_for, request, jsonify
import requests

import numpy as np

import json

from dtop_energycapt.business import core
from dtop_energycapt.service import config

from dtop_energycapt.service.models import db, Project, Farm, Device
from dtop_energycapt.service.schemas import ProjectSchema

bp = Blueprint('gui_core', __name__)


# @bp.route('/stage1', methods=['POST'])
# def stage1_output():
#     try:
#         ecID = request.get_json()['id']
#         project = Project.query.get(ecID)
#         device_cwr = request.get_json()['device_cwr']
#         device_length = request.get_json()['device_length']
#         loc_resource = request.get_json()['resource']
#         farm_power = request.get_json()['farm_power']

#         # TODO: inputs: where are these coming from
#         device_capture_width = device_cwr * device_length
#         name = 'NAME ERROR'
#         nb_devices = 10
#         rated_pow_device = farm_power / nb_devices
#         loc_position = [[0.1, 0.1]]
#         main_dim_device = device_length
#         sec_dim_device = 10
#         if project.type == 'WEC':
#             args = {
#                 'nb_devices': nb_devices,
#                 'rated_pow_device': rated_pow_device,
#                 'loc_resource': loc_resource,
#                 'device_capture_width': device_cwr,
#                 'loc_position': loc_position,
#                 'main_dim_device': main_dim_device,
#                 'sec_dim_device': sec_dim_device
#             }
#         else:
#             args = {
#                 'nb_devices': nb_devices,
#                 'rated_pow_device': rated_pow_device,
#                 'loc_resource': loc_resource,
#                 'char_length': device_length,
#                 'loc_position': loc_position,
#                 'main_dim_device': main_dim_device,
#                 'sec_dim_device': sec_dim_device
#             }

#         print(config.PROJECT_OBJECTS)
#         obj = config.PROJECT_OBJECTS[str(ecID)]
#         obj.get_inputs(name, ecID, **args)

#         obj.calc_powprod()
#         farm, devices = obj.send_output()
#         print('*' * 80)
#         print('FARM')
#         print(farm)
#         print('*' * 80)
#         print('DEVICES')
#         print(devices)

#         farm_path = f"http://localhost:5000/ec/{ecID}/farm"
#         # del farm['layout']
#         # farm['captured-power-per-condition'] = farm['captured-power-each-site-condition'][0]
#         # del farm['captured-power-each-site-condition']

#         farm_res = requests.post(farm_path, json=farm)
#         print(farm_res.json())

#         devices_path = f"http://localhost:5000/ec/{ecID}/devices"
#         # for device in devices:
#         #   del device['deviceID']
#         #   layout = device['layout']
#         #   del device['layout']
#         #   device['easting'] = layout['easting']
#         #   device['northing'] = layout['northing']
#         #   device['captured-power-per-condition'] = device['captured-power-each-site-condition'][0]
#         #   del device['captured-power-each-site-condition']

#         dev_res = requests.post(devices_path, json=devices)
#         print(dev_res.json())

#         # TODO: response format the correct response structure
#         result = {'message': 'project added', 'response': dev_res.json()}
#     except AssertionError as error:
#         print(error)
#         result = {'message': 'no project added'}

#     return jsonify({'result': result})


# @bp.route('/print', methods=['GET'])
# def return_bl_objects():
#     print(config.PROJECT_OBJECTS)

#     return jsonify({'message': 'to be developed'})


# @bp.route('/stage2', methods=['POST'])
# def stage2_output():
#     print(request.get_json())
#     body = request.get_json()
#     ecID = body['id']
#     del body['id']

#     obj = config.PROJECT_OBJECTS[str(ecID)]

#     obj.get_inputs(**body)
#     obj.calc_powprod()
#     farm, devices = obj.send_output()

#     print(farm)
#     print(devices)

#     farm_path = f"http://localhost:5000/ec/{ecID}/farm"
#     # del farm['layout']
#     # farm['captured-power-per-condition'] = farm['captured-power-each-site-condition'][0]
#     # del farm['captured-power-each-site-condition']

#     farm_res = requests.post(farm_path, json=farm)
#     print(farm_res.json())

#     devices_path = f"http://localhost:5000/ec/{ecID}/devices"
#     # for device in devices:
#     #   del device['deviceID']
#     #   layout = device['layout']
#     #   del device['layout']
#     #   device['easting'] = layout['easting']
#     #   device['northing'] = layout['northing']
#     #   device['captured-power-per-condition'] = device['captured-power-each-site-condition'][0]
#     #   del device['captured-power-each-site-condition']
#     print(json.dumps(devices, indent=2))
#     dev_res = requests.post(devices_path, json=devices)
#     print(dev_res.json())

#     # TODO: response format the correct response structure
#     result = {'message': 'farm and devices added', 'response': dev_res.json()}

#     return jsonify(result)


# @bp.route('/stage3', methods=['POST'])
# def stage3_output():
#     body = request.get_json()
#     ecID = body['id']
#     del body['id']

#     # for key, value in body['data'].items():
#     #     body['data'][key] = np.asarray(value)

#     body['array_layout'] = np.asarray(body['array_layout'])
#     obj = config.PROJECT_OBJECTS[str(ecID)]
#     obj.get_inputs('test', ecID, **body)
#     # obj.get_inputs(body['data'],body['features'])
#     obj.calc_powprod()
#     farm, devices = obj.send_output()

#     print(farm)
#     print(devices)

#     farm_path = f"http://localhost:5000/ec/{ecID}/farm"
#     # del farm['layout']
#     # farm['captured-power-per-condition'] = farm['captured-power-each-site-condition'][0]
#     # del farm['captured-power-each-site-condition']

#     farm_res = requests.post(farm_path, json=farm)
#     print(farm_res.json())

#     devices_path = f"http://localhost:5000/ec/{ecID}/devices"
#     # for device in devices:
#     #   del device['deviceID']
#     #   layout = device['layout']
#     #   del device['layout']
#     #   device['easting'] = layout['easting']
#     #   device['northing'] = layout['northing']
#     #   device['captured-power-per-condition'] = device['captured-power-each-site-condition'][0]
#     #   del device['captured-power-each-site-condition']
#     print(json.dumps(devices, indent=2))
#     dev_res = requests.post(devices_path, json=devices)
#     print(dev_res.json())

#     # TODO: response format the correct response structure
#     result = {'message': 'farm and devices added', 'response': dev_res.json()}

#     return jsonify(result)
