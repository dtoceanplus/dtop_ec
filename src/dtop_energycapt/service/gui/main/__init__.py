# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, render_template, url_for, request, redirect

from dtop_energycapt.business import core
from dtop_energycapt.business import ArrayFactory

bp = Blueprint('main', __name__)


# @bp.route('/gui' , methods=['GET', 'POST'])
# def main():

  # return render_template('gui/main.html', data=[[{'stage':1}, {'stage':2}, {'stage':3}],[{'machine':'TEC'}, {'machine':'WEC'}]])

# @bp.route("/" , methods=['GET', 'POST'])
# def create():

#     if request.method == 'POST':
#         prj_name = request.form['prj_name']
#         prj_folder = request.form['prj_folder']
#         error = None
	   
#         if not prj_name:
#             error = "Project name is required."
#         if not prj_folder:
#             error = "Project folder is required."			
#         if error is not None:
#             flash(error)		

#         stage = request.form.get('stage_select')
#         machine = request.form.get('machine_select')
#         ARRAY = ArrayFactory.get_array(machine, str(stage))

#         print(ARRAY.project_id, ARRAY.stage, ARRAY.type)

#         return redirect(url_for('main.energy_capt_project', itemid=ARRAY.project_id))
#     return render_template('gui/create.html',
#                            data=[[{'stage': 1}, {'stage': 2}, {'stage': 3}], [{'machine': 'TEC'}, {'machine': 'WEC'}]])


# @bp.route('/projects/<itemid>')
# def energy_capt_project(itemid):
#     # return f'''<h1>DTOcea+ new project </h1>
#                  # <h2>project ID: {itemid}</h2>
#                 # '''
#     return render_template("gui/energy_capt_project.html", data=[itemid])

