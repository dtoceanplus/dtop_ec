# Developing a Single Page App with Flask and Vue.js

### Want to learn how to build this?

Check out the [post](https://testdriven.io/developing-a-single-page-app-with-flask-and-vuejs).

## Want to use this project?

1. Create and activate a virtual env
```sh
conda create --prefix=env python=3.6
source activate env/
```

2. Install the requirements for the back-end (server)

```sh
$ pip install Flask==1.0.2 Flask-Cors==3.0.7
```

Run the server:
```sh
$ cd back-end
$ python app.py
```

Navigate to [http://localhost:5000/projects](http://localhost:8080/projects)

3. Install the requirements for the front-end (Vue)

    ```sh
    npm install -g @vue/cli
    vue create --preset preset.json front-end
    cd front-end
    npm add axios@0.18.0 
    npm add bootstrap@4.3.1

    npm run serve
    ```

Navigate to [http://localhost:8080/create](http://localhost:8080/create)
    
