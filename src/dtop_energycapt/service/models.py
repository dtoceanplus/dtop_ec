# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energycapt.service import db
from sqlalchemy.ext import mutable
import json

from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    desc = db.Column(db.String(500))
    complexity = db.Column(db.Integer)
    type = db.Column(db.String(50))
    date = db.Column(db.String(50))
    status = db.Column(db.Integer)
    tags = db.Column(db.String(50))
    # one-to-one relationship with the farm
    inputs = db.relationship('EnergyCaptureInputs',
                             backref='project',
                             uselist=False,
                             cascade="all, delete-orphan")
    # one-to-one relationship with the farm
    farm = db.relationship('Farm',
                           backref='project',
                           uselist=False,
                           cascade="all, delete-orphan")
    # one-to-many relationship with the devices
    devices = db.relationship('Device',
                              backref='project',
                              cascade="all, delete-orphan")

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class JsonEncodedDict(db.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = db.Text

    def process_bind_param(self, value, dialect):
        if value is None:
            return '{}'
        else:
            return json.dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return {}
        else:
            return json.loads(value)

class Array(db.TypeDecorator):
    """Enables List storage by encoding and decoding on the fly."""
    impl = db.Text

    def process_bind_param(self, value, dialect):
        if not value:
            return ""
        return ';'.join(str(v) for v in value)

    def process_result_value(self, value, dialect):
        if value == "":
            return []
        return [float(v) for v in value.split(';')]

mutable.MutableDict.associate_with(JsonEncodedDict)


class Farm(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id", onupdate='cascade'))
    layout = db.relationship('Device',
                             backref='farm',
                             cascade="all, delete-orphan")
    # name = db.Column(db.String(50))
    # numberDevices = db.Column(db.Integer)
    number_devices = db.Column(db.Integer)
    # qFactor = db.Column(db.Float)
    q_factor = db.Column(db.Float)
    aep = db.Column(db.Float)
    # capturedPower = db.Column(db.Float)
    captured_power = db.Column(db.Float)
    # resourceReduction = db.Column(db.Float)
    resource_reduction = db.Column(db.Float)
    # capturedPowerPerCondition = db.Column(JsonEncodedDict, default={})
    captured_power_per_condition = db.Column(JsonEncodedDict, default={})
    # @staticmethod NOT IN USE
    # def to_camel_case(snake_str):
    #     components = snake_str.split('-')
    #     if len(components) > 1:
    #         # capitalize the first letter of each component except the first one
    #         # with the 'title' method and join them together.
    #         return components[0] + ''.join(x.title() for x in components[1:])
    #     return components[0]

    def update(self, **kwargs):
        project_exclude = ['layout'] 
        for key, value in kwargs.items():
            if hasattr(self, key) and not key in project_exclude:
                setattr(self, key, value)
            elif not hasattr(self, key):
                print(key)
                raise IOError('Input schema is incorrect')

            


class Device(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    farm_id = db.Column(db.Integer, db.ForeignKey("farm.id"))
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"))
    northing = db.Column(db.Float)
    easting = db.Column(db.Float)
    # name = db.Column(db.String(50))
    # qFactor = db.Column(db.Float)
    q_factor = db.Column(db.Float)
    aep = db.Column(db.Float)
    # capturedPower = db.Column(db.Float)
    captured_power = db.Column(db.Float)
    # capturedPowerPerCondition = db.Column(JsonEncodedDict, default={})
    captured_power_per_condition = db.Column(JsonEncodedDict, default={})
    velocity_field = db.Column(JsonEncodedDict, default={})

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class EnergyCaptureInputs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"))
    orientation_angle = db.Column(db.String(1000), nullable=True)
    minimum_interdistance = db.Column(Array)
    nb_devices = db.Column(db.Integer)
    layout_type = db.Column(db.String(50))
    # TODO: the layout shoudl be given as JSON??
    array_layout = db.Column(db.String(1000), nullable=True)
    # layout = db.Column(JsonEncodedDict, default={})
    optimisation_threshold = db.Column(db.Float, nullable=True)
    opt_method = db.Column(db.String(50), nullable=True)
    array_generic_type = db.Column(db.String(50), nullable=True)
    notes = db.Column(db.String(1000))
    site_url = db.Column(db.String(100), nullable=True)
    machine_url = db.Column(db.String(100), nullable=True)
    machine_model = db.Column(JsonEncodedDict, default={})
    site_model = db.Column(JsonEncodedDict, default={})

    def update(self, **kwargs):
         for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                if key == 'array_layout':
                    value = json.dumps(value)
                elif key == 'orientation_angle':
                    value = json.dumps(value)
                setattr(self, key, value)

class Task(db.Model):
    __tablename__ = 'tasks'
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    rq_job_id = db.Column(db.String, nullable=False)
    message = db.Column(db.String, nullable=True)
    status = db.Column(db.String, nullable=True)


