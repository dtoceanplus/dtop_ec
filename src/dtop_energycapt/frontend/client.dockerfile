FROM node:8.16-alpine

RUN apk add --no-cache make

ENV VUE_APP_API_URL ''

WORKDIR /app

COPY package*.json ./
RUN npm ci

COPY *rc ./
COPY *.js ./
COPY Makefile .

CMD make x-frontend-run
