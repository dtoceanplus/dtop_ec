<!--
This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
Copyright (C) 2021 AAU-Build - Francesco Ferri

This program is free software: you can redistribute it and/or modify it
under the terms of the Affero GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the Affero GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
-->
<template>
  <div style="padding: 20px">
    <h2>Energy Capture Logs</h2>
    <h5>logs are requested to the server and updated every 30s</h5>
    <el-table
      :data="LogTable"
      height="1000"
      style="width: 100%"
      :row-class-name="tableRowClassName"
      border
    >
      <el-table-column
        prop="date"
        label="Date"
        width="200"
      />
        <!-- <el-table-column
        prop="status"
        label="Status"
      /> -->
        <el-table-column
        prop="module"
        label="Sender"
        width="300"
      />
        <el-table-column
        prop="msg"
        label="Message"
      />
    </el-table>
  </div>
</template>

<script>
import { mapGetters } from 'vuex'
import axios from 'axios'

export default {
  name: 'Logs',
  data() {
    return {
      interval_log: null,
      LogTable: [],
      LogFile: null,
    }
  },
  created() {
    this.fetchLogs()
    this.interval_log = setInterval(function () {
      this.fetchLogs()
    }.bind(this), 30000)
  },
  beforeDestroy: function(){
    clearInterval(this.interval_log)
  },
  methods: {
    tableRowClassName({row, rowIndex}) {
      if ( row.status != undefined ) {
        if (row.status.includes('ERR')) {
          return 'error-row'
        } else if (row.status.includes('WARN')) {
          return 'warning-row'
        }
      }

      return ''
    },
    fetchLogs() {
      debugger
      const path = `${process.env.VUE_APP_API_URL}/ec/logs`
      axios
        .get(path)
        .then((res) => {
          this.LogTable = []
          this.LogFile = res.data
          var today = new Date()
          var date = today.getFullYear() + '-'
          this.LogFile = this.LogFile.replaceAll(date, '\n' + date)
          this.LogFile = this.LogFile.split('\n')
          var line = ""
          for (let i = this.LogFile.length - 1; i >= 0; i--) {
            line = this.LogFile[i]
            line = line.replaceAll('[', "!!").replaceAll(']', "!!")
            line = line.split("!!")

            this.LogTable.push({
              date: line[1],
              status: line[2],
              module: line[3],
              msg: line[4] })
          }
        })
        .catch((error) => {
          console.log(error.message)
        })
    },
  }
}
</script>

<style lang="scss" scoped>
.dashboard {
  &-container {
    margin: 30px;
  }
  &-text {
    font-size: 30px;
    line-height: 46px;
  }
}
</style>
