// This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import LayoutNoSide from '@/layout/LayoutNoSide.vue'


export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    hidden: true,
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Home', icon: 'dashboard' }
    },
    {
      path: 'logs',
      name: 'ECLogs',
      component: () => import('@/views/logs/index'),
      meta: { title: 'Logs', icon: 'logs' }
    }
    ]
  },
  {
    path: '/ec-entity',
    name: 'MCEntity',
    component: () => import('@/views/ec_entity/index'),
    meta: { title: 'Integration' },
    hidden: true
  },
  {
    path: '/ec-studies',
    component: Layout,
    name: 'ECStudies',
    meta: { title: 'Studies', icon: 'table' },
    children: [
      {
        path: 'index',
        hidden: true,
        name: 'ECStudiesIndex',
        component: () => import('@/views/ec_studies/index'),
        meta: { title: 'List' },
      },
      {
        path: 'inputs',
        name: 'ECStudyInput',
        component: () => import('@/views/ec_study/index'),
        meta: { title: 'Inputs' },
        children: [
          {
            path: 'site',
            name: 'SiteInput',
            component: () => import('@/views/ec_study/site/index'),
            meta: { title: 'Site' }
          },
          {
            path: 'machine',
            name: 'MachineInput',
            component: () => import('@/views/ec_study/machine/index'),
            meta: { title: 'Machine' }
          },
          {
            path: 'farm',
            name: 'FarmInput',
            component: () => import('@/views/ec_study/farm/index'),
            meta: { title: 'Farm' }
          },
          {
            path: 'outputs',
            name: 'Outputs',
            component: () => import('@/views/ec_study/output/index'),
            meta: { title: 'Output' },
          }
        ]
      },
      {
        path: 'outputs',
        name: 'ECStudyOutput',
        component: () => import('@/views/ec_study/output/index'),
        meta: { title: 'Output' },
      },
    ]
  },

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example' },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table' }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },

  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'Form', icon: 'form' }
  //     }
  //   ]
  // },

  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index'),
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },

  {
    path: 'external-link',
    component: Layout,
    name: 'Links',
    meta: {
      title: 'Links',
      icon: 'link'
    },
    children: [
      {
        path: 'https://www.dtoceanplus.eu/',
        meta: { title: 'DTOceanPlus' }
      },
      {
        path: 'http://127.0.0.1:5000/static/docs/index.html',
        meta: { title: 'Sphinx' }
      },
      {
        path: 'http://127.0.0.1:5000/api/',
        meta: { title: 'ReDoc (API)' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
