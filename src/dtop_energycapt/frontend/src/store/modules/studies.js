// This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import axios from 'axios'

const state = {
  studyId: null,
  studyTitle: null,
  status: 0,
  complexity: 1,
  machine_type: 'tec',
  completion_state: {farm: false, site: false, machine: false, results: false},
  project_structure: []
}

const mutations = {
  SET_ID: (state, studyId) => {
    state.studyId = studyId
  },
  SET_TITLE: (state, studyTitle) => {
    state.studyTitle = studyTitle
  },
  UPDATE_STATUS: (state, {key, value}) => {
    state.completion_state[key] = value;
    var status = 0;
    status += state.completion_state['farm']*18;
    status += state.completion_state['site']*18;
    status += state.completion_state['machine']*18;
    status += state.completion_state['results']*46;
    state.status = status
    state.status = status
    const path = `${process.env.VUE_APP_API_URL}/ec/${state.studyId}`
    axios.put(path, { complexity: state.complexity, status: state.status })
      .then(response => {
        console.log('STATUS UPDATED', { complexity: state.complexity, status: state.status })
      }
    )
  },
  SET_TYPE: (state, machine) => {
    state.machine_type = machine.toLowerCase()
  },
  SET_COMPLEXITY: (state, complexity) => {
    state.complexity = complexity
  },
  SET_PROJECT_STRUCTURE: (state, project_structure) => {
    state.project_structure = project_structure
  }
}

const actions = {
  setId({ commit }, { studyId }) {
    commit('SET_ID', studyId)
  },
  setTitle({ commit }, { studyTitle }) {
    commit('SET_TITLE', studyTitle)
  },
  updateStatus({ commit }, { key, value }) {
    commit('UPDATE_STATUS', { key: key, value: value })
  },
  setMachine({ commit }, { machine }) {
    commit('SET_TYPE', machine)
  },
  setComplexity({ commit }, { complexity }) {
    commit('SET_COMPLEXITY', complexity)
  },
  setProjectStructure({ commit }, { project_structure }) {
    commit('SET_PROJECT_STRUCTURE', project_structure)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
