// This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import axios from "axios";

export default {
  data () {
     msg: "Hello World"
     form: null
  },
  created: function () {
     console.log("Printing from the Mixin tested")
  },
  computed: {
    studyId() {
      return this.$store.getters.studyId
    }
  },
  methods: {
      updateProgress() {
        const path = `${process.env.VUE_APP_API_URL}/ec/${this.studyId}`
        axios.put(path, {status: this.$store.getters.status})
      },
      displayMessage: function () {
         console.log("Now printing from a mixin function")
      },
      fetch_project(id) {
         const path = `http://localhost:5000/mc/${id}`;

         axios
         .get(path)
         .then(res => {
            this.complexity = res.data.complexity;
            this.machine = res.data.type;
         })
         .catch(error => {
            console.error(error);
         });
      },
      fetch_inputs_model(id, complexity, machine) {
         const path = `http://localhost:5000/mc/${id}/model/${machine}/complexity${complexity}`;
         axios
         .get(path)
         .then(res => {
            this.form = res.data;
         })
         .catch(error => {
            console.error(error);
         });
      },
      save_inputs_model(id, complexity, machine, payload) {
         const path = `http://localhost:5000/mc/${id}/model/${machine}/complexity${complexity}`;
         console.log('SAVE MODLE', payload)
         axios
         .put(path, payload)
         .then(res => {
            this.$message({
               message: 'Inputs saved',
               type: 'success'});
         })
         .catch(error => {
            console.error(error);
            this.$message({
               message: error,
               type: 'error'});
         });



      }
  }
}
