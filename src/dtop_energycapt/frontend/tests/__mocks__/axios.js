// This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
export default {
    get() {
      return new Promise((resolve) => {
        if (this.__mockError)
            reject(new Error())
        resolve(this.__result)
    })
    },
    delete() {
        return new Promise((resolve) => {
            if (this.__mockError)
                reject(new Error())
            resolve(this.__result)
        })
    },
    patch() {
        return Promise.resolve(this.__result)
    },
    post() {
        return new Promise((resolve) => {
            if (this.__mockError)
                reject(new Error())
            resolve(this.__result)
        })
    },
    put() {
        return new Promise((resolve) => {
            if (this.__mockError)
                reject(new Error())
            resolve(this.__result)
        })
    },
    resolveWith(data, mockError = false) {
        this.__mockError = mockError
        this.__result = data
    }
}
