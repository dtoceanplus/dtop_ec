// This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

// import DashboardInput from '@/views/dashboard/index.vue'
import ECStudies from '@/views/ec_studies/index.vue'
import ElementUI from 'element-ui';

import ECStudyData from '../json/ECstudy.json'
import ECStudiesData from '../json/ECstudies.json'

/* Layout */
import Layout from '@/layout'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")
describe('Test EC Studies index.vue', () => {
    // const localVue = createLocalVue()
    // localVue.use(VueRouter)

    // const routes = [
    //     {
    //       path: '/ec-studies',
    //       component: Layout,
    //       name: 'ECStudies',
    //       meta: { title: 'Studies', icon: 'table' },
    //       children: [
    //         {
    //           path: 'index',
    //           name: 'ECStudiesIndex',
    //           component: () => import('@/views/ec_studies/index'),
    //           meta: { title: 'List' },
    //         },
    //         {
    //           path: 'inputs',
    //           name: 'ECStudyInput',
    //           component: () => import('@/views/ec_study/index'),
    //           meta: { title: 'Inputs' },
    //           children: [{
    //               path: 'general',
    //               name: 'FarmInput',
    //               component: () => import('@/views/ec_study/farm/index'),
    //               meta: { title: 'Farm' }
    //             }
    //           ]
    //         }
    //       ]
    //     }
    //   ];
    // const router = new VueRouter({routes})
    const index = 0
    const rows = [{"id": 1,"message": "Project 1"}]
    const ecId = 1
    const wrapper = shallowMount(ECStudies)
    // const wrapper = shallowMount(ECStudies, {
    //     localVue,
    //     router
    // })

    it('getProjects error', async () => {
        await wrapper.vm.getProjects();
        expect(wrapper.vm.ec_studies).toEqual([]);
    })

    it('getProjects', async () => {
        axios.resolveWith(ECStudiesData);
        await wrapper.vm.getProjects();
        expect(wrapper.vm.ec_studies).toEqual(ECStudiesData.data);
    })

    it.skip('postProject error', async () => {
        axios.resolveWith("", true)
        await wrapper.vm.postProject(ECStudyData.data);
    })

    it('postProject', async () => {
        const testData = {"data": [{"ID": 1,"message": "Project added"}]}
        axios.resolveWith(testData)
        await wrapper.vm.postProject(ECStudyData.data);
        expect(wrapper.vm.message).toEqual(testData.data);
        expect(wrapper.vm.showMessage).toEqual(true);
    })

    it.skip('putProject error', async () => {
        const payload = { title: "test", tags: "test", complexity: "1", type: "TEC", desc: "test", date: "2016-05-02", status: 0 }
        axios.resolveWith("", true)
        await wrapper.vm.putProject(payload);
    })

    it('putProject', async () => {
        const testDataPut = {"data": [{"ID": 1,"message": "Project updated"}]}
        axios.resolveWith(testDataPut)
        await wrapper.vm.putProject(ECStudyData.data);
        expect(wrapper.vm.message).toEqual(testDataPut.data);
        expect(wrapper.vm.showMessage).toEqual(true);
    })

    it('removeProject', async () => {
        await wrapper.vm.removeProject(ecId);
        expect(wrapper.vm.showMessage).toEqual(true);
    })

    it.skip('putConfirmation', async () => {

      let payload = {status: 18};
      wrapper.vm.putConfirmation(payload)

      const closeBtn = wrapper.find('.el-button');
      closeBtn.trigger('click');
      // console.log(wrapper.find('.el-button').trigger('click'))
    })

    it('copyRow', () => {
      wrapper.vm.copyRow(0, [{id:0, farm:1, devices:[1], status:0}])
    })

    it('deleteInputs', async () => {
      let res = await wrapper.vm.deleteInputs(0)
      expect(res.data[0].ID).toEqual(1)
    })

    it('deleteFarm', async () => {
      let res = await wrapper.vm.deleteFarm(0)
      expect(res.data[0].ID).toEqual(1)
    })

    it('deleteDevices', async () => {
      let res = await wrapper.vm.deleteDevices(0)
      expect(res.data[0].ID).toEqual(1)
    })

    it.skip('removeProject error', async () => {
        axios.resolveWith("", true)
        await wrapper.vm.removeProject(ecId);
    })

    it('deleteRow', () => {
        expect(wrapper.vm.deleteRow(index, rows)).toEqual();
    })

    it('editRow', () => {
        wrapper.vm.editRow(index, rows);
        expect(wrapper.vm.editingRole).toEqual(true);
        expect(wrapper.vm.ruleForm).toEqual(rows[index]);
        expect(wrapper.vm.dialogVisible).toEqual(true);
    })

    it.skip('openRow', async() => {
        await wrapper.vm.openRow(index, rows);
        await wrapper.vm.$nextTick()
        console.log(wrapper.vm.$route.path)
    })

    it.skip('formSetDefault', async () => {
        wrapper.vm.formSetDefault();
        expect(wrapper.vm.ruleForm.title).toEqual("");
        expect(wrapper.vm.ruleForm.tags).toEqual("");
        expect(wrapper.vm.ruleForm.complexity).toEqual("");
        expect(wrapper.vm.ruleForm.type).toEqual("");
        expect(wrapper.vm.ruleForm.desc).toEqual("");
        await wrapper.vm.$nextTick()
    })
})
