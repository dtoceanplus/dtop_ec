// This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
// Copyright (C) 2021 AAU-Build - Francesco Ferri
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import { shallowMount } from '@vue/test-utils'

// import DashboardInput from '@/views/dashboard/index.vue'
import Farm from '@/views/ec_study/farm/index.vue'
import ElementUI from 'element-ui';

/* Layout */
import Layout from '@/layout'

import FarmData from '../json/FarmData.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('Test EC Study Farm index.vue', () => {
    const index = 0
    const rows = [{"id": 1,"message": "Project 1"}]
    const ecId = 1
    const wrapper = shallowMount(Farm, {
      mocks: {
        $store: {
          getters: {
            studyId : 1,
          }
        }
      }
    })

    it('updateTable', () => {
      expect(wrapper.vm.updateTable()).toEqual();
      wrapper.setData({ rows: [{x:0, y:0},{x:100,y:0}] })
      wrapper.setData({ ruleForm: { nb_devices: 1 }})
      console.log(wrapper.vm.rows, wrapper.vm.ruleForm.nb_devices)
      expect(wrapper.vm.updateTable()).toEqual();
      wrapper.setData({ ruleForm: { nb_devices: 2 }})
      console.log(wrapper.vm.rows, wrapper.vm.ruleForm.nb_devices)
      expect(wrapper.vm.updateTable()).toEqual();
    })

    it('update_array_form', () => {
      wrapper.setData({ rows: [{x:0, y:0},{x:100,y:0}] })
      let res = wrapper.vm.update_array_form({array_layout: null})
      expect(res).toEqual({ array_layout: [[0,0],[100,0]]})
    })

    it('update_rows', () => {
      wrapper.setData({ ruleForm: { nb_devices: 1 }})
      let res = wrapper.vm.update_rows()
      expect(wrapper.vm.rows.length).toEqual(1)
    })

    it('get_farm_inputs', async () => {
      axios.resolveWith(FarmData);
      await wrapper.vm.get_farm_inputs();
      expect(wrapper.vm.ruleForm).toEqual(FarmData.data);
    })

    it('submit_input', async () => {
      axios.resolveWith({res: 'ok'});
      await wrapper.vm.submit_input({array_layout: null});
    })
})
