# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
"""

from dtop_energycapt.business.arraytecstg1 import ArrayTECStg1
from dtop_energycapt.business.arraytecstg2 import ArrayTECStg2
from dtop_energycapt.business.arraytecstg3 import ArrayTECStg3
from dtop_energycapt.business.arraywecstg1 import ArrayWECStg1
from dtop_energycapt.business.arraywecstg2 import ArrayWECStg2
from dtop_energycapt.business.arraywecstg3 import ArrayWECStg3

import logging

import os
from logging.handlers import RotatingFileHandler
if not os.path.isdir('./logs'):
    os.mkdir('./logs')

from dtop_energycapt.service.logsetup import LOGGING_CONFIG
import logging.config
logging.config.dictConfig(LOGGING_CONFIG)
LOG = logging.getLogger(__name__)


class ArrayFactory():
    
    @staticmethod
    def get_array(ar_type, ar_stg):
        LOG.info(f'Creating the Farm Object for the case {ar_type} ad complexity {ar_stg}')
        try: 
            if ar_type == "TEC" and ar_stg == "1":
                return ArrayTECStg1()
            elif ar_type == "TEC" and ar_stg == "2":
                return ArrayTECStg2()
            elif ar_type == "TEC" and ar_stg == "3":
                return ArrayTECStg3()
            elif ar_type == "WEC" and ar_stg == "1":
                return ArrayWECStg1()
            elif ar_type == "WEC" and ar_stg == "2":
                return ArrayWECStg2()
            elif ar_type == "WEC" and ar_stg == "3":
                return ArrayWECStg3()
            raise AssertionError('Array type and stage not found')
        except AssertionError as _e:
            print(_e)
            raise

# if __name__ == "__main__":
# #     ARRAY = ArrayFactory.get_array('WEC', "2")
# # #    print(f"{ARRAY.__class__} : {ARRAY.get_inputs()}")
# #     # ARRAY.get_inputs(10, 10000., 28., 120, [[2000, 4000]], 600., 600.)
# #     ecID = 1
# #     name = "test_project"
# #     inputs = {  
# #                 'nb_devices': 10, 
# #                 'opt_method': 'monte_carlo',
# #                 'optimisation_threshold': 0.8,
# #                 'array_generic_type':'rectangular',
# #                 'ratedPowerArray': 5000.,
# #                 'distance_min': (10, 10),
# #                 # 'layout':  np.array(([-75,-50], [-75,0], [-75,50], [-50,-50], [-50,0], [-50,50], [-25,-50], [-25,0], [-25,50], [0,0]))
# #              }
# #     site = {
# #                 'scat_diag': np.array([[3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0], 
# #                                       [0, 1, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [0, 5, 7, 11,56, 24, 14, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 35, 3, 1, 0, 0, 0, 0, 0, 0]], np.float),
# #                 'Hs': np.array([0.5, 1., 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5]),
# #                 'Tp': np.array([1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75]),
# #                 'loc_position': [[2000, 4000]], 
# #     }
# #     converter = {
# #                 'rated_pow_device': 40000., 
# #                 'capture_width_matrix': np.array([[2, 1, 5, 3, 1, 0, 0, 0, 0, 0, 0], 
# #                                       [0, 1, 35, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [0, 5, 7, 11,7, 24, 14, 0, 0, 0, 0],
# #                                       [3, 5, 4, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 1, 3, 1, 5, 6, 0, 0, 0, 0],
# #                                       [3, 5, 1, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 5, 1, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [3, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0],
# #                                       [0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0]], np.float),
# #                 'sec_dim_device': 650.,
# #                 'main_dim_device': 700.,
# #                 'arch_type': 'terminator',
# #     }
# #     inputs.update(site)
# #     inputs.update(converter)
# #     ARRAY.get_inputs(name, ecID, **inputs)
# #     print(ARRAY.iconverter.main_dim_device)


# #     ARRAY = ArrayFactory.get_array('TEC', "2")
# # #    print(f"{ARRAY.__class__} : {ARRAY.get_inputs()}")
# #     # ARRAY.get_inputs(10, 10000., 28., 120, [[2000, 4000]], 600., 600.)
# #     ecID = 1
# #     name = "test_project"
# #     inputs = {  
# #                 'nb_devices': 10, 
# #                 'opt_method': 'monte_carlo',
# #                 'optimisation_threshold': 0.8,
# #                 'array_generic_type':'rectangular',
# #                 'ratedPowerArray': 5000.,
# #                 'distance_min': (10, 10),
# #                 'layout':  [[0,0],[100,50],[0,100]]
# #              }
# #     site = {
# #                 'velocity_field':  ((0,5),),
# #                 'loc_position': [[2000, 4000]], 
# #     }
# #     converter = {
# #                 'rated_pow_device': 40000., 
# #                 'rotor_diameter': 10,
# #                 'cp': 1.,
# #                 'ct': 1.,
# #                 'hub_height': 10,
# #                 'cut_IO': [0,np.inf],
# #     }
# #     inputs.update(site)
# #     inputs.update(converter)
# #     ARRAY.get_inputs(name, ecID, **inputs)
# #     print(ARRAY.iconverter.hub_height)

# #     ARRAY.PerfMetrics.set_device_distances()
# #     print(ARRAY.PerfMetrics.dij)
# #     print(ARRAY.PerfMetrics.xij)
# #     ARRAY.PerfMetrics.set_tubine_overlaps()
# #     print(ARRAY.PerfMetrics.aij)
# #     print(ARRAY.PerfMetrics.rij)
# #     ARRAY.PerfMetrics.velocity_at_devices_location()
# #     print(ARRAY.PerfMetrics.hubs_velocity)
# #     ARRAY.PerfMetrics.power_performance()
# #     print(ARRAY.PerfMetrics.outputs)
# #     ARRAY.PerfMetrics.plot_farm()