# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np


class Converter(object):
    """
    Converter class: The class contains all the information relative to the 
    machine deployed in the array.
    
    Args:
        rated_pow_device (float): Rated power of a single device [kW].
        capwidth_matrix (numpy.ndarray): Capture width matrix of a single device [m].
        main_dim_device (float): Main dimension of the device, i.e. 
                                    dimension of the device perpendicular to 
                                    the incoming waves.
        sec_dim_device (float): Secondary dimension of the device, i.e 
                                dimension of the device parallel to the 
                                incoming waves.
        arch_type:  Basic categorisation of the WEC device. Possible values 
                    are "attenuator", "terminator", "point_absorber" or None
                    (if not speicified by the user).

    """
    def __init__(self, **kwargs):

        self.rated_pow_device = None
        self.capture_width_ratio = None
        self.tp = None
        self.hs = [1]
        self.w_dirs = None
        self.main_dim_device = None
        self.sec_dim_device = None
        self.arch_type = None
        self.max_dim = None
        self.min_distance = None

        # internal matrix format used for quicker answer
        self._x = None
        self._y = None
        self._z = None
        self._cwr_mat = None

        if 'rated_pow_device' in kwargs:
            self.rated_pow_device = kwargs.get("rated_pow_device")
        else:
            print('Rated power of the device input missing!')
            raise
        if 'device_capture_width_ratio' in kwargs:
            self.capture_width_ratio = kwargs.get("device_capture_width_ratio")
        else:
            print('Device capture width matrix input missing!')
            raise
        if 'tp_cwr' in kwargs:
            self.tp = kwargs.get("tp_cwr")
        else:
            if 'tp_capture_width' in kwargs: 
                self.tp = kwargs.get("tp_capture_width")
            else:
                print('Capture width ratio Tp indexes missing!')
                raise

        if 'hs_cwr' in kwargs:
            self.hs = kwargs.get("hs_cwr")
        elif 'hs_capture_width' in kwargs: 
            self.hs = kwargs.get("hs_capture_width")

        if 'wave_angle_capture_width' in kwargs:
            self.w_dirs = kwargs.get("wave_angle_capture_width")

        if 'main_dim_device' in kwargs:
            self.main_dim_device = kwargs.get("main_dim_device")
        else:
            print('Main dimension of the device input missing!')
            raise
        if 'sec_dim_device' in kwargs:
            self.sec_dim_device = kwargs.get("sec_dim_device")
        else:
            print('Secondary dimension of the device input missing!')
            raise
        if 'arch_type' in kwargs:
            self.arch_type = kwargs.get("arch_type")
        else:
            if 'machine_archetype' in kwargs: self.arch_type = kwargs.get("machine_archetype")
            self.arch_type = None

        self.check_inputs()
        self.min_distance = (2 * self.main_dim_device,
                             max(2 * self.main_dim_device,
                                 self.sec_dim_device))
        self.cwr_matrix_dimensions()
    
    def cwr_matrix_dimensions(self):
        '''cwr_matrix_dimensions checks the dimension of the cwr variable 
        and homogeinizes it to have have dimension #Hs, #Tp.
        The cwr can be give as a vector in function of Tp, in that case
        the output matrix is constructed tiling from the original vector.
        '''
        nt, nh, nd = np.shape(self.tp), np.shape(self.hs), np.shape(self.w_dirs)
        cwr_s = self.capture_width_ratio.shape
        
        # the modified scatter diagram is a flattened list
        if (nt != nh or nt != nd or nh != nd):
            raise ValueError(f'Incorrect format of the CWR matrix: wave period (shape {nt}), wave direction (shape {nd}) and wave height (shape {nh}) must have the same shape.')
        if (nt != cwr_s ):
            raise ValueError(f'Incorrect format of the CWR matrix: The CWR (shape {cwr_s}) must have the same shape of the wave period (shape {nt}).')
        
        
        # convert in matrix format
        x, y, z = (np.unique(self.tp), np.unique(self.hs), np.unique(self.w_dirs))

        cwr_mat = np.zeros((len(x), len(y), len(z)))
        for ii in range(len(self.capture_width_ratio)):
            l, = np.where(x==self.tp[ii])
            m, = np.where(y==self.hs[ii])
            n, = np.where(z==self.w_dirs[ii])
            cwr_mat[l,m,n] = self.capture_width_ratio[ii]

        self._x = x
        self._y = y
        self._z = z
        self._cwr_mat = cwr_mat

    def printInput(self, indent=4):
        """print the Converter class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """

        print('Print all arguments of this instance of Converter')

        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)

        return

    def check_inputs(self):

        status = True
        errStr = []
        """ self.rated_pow_device should be a positive non null float """
        try:
            if not isinstance(self.rated_pow_device, (float,int)):
                raise TypeError
        except TypeError:
            print('Rated power of the device must be a float value.')
            raise
        try:
            if self.rated_pow_device <= 0:
                raise ValueError
        except ValueError:
            print("The rated power of the device must be a positive non null \
                    value.")
            raise
        """ self.device_capture_width should be a positive non null float """
        try:
            if not isinstance(self.capture_width_ratio, (np.ndarray, np.generic, list)):
                raise TypeError
            else:
                self.capture_width_ratio = np.asarray(self.capture_width_ratio)
        except TypeError:
            print('The capture width matrix must be of type numpy.ndarray.')
            raise
        try:
            if not np.all(self.capture_width_ratio>=0):
                raise ValueError
        except ValueError:
            print(
                "One or some values of the capture width matrix are negative \
                    value(s).")
            raise
        """ self.main_dim_device should be a positive non null float """
        try:
            if not isinstance(self.main_dim_device, (float,int)):
                raise TypeError
        except TypeError:
            print('Main dimension of the device must be a float value.')
            raise
        try:
            if self.main_dim_device <= 0:
                raise ValueError
        except ValueError:
            print("Main dimension of the device must be a positive non null \
                    value.")
            raise

        l_arch_type = ["attenuator", "terminator", "point_absorber"]
        """ self.arch_type should be a specific string or None """
        try:
            if not (self.arch_type in l_arch_type or self.arch_type == None):
                print(self.arch_type)
                raise ValueError
        except ValueError:
            print("Archetype of the device should be: {}, {} or \
                    {}.".format(*l_arch_type))
            raise
        """ self.sec_dim_device should be a positive non null float """
        try:
            if not isinstance(self.sec_dim_device, (int,float)):
                raise TypeError
        except TypeError:
            print('Secondary dimension of the device must be a float value.')
            raise
        try:
            if self.sec_dim_device <= 0:
                raise ValueError
        except ValueError:
            print("Secondary dimension of the device must be a positive non \
                    null value.")
            raise
        """ Defining the maximum dimension of the device (limiting \
            dimension) """
        if self.main_dim_device >= self.sec_dim_device:
            self.max_dim = self.main_dim_device
        else:
            self.max_dim = self.sec_dim_device

    def checkPowerConsistency(self, rPowArray):

        status = True
        errStr = []
        
        """ self.maxNumDevices should be a positive non null integer """
        try:
            maxNumDevices = int(rPowArray / self.rated_pow_device)
        except TypeError:
            print('Maximum number of device must be an integer number.')
            status = False
            errStr.append(
                'Maximum number of device must be an integer number.')
            raise
        try:
            if maxNumDevices <= 0:
                raise ValueError("Must be a positive non null integer!")
        except ValueError:
            print(
                "The ratio between array and machine rated powers is below 1.\n\
                Verify the rated power inputs.")
            status = False
            errStr.append(
                "The ratio between array and machine rated powers is \
                        below 1.'\n' Verify the rated power inputs. ")
            raise

        return maxNumDevices, status, errStr