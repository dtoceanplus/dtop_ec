# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np
from shapely.geometry import Polygon


class SiteConditions(object):
    """
    SiteConditions class: The class contains all the information relative to the 
    site conditions at the deployment location of the array.
    
    Args:
        scat_diag  (numpy.ndarray): Scatter diagram for the deployment 
                                    location.
        Hs (numpy.ndarray): Wave heights values corresponding to the rows of 
                            the scatter diagram.
        Tp (numpy.ndarray): Wave periods values corresponding to the columns of 
                            the scatter diagram.
        loc_position (List[[float,float]]): Position of the deployment site 
                                            [Easting [m], Northing [m]].
        central_deploy_pos(List[[float,float]]): Central position of the array 
                                                 layout.
    """
    def __init__(self, **kwargs):

        self.scatter_diagram = None
        self.hs = None
        self.tp = None
        self.b = None
        self.loc_position = None
        self.main_angle = 0  # by default at stage 1 the main angle is set to 0
        self.lease_reduced = None
        self.central_deploy_pos = None

        # internal matrix format used for quicker
        self._x = None
        self._y = None
        self._z = None
        self._ejpd = None
        self._ids = None

        if 'scatter_diagram' in kwargs:
            self.scatter_diagram = kwargs.get("scatter_diagram")
        else:
            print('Scatter diagram for the deployment site missing!')
            raise
        if 'sea_state_wave_height' in kwargs:
            self.hs = kwargs.get("sea_state_wave_height")
        else:
            raise ValueError('Hs bins values of the scatter diagram for the deployment site missing!')
        if 'sea_state_wave_period' in kwargs:
            self.tp = kwargs.get("sea_state_wave_period")
        else:
            raise ValueError('Tp bins values of the scatter diagram for the deployment site missing!')
        if 'sea_state_directions' in kwargs:
            self.b = kwargs.get("sea_state_directions")
        else:
            raise ValueError('B bins values of the scatter diagram for the deployment site missing!')
        if 'loc_position' in kwargs:
            self.loc_position = kwargs.get("loc_position")
        else:
            print('Position of the location input missing!')
            raise ValueError('Lease area vertexes of the seletected location input missing')
        self.check_inputs()
        x = [t[0] for t in self.loc_position]
        y = [t[1] for t in self.loc_position]
        self.central_deploy_pos = [sum(x) / len(x), sum(y) / len(y)]
        self.lease_reduced = Polygon(self.loc_position)

        self.ejpd_matrix_dimensions()
    
    def ejpd_matrix_dimensions(self):
        '''ejpd_matrix_dimensions checks the dimension of the ejpd variable 
        and homogeinizes it to have have dimension #Hs, #Tp, #Dir.
        '''
        nt, nh, nd = np.shape(self.tp), np.shape(self.hs), np.shape(self.b)
        ejpd_s = self.scatter_diagram.shape

        # the modified scatter diagram is a flattened list
        if (nt != nh or nt != nd or nh != nd):
            raise ValueError('Incorrect format of the EJPD matrix: wave period (shape {nt}), wave direction (shape {nd}) and wave height (shape {nh}) must have the same shape.')
        if (nt != ejpd_s ):
            raise ValueError('Incorrect format of the EJPD matrix: The EJPD (shape {ejpd_s}) must have the same shape of the wave period (shape {nt}).')
        
        # convert in matrix format
        x, y, z = (np.unique(self.tp), np.unique(self.hs), np.unique(self.b))

        sd_mat = np.zeros((len(x), len(y), len(z)))
        ids = np.zeros((len(x), len(y), len(z)))
        for ii in range(len(self.scatter_diagram)):
            l, = np.where(x==self.tp[ii])
            m, = np.where(y==self.hs[ii])
            n, = np.where(z==self.b[ii])
            sd_mat[l,m,n] = self.scatter_diagram[ii]
            ids[l,m,n] = ii
        
        self._x = x
        self._y = y
        self._z = z
        self._ejpd = sd_mat
        self._ids = ids

    def printInput(self, indent=4):
        """print the SiteConditions class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """

        print('Print all arguments of this instance of SiteConditions')

        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)

        return

    def check_inputs(self):

        status = True
        errStr = []
        """ self.scatter_diagram should be a numpy.ndarray of positive float """
        try:
            if not isinstance(self.scatter_diagram, (np.ndarray,list)):
                raise TypeError
        except TypeError:
            print('Scatter diagram must be a numpy.ndarray.')
            raise
        self.scatter_diagram = np.array(self.scatter_diagram)
        try:
            if not (self.scatter_diagram >= 0).all():
                raise ValueError
        except ValueError:
            print("All values in the scatter diagram must be positive or 0.")
            raise

        """ self.Hs should be a numpy.ndarray of positive float """
        try:
            if not isinstance(self.hs, (np.ndarray,list)):
                raise TypeError
        except TypeError:
            print('Hs must be a numpy.ndarray.')
            raise
        self.hs = np.array(self.hs)
        try:
            if not (self.hs >= 0).all():
                raise ValueError
        except ValueError:
            print("All values Hs must be positive.")
            raise
        """ self.Tp should be a numpy.ndarray of positive float """
        try:
            if not isinstance(self.tp, (np.ndarray, list)):
                raise TypeError
        except TypeError:
            print('Tp must be a numpy.ndarray.')
            raise
        self.tp = np.array(self.tp)
        try:
            if not (self.tp >= 0).all():
                raise ValueError
        except ValueError:
            print("All values Tp must be positive.")
            raise
        """ self.loc_position should be a list """
        try:
            list_ = isinstance(self.loc_position, list)
            if not list_:
                raise TypeError
        except TypeError:
            print("Deployment location should be a list of points.")
            raise
        """ All elements in self.loc_position should be a list """
        try:
            if not all(isinstance(x, list) for x in self.loc_position):
                raise TypeError
        except TypeError:
            print("Each element in loc_position should be a list of points.")
            raise
        """ All elements in the elements in self.loc_position should be a \
            float (or int) """
        try:
            if not all(
                    isinstance(x, float) for sublist in self.loc_position
                    for x in sublist) and not all(
                        isinstance(x, int) for sublist in self.loc_position
                        for x in sublist):
                raise TypeError
        except TypeError:
            print("The easting and northing position should be float.")
            raise
        """ All elements in the elements in self.loc_position should be \
            positive non null float (or int) """
        try:
            if not all(x >= 0 for sublist in self.loc_position
                       for x in sublist):
                raise ValueError
        except ValueError:
            print("The easting and northing position should be positive non \
                null float.")
            raise
        """ All elements in self.loc_position should be pairs of points """
        try:
            if not all(len(sublist) == 2 for sublist in self.loc_position):
                raise ValueError
        except ValueError:
            print("The number of easting values does not match the number of \
                northing values. Example: [[3440, 4000],[3550, 4550]]")
            raise
