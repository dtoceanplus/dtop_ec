# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pickle
import cma

def cmaes(self, tolfun=1e0, tolx=1e-2, maxiter=50, maxfevals=1000):
    """
    cmaes: calls the cma package to optimise the power production
        of the array

    Args (optional):
        tolfun (float)[W]: minimun allowed variation of the fit to decide
            for the solution stagnation
        tolx (float)[-]: minimun allowed variation of the parameters to
            decide for the solution stagnation
        maxiter (int)[-]: max number of population regeneration
        maxfevals (int)[-]: max number of total function evaluation

    Returns:
            x (list): list of normalised parameters that represent the best
            solution found
    """
    print('CMAES')
    print('{}\n'.format(self.optfunname))
    # TODO: dimension problem with the function estimate_start_point
    # x0, self._normalisation_point = self.estimate_start_point()
    # if not x0:
    #     warning_str = ('Could not find a suitable starting point '
    #                     'for the optimiser, the centroid of the '
    #                     'parameter space is used instead')
    #     # module_logger.warning(warning_str)
    #     print(warning_str)
    x0 = self.opt_dim * [(self._min_bound+self._max_bound)/2]
    
    # print(x0)
    es = cma.CMAEvolutionStrategy(
                    x0,
                    2,
                    {'bounds': [self._min_bound,self._max_bound],
                        'verb_disp': 0})
    
    es.opts.set('tolfun', tolfun)
    es.opts.set('tolx', tolx)
    es.opts.set('maxiter', maxiter)
    es.opts.set('maxfevals',maxfevals)

    while not es.stop():
        
        solutions = es.ask()
        
        # reduce the significant digits of the search space
        # solutions = [np.around(s, decimals=1) for s in solutions]
        temp = [self.optimCostFunNorm(s) for s in solutions]
        fitness = [(-el[0]) for el in temp]
        es.tell(solutions, fitness)
        
        # if self._debug:
        es.logger.add()
        es.disp(10)
    
    # if self._debug:
    es.result_pretty()
#            es.logger.plot_all()
#            pickle.dump(es.archive.data,
#                        open("optimisation_results_cma-es_new.pkl", "wb"))

    if es.best.f > 0.:
        return -1
    else:

        # print('The solution is: ', (es.best.x).tolist())
        v1, v2 = self.optimCostFunNorm((es.best.x).tolist())

        # return (es.best.x).tolist()


def meta_model(self):
    print('Meta Model')
    # self.costfunc()
    print('{}\n'.format(self.optfunname))

def monte_carlo(self, maxiter=25):
    """
    monte_carlo: optimise the array layout using the Motecarlo
                 simulation approach

    Args:
        maxiter (int): number of simulation to be run. Since there is no
            rational behind this method, but everything is based on the
            randomness of the solution, the number of maxiter is directly
            affecting the stability of the solution

    """
    xx = np.random.random_sample((self.opt_dim, maxiter))
    xx = xx * (self._max_bound-self._min_bound)
    xx = xx + self._min_bound
    # print(xx)
    fit = np.zeros(maxiter)

    for i in range(maxiter):
        # module_logger.info('iteration #: {}'.format(i))
        x = xx[:,i]
        fit[i] = -self.optimCostFunNorm(x)[0]
        # print('The fit is:', fit[i])
    # find max average energy for arrays with q-factor larger than q_min
    index = fit.argmin()
    pickle.dump([fit, xx],
                open("optimisation_results_brutal_force.pkl", "wb"))
    # print('Optimum parameters are:', xx[:,index])

    v1, v2 = self.optimCostFunNorm(xx[:,index])


def brute_force(self, N=6):
    """
    brute_force: optimise the array layout using the brute force approach

    Args:
        N (int): discretisation parameter for the parameter space 

    """
    print('Brute force optimisation method')
    print('{}\n'.format(self.optfunname))
    x = np.linspace(self._min_bound,self._max_bound,N)
    y = np.linspace(self._min_bound,self._max_bound,N)
    fit = np.zeros((N*N))
    xxx = np.zeros((N*N))
    yyy = np.zeros((N*N))
    ind = -1
    for ii, inter_col in enumerate(x):
        for jj, beta in enumerate(y):
            ind += 1
            # print('iteration {} over {}'.format(ind+1, len(x)*len(y)))
            fit[ind] = -self.optimCostFunNorm((inter_col, beta))[0]
            xxx[ind] = inter_col
            yyy[ind] = beta
    # index = np.unravel_index(fit.argmin(), fit.shape)
    # print('All solutions:', fit)
    index = fit.argmin()
    # print('Best solution is at {} and is {}:'.format(index, fit[index]))
    pickle.dump([fit, x, y],
                open("optimisation_results_brutal_force.pkl", "wb"))

    if fit[index] > 0:
        print('No optimum found.')
        # return -1
    else:
        # print(xxx[index], yyy[index], x[index//N],  y[int(index%N)])
        v1, v2 = self.optimCostFunNorm([x[index//N], y[int(index%N)]])
        # return (x[index//N], y[int(index%N)])

dictfunc = {
            'cmaes': cmaes,
            'meta_model': meta_model,
            'brute_force': brute_force,
            'monte_carlo': monte_carlo,
            }


# if __name__ == "__main__":
#     from dtop_energycapt import business
#     device = {
#         'rotor diameter': 10,
#         'hub height': 20,
#         'floating': False,
#         'cut_IO': [1, 10],
#         'cp': 0.3,
#         'ct': 1,
#         'alpha': 0.07,
#         'rated_pow_device': 5e6,
#     }

#     layout = np.array([[2.39964301, 493.2435497], [2.39964301, 280.02992788],
#                     [19.69090866, 386.63673879], [36.98217431, 493.2435497],
#                     [2.39964301, 66.81630606], [19.69090866, 173.42311697],
#                     [36.98217431, 280.02992788], [54.27343996, 386.63673879],
#                     [71.5647056, 493.2435497], [36.98217431, 66.81630606],
#                     [54.27343996, 173.42311697], [71.5647056, 280.02992788],
#                     [88.85597125, 386.63673879], [106.1472369, 493.2435497],
#                     [88.85597125, 173.42311697], [106.1472369, 280.02992788],
#                     [123.43850255, 386.63673879], [140.72976819, 493.2435497],
#                     [123.43850255, 173.42311697], [140.72976819, 280.02992788],
#                     [158.02103384, 386.63673879], [175.31229949, 493.2435497],
#                     [158.02103384, 173.42311697], [175.31229949, 280.02992788],
#                     [192.60356514, 386.63673879], [209.89483079, 493.2435497],
#                     [209.89483079, 280.02992788], [227.18609643, 386.63673879],
#                     [244.47736208, 493.2435497], [244.47736208, 280.02992788],
#                     [261.76862773, 386.63673879], [279.05989338, 493.2435497],
#                     [279.05989338, 280.02992788], [296.35115903, 386.63673879],
#                     [313.64242467, 493.2435497], [330.93369032, 386.63673879],
#                     [348.22495597, 493.2435497], [365.51622162, 386.63673879],
#                     [382.80748727, 493.2435497], [417.39001856, 493.2435497],
#                     [451.97254986, 493.2435497]])
#     velocity = ((0, 5), )

#     name = 'testes'
#     ecID = 4
#     body = {
#         'device': device,
#         # 'layout': layout.tolist(),
#         'velocity_field': velocity,
#         'nb_devices': 42,
#         'array_generic_type': 'staggered',
#         'loc_position': [[0, 0], [500, 500], [0, 500]],
#         'rated_pow_device': 5e6,
#         'rotor_diameter': 10,
#         'hub_height': 20,
#         'floating': False,
#         'cut_IO': [1, 10],
#         'cp': 0.3,
#         'ct': 1,
#         'alpha': 0.07,
#         'optimisation_threshold': 0.8,
#         'opt_method': 'cmaes',
#     }            

#     arr = business.ArrayFactory.get_array('TEC', "2")
    
#     arr.get_inputs(name, ecID, **body)
#     arr.optimize_array()