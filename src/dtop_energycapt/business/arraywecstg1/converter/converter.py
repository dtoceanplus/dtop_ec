# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np


class Converter(object):
    """
    Converter class: The class contains all the information relative to the 
    machine deployed in the array.
    
    Args:
        rated_pow_device (float): Rated power of a single device [kW].
        device_capture_width (float): Capture width of a single device [m].
        main_dim_device (float): Main dimension of the device, i.e. 
                                    dimension of the device perpendicular to 
                                    the incoming waves.
        sec_dim_device (float): Secondary dimension of the device, i.e 
                                dimension of the device parallel to the 
                                incoming waves.
        arch_type:  Basic categorisation of the WEC device. Possible values 
                    are "attenuator", "terminator", "point_absorber" or None
                    (if not speicified by the user).

    """
    def __init__(self, **kwargs):

        self.rated_pow_device = None
        self.device_capture_width_ratio = None
        self.main_dim_device = None
        self.sec_dim_device = None
        self.arch_type = None
        self.max_dim = None
        self.min_distance = None

        if 'rated_pow_device' in kwargs:
            self.rated_pow_device = kwargs.get("rated_pow_device")
        else:
            print('Rated power of the device input missing!')
            raise
        if 'device_capture_width_ratio' in kwargs:
            self.device_capture_width_ratio = kwargs.get(
                "device_capture_width_ratio")
        else:
            print('Device capture width input missing!')
            raise
        if 'main_dim_device' in kwargs:
            self.main_dim_device = kwargs.get("main_dim_device")
        else:
            print('Main dimension of the device input missing!')
            raise
        if 'sec_dim_device' in kwargs:
            self.sec_dim_device = kwargs.get("sec_dim_device")
        else:
            print('Secondary dimension of the device input missing!')
            raise
        if 'arch_type' in kwargs:
            self.arch_type = kwargs.get("arch_type")
        else:
            self.arch_type = None
        self.check_inputs()

        # default value
        self.min_distance = (2 * self.main_dim_device,
                             max(2 * self.main_dim_device,
                                 self.sec_dim_device))
        if 'minimum_interdistance' in kwargs:
            min_inter = kwargs.get('minimum_interdistance')
            if isinstance(min_inter, (list, np.ndarray)):
                if len(min_inter) == 0:
                    pass
                elif len(min_inter) > 1:
                    self.min_distance = (min_inter[0], min_inter[1])
                else:
                    self.min_distance = (min_inter[0], min_inter[0])
            
        



    def printInput(self, indent=4):
        """print the Converter class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """

        print('Print all arguments of this instance of Converter')

        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)

        return

    def check_inputs(self):

        status = True
        errStr = []
        """ self.rated_pow_device should be a positive non null float """

        try:
            if not isinstance(self.rated_pow_device, (int, float)):
                raise TypeError
        except TypeError:
            print('Rated power of the device must be a float value.')
            raise TypeError('Rated power of the device must be a float value.')
        try:
            if self.rated_pow_device <= 0:
                raise ValueError
        except ValueError:
            print("The rated power of the device must be a positive non null \
                    value.")
            raise ValueError("The rated power of the device must be a positive non null value.")

        """ self.device_capture_width_ratio should be a positive non null float """
        try:
            if not isinstance(self.device_capture_width_ratio, (int, float)):
                raise TypeError
        except TypeError:
            print('Capture width of the device must be a float value.')
            raise TypeError('Capture width of the device must be a float value.')
        try:
            # if (self.device_capture_width_ratio <= 0
            #         or self.device_capture_width_ratio > 1):
            if (self.device_capture_width_ratio <= 0):
                raise ValueError
        except ValueError:
            print("Capture width of the device must be a positive non null \
                    value.")
            raise ValueError(f"Capture width ratio of the machine cannot be negative or zero. Acutal: {self.device_capture_width_ratio}")
        """ self.main_dim_device should be a positive non null float """
        try:
            if not isinstance(self.main_dim_device, (int, float)):
                raise TypeError
        except TypeError:
            print('Main dimension of the device must be a float value.')
            raise TypeError('Main dimension of the device must be a float value.')
        try:
            if self.main_dim_device <= 0:
                raise ValueError
        except ValueError:
            print("Main dimension of the device must be a positive non null \
                    value.")
            raise ValueError("Main dimension of the device must be a positive non null value.")

        l_arch_type = ["attenuator", "terminator", "point_absorber"]
        """ self.arch_type should be a specific string or None """
        try:
            if not (self.arch_type in l_arch_type or self.arch_type == None):
                print(self.arch_type)
                raise ValueError
        except ValueError:
            print("Archetype of the device should be: {}, {} or \
                    {}.".format(*l_arch_type))
            raise ValueError("Archetype of the device should be: {}, {} or {}.".format(*l_arch_type))
        """ self.sec_dim_device should be a positive non null float """
        try:
            if not isinstance(self.sec_dim_device, (int, float)):
                raise TypeError
        except TypeError:
            print('Secondary dimension of the device must be a float value.')
            raise TypeError('Secondary dimension of the device must be a float value.')
        try:
            if self.sec_dim_device <= 0:
                raise ValueError
        except ValueError:
            print("Secondary dimension of the device must be a positive non \
                    null value.")
            raise ValueError("Secondary dimension of the device must be a positive non null value.")
        """ Defining the maximum dimension of the device (limiting \
            dimension) """
        if self.main_dim_device > self.sec_dim_device:
            self.max_dim = self.main_dim_device
        else:
            self.max_dim = self.sec_dim_device

    def checkPowerConsistency(self, rPowArray):

        status = True
        errStr = []
        
        """ self.maxNumDevices should be a positive non null integer """
        try:
            maxNumDevices = int(rPowArray / self.rated_pow_device)
        except TypeError:
            print('Maximum number of device must be an integer number.')
            status = False
            errStr.append(
                'Maximum number of device must be an integer number.')
            raise TypeError('Maximum number of device must be an integer number.')
        try:
            if maxNumDevices <= 0:
                raise ValueError("Must be a positive non null integer!")
        except ValueError:
            print(
                "The ratio between array and machine rated powers is below 1.\n\
                Verify the rated power inputs.")
            status = False
            errStr.append(
                "The ratio between array and machine rated powers is \
                        below 1.'\n' Verify the rated power inputs. ")
            raise ValueError("The ratio between array and machine rated powers is below 1.\n Verify the rated power inputs.")

        return maxNumDevices, status, errStr
