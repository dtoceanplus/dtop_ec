# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractstaticmethod
import random
import numpy as np
import json
from shapely.geometry import MultiPoint, MultiPolygon, Polygon, Point
from shapely.affinity import skew, scale

from dtop_energycapt.business.stg3utils.utils.watwaves import len2
from dtop_energycapt.business.optimisation.optstrategy import dictfunc
# import pickle
import cma

from rq import get_current_job
import time

import logging
LOG = logging.getLogger(__name__)

class JOB_MOCK():
    """Class to Mock the Redis-Queue JOB, only used for DEBUG purposes."""
    def __init__(self):
        self.meta = {'tta': 0}
        self.id = 1
    
    def save_meta(self):
        return 0

class IArray(metaclass=ABCMeta):
    """
    An interface class used to represent an Array

    ...

    Attributes
    ----------
    results: dictionay
        keys: device ID
        values: dictionary
            pos (list, [float, float]): device location in Easting and Northing
            q_factor (float): interaction factor of the device in the array
            p_avg (float): average power production of the device
            aep (float): annual energy production of the machine

    Abstract Static Methods
    -------
    get_inputs()
        Static method for the Array Interface
    check_inputs()
        Static method for the Array Interface
    read_digrep()
        Static method for the Array Interface
    write_digrep()
        Static method for the Array Interface
    cal_powerprod()
        Static method for the Array Interface        
    optimize_array()
        Static method for the Array Interface 
    generate_array()
        Static method for the Array Interface 
    send_output()
        Static method for the Array Interface
    """
    def __init__(self):
        self.results = None
        self.nogo_areas_bathymetry = None
        self.optfunname = ""
        self.layout_method = 'verification'
        # TODO: the following attributes are based on the optimisation from DTOcean 1 assuming a staggered grid. May need to be changed
        self.opt_dim = None
        self.par_a = None
        self.par_b = None
        self.opt_dim = 4
        # TODO: the following attributes are based on the optimisation from DTOcean 1 assuming a staggered grid. May need to be changed
        # set search space bounds
        self._min_bound = 0.
        self._max_bound = 10.
        self._min_q_factor = 0.95
        self.min_distance_constraint = False  
        self.job = None  
        self.fix_layout = []


    def get_inputs(self):
        """get_inputs static method for the Array Interface"""

    def check_inputs(self):
        """check_inputs static method for the Array Interface"""

    def read_digrep(self):
        """read_digrep static method for the Array Interface"""

    def write_digrep(self):
        """write_digrep static method for the Array Interface"""

    def verify_farm_layout(self):
        """verify_fam_layout estimates the power production 
        for the given array layout"""
    
    def optimize_farm_layout(self):
        """optimize_farm_layout optimizes the array layout for the
        maximum annual energy production"""

        if 'fix' in self.array_generic_type.lower():
          self.fix_layout = self.generate_fix_element_layout()
          self.__set_problem_parameters()
        else:
          self.estimate_start_point()
        
        self.update_redis_job(15)

        if "cma" in self.optfunname.lower():
            self.cmaes()
        elif "meta_model" in self.optfunname.lower():
            self.meta_model()
        elif "brute_force" in self.optfunname.lower():
            self.brute_force()
        elif "monte_carlo" in self.optfunname.lower():
            self.monte_carlo()
        else:
            raise IOError('Invalid optimization method specified: {method}')

    def multirotor_layout(self):
        """Modifies the user layout for the Tidal case with multirotors"""
        if len(self.layout) < 1:
            return 0
        inside_devices = self.layout
        n_device = len(inside_devices)
        rotor_layout = np.array([el for el in inside_devices]*self.i_converter.number_rotor, dtype='float')

        rotor_interdistance = self.i_converter.rotor_interdistance
        rotor_pos = np.array([el*rotor_interdistance for el in range(self.i_converter.number_rotor)], dtype='float')
        rotor_pos -= np.mean(rotor_pos)
        rotors_pos = np.tile(rotor_pos, (n_device,1)).flatten('F')
        angles = np.deg2rad(np.repeat(np.array(self.i_converter.orientation_angle), n_device*self.i_converter.number_rotor))
        rotor_layout[:, 0] += np.cos(angles) * rotors_pos 
        rotor_layout[:, 1] += np.sin(angles) * rotors_pos 

        self.layout = rotor_layout

    def scale_fix_layout(self, x):
        """Apply scaling and skew to the given layout, given the 
        parameter in the variable x
        """
        print(f'Scaled: {x}')
        coords = MultiPoint(self.fix_layout)
        coords = scale(coords, xfact=x[0], yfact=x[1])
        coords = skew(coords, xs=x[2], ys=x[3])

        tentative_points = np.array([[el.x,el.y] for el in coords])
        lease_mask = self.__checkout(tentative_points)
        inside_devices = tentative_points[lease_mask]
        self.layout = inside_devices
        if self.type.lower() == 'tec':
            if self.i_converter.number_rotor > 1:
                self.multirotor_layout()

    def generate_fix_element_layout(self):
        """Generates layout for the case of optimization with fixed number of 
        devices.
        """
        # 2D rotation matrix to apply main angle rotation
        self._normalisation_point = 1.0
        dist = max(self.i_converter.min_distance)*1.5
        tentative_points = self.__generate_tentative_points(25, 25, dist, dist, 
                                                                0, np.pi/2, 
                                                                self.i_site_conditions.main_angle)

        tentative_points += self.i_site_conditions.central_deploy_pos
        
        initial_lease = self.i_site_conditions.lease_reduced
        original_el = MultiPoint(tentative_points)

        lease_mask = np.array([initial_lease.intersects(el)
                                    for el in original_el],'bool')
        p = tentative_points[lease_mask] 

        if len(p) < self.nb_devices:
            raise ValueError("The given optimization's configuration does not allow to have the specified number of device inside the lease area.")

        for ii in range(1, 100):
            lease = initial_lease.buffer(-10*ii)
            if lease.area < 100:
                break
            lease_mask = np.array([lease.intersects(el) for el in original_el],'bool')
            p = tentative_points[lease_mask] 
            if len(p) < self.nb_devices:
                lease = initial_lease.buffer(-10*(ii-1))
                break
            elif len(p) == self.nb_devices:
                return p
        
        lease_mask = np.array([lease.intersects(el)
                                                for el in original_el],'bool')
        p = tentative_points[lease_mask]
        o = np.array([min(p[:,0]), self.i_site_conditions.central_deploy_pos[-1]])
        distances = np.linalg.norm(p-o, axis=1, ord=2)
        p = p[np.argsort(distances)[:self.nb_devices],:]

        return p

    def check_layout(self):
        """check_layout perform a simple verification of the user provided
        layout to see if the machinres are inside the given Lease Area
        """
        if isinstance(self.layout, (list)):
            self.layout = np.array(self.layout)
        inside = self.__checkout(self.layout, nogo_list=self.nogo_areas_bathymetry)
        self.layout = self.layout[inside]
        if len(self.layout) == 0:
          raise RuntimeError('The provided array layout has no machine inside the lease area. Calculation cannot be continued.')

        LOG.info('check the verification layout, for possible devices outside the lease area')
        if self.type.lower() == 'tec':
            self.i_converter.orientation_angle = (np.array(self.i_converter.orientation_angle)[inside]).tolist()
            if (self.i_converter.number_rotor > 1):
                LOG.info('Tidal Case with Multiple rotors')
                LOG.info('initial layout with multiple rotors')
                LOG.info(self.layout)
                number_rotor = self.i_converter.number_rotor
                n_device = len(self.layout)
                rotor_layout = np.array([el for el in self.layout]*number_rotor, dtype='float')

                rotor_dimeter = self.i_converter.rotor_diameter
                rotor_interdistance = self.i_converter.rotor_interdistance
                rotor_pos = np.array([el*rotor_interdistance for el in range(number_rotor)], dtype='float')
                rotor_pos -= np.mean(rotor_pos)
                rotors_pos = np.tile(rotor_pos, (n_device,1)).flatten('F')
                angles = np.deg2rad(np.array(self.i_converter.orientation_angle*number_rotor))
                rotor_layout[:, 0] += np.cos(angles) * rotors_pos 
                rotor_layout[:, 1] += np.sin(angles) * rotors_pos 

                self.layout = rotor_layout.tolist()
                LOG.info('Modified layout with multiple rotors')
                LOG.info(self.layout)

    def setup_redis_job(self):
        """Setter method for the RQ-Job task. Used in conjunction 
        with update_redis_job, to provide feedback to the user about the job status."""
        job = get_current_job()
        self.job = job
        if job is not None:
            print('Current job: %s' % (job.id,))
        else:
            self.job = JOB_MOCK()
        
        self.update_redis_job(0)
    
    def update_redis_job(self, val):
        """Setter method for the RQ-Job task. Used in conjunction 
        with setup_redis_job, to provide feedback to the user about the job status."""
        if self.job is None:
            return 0
                
        self.job.meta['tta'] = val
        self.job.save_meta()

    def solve(self):
        """solve calls the verification or optimization methos and 
        return the estimated results.
        """
        print(self.layout_method)
        self.setup_redis_job()
        try:
            if self.layout_method == 'verification':
                # first check which devices are inside the lease area
                self.update_redis_job(5)
                self.check_layout()
                self.update_redis_job(15)
                self.verify_farm_layout()
                self.update_redis_job(80)
            else:
                self.update_redis_job(5)
                self.optimize_farm_layout()
                self.update_redis_job(80)
        except Exception as e:
            LOG.error(repr(e))
            self.update_redis_job(100)
            return {"type": self.type, "stage": self.stage,
                    "message": f"Error: {repr(e)}", "status": "failed"}, -1
        try:
            farm, devices = self.send_output()
            if devices == -1:
                raise RuntimeError('Invalid results retrieved from the Worker. ')
            self.update_redis_job(100)
            return farm, devices
        except Exception as e:
            LOG.error(repr(e))
            self.update_redis_job(100)
            return {"type": self.type, "stage": self.stage,
                    "message": f"Error: {repr(e)}", "status": "failed"}, -1
    
    def calc_powprod(self):
        """calc_powprod static method for the Array Interface"""

    def optimize_array(self):
        """optimize_array static method for the Array Interface"""

    def generate_array(self, NR, NC, IR, IC, beta, psi, tentative=False):
        """ Define the array layout to be assess for the machines and 
            levels of complexity. It will generate the 
            coordinates of the grid nodes as a function of the input
            arguments

        Args:
            NR (int): number of grid rows
            NC (int): number of grid columns
            IR (float)[m]: rows interdistance
            IC (float)[m]: columns interdistance
            beta (float)[rad]: angle between the rows and the main direction
            psi (float)[rad]: angle between the columns and the main direction

        Example:
            IArray.generate_array(10, 10, 100, 100, 45/180*pi, -45/180*pi)

        Note:
            The function update the following class attributes:
                layout (numpy.ndarray): x,y coordinates of the array nodes
                min_distance_constraint (bool): flag identifying whether the min
                    distance constraint is violated or not
            The function relies on the following private method
                _check_grid_distance


        """
        if np.any(self.__check_grid_distance(IR, IC, beta, psi)):
            self.min_distance_constraint = True
        else:
            self.min_distance_constraint = False
            
        if not self.min_distance_constraint:
        
            tentative_points = self.__generate_tentative_points(NR, NC, IR, IC, 
                                                                beta, psi, 
                                                                self.i_site_conditions.main_angle)
            tentative_points += self.i_site_conditions.central_deploy_pos

            lease_mask = self.__checkout(tentative_points)

            inside_devices = tentative_points[lease_mask]
            self.layout = inside_devices

            if self.type.lower() == 'tec' and tentative == False:
                if self.i_converter.number_rotor > 1:
                    self.multirotor_layout()  
        else:
            print('Minimum distance constraints violated during the generation of the array')
            self.layout = np.zeros((0,2))

    def send_output(self):
        """send_ouput static method for the Array Interface"""
        if self.results:
            # import pickle
            # pickle.dump(self.results, open('DEBUG_TIDAL.p','wb'))
            farm_p_avg = 0
            farm_aep = 0
            farm_q_factor = 0         
            output_devices = []
            farm_avg_x_cond = []

            for kdev_ in self.results.keys():
                if kdev_ != 'resource_reduction': 
                    dev_ = self.results[kdev_]
                    if isinstance(dev_['p_avg_condition'], np.ndarray):
                        dev_['p_avg_condition'] = dev_['p_avg_condition'].tolist()
                    farm_avg_x_cond.append(dev_['p_avg_condition'])
                    output_devices.append(
                            {
                            "easting": float(dev_['pos'][0]),
                            "northing": float(dev_['pos'][1]),
                            "q_factor": np.nan_to_num(dev_['q_factor']),
                            "aep": dev_['aep']/1000,  # conversion to kWh
                            "captured_power": dev_['p_avg']/1000,  # conversionn to kW
                            "captured_power_per_condition": {
                                        "siteConditionID": list(range(np.size(dev_['p_avg_condition']))),
                                        "capturedPower": (np.array(dev_['p_avg_condition'])/1000).flatten().tolist()
                                    },
                            "velocity_field": {
                                        "siteConditionID": list(range(np.size(dev_['p_avg_condition']))),
                                        "hubVelocity": np.array(dev_.get('hub_velocity')).flatten().tolist()
                                    },
                            }
                        )
                    farm_p_avg += dev_['p_avg']
                    farm_aep += dev_['aep']
                    farm_q_factor += dev_['q_factor']

            nb_devices = len(output_devices)
            farm_avg_x_cond = np.sum(np.array(farm_avg_x_cond),0).tolist()

            siteIDs = list(range(np.size(farm_avg_x_cond)))
            outputs_farm = {
                    "number_devices": nb_devices,
                    "q_factor": farm_q_factor/nb_devices,
                    "aep": farm_aep/1000,  # conversionn to kWh,
                    "captured_power": farm_p_avg/1000,  # conversionn to kW,
                    "captured_power_per_condition": {
                                "siteConditionID": siteIDs,
                                "capturedPower": (np.array(farm_avg_x_cond)/1000).flatten().tolist()
                            },
                    "resource_reduction": self.results['resource_reduction']
                }

            return outputs_farm, output_devices

        outputs = {"type": self.type, "stage": self.stage,
                    "message": "The results are not available yet", "status": "failed"}

        return outputs, -1

    #### Optimization Matters
    def __set_problem_parameters(self):
        """
        __set_problem_parameters: method used to set up the problem order based
            on the __Opt and _Val attributes
            
        Returns:

        """
        # set main angle of the layout
        self.i_site_conditions.main_angle = np.mean(self.i_converter.orientation_angle)

        # set array layout parameters
        self.opt_dim = 4
        self.par_a = np.eye(self.opt_dim)
        self.par_b = np.zeros(self.opt_dim)

        if 'fix' in self.array_generic_type.lower():
            if 'staggered' in self.array_generic_type.lower():
                self.opt_dim = 2
                self.par_a = self.par_a[:, :2]*0.
                self.par_a[0,0] = 1.
                self.par_a[1,0] = 1.
                self.par_a[2,1] = 54.0
                self.par_a[3,1] = -54.0
            elif 'full' in self.array_generic_type.lower():
                self.par_a[-2,-2] = 54.0
                self.par_a[-1,-1] = -54.0
            else:  # defualt value is rectangular
                self.opt_dim = 2
                self.par_a = self.par_a[:, :2]
            return 0

        if 'staggered' in self.array_generic_type.lower():
            self.opt_dim = 2
            self.par_a = self.par_a[:, :2]*0.
            self.par_a[0,0] = 1.
            self.par_a[1,0] = 1.
            self.par_a[2,1] = 1.
            self.par_a[3,1] = -1.
        elif 'full' in self.array_generic_type.lower():
            self.par_a[-1,-1] = -1.
        else:  # defualt value is rectangular
            self.opt_dim = 2
            self.par_a = self.par_a[:, :2]
            self.par_b[-1] = -np.pi/2

    def cmaes(self, tolfun=1e0, tolx=1e-2, maxiter=50, maxfevals=1000, sigma=2):
        """
        cmaes: calls the cma package to optimise the power production
            of the array

        Args (optional):
            tolfun (float)[W]: minimun allowed variation of the fit to decide
                for the solution stagnation
            tolx (float)[-]: minimun allowed variation of the parameters to
                decide for the solution stagnation
            maxiter (int)[-]: max number of population regeneration
            maxfevals (int)[-]: max number of total function evaluation

        Returns:
                x (list): list of normalised parameters that represent the best
                solution found
        """
        LOG.info('CMAES')
        LOG.info('{}\n'.format(self.optfunname))

        task_status = 0
        LOG.debug(f'Task Status: {task_status}')
        if self.job is not None:
            LOG.debug('Get task status from saved one, probably do to an optimization restart')
            task_status = self.job.meta['tta']
            LOG.debug(f'Task Status: {task_status}')
        
        LOG.debug('DEBUG TTA FROM WORKER')
        LOG.debug('*'*80)
        
        # self._normalisation_point *= 1.5  # why this is needed
        # TODO: dimension problem with the function estimate_start_point
        # x0, self._normalisation_point = self.estimate_start_point()
        # if not x0:
        #     warning_str = ('Could not find a suitable starting point '
        #                     'for the optimiser, the centroid of the '
        #                     'parameter space is used instead')
        #     # module_logger.warning(warning_str)
        #     print(warning_str)
        x0 = self.opt_dim * [(self._min_bound+self._max_bound)/2]
        # x0 = self._x0
        # self._min_bound = min(x0)/2
        # self._max_bound = max(x0)*2
        # print(x0)
        es = cma.CMAEvolutionStrategy(
                        x0,
                        sigma,
                        {'bounds': [self._min_bound,self._max_bound],
                            'verb_disp': 0})
        
        es.opts.set('tolfun', tolfun)
        es.opts.set('tolx', tolx)
        es.opts.set('maxiter', maxiter)
        es.opts.set('maxfevals',maxfevals)
        while not es.stop():
            
            solutions = es.ask()
            
            # reduce the significant digits of the search space
            # solutions = [np.around(s, decimals=1) for s in solutions]
            temp = [self.optimCostFunNorm(s) for s in solutions]
            fitness = [(-el[0]) for el in temp]
            es.tell(solutions, fitness)
            
            # if self._debug:
            es.logger.add()
            es.disp(10)
            
            self.update_redis_job(task_status + (80-task_status)*es.countiter/maxiter)
        
        # if self._debug:
        es.result_pretty()
        #            es.logger.plot_all()
        #            pickle.dump(es.archive.data,
        #                        open("optimisation_results_cma-es_new.pkl", "wb"))

        if es.best.f > 0.:
            if es.countiter < maxiter and es.countevals < maxfevals:
                # if the solution is not found and there is still room try again
                LOG.info('Absolute Tollerance Reached without a solution: Restart the Optimization')
                LOG.debug('DEBUG TTA FROM WORKER RESTART')
                LOG.debug('*'*80)
                self.cmaes( tolfun=1e2, tolx=1e-2, 
                            maxiter=(maxiter-es.countiter), 
                            maxfevals=(maxfevals-es.countevals), sigma=5)
            return -1
        else:

            # print('The solution is: ', (es.best.x).tolist())
            v1, v2 = self.optimCostFunNorm((es.best.x).tolist())

            # return (es.best.x).tolist()

    def meta_model(self):
        print('Meta Model')
        # self.costfunc()
        print('{}\n'.format(self.optfunname))

    def monte_carlo(self, maxiter=200):
        """
        monte_carlo: optimise the array layout using the Motecarlo
                    simulation approach

        Args:
            maxiter (int): number of simulation to be run. Since there is no
                rational behind this method, but everything is based on the
                randomness of the solution, the number of maxiter is directly
                affecting the stability of the solution

        """
        
        xx = np.random.random_sample((self.opt_dim, maxiter))
        xx = xx * (self._max_bound-self._min_bound)
        xx = xx + self._min_bound
        # print(xx)
        fit = np.zeros(maxiter)

        for i in range(maxiter):
            # module_logger.info('iteration #: {}'.format(i))
            x = xx[:,i]
            fit[i] = -self.optimCostFunNorm(x)[0]
            self.update_redis_job(15 + (80-15)*(i+1)/maxiter)
            # print('The fit is:', fit[i])
        # find max average energy for arrays with q-factor larger than q_min
        index = fit.argmin()
        # pickle.dump([fit, xx],
        #             open("optimisation_results_brutal_force.pkl", "wb"))
        # print('Optimum parameters are:', xx[:,index])

        v1, v2 = self.optimCostFunNorm(xx[:,index])


    def brute_force(self, N=20):
        """
        brute_force: optimise the array layout using the brute force approach

        Args:
            N (int): discretisation parameter for the parameter space 

        """
        print('Brute force optimisation method')
        print('{}\n'.format(self.optfunname))
        if 'full' in self.array_generic_type:
            # the brute force is only used for debug purposes
            # therefore is used is constrained to rectangular and staggered only
            self.array_generic_type = 'staggered'
            self.__set_problem_parameters()

        x = np.linspace(self._min_bound,self._max_bound, N)
        y = np.linspace(self._min_bound,self._max_bound, N)
        fit = np.zeros((N*N))
        xxx = np.zeros((N*N))
        yyy = np.zeros((N*N))
        ind = -1
        for ii in x:
            for jj in y:
                ind += 1
                print(f'iteration {ind+1} over {N**2}')
                x_Norm = np.repeat((ii,jj), self.opt_dim//2).tolist()
                fit[ind] = -self.optimCostFunNorm(x_Norm)[0]
                xxx[ind] = ii
                yyy[ind] = jj

                self.update_redis_job(15 + (80-15)*(ind)/(N**2))

        # index = np.unravel_index(fit.argmin(), fit.shape)
        # print('All solutions:', fit)
        index = fit.argmin()
        # print('Best solution is at {} and is {}:'.format(index, fit[index]))
        # pickle.dump([fit, x, y],
        #             open("optimisation_results_brutal_force.pkl", "wb"))

        if fit[index] > 0:
            print('No optimum found.')
            # return -1
        else:
            # print(xxx[index], yyy[index], x[index//N],  y[int(index%N)])
            x_Norm = np.repeat((xxx[index],yyy[index]), self.opt_dim//2).tolist()
            v1, v2 = self.optimCostFunNorm(x_Norm)
            # return (x[index//N], y[int(index%N)])
    ####
    @staticmethod
    def __generate_tentative_points(NR, NC, IR, IC, beta, psi, angle):
            # 2D rotation matrix to apply main angle rotation
            Rz = np.array([[np.cos(angle), 
                            -np.sin(angle)],
                           [np.sin(angle), 
                           np.cos(angle)]])
            i, j = np.meshgrid(np.arange(NR), np.arange(NC))
            i = i - NR/2
            j = j - NC/2
            
            x = IR*np.cos(beta)*i+IC*np.cos(psi)*j
            y = IR*np.sin(beta)*i+IC*np.sin(psi)*j
            
            coord_raw = np.zeros((2,NR*NC))
            coord_raw[0,:] = x.ravel()
            coord_raw[1,:] = y.ravel()
            coord = np.dot(Rz,coord_raw).T

            return coord

    def __check_grid_distance(self, x1, x2, a1, a2):
        """
        check_grid_distance: check if the actual layout brake the constraint
            imposed by dMin, using an ellipsoid as base shape.
            
        Args:
            x1 (float)[m]: inter-column distance
            x2 (float)[m]: inter-row distance
            a1 (float)[rad]: column angle wrt the main direction
            a2 (float)[rad]: row angle wrt the main direction

        Returns:
            test (bool): if true the actual array layout is not valid

        """
        # alpha = a1-a2
        # d1 = np.sqrt(x1**2+x2**2+2*x1*x2*np.cos(alpha))
        circle = Point(0, 0).buffer(1)  # type(circle)=polygon
        dMin = self.i_converter.min_distance
        ellipse = scale(circle, dMin[0], dMin[1])  # type(ellipse)=polygon
        tentative_points = self.__generate_tentative_points(2, 2, x1, x2, 
                                                                a1, a2, 0)
        
        grid = MultiPoint(tentative_points[:-1,:])
        mask = np.array([ellipse.intersects(el) for el in grid],'bool')
                        
        return mask
    
    def __checkout(self, tentative_points, nogo_list=None):
        """
        checkout: return a boolean mask representing the feasible nodes in the self.coord attribute.
        The method is implemented base on the shapely.Polygon and shapely.MultiPoint classes.

        Args:
            nogo_list (list)[m]: list of nogo areas vertex given by the user or by other WPs

        Attributes:

        """
        if isinstance(tentative_points, (list)):
            tentative_points = np.array(tentative_points)
        machine_mask = np.zeros(tentative_points.shape[0],dtype=bool)

        if not self.min_distance_constraint:
            original_el = MultiPoint(tentative_points)
            lease_mask = np.array([self.i_site_conditions.lease_reduced.intersects(el)
                                                for el in original_el],'bool')
            
            if np.any(lease_mask):  # identify the points inside the lease
                if not self.nogo_areas_bathymetry is None:  # bathymetry related nogo zones
                    # in this case the NoGo zones can have feasible zones inside,
                    # this is checked using MultiPolygons features of shapely.
                    reduced_el_array = tentative_points[lease_mask]
                    reduced_mask = np.zeros(reduced_el_array.shape[0],'bool')
                    reduced_el = MultiPoint(reduced_el_array)
                    reduced_mask = np.array([
                            self.nogo_areas_bathymetry.intersects(el)
                                                for el in reduced_el],'bool')
                    nogo_bath_mask = lease_mask.copy()
                    nogo_bath_mask[nogo_bath_mask] = np.logical_not(
                                                                reduced_mask)
                else:
                    nogo_bath_mask = True
                                    
                if not nogo_list is None:  # other nogo zones (external)
                    self.nogo = nogo_list
                    nogo_polygons = Polygon(nogo_list)
                    # nogo_polygons = []
                    # for el in nogo_list:  # instantiates a list of Polygons for each nogo zone
                        # nogo_polygons.append(Polygon(el))
                    nogo = MultiPolygon([nogo_polygons])
                    self._nogo_polygons = nogo_polygons
                    reduced_el_array = tentative_points[lease_mask]
                    reduced_mask = np.zeros(reduced_el_array.shape[0],'bool')
                    reduced_el = MultiPoint(reduced_el_array)
                    for ng in nogo:
                        reduced_mask = np.array([ng.intersects(el)
                                for el in reduced_el],'bool') + reduced_mask
                    nogo_mask = lease_mask.copy()
                    nogo_mask[nogo_mask] = np.logical_not(reduced_mask)
                else:
                    nogo_mask = True
                    
                machine_mask = lease_mask*nogo_mask*nogo_bath_mask
            else:
                machine_mask = np.zeros(tentative_points.shape[0],dtype=bool)

        return machine_mask

    def param_conditioning(self, x_scale):
        """
        param_conditioning: the function applies truncation to the scale
            parameters.
        Args:
            x_scale (numpy.ndarray): array of scaled parameters

        Returns:
            array_vals (list): conditioned scaled parameters
        """
        Nb = self.max_num_devices
        # the factor 10 is used because for small skewing angles there is a
        # risk to do not fill the lease area. The extra number of bodies do not
        # affect the calculation.
        NR = 10 * int(np.sqrt(Nb))
        NC = 10 * int(np.sqrt(Nb)) + 1
        IC = int(x_scale[0])
        IR = int(x_scale[1])
        beta = int(x_scale[2] * 100) / 100.0
        psi = int(x_scale[3] * 100) / 100.0

        return NR, NC, IC, IR, beta, psi

    def scale_param(self, x_norm):
        """
        scale_param: the function denormalise the parameters.
        Args:
            x_norm (numpy.ndarray): array of normalised parameters

        Returns:
            x_scale (numpy.ndarray): array of scaled parameters
        """
        print(f'Normalized: {x_norm}')
        xmap = np.dot(self.par_a, x_norm)
        xmap[0] *= self._normalisation_point
        xmap[1] *= self._normalisation_point
        xmap[0] += self._normalisation_point
        xmap[1] += self._normalisation_point
        xmap[2] *= 0.1 * np.pi / 2
        xmap[3] *= 0.1 * np.pi / 2
        x_scale = xmap + self.par_b

        return x_scale

    def optimCostFunNorm(self, x):
        """
        optimCostFunNorm: the function scale the parameters and calls the
                            optimCostFun method.
        Args:
            x (list): list of normalised parameters

        Returns:
            fval[0] (float): annual energy production for the given array
            fval[1] (float): q factor for the given array
        """
        x_b = np.array(x)

        # un-normalize optimization variable
        xNorm = self.scale_param(x_b)

        # call dernorm cost fun
        fval_0, fval_1 = self.optimCostFun(xNorm)
        return fval_0, fval_1

    def optimCostFun(self, x):
        """
        optimCostFun: the method calculate the AEP and q-factor for the given
            configuration

        Args:
            x (list): list of 4 parameters used to build the array layout

        Return:
            AEP (float): annual energy production for the given array
            q (float): q factor for the given array
        """
        if 'fix' in self.array_generic_type.lower():
            self.scale_fix_layout(x)
        else:
            NR, NC, IR, IC, beta, psi = self.param_conditioning(x)

            if beta-psi < 0.05:
                print(f"The angle between rows and columns is too close to zero.")

            # if self._Opt == 3:
            #     scaleX = IC / self._normalisation_point
            #     scaleY = IR / self._normalisation_point
            #     self.layout = self._Val * np.array([scaleX, scaleY])
            #     # self._checkMinDist()
            # else:
            self.generate_array(NR, NC, IR, IC, beta, psi)

        # tentative_points = self.__generate_tentative_points(NR, NC, IR, IC, 
        #                                                         beta, psi, 
        #                                                         self.i_site_conditions.main_angle)
        # inside = self.__checkout(tentative_points, nogo_list=self.nogo_areas_bathymetry)
        # check conditions prior to solve the array interaction
        n_pto = 1
        if self.type.lower() == 'tec':
            n_pto = self.i_converter.number_rotor

        if self.layout.shape[0] > 0:  # and not self.distance_constraint_cond:
            if (self.layout.shape[0] > n_pto*self.max_num_devices*1.5) or (self.layout.shape[0] < n_pto*self.max_num_devices*0.5):

                print(f"Not valid: N_dev {self.layout.shape[0]} outiside the range [{0.5*self.max_num_devices*n_pto}-{1.5*self.max_num_devices*n_pto}]")

                # return the squared error from actual value and bound
                # due to the computational constraints this penality term is
                # magnified by a factor 2
                maxdev_error = -(
                    (self.layout.shape[0] / (n_pto*self.max_num_devices) - 1) *
                    200)**2

                return maxdev_error, -1

            self.verify_farm_layout()
            temp, dump = self.send_output()
            qarray = temp.get('q_factor')
            aep = temp.get('aep')
            # print("Number of devices: {} AEP: {} q-factor: "
                    # "{}".format(self.layout.shape[0], aep, qarray,))
            if qarray >= self.optimisation_threshold:
                # if self._debug:
                #     module_logger.info("Valid config: actual q-factor {} --> "
                #                         "min q-factor {}".format(
                #                                         res.q_array,
                #                                         self._min_q_factor))

                return aep, qarray

            else:
                # TODO: verify that this condition is actually used.
                # if self._debug: module_logger.info("Not valid: q < q_min")

                # return the squared error from actual value and bound
                qfactor_error = -(
                    (qarray / self._min_q_factor - 1) * 100.)**2
                return qfactor_error, qarray
        else:
            return -1000, 0.1
        # else:

        #     # if self._debug:
        #     #     module_logger.warning('WARNING! For the given configuration '
        #     #                           'no device is inside the active area!. '
        #     #                           'No calculation is performed!')

        #     mindist_error = -(
        #         (self._actual_mindist / self._min_dist - 1) * 100.)**2

        #     return mindist_error, -1

    # def __remap(val, sc, IR, IC, beta, psi):
    # TODO: the generation of the array is in full mode. Check the validity for 
    # other configurations
    def __remap(self, sc, IR, IC, beta, psi):
        self._normalisation_point = (IR+IC)/2/6

        return 1
        

    def estimate_start_point(self):
        LOG.info("Estimating the optimisation starting point")
            
        # module_logger.info("Estimating the optimisation starting point")
        max_eval = 100
        array_type = ''.join(self.array_generic_type)
        if 'full' in array_type:
          self.array_generic_type = 'staggered'
        
        self.__set_problem_parameters()    
        
         
                    
        # un-normalize optimization variable
        x_b = np.array([5,5],'f')
        xNorm = self.scale_param(x_b)
    
        NR, NC, IR, IC, beta, psi = self.param_conditioning(xNorm)
        IR = self.i_converter.min_distance[0]
        IC = self.i_converter.min_distance[1]
        
        for i in range(max_eval):
            
            self.generate_array(NR, NC, IR, IC, beta, psi, tentative=True)
    
            inside = self.__checkout(self.layout, nogo_list=self.nogo_areas_bathymetry)
    
            # check conditions prior to solve the array interaction
            if inside.any() and not self.min_distance_constraint:
                # print(f'Iteration: {i} / Number Devices: {self.layout[inside].shape[0]}')
                if not (0.5*self.max_num_devices <= self.layout[inside].shape[0] <= 2*self.max_num_devices):
                    IC *= 1.05
                    IR *= 1.05
                else:
                    LOG.info("Found a new starting point for the optimization")
                    self.array_generic_type = array_type
                    self.__set_problem_parameters()
                    return self.__remap(self._normalisation_point,
                                                IC,
                                                IR, 
                                                beta,
                                                psi)
                    
            else:
                # print(f'Iteration: {i} / Number Devices: 0')
                IC *= 1.05
                IR *= 1.05
        
        raise RuntimeError('Cannot estimate optimization starting point. Please check the number of devices in relation to the given lease area and the minimum array interdistance contraints.')
                    
            

    # def __inner_region(self, points):
    #     """
    #     inner_region: used to verify whether the grid nodes adiacent to the first node passed
    #     are inside the ellipse generated using the dMin attribute

    #     Args:
    #         points (numpy.ndarray): array of nodes. The first element is used as reference point

    #     Returns:
    #         out (list): list of boolean used to identify which point does not fulfil the dMin attributes
    #     """
    #     a = self.i_converter.minDist[0]
    #     b = self.i_converter.minDist[1]
    #     nodedistance = np.zeros(4)
    #     for inp, point in enumerate(points[1:]):
    #         nodedistance[inp] = np.sqrt(((point[0]-points[0][0])**2+(point[1]-points[0][1])**2))
    #     nodedistance[-1] = np.sqrt(((points[1][0]-points[-1][0])**2+(points[1][1]-points[-1][1])**2))
            
    #     self._actual_mindist = nodedistance.min()
    #     return (nodedistance > np.sqrt((a*b)))
    
    # def show_layout(self, tentative_points=np.nan*np.zeros((1,2)), inside=None, ax=None):
    #     """
    #     show: visualise the feasible and unfeasible grid nodes along with the Lease area and Nogo areas

    #     Args:
    #         inside (numpy.ndarrray): boolean mask to differentiate feasible and unfeasible points
    #         ax (matplotlib.pyplot.axes): plot on the specified ax if given

    #     Returns:
    #         ax (matplotlib.pyplot.axes): anchor to the plot in which the data has been stacked.
    #     """
    #     if inside is None:
    #         inside = np.ones(len2(self.layout),dtype=bool)
        
    #     x, y = self.i_site_conditions.lease_reduced.exterior.coords.xy
    #     Leasep_unbuffered = np.zeros((len(x)+1,2),dtype=float)
    #     Leasep_unbuffered[:-1]= np.array([x,y]).T
    #     Leasep_unbuffered[-1]= Leasep_unbuffered[0]
        
    #     Leasep = np.zeros((len2(self.i_site_conditions.loc_position)+1,2),dtype=float)
    #     Leasep[:-1]= self.i_site_conditions.loc_position
    #     Leasep[-1]= Leasep[0]
   
    #     if ax is None:        
    #         ax = plt.figure().add_subplot(111)

    #     # if not self.i_site_conditions.nogoAreas is None:
    #     #     plotCompositePolygon(self.i_site_conditions.nogoAreas, ax)
    #     # if not self.i_site_conditions._nogo_P is None:
    #     #     for NG in self.i_site_conditions._nogo_P:
    #     #         plotCompositePolygon(NG, ax)

    #     patterns = ['\\','/','-', '+', 'x', 'o', 'O', '.', '*']  # more patterns
    #     scale = np.mean(np.max(self.i_site_conditions.loc_position,0)-np.min(self.i_site_conditions.loc_position,0))
    #     ax.add_patch(
    #                     patches.Arrow(
    #                         self.i_site_conditions.central_deploy_pos[0]-scale*np.cos(self.i_site_conditions.main_angle-2*np.pi),            # x
    #                         self.i_site_conditions.central_deploy_pos[1]-scale*np.sin(self.i_site_conditions.main_angle-2*np.pi),            # y
    #                         0.5*scale*np.cos(self.i_site_conditions.main_angle),            # dx
    #                         0.5*scale*np.sin(self.i_site_conditions.main_angle),            # dy
    #                         width=0.3*scale,       # optional - defaults to 1.0
    #                         alpha=0.5,
    #                         #facecolor="#00ffff",#'none' no background
    #                         #linewidth=3,
    #                         #linestyle='dashdot',
    #                         #hatch=patterns[-2],
    #                         fill='#0000dd',
    #                         edgecolor='k'
    #                     )
    #                 )

    #     Nbody = np.shape(self.layout[inside,0])[0]
    
    #     for mac, Str in enumerate(range(Nbody)):
    #         ax.annotate(Str,(self.layout[inside,0][mac],
    #                          self.layout[inside,1][mac]))
        
    #     ax.plot(Leasep[:,0],Leasep[:,1], color='#6699cc', alpha=0.7,
    #                     linewidth=2, solid_capstyle='round', zorder=10000)
        
    #     ax.plot(tentative_points[:,0],tentative_points[:,1],'k+',
    #                 self.layout[inside,0],self.layout[inside,1],'bo',
    #                 Leasep_unbuffered[:,0],Leasep_unbuffered[:,1],'r',linewidth=2)
        

    #     ax.axis('equal')
    #     # plt.show()
    #     return ax
# if __name__ == "__main__":
#     from dtop_energycapt import business
#     ARRAY = business.ArrayFactory.get_array('TEC', "1")
#     inputs_tec = {
#         'nb_devices': 10,
#         'opt_method': 'monte_carlo',
#         'optimisation_threshold': 0.8,
#         'array_generic_type': 'rectangular',
#         'rated_pow_device': 40000.,
#         'loc_resource': 25.0,
#         'loc_position': [[0, 0], [500, 500], [0, 500]],
#         'cp': 0.3,
#         'sec_dim_device': 10.,
#         'main_dim_device': 10.,
#     }

#     ID = 9
#     pj_name = "_tec_test_project"
#     ARRAY.get_inputs(pj_name, ID, **inputs_tec)
#     assert ARRAY.nb_devices == 10
#     ARRAY.nogo_areas_bathymetry = Polygon([[0,0],[1,0],[1,1],[0,1]])
#     ARRAY.optimCostFun([100,100,90,0]) 
#     ARRAY.scale_param(np.array([5,5,0,0]))               