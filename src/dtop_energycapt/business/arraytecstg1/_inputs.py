# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energycapt.business.arraytecstg1.site_conditions import SiteConditions
from dtop_energycapt.business.arraytecstg1.converter import Converter
from dtop_energycapt.business.optimisation.optstrategy import dictfunc
import types

import numpy as np

def get_inputs(self, name, ecID, **kwargs):
    """Gets the inputs from the user for the TEC Stage 1 array type.
    Args:
        name (str): Name of the EC project
        ecID (int): Unique identifier for the EC project.
    Kwargs:
        nb_devices (int): Number of devices in the array.


        array_layout (List[[float,float]]): Suggested array layout by the 
                                            user (mainly for the optimisation 
                                            functionality of the module). 
                                            Given as a list of [Easting [m], 
                                            Northing [m]] coordinates.
        opt_method (string):
        optimisation_threshold (float):
        rated_pow_array (float):
        array_generic_type (string):

    """
    self.name = name
    self.ecID = ecID

    if 'nb_devices' in kwargs:
        self.nb_devices = kwargs.get("nb_devices")
    else:
        print('Number of devices input missing!')
        raise 

    self.layout_method = 'verification'  # fallback option
    if 'layout_type' in kwargs:
        self.layout_method = kwargs['layout_type']        

    if self.layout_method == 'optimization':
        # try to fetch the method from the optimization module, default brute force
        optfunc = dictfunc.get(kwargs.get('opt_method', 'brute_force'), dictfunc['brute_force'])
        self.optimisation_threshold = kwargs.get('optimisation_threshold', 0.85)
        self.array_generic_type = kwargs.get('array_generic_type', 'staggered')
        
        self.optfunname = '{}_{}'.format(self.__class__.__name__, optfunc.__name__)
    else:  # if the system is verified, the layout must be given
        self.layout = kwargs.get("array_layout", [])
        if self.layout == []:
            raise ValueError('Invalid inputs: either specify a valid array layout or a valid optimization method')
    
    
    self.check_inputs()

    self.i_site_conditions = SiteConditions(**kwargs)
    self.i_converter = Converter(**kwargs)
    self.i_converter.orientation_angle = kwargs.get('orientation_angle', [0])

    self._normalisation_point = max(self.i_converter.main_dim_device, 
                                    self.i_converter.sec_dim_device)

    self.max_num_devices = self.nb_devices
    self._min_q_factor = self.optimisation_threshold

    self.i_converter.rotor_interdistance = self.i_converter.main_dim_device*1.05
    # if (self.i_converter.number_rotor > 1 and self.layout):
    #     n_device = len(self.layout)
    #     rotor_layout = np.array([el for el in self.layout]*self.i_converter.number_rotor, dtype='float')

    #     rotor_pos = np.array([el*self.i_converter.main_dim_device for el in range(self.i_converter.number_rotor)], dtype='float')
    #     rotor_pos -= np.mean(rotor_pos)
    #     rotors_pos = np.tile(rotor_pos, (n_device,1)).flatten('F')
    #     angles = np.deg2rad(np.array(self.i_converter.orientation_angle*self.i_converter.number_rotor))
    #     rotor_layout[:, 0] += np.cos(angles) * rotors_pos 
    #     rotor_layout[:, 1] += np.sin(angles) * rotors_pos 

    #     self.layout = rotor_layout.tolist()

    # if self.layout == None:
    #     self.generate_array()
    # else:
    #     self.optimize_array()
    # print("The array layout is:\n", self.df_array[['easting', 'northing']])
    # print("The array width is:", self.array_layout_width, "in m")
    # print("The array depth is:", self.array_layout_depth, "in m")
    # self.calc_powprod()


def check_inputs(self):
    """Checks the inputs for the TEC Stage 1 array type.

    Returns:
        True if successful, False otherwise.

    """
    """ self.name should be a string """
    try:
        if not isinstance(self.name, str):
            raise TypeError("Must be a string!")
    except TypeError:
        print('The name must be a string.')
        raise
    """ self.ecID should be an integer """
    try:
        if not isinstance(self.ecID, int):
            raise TypeError("The ID must be of type integer.")
    except (TypeError):
        print('The ID must be of type integer.')
        raise

    """ self.nb_devices should be an integer """
    try:
        if not isinstance(self.nb_devices, int):
            raise TypeError("Number of devices must be an integer!")
    except TypeError:
        print('Number of devices must be an integer number.')
        raise

    """ self.nb_devices should be a positive non null integer """
    try:
        self.nb_devices = int(self.nb_devices)
        if self.nb_devices <= 0:
            raise ValueError("The number of devices must be a positive non null integer!")
    except ValueError as e:
        print("The number of device must be a positive non null integer.")
        raise e
    """ self.optimisation_threshold should be a float """
    if not self.optimisation_threshold is None:
        try:
            if not isinstance(self.optimisation_threshold, float):
                raise TypeError(
                    "The optimisation threshold must be of type float.")
        except (TypeError):
            print('The optimisation threshold must be of type float.')
            raise

    if not self.array_generic_type is None:
        try:
            if not isinstance(self.array_generic_type, str):
                raise TypeError(f'The input format of the array generic type is'
                  f' incorrect.\n  Accepted format are: string.')
        except TypeError:
            status = False
            print(f'The input format of the array generic type is'
                  f' incorrect.\n  Accepted format are: string.')
            raise
