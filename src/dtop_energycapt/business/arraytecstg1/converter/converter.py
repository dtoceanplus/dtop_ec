# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np


class Converter(object):
    """
    Converter class: The class contains all the information relative to the 
    machine deployed in the array.
    
    Args:
        rated_pow_device (float): Rated power of a single device [kW].
        char_length (float): Characteristic length of a single 
                             device [m].
        main_dim_device (float): Main dimension of the device, i.e. 
                                    dimension of the device perpendicular to 
                                    the incoming waves.
        sec_dim_device (float): Secondary dimension of the device, i.e 
                                dimension of the device parallel to the 
                                incoming waves.

    """
    def __init__(self, **kwargs):

        self.rated_pow_device = None
        self.cp = 0.3  # default value
        self.main_dim_device = None
        self.sec_dim_device = None
        self.max_dim = None
        self.number_rotor = kwargs.get("number_rotor", 1)
        self.rotor_interdistance = 0.0
        self.rotor_diameter = None
        self.min_distance = None
        self.orientation_angle = [0.0]

        if 'rated_pow_device' in kwargs:
            self.rated_pow_device = kwargs.get("rated_pow_device")
        else:
            print('Rated power of the device input missing!')
            raise ValueError
        if 'cp' in kwargs:
            self.cp = kwargs.get("cp")
        if 'main_dim_device' in kwargs:
            self.main_dim_device = kwargs.get("main_dim_device")
            self.rotor_diameter = self.main_dim_device  # adding the rotor diameter to uniform with the two other complexities
        else:
            print('Main dimension of the device input missing!')
            raise ValueError
        if 'sec_dim_device' in kwargs:
            self.sec_dim_device = kwargs.get("sec_dim_device")
        else:
            print('Secondary dimension of the device input missing!')
            raise ValueError

        self.check_inputs()

        self.min_distance = (2 * self.main_dim_device,
                             max(5 * self.main_dim_device,
                                 self.sec_dim_device))

    def printInput(self, indent=4):
        """print the Converter class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """

        print('Print all arguments of this instance of Converter')

        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)

        return

    def check_inputs(self):

        status = True
        errStr = []
        """ self.rated_pow_device should be a float """
        try:
            if not isinstance(self.rated_pow_device, (int, float)):
                raise TypeError("Rated power must be an integer or a float!")
        except TypeError:
            print('Rated power of the device must be a float value.')
            raise
        """ self.rated_pow_device should be a positive non null float """
        try:
            self.rated_pow_device = float(self.rated_pow_device)
            if self.rated_pow_device <= 0:
                raise ValueError
        except ValueError:
            print("The rated power of the device must be a positive non null \
                    value.")
            raise
        """ self.cp should be a positive non null float """
        try:
            if not isinstance(self.cp, (int, float)):
                raise TypeError
            if self.cp <= 0 or self.cp > 1:
                raise IOError
        except IOError:
            raise IOError(
                'The power coefficient (cp) of the machine should be inside the  range [0,1['
            )
        except TypeError:
            print("Power coefficient of the device must be a positive non \
                    null value.")
            raise

        """ self.main_dim_device should be a float """
        try:
            if not isinstance(self.main_dim_device, (int, float)):
                raise TypeError("Main dimension of the device must be an integer or a float value.!")
        except TypeError:
            print('Main dimension of the device must be a float value.')
            raise

        """ self.main_dim_device should be a positive non null float """
        try:
            if self.main_dim_device <= 0:
                raise ValueError
        except ValueError:
            print("Main dimension of the device must be a positive non null \
                    value.")
            raise


        """ self.sec_dim_device should be a positive non null float """
        try:
            if not isinstance(self.sec_dim_device, (int, float)):
                raise TypeError("Secondary dimension of the device must be an integer or a float value.!")
        except TypeError:
            print('Secondary dimension of the device must be a float value.')
            raise

        try:
            if self.sec_dim_device <= 0:
                raise ValueError
        except ValueError:
            print("Secondary dimension of the device must be a positive non \
                    null value.")
            raise


        """ Defining the maximum dimension of the device (limiting \
            dimension) """
        if self.main_dim_device > self.sec_dim_device:
            self.max_dim = self.main_dim_device
        else:
            self.max_dim = self.sec_dim_device

    # def checkPowerConsistency(self, rPowArray):

    #     status = True
    #     errStr = []
    #     maxNumDevices = int(rPowArray / self.ratedPower)
    #     """ self.maxNumDevices should be a positive non null integer """
    #     try:
    #         maxNumDevices = int(maxNumDevices)
    #         if maxNumDevices <= 0:
    #             raise ValueError("Must be a positive non null integer!")
    #     except TypeError:
    #         print('Maximum number of device must be an integer number.')
    #         status = False
    #         errStr.append(
    #             'Maximum number of device must be an integer number.')
    #         raise
    #     except ValueError:
    #         print(
    #             "The ratio between array and machine rated powers is below 1.\n\
    #             Verify the rated power inputs.")
    #         status = False
    #         errStr.append(
    #             "The ratio between array and machine rated powers is \
    #                     below 1.'\n' Verify the rated power inputs. ")
    #         raise

    #     return maxNumDevices, status, errStr
