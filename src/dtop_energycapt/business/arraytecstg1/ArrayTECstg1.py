# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

from dtop_energycapt.business.iarray.IArray import IArray
import numpy as np
from scipy.spatial import distance

import logging
LOG = logging.getLogger(__name__)

class ArrayTECStg1(IArray):
    """Instantiation of the IArray class corresponding to the TEC stage 1 type.

    Attributes:
        type (str): Type of the array. In this case "TEC".
        stage (str): Stage of the array. In this case "Stage 1".
        nb_devices (int): Number of devices in the array.
        layout (List[[float,float]]): Array layout given as a list of 
                                            [Easting [m], Northing [m]] 
                                            coordinates. 
        optimisation_threshold (float):
        rated_pow_array (float):
        array_generic_type (string):
        i_converter (Converter):
        i_site_conditions (SiteConditions):
        pow_prod_device (float): Power production of the device [kW]. 
        pow_prod_array (float): Power production of the array [kW]. 
        aep_device (float): Annual energy production of the device [MWh/year].
        aep_array (float): Annual energy production of the array [MWh/year]
        resource_red (float): Resource reduction of the array [%].
        cf_device (float): Capacity factor of the device [%].
        cf_array (float): Capacity factor of the array [%].        

    """
    def __init__(self):
        super().__init__()
        self.type = "TEC"
        self.stage = "Stage 1"

        self.name = None
        self.ecID = None
        self.nb_devices = None
        self.optimisation_threshold = None
        self.layout = None
        self.rated_pow_array = None
        self.array_generic_type = None
        self.i_converter = None
        self.i_site_conditions = None

        # TODO: to be updated for staggered grid
        self.__q_factor_tab = [[2, 0.3], [3, 0.5], [5, 0.6], [7, 0.85],
                               [10, 1]]
        
        LOG.info(f'Created the Farm - {self.type.capitalize()} object at {self.stage}')

        # TODO: check the power output units

    from ._digrep import read_digrep, write_digrep
    from ._inputs import get_inputs, check_inputs

    # def calc_powprod(self):
    def verify_farm_layout(self):
        """Calculates power metrics for a single device and for the array.

        Returns:
            True if successful, False otherwise.

        """
        if not self.layout is None:
            q_factor = self.__q_factor()
            power_prod = (0.5 * 1025 * self.i_site_conditions.loc_resource**3 *
                          self.i_converter.cp * 0.25* np.pi *
                          self.i_converter.main_dim_device**2 * q_factor)
            aep = 8766. * power_prod

            # TODO: check the resourse reduction calculation
            resource_red = self.i_converter.cp * 100

            results = {}

            for idev, dev_ in enumerate(self.layout):
                results[str(idev)] = {
                    'pos': [dev_[0], dev_[1]],
                    'q_factor': q_factor,
                    'p_avg': power_prod,
                    'p_avg_condition': [power_prod],
                    'aep': aep,
                    'hub_velocity': [self.i_site_conditions.loc_resource*q_factor**(1.0/3.0)]
                }

            results['resource_reduction'] = resource_red

            self.results = results
        else:
            raise Warning(
                'The layout attribute has not been set, therefore the power performance assessment cannot be carried out'
            )

    def __q_factor(self):
        # TODO: update the tabled values using TEC2
        if len(self.layout) <= 1:
            return 1
        d = self.i_converter.main_dim_device
        dw = self.__down_wind_distance()
        absolute_difference_function = lambda list_value : abs(list_value[0] - dw/d)

        closest_dist = min(self.__q_factor_tab, key=absolute_difference_function)
        return float(closest_dist[1])

    def __down_wind_distance(self):
        dw = distance.cdist(self.layout , self.layout , 'euclidean')
        dw[dw==0] = np.nan
        dw = np.nanmean(dw)
        return dw
