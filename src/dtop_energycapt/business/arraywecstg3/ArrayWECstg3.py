# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
import numpy as np
from dtop_energycapt.business.iarray.IArray import IArray


class ArrayWECStg3(IArray):
    _site_keys = ('sea_state_directions', 'sea_state_wave_height',
                  'sea_state_wave_period', 'water_depth', 'scat_diagram',
                  'bathymetry', 'spectral_type',
                  'lease', 'nogo_areas')

    _converter_keys = ('yaw_angle', 'floating', 'install_depth',
                       'minimum_interdistance', 'rated_power_device',
                       'data_folder','ver')

    _converter_keys_v2 = ('rated_power_device', 'installation_depth','minimum_interdistance',
                          'added_mass', 'force_transfer_matrix_real',
                          'force_transfer_matrix_imag', 'frequency',
                          'additional_damping', 'mass_matrix', 'radiation_damping',
                          'hydrostatic_stiffness', 'diffraction_transfer_matrix_real',
                          'diffraction_transfer_matrix_imag',
                          'hs_pm', 'tp_pm', 'dir_pm', 'power_matrix',
                          'fitting_damping', 'scatter_diagram', 'order',
                          'heading_angle_span', 'additional_stiffness',
                          'directions', 'truncation_order','excitation_force_real',
                          'radiation_amplitude_coefficients_real',
                          'radiation_amplitude_coefficients_imag',
                          'excitation_force_imag', 'fitting_stiffness',
                          'water_depth', 'mooring_stiffness',
                          'modes', 'pto_damping', 'inscribing_cylinder_radius',
                          'orientation_angle')

    def __init__(self):
        super().__init__()
        self.type = "WEC"
        self.stage = "Stage 3"
        self.name = None
        self.ecID = None
        self.nb_devices = None
        self.layout = None
        # self.optSpecs = None
        self.array_generic_type = None
        self.optimisation_threshold = None
        # self.userOutputTable = None
        self.i_site_conditions = None
        self.i_converter = None
        self.max_num_devices = None
        self.orientation_angle = 0.0
        # Set search space bounds for the optimisation
        self._min_bound = 0.
        self._max_bound = 10.
        self.nogo_areas_bathymetry = None

    from ._inputs import get_inputs, check_inputs, set_opt_parameters
    from ._digrep import read_digrep, write_digrep

    # def calc_powprod(self):
    def verify_farm_layout(self):
        """Calculates power metrics for a single device and for the array.

        Returns:
            True if successful, False otherwise.

        """
        if not self.layout is None:
            farm_design = self.PerfMetrics.energy(self.layout)

            results = {}

            for idev, dev_ in enumerate(self.layout):
                results[str(idev)] = {
                    'pos': [dev_[0], dev_[1]],
                    'q_factor': farm_design[1][idev],
                    'p_avg': farm_design[2][idev],
                    'p_avg_condition': farm_design[3][idev, :, :, :],
                    'aep': farm_design[0][idev]
                }

            results['resource_reduction'] = 0.1

            self.results = results
        else:
            raise Warning(
                'The layout attribute has not been set, therefore the power performance assessment cannot be carried out'
            )

    # def param_conditioning(self, x_scale):
    #     """
    #     param_conditioning: the function applies truncation to the scale
    #         parameters.
    #     Args:
    #         x_scale (numpy.ndarray): array of scaled parameters

    #     Returns:
    #         array_vals (list): conditioned scaled parameters
    #     """
    #     Nb = self.maxNumbDevices
    #     # the factor 10 is used because for small skewing angles there is a
    #     # risk to do not fill the lease area. The extra number of bodies do not
    #     # affect the calculation.
    #     NR = 10 * int(np.sqrt(Nb))
    #     NC = 10 * int(np.sqrt(Nb)) + 1
    #     IC = int(x_scale[0])
    #     IR = int(x_scale[1])
    #     beta = int(x_scale[2] * 100) / 100.0
    #     psi = int(x_scale[3] * 100) / 100.0

    #     return NR, NC, IC, IR, beta, psi

    # def scale_param(self, x_norm):
    #     """
    #     scale_param: the function denormalise the parameters.
    #     Args:
    #         x_norm (numpy.ndarray): array of normalised parameters

    #     Returns:
    #         x_scale (numpy.ndarray): array of scaled parameters
    #     """
    #     xmap = np.dot(self.par_a, x_norm)
    #     xmap[0] *= self._normalisation_point
    #     xmap[1] *= self._normalisation_point
    #     xmap[0] += self._normalisation_point
    #     xmap[1] += self._normalisation_point
    #     xmap[2] *= 0.1 * np.pi / 2
    #     xmap[3] *= 0.1 * np.pi / 2
    #     x_scale = xmap + self.par_b

    #     return x_scale

