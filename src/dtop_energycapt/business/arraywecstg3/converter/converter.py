# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np
from .utils import read_bem_solution as reader
from dtop_energycapt.business.stg3utils.utils.watwaves import WNumber

from pickle import Pickler, Unpickler


class Converter(object):
    """
    Converter class: The class contains all the information relative to the 
    machine deployed in the array.
    
    Args:
        lCS (numpy.ndarray)[m]: position vector of the position coordinate of 
                                the body CS with respect to the mesh CS.
        yawAngle (float)[rad]: Yaw angle span, with respect to the main 
                               direction of the array. The total yawing range is
                               two-fold the span. -Yaw/+Yaw
        floatFlag (bool)[-]: defines whether the machine is floating (True) or 
                              not (False). Default value: True
        installDepth (list)[m]: defines the min and maximum water depth at which
                                the device can be installed
        min_distance (tuple)[m]: defines the minimum allowed distance between devices
                            in the array configuration the first element is the 
                            distance in the x axis the second element is the 
                            distance in the y axis
        ratedPower (float)[W]: Rated power of the single isolated device.
        dataFolder (string): path name of the hydrodynamic results
                                 generate by the wave external module

        Attributes:
        Same as the Args list plus:
            maxNumDevices (float) [-]: max number of devices allowed in the 
                                       array estimated from the array rated 
                                       power and the machine rated power
        Note:
        The attribute(s) are modified as follow:
            .installDepth:  - set to [-inf,0] if None
                            - switch the numbers if the first element is bigger 
                              than the second
    """
    _converter_keys_v2 = ('dofs', 'added_mass', 'force_transfer_matrix',
                          'additional_damping', 'mass_matrix', 'radiation_damping',
                          'hydrosatic_stiffness', 'shared_dof', 'mooring_dof',
                          'diffraction_transfer_matrix', 'ndof', 'sea_state_wave_height',
                          'fitting_damping', 'sea_state_directions',  'scat_diagram',
                          'heading_angle_span', 'max_truncation_order',
                          'additional_stiffness', 'truncation_order',
                          'excitation_force_real', 'radiation_amplitude_coefficients',
                          'excitation_force_imag', 'fitting_stiffness',
                          'pto_dof', 'spectral_type', 'water_depth', 'mooring_stiffness',
                          'modes', 'pto_damping', 'sea_state_wave_period',
                          'inscribing_cylinder_radius')
                          
    def __init__(self, **kwargs):

        self.lCS = None
        self.yawAngle = None
        self.floatFlag = None
        self.installDepth = None
        self.min_distance = None
        self.dataFolder = None
        self.ratedPower = None
        self.ver = 1
        self.db_model = kwargs

        self.convention = 'N'

        (self.M, self.Madd, self.Crad, self.Khyd, self.Fex, self.period,
         self.directions, self.D, self.G, self.AR, self.water_depth,
         self.radius, self.modes, self.order, self.truncorder) = (None, ) * 15

        (self.Cfit, self.Kfit, self.CPTO, self.Kmooring, self.w_te, self.w_hm0,
         self.w_dirs, self.scatter_diagram, self.power_matrix) = (None, ) * 9

        self.depth = None
        self.w_tp = None
        self.power_matrix = None
        self.wnumber = None
        self.orientation_angle = [0]

        if 'ver' in kwargs:
            self.ver = kwargs.get("ver")
        if 'position_vector' in kwargs:
            self.lCS = kwargs.get("position_vector")
        if 'yaw_angle' in kwargs:
            self.yawAngle = kwargs.get("yaw_angle")
        if 'orientation_angle' in kwargs:
            oa = kwargs.get("orientation_angle")
            if not oa is None:
                self.orientation_angle = [np.deg2rad(np.mean(oa))]
        if 'floating' in kwargs:
            self.floatFlag = kwargs.get("floating")
        if 'install_depth' in kwargs:
            self.installDepth = kwargs.get("install_depth")
        if 'minimum_interdistance' in kwargs:
            self.min_distance = kwargs.get("minimum_interdistance")
        if 'data_folder' in kwargs:
            self.dataFolder = kwargs.get('data_folder')
        if 'rated_power_device' in kwargs:
            self.ratedPower = kwargs.get('rated_power_device')

        self.check_inputs()

    def printInput(self, indent=4):
        """print the Converter class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """

        print('Print all arguments of this instance of Converter')

        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)

        return

    def check_inputs(self):

        status = True
        errStr = []
        """ self.installDepth if provided should be a list, np.ndarray or tuple 
            of two non-identical values """
        if not self.installDepth is None:
            try:
                if not isinstance(self.installDepth,
                                  (list, np.ndarray, tuple)):
                    raise TypeError
                elif not len(self.installDepth) == 2:
                    raise ValueError('SizeError')
                elif self.installDepth[0] == self.installDepth[1]:
                    raise ValueError('EqualError')
            except TypeError:
                status = False
                errStr.append(f'The installation depth constraints format is '
                              f'incorrect.\n Accepted format are: None, list, '
                              f'tuple, numpy.ndarray.')
            except ValueError as e:
                x = getattr(e, 'message', str(e))
                status = False
                if 'SizeError' in x:
                    errStr.append(f'The installation depth requires two '
                                  f'water depth values but '
                                  f'{len(self.installDepth)} was given.')
                    print(f'The installation depth requires two '
                          f'water depth values but {len(self.installDepth)}'
                          f'was given.')
                elif 'EqualError' in x:
                    errStr.append(
                        f'The two installation depth values need to be'
                        f'dinstinct.')
                    print(f'The two installation depth values need to be'
                          f' dinstinct.')
                # else:
                #     errStr.append(f'Unexpected error encountered!')
                #     print(f'Unexpected error encountered!')

            try:
                if not all(isinstance(x, (int, float)) for x in self.installDepth):
                    raise TypeError
            except TypeError:
                status = False
                errStr.append(f'The installation depth constraints values '
                              f'format is '
                              f'incorrect.\n Accepted format are: float.')
        # else:
        #     self.installDepth = [-np.inf, 0]
        """ self.dataFolder is a valid path folder where the wec solution file
            is found """
        if self.ver == 1:
            try:
                fname = os.path.join(self.dataFolder, 'wec_solution.h5')
                # print(fname)
                # print(os.listdir(self.dataFolder))
                if not os.path.isfile(fname):
                    raise ValueError
            except ValueError:
                status = False
                errStr.append(f"The wec_solution.h5 file is missing!\n"
                            f"The wec_solution.h5 needs to be contained into"
                            f"the DataFolder.")
                print(
                    f"The wec_solution.h5 file is missing!\n The wec_solution.h5 "
                    f"needs to be contained into the DataFolder.")
                raise
            """ self.lCS is a numpy.ndarray """
        # if not self.lCS is None:
        #     try:
        #         if not isinstance(self.lCS, (np.ndarray, list)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The input format of the position vector is '
        #                       f'incorrect.\n Accepted format are: None or '
        #                       f' numpy.ndarray')
        #         print(
        #             f'The input format of the main direction is incorrect.\n '
        #             f' Accepted format are: None, list, or numpy.ndarray')
        #         raise
        """ self.yawAngle is a float """
        if not self.yawAngle is None:
            try:
                if not isinstance(self.yawAngle, (int, float)):
                    raise TypeError
            except TypeError:
                status = False
                errStr.append(f'The input format of the yaw angle is '
                              f'incorrect.\n Accepted format are: float')
                print(f'The input format of the yaw angle is incorrect.\n '
                      f' Accepted format are: float.')
                raise
        """ self.floatFlag is a bool """
        if not self.floatFlag is None:
            try:
                if not isinstance(self.floatFlag, (bool)):
                    raise TypeError
            except TypeError:
                status = False
                errStr.append(f'The input format of the floating flag is '
                              f'incorrect.\n Accepted values are: True or '
                              f' False')
                print(f'The input format of the floating flag is incorrect.\n '
                      f' Accepted values are: True or False.')
                raise
        """ self.min_distance is a tuple of two values """
        if not self.min_distance is None:
            try:
                if not isinstance(self.min_distance, (tuple, list)):
                    raise TypeError
            except TypeError:
                status = False
                errStr.append(f'The input format of the minimum distance is '
                              f'incorrect.\n Accepted format are: tuple or list')
                print(f'The input format of the minimum distance is incorrect.'
                      f'\n  Accepted format are: tuple or list.')
                raise
            try:
                if not len(self.min_distance) == 2:
                    raise ValueError
            except ValueError:
                status = False
                errStr.append(
                    f'The dimension of the minimum distance input is '
                    f' incorrect.\n Accepted dimension: 2.')
                print(
                    f'The dimension of the minimum distance input is incorrect'
                    f'.\n Accepted dimension: 2.')
                raise
            try:
                if not all(isinstance(x, (float,int)) for x in self.min_distance):
                    raise TypeError
            except TypeError:
                status = False
                errStr.append(
                    f'The input format of the minimum distance values'
                    f' incorrect.\n Accepted format are: float')
                print(f'The input format of the minimum distance values is '
                      f'incorrect.\n  Accepted format are: float.')
                raise
        """ self.ratedPower is a positive float """
        if not self.ratedPower is None:
            try:
                if not isinstance(self.ratedPower, (float, int)):
                    raise TypeError
                else:
                    self.ratedPower = float(self.ratedPower)
            except TypeError:
                status = False
                errStr.append(f'The input format of the rated power '
                              f'is incorrect.\n Accepted format are: float')
                print(f'The input format of the rated power is'
                      f' incorrect.\n  Accepted format are: float.')
                raise      
            try:
                if not (self.ratedPower > 0):
                    raise ValueError
            except ValueError:
                status = False
                errStr.append(
                    f'The value of the rated power of the device '
                    f'is incorrect.\n Only positive values accepted.')
                print(f'The value of the rated power of the device is'
                      f' incorrect.\n Only positive values accepted.')
                raise

    def checkPowerConsistency(self, rPowArray):

        status = True
        errStr = []
        """ rPowArray should be a positive non null integer """
        try:
            if not isinstance(rPowArray, (int, float)):
                raise TypeError
        except TypeError:
            print('Maximum number of device must be an integer number.')
            status = False
            errStr.append(
                'Maximum number of device must be an integer number.')
            raise
        maxNumDevices = int(rPowArray / self.ratedPower)
        try:
            if maxNumDevices <= 0:
                raise ValueError("Must be a positive non null integer!")
        except ValueError:
            print(
                "The ratio between array and machine rated powers is below 1.\n\
                Verify the rated power inputs.")
            status = False
            errStr.append(
                "The ratio between array and machine rated powers is \
                        below 1.'\n' Verify the rated power inputs. ")
            raise

        return maxNumDevices, status, errStr

    def load_single_machine_model(self, specType, model=None):
        """
        load_single_machine_model: loads the hydrodynamic model of the single 
        machine passed by the user.

        Returns:

        """
        if self.ver==1:
            (self.M, self.Madd, self.Crad, self.Khyd, self.Fex, self.period,
            self.directions, self.D, self.G, self.AR, self.water_depth,
            self.radius, self.modes, self.order,
            self.truncorder) = reader.read_hydrodynamic_solution(self.dataFolder)

            (self.Cfit, self.Kfit, self.CPTO, self.Kmooring, self.w_te, self.w_hm0,
            self.w_dirs, self.scatter_diagram,
            self.power_matrix) = reader.read_performancefit_solution(
                self.dataFolder)

            # Calculate Tp values for power matrix
            self.w_tp = convert_te2tp(self.w_te, specType[0], specType[1])
            self.frequency = 1./self.period

            # import json
            # data = {
            #     "minimum_interdistance": self.min_distance,
            #     "installation_depth": self.installDepth,
            #     "rated_power_device": self.ratedPower,
            #     "frequency": 1/self.period,
            #     "directions": self.directions,
            #     "pto_damping": self.CPTO,
            #     "mooring_stiffness": self.Kmooring,
            #     "additional_damping": self.Cfit*0,
            #     "additional_stiffness": self.Kfit*0,
            #     "fitting_damping": self.Cfit,
            #     "fitting_stiffness": self.Kfit,
            #     "mass_matrix": self.M,
            #     "added_mass": self.Madd,
            #     "radiation_damping": self.Crad,
            #     "excitation_force_real": self.Fex.real,
            #     "excitation_force_imag": self.Fex.imag,
            #     "hydrosatic_stiffness": self.Khyd,
            #     "diffraction_transfer_matrix_real": self.D.real,
            #     "diffraction_transfer_matrix_imag": self.D.imag,
            #     "force_transfer_matrix_real": self.G.real,
            #     "force_transfer_matrix_imag": self.G.imag,
            #     "radiation_amplitude_coefficients_real": self.AR.real,
            #     "radiation_amplitude_coefficients_imag": self.AR.imag,
            #     "inscribing_cylinder_radius": self.radius,
            #     "modes": self.modes,
            #     "order": self.order,
            #     "truncation_order": self.truncorder,
            #     "heading_angle_span": self.yawAngle,
            #     "water_depth": self.water_depth,
            #     "power_matrix": self.power_matrix,
            #     "hs_pm": self.w_hm0,
            #     "tp_pm": self.w_tp,
            #     "dir_pm": self.w_dirs,
            #     "scatter_diagram": self.scatter_diagram
            # }
            # serial_data = {}
            # for k, v in data.items():
            #     try:
            #         serial_data[k] = v.tolist()
            #     except:
            #         serial_data[k] = v
            
            # print(json.dumps(serial_data))
            # json.dump(serial_data, open('machine_3_WEC_hydro.json', 'w'))

        else:  # load data given in the model in json format
            if model is None: 
                raise ValueError('Missing Input: DTOcean+ requires the data single machine hydrodynamic model to be given in JSON format')
            self.extract_model_from_json(model)

        # Clip any values above rated power in power matrix
        self.power_matrix = np.clip(self.power_matrix, None, self.ratedPower)

        # TODO: place this in a proper place
        self.depth = self.water_depth
        self.wnumber = WNumber(self.period, self.depth)
        self.dof = self.Madd.shape[-1]

    def extract_model_from_json(self, model):
        variable_mapping = {
                            'M': 'mass_matrix', 
                            'Madd':'added_mass', 
                            'G': ['force_transfer_matrix_real', 'force_transfer_matrix_imag'],
                            'D': ['diffraction_transfer_matrix_real', 'diffraction_transfer_matrix_imag'],
                            'Crad': 'radiation_damping',
                            'Khyd': 'hydrostatic_stiffness',
                            'Fex': ['excitation_force_real', 'excitation_force_imag'],
                            'AR': ['radiation_amplitude_coefficients_real', 'radiation_amplitude_coefficients_imag'],
                            'truncorder': 'truncation_order',
                            'frequency': 'frequency',
                            'directions': 'directions',
                            'water_depth': 'water_depth',
                            'modes': 'modes',
                            'order': 'order',
                            'radius': 'inscribing_cylinder_radius',
                            'CPTO': 'pto_damping',
                            'Kmooring': 'mooring_stiffness',
                            'Cfit': ['additional_damping','fitting_damping'],
                            'Kfit': ['additional_stiffness', 'fitting_stiffness'],
                            'w_hm0': 'hs_pm',
                            'w_tp': 'tp_pm',
                            'w_dirs': 'dir_pm',
                            'scatter_diagram': 'scatter_diagram',
                            'power_matrix': 'power_matrix',
                            'installDepth': 'installation_depth',
                            'yawAngle': 'heading_angle_span'
                            }
        for k, v in variable_mapping.items():
            if isinstance(v, str):
                val = np.array(model[v])
            else:
                if v[0][-4:] == 'real':
                    val = np.array(model[v[0]]) + 1j*np.array(model[v[1]])
                else:
                    val = np.array(model[v[0]]) + np.array(model[v[1]])

            setattr(self, k, val)
        setattr(self, 'period', 1/self.frequency)

    def matrix_zoh_interp(self, site_conditions):
        """ZOH interpolation of numpy matrixes
        """
        cpto = self.__reshape_matrix(self.CPTO, site_conditions, self.w_tp,
                                     self.w_hm0, self.w_dirs)
        cfit = self.__reshape_matrix(self.Cfit, site_conditions, self.w_tp,
                                     self.w_hm0, self.w_dirs)
        kmoor = self.__reshape_matrix(self.Kmooring, site_conditions,
                                      self.w_tp, self.w_hm0, self.w_dirs)
        kfit = self.__reshape_matrix(self.Kfit, site_conditions, self.w_tp,
                                     self.w_hm0, self.w_dirs)

        return (cpto, cfit, kmoor, kfit)

    @staticmethod
    def __reshape_matrix(mat, site_conditions, w_tp, w_hm0, w_dirs):
        """ZOH interpolation of numpy matrixes between the isolated scatter diagram and the lease area scatter diagram
        Args:
            mat: numpy array to be used
            site_conditions: metocean conditions at the lease area site

        Returns: out_mat: zoh copy of the input matrix mat

        """
        per_mb = site_conditions.Tp
        hei_mb = site_conditions.Hs
        dir_mb = site_conditions.B
        n_mb_per = len(per_mb)
        n_mb_hei = len(hei_mb)
        n_mb_dir = len(dir_mb)
        n_dof = mat.shape[-1]

        i_per = np.argmin(np.abs(np.subtract.outer(np.unique(w_tp), per_mb)), 0)
        i_hei = np.argmin(np.abs(np.subtract.outer(np.unique(w_hm0), hei_mb)), 0)
        i_dir = np.argmin(np.abs(np.subtract.outer(np.unique(w_dirs), dir_mb)), 0)

        out_mat = np.zeros((n_mb_per, n_mb_hei, n_mb_dir, n_dof, n_dof))
        for iper in range(n_mb_per):
            for ihei in range(n_mb_hei):
                for idir in range(n_mb_dir):
                    out_mat[iper, ihei, idir, :, :] = mat[
                        i_per[iper], i_hei[ihei], i_dir[idir], :, :]
        return out_mat


def convert_te2tp(te, specType, gamma):

    coeff = np.array([[1.22139232e+00], [-7.26257028e-02], [1.74397331e-02],
                      [-2.19288663e-03], [1.07357912e-04]])

    # convert Te to Tp for the Metocean condition relative to the deployment site
    conversion_factor = 1.16450471

    if specType == 'Jonswap':
        # if gamma > 7 or gamma < 1:
        # module_logger.warning('The gamma value of the JONSWAP spectrum '
        #                       'in the metocean data specification is out '
        #                       'of the confident range [1-7].')
        conversion_factor = coeff[0] + \
                            coeff[1] * gamma + \
                            coeff[2] * gamma**2 + \
                            coeff[3] * gamma**3 + \
                            coeff[4] * gamma**4

    tp = te * conversion_factor

    return tp