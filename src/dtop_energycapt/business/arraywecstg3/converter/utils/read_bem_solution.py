# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

#    Copyright (C) 2016 Francesco Ferri
#    Copyright (C) 2017-2018 Mathew Topper
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module contains the methods used to load the hydrodynamic module of the isolated WEC
using the Nemoh software.

.. module:: read_bem_solution
   :platform: Windows
   :synopsis: WEC Numerical model loader

.. moduleauthor:: Francesco Ferri <ff@civil.aau.dk>
.. moduleauthor:: Mathew Topper <mathew.topper@dataonlygreater.com>
"""

import os
import h5py
import numpy as np

def read_hydrodynamic_solution(data_folder):
    with h5py.File(__check_filename(data_folder), 'r+') as f:
        m_m = np.array(f.get('m_m'))
        m_add = np.array(f.get('m_add'))
        c_rad = np.array(f.get('c_rad'))
        f_ex = np.array(f.get('f_ex'))
        periods = np.array(f.get('periods'))
        directions = np.array(f.get('directions'))
        k_hst = np.array(f.get('k_hst'))
        diffraction_tr_mat = np.array(f.get('diffraction_tr_mat'))
        force_tr_mat = np.array(f.get('force_tr_mat'))
        amplitude_coefficient_radiation = np.array(f.get('amplitude_coefficient_radiation'))
        water_depth = np.array(f.get('water_depth'))
        cyl_radius = np.array(f.get('cyl_radius'))
        modes = np.array(f.get('modes'))
        order = np.array(f.get('max_order'))
        truncation_order = np.array(f.get('truncation_order'), np.int)

    return (m_m, m_add, c_rad, k_hst, f_ex, periods, directions,
            diffraction_tr_mat, force_tr_mat, amplitude_coefficient_radiation,
            water_depth, cyl_radius, modes, order, truncation_order)


def read_freq(data_folder):
    with h5py.File(__check_filename(data_folder), 'r+') as f:
        data = np.array(f.get('periods'))
    return 1./data


def read_directions(data_folder):
    with h5py.File(__check_filename(data_folder), 'r+') as f:
        data = np.array(f.get('directions'))
    return data


def read_performancefit_solution(data_folder):
    with h5py.File(__check_filename(data_folder), 'r+') as f:
        c_ext = np.array(f.get('c_ext'))
        k_ext = np.array(f.get('k_ext'))
        c_fit = np.array(f.get('c_fit'))
        k_fit = np.array(f.get('k_fit'))
        c_pto = np.array(f.get('c_pto'))
        k_mooring = np.array(f.get('k_mooring'))
        te = np.array(f.get('te'))
        hm0 = np.array(f.get('hm0'))
        wave_dir = np.array(f.get('wave_dir'))
        scatter_diagram = np.array(f.get('scatter_diagram'))
        power_matrix = np.array(f.get('power_matrix'))

    return (c_fit+c_ext,
            k_fit+k_ext,
            c_pto,
            k_mooring,
            te,
            hm0,
            wave_dir,
            scatter_diagram,
            power_matrix)


def __check_filename(data_folder, fn="wec_solution.h5"):
    f_n = os.path.join(data_folder, 'wec_solution.h5')
    if not os.path.isfile(f_n):
        raise ValueError("Missing the wec_solution.h5 file in the specified data folder [{}] ".format(data_folder))
    return f_n

