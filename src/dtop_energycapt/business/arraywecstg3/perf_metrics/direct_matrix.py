# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
"""

# Imports
from math import pi
import numpy as np
from numpy.linalg import solve
from scipy.special import jv, yv
from dtop_energycapt.business.stg3utils.utils.watwaves import len2

# Settings


class DirectMatrix(object):
    def __init__(self, iBody, cylamplitude=True):
        """
        """
        # cylamplitude = True ==> will save cylindrical amplitude coefficients
        self.cylamplitude = cylamplitude
        # wave data from iBody
        self.depth = iBody.depth
        self.period = iBody.period
        self.wnumber = iBody.wnumber
        # wave directions
        self.dir = [0]
        # Body instance
        self.Body = iBody
        self.orientation = 0
        self.d_rot = None
        self.g_rot = None
        self.ar_rot = None
        self.Nm = np.array(range(-self.Body.order.max(),
                    self.Body.order.max() + 1),
                    dtype=float)
        self.m, self.n = np.meshgrid(self.Nm, self.Nm, indexing='ij')

    def apply_body_rotation(self):
        Gp = np.transpose(self.Body.G, axes=(0, 2, 1))
        d_rot = self.Body.D * np.exp(-1j * (self.n - self.m) * self.orientation)
        g_rot = np.transpose(Gp *
                             np.exp(1j * self.Nm * self.orientation),
                             axes=(0, 2, 1))
        ar_rot = self.Body.AR * np.exp(1j * self.Nm * self.orientation)

        self.d_rot = d_rot
        self.g_rot = g_rot
        self.ar_rot = ar_rot

    def set_wave_directions(self, wave_directions):
        if isinstance(wave_directions, (list, tuple)):
            wave_directions = np.array(wave_directions)
        self.dir = wave_directions

    def Scattering(self, coord):
        """
        """
        Nb = len2(coord)
        Nm = self.Body.order.max()
        dim = 2 * Nm + 1
        k0 = self.wnumber
        direction = self.dir
        #
        T = self.Transformation(coord, Nm)
        T, D_array, G_array, M_int = self.Interaction(T, Nm, 'S')
        # Get the ambient planar wave, AP, for the scattering problem for the entire array
        matk0, matdir, X, mode = np.meshgrid(k0,
                                             direction,
                                             coord[:, 0],
                                             range(-Nm, Nm + 1),
                                             indexing='ij')
        Y = np.meshgrid(k0,
                        direction,
                        coord[:, 1],
                        range(-Nm, Nm + 1),
                        indexing='ij')[2]
        # Calculate AP using (k0,dir,bodycoord,mode)
        if self.Body.convention == 'N':
            AP = np.exp(1j * mode * (pi / 2 - matdir))
            AP *= np.exp(1j * matk0 *
                         (X * np.cos(matdir) + Y * np.sin(matdir)))
        else:
            AP = np.exp(-1j * mode * (pi / 2 + matdir))
            AP *= np.exp(-1j * matk0 *
                         (X * np.cos(matdir) + Y * np.sin(matdir)))
        AP = np.reshape(AP, (len2(k0), len2(direction), (2 * Nm + 1) * Nb))
        Fex = np.zeros((len2(k0), len2(direction), len2(G_array[0][0])),
                       dtype=complex)
        # Save amplitude coefficients
        if self.cylamplitude:
            self.aS = []
            self.AP = []
        for i in range(len2(k0)):
            Nmi = (len2(T[i]) // Nb - 1) // 2
            dimi = 2 * Nmi + 1
            col = (np.array([range(dimi)] * Nb).T + np.array(
                range(0, dim * Nb, dim))).T.reshape(-1) + (Nm - Nmi)
            aS = np.dot(
                np.dot(AP[i][:, col], D_array[i]), M_int[i]
            )  # aS= (Id-D*T)\D*AP with M_Interaction=(Id-D*T)**-1. However, all have been transposed for sake of convenience
            # Save amplitude coefficients
            if self.cylamplitude:
                self.aS.append(aS)
                self.AP.append(AP[i][:, col])
            # Calculation of the excitation force for the entire array
            Fex[i, :, :] = np.dot(
                AP[i][:, col] + np.dot(aS, T[i]), G_array[i]
            )  # Fex= G*aI with aI=AP+T*aS so the overall incident wave for the scattering problem
        self.Fex = Fex

    def Radiation(self, coord):
        """
        """
        Nb = len2(coord)
        Nm = self.Body.order.max()
        dof = self.Body.dof
        AR_iso = self.ar_rot
        Madd_iso = self.Body.Madd
        Crad_iso = self.Body.Crad
        k0 = self.wnumber
        freq = 2 * pi / self.period
        #
        T = self.Transformation(coord, Nm)
        T, D_array, G_array, M_int = self.Interaction(T, Nm, 'R')
        # From now on we work for each wave-frequency
        Madd = np.zeros((len2(k0), Nb * dof, Nb * dof))
        Crad = np.zeros((len2(k0), Nb * dof, Nb * dof))
        # For later usage D, G, T and M_inter are transposed
        # Save amplitude coefficients
        if self.cylamplitude:
            self.aR = []
            self.AR = []
        for k in range(len2(k0)):
            Nmk = (len2(T[k]) // Nb - 1) // 2
            dimk = 2 * Nmk + 1
            colT = np.array(np.arange(0, dimk * Nb, dimk).tolist() * dimk,
                            dtype=int) + np.linspace(
                                0, dimk, dimk * Nb, endpoint=False, dtype=int)
            colAR = (np.array([range(dimk)]) + (Nm - Nmk)).reshape(-1)
            T_re = np.reshape(
                T[k][colT], (dimk, dimk * Nb**2)
            )  # remember that T is already transposed so we are already getting T columns
            AR = np.dot(AR_iso[k][:, colAR],
                        T_re)  # each row of aR is constant dof.
            AR = np.reshape(AR, (dof, Nb, Nb * dimk))
            Fex_rad = np.zeros((dof, Nb, dof * Nb), dtype=complex)
            # Save amplitude coefficients
            if self.cylamplitude:
                self.AR.append(AR)
                aRaux = np.zeros(AR.shape, dtype=complex)
            for i in range(dof):
                aR = np.dot(np.dot(AR[i, :, :], D_array[k]), M_int[k])
                # Save amplitude coefficients
                if self.cylamplitude:
                    aRaux[i] = aR
                Fex_rad[i, :, :] = np.dot(AR[i, :, :] + np.dot(aR, T[k]),
                                          G_array[k])
            # Save amplitude coefficients
            if self.cylamplitude:
                self.aR.append(aRaux)
            Madd_t = Madd_iso[k, :, :].T.repeat(Nb, axis=0).reshape(
                (dof, 1, dof * Nb))
            Crad_t = Crad_iso[k, :, :].T.repeat(Nb, axis=0).reshape(
                (dof, 1, dof * Nb))
            Ones = np.eye(Nb, dtype=int).repeat(
                dof,
                axis=1)  # will be used to distribute Crad_t over eye() type
            if self.Body.convention == 'N':
                Frad_k = -(-freq[k]**2 * Ones * Madd_t -
                           1j * freq[k] * Ones * Crad_t) + Fex_rad
            else:
                Frad_k = -(-freq[k]**2 * Ones * Madd_t +
                           1j * freq[k] * Ones * Crad_t) + Fex_rad
            # Redistribution to a conventional way
            Frad_k = Frad_k.reshape((Nb * dof, Nb * dof), order='F').T
            Madd[k, :, :] = 1 / freq[k]**2 * Frad_k.real
            if self.Body.convention == 'N':
                Crad[k, :, :] = 1 / freq[k] * Frad_k.imag
            else:
                Crad[k, :, :] = -1 / freq[k] * Frad_k.imag
        self.Madd = Madd
        self.Crad = Crad

    def Interaction(self, T, Nm, problem):
        """
        """
        dof = self.Body.dof
        dim = 2 * Nm + 1
        Nb = T.shape[1] // dim
        k0 = self.wnumber
        D = self.d_rot
        G = self.g_rot
        if problem == 'R':  # radiation
            TruncOrder = self.Body.truncorder[:, 1]
        else:  # scattering
            TruncOrder = self.Body.truncorder[:, 0]
        # Interaction
        D_array = np.zeros((len2(k0), Nb * dim, Nb * dim), dtype=complex)
        G_array = np.zeros(
            (len2(k0), Nb * dim, Nb * dof),
            dtype=complex)  # G is already transposed, numb columns= Nb*dof ok!
        for i in range(Nb):
            D_array[:, dim * i:dim * (i + 1), dim * i:dim * (
                i +
                1)] = D  # diffraction transfer matrix for the isolated device
            G_array[:, dim * i:dim * (i + 1), dof * i:dof * (i + 1)] = G

        dimp = 2 * TruncOrder + 1
        T_trunc, D_trunc, G_trunc, M_inter = [], [], [], []

        for i in range(len2(k0)):
            rowcol = (
                np.array([range(dimp[i])] * Nb).transpose() +
                np.array(range(0, dim * Nb, dim))).transpose().reshape(-1)
            row, col = np.meshgrid(rowcol, rowcol, indexing='ij')
            ii = Nm - TruncOrder[i]
            D_trunc.append(D_array[i, row + ii, col + ii])
            G_trunc.append(G_array[i, rowcol + ii, :])
            T_trunc.append(T[i, row, col])
            M_inter.append(
                solve(
                    np.eye(Nb * dimp[i]) - np.dot(T_trunc[i], D_trunc[i]),
                    np.eye(Nb * dimp[i])))
        return T_trunc, D_trunc, G_trunc, M_inter

    def Transformation(self, coord, Nm):
        """
        """
        ##
        (k0, Nb, Nf, dim) = (self.wnumber, len2(coord), len2(self.wnumber),
                             2 * Nm + 1)
        # Geometric properties
        (row, col) = np.meshgrid(range(Nb), range(Nb), indexing='ij')
        Dx = (coord[:, 0] * np.ones((Nb, Nb))).T - coord[:, 0] * np.ones(
            (Nb, Nb))
        Dy = (coord[:, 1] * np.ones((Nb, Nb))).T - coord[:, 1] * np.ones(
            (Nb, Nb))
        L = np.sqrt(Dx**2 + Dy**2)[row < col]
        alf = np.arctan2(Dy, Dx)[row < col]
        ij_i = row[row < col]
        ij_j = col[row < col]
        ##
        (D, kk0, nu) = np.meshgrid(L, k0, range(dim), indexing='ij')
        H = jv(nu, kk0 * D) - 1j * yv(nu, kk0 * D)
        if self.Body.convention == 'N':
            H = np.conj(H)
        del (D, kk0, nu)
        # Transformation matrix
        (p, q) = np.meshgrid(range(-Nm, Nm + 1),
                             range(-Nm, Nm + 1),
                             indexing='ij')
        mapk0 = np.repeat(range(Nf), dim * dim).reshape((Nf, dim, dim))
        sign = (np.tri(dim).T - np.tri(dim))**(q - p)
        T = np.zeros((Nf, Nb * dim, Nb * dim), dtype=complex)
        for i in range(len2(L)):
            r = dim * ij_i[i]
            c = dim * ij_j[i]
            # Assembling Tij and Tji
            Tij = sign * H[i][mapk0, abs(q - p)] * np.exp(
                (q - p) * alf[i] * 1j)
            T[:, r:r + dim, c:c + dim] = Tij  # Tij
            T[:, c:c + dim, r:r + dim] = Tij * np.exp((q - p) * pi * 1j)  # Tji
        #Transpose for later usage since we finally  decided
        #to work with transposed(T). D and G for the isolated device
        #have been already obtained transposed as well as
        #AP, aS, AR and aR
        T = np.transpose(T, (0, 2, 1))
        return T