# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
"""

import numpy as np

def jonswap(Tp, Hm0, Dir, freqs, dirs, gamma):
    """
    """
    f = freqs/2./np.pi
    df = np.abs(f[1:]-f[:-1])
    fp = 1./Tp
    # parameters
    sigma = 0.07*np.ones(freqs.shape, float)
    sigma[f>fp] = 0.09
    alpha = 0.0624/(0.230+0.0336*gamma-0.185/(1.9+gamma))
    beta = np.exp(-(f-fp)**2/2./sigma**2/fp**2)
    # variance
    S_eta_f = alpha*Hm0**2*fp**4*f**-5*gamma**beta*np.exp(-5./4.*(fp/f)**4)

    S_eta = np.zeros((len(freqs), len(dirs)), float)
    try:
        ind = np.arange(len(dirs))[dirs==Dir][0]
    except:
        raise IndexError('Definition of the Numerical model direction and the Scatter Diagram Ddirection are not the same')
    S_eta[:, ind] = 1.*S_eta_f
    if len(dirs) > 1:
        S_eta[:, ind] *= 2.
    if len(freqs) == 1: 
        df = freqs
    return df, 1., S_eta

# def spreading():
#     """
#     """
#     pass

# if __name__ == "__main__":
    
#     for Hm0 in np.linspace(1., 13., 20.):
#         Tp = 0.84
#         Dir = 0.
#         freqs = np.linspace(3.1, 8.3, 100.)
#         dirs = np.array([0.,], float)
#         gamma = 3.3
#         S = jonswap(Tp, Hm0, Dir, freqs, dirs, gamma)
#         Sintgrl = S[2][:-1, 0]+S[2][1:, 0]
#         Sintgrl = .5*Sintgrl.dot(S[0])
#         Hm0sp = 4.*np.sqrt(Sintgrl)
#         print(Hm0/Hm0sp, 4./(Hm0/Hm0sp))
    

