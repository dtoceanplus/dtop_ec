# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
"""

import numpy as np
from dtop_energycapt.business.arraywecstg3.perf_metrics.utils.StrDyn import MotionFreq
from scipy.linalg import block_diag


def power_irregular(NBodies, Dirs, Hs, Tp, spectrum, freqs, dirs, Fex, Madd,
                    Crad, M, Khst, Cpto, Kex, Cex, convention, spreading_weights, mask=None):
    """
    power in irregular waves, characterised by a variange spectrum, 'spectrum'.
    compute power matrices for each body.

    spectrum = (method, *args), e.g. (jonswap, (gamma,)). The method will be
    called as method(*((tp, hs, dr, freqs, dirs)+args)) and must return
    (delta_fr, delta_dir, variance(fr, dir)) M and Khst should be given as
    numpy.array(NDOF, NDOF) Cpto, Kex and Cex should be given as
    numpy.array(NTp, NHs, NDir, NDOF, NDOF) or numpy.array(NDOF, NDOF)
    """
    NDir, NHs, NTp, Nfr, Ndr = len(Dirs), len(Hs), len(Tp), len(freqs), len(
        dirs)
    power = np.zeros((NBodies, NTp, NHs, NDir), float)
    spec_val = np.zeros((NTp, NHs, NDir, Nfr, Ndr), float)
    pwr_reg = np.zeros((NBodies, NTp, NHs, NDir, Nfr, Ndr), float)
    for i_Tp, tp in enumerate(Tp):
        for i_Hs, hs in enumerate(Hs):
            for i_Dir, dr in enumerate(Dirs):
                if mask is not None:
                    if mask[i_Tp, i_Hs]:  # if the mask is true skip the calculation
                        continue
                # wave amplitude for each regular wave component
                (dfr, ddr, spec_val[i_Tp, i_Hs, i_Dir]) = spectrum[0](*(
                    (tp, hs, dr, freqs, dirs) + spectrum[1]))
                # power per unit squared amplitude wave for each regular wave component
                pwr_reg[:, i_Tp, i_Hs, i_Dir] = power_regular(
                        NBodies, freqs, dirs, Fex, Madd, Crad, 
                        M, Khst[i_Tp, i_Hs],
                        Cpto[i_Tp, i_Hs], Kex[i_Tp, i_Hs],
                        Cex[i_Tp, i_Hs], convention)[1]
                # integrand
                intgrnd = 2 * spec_val[
                        i_Tp, i_Hs,
                        i_Dir] * pwr_reg[:, i_Tp, i_Hs,
                                         i_Dir]  # a**2 = 2*spec_val*dfr

                # power matrix
                if len(dirs) > 1:
                    intgrnd = .5 * (
                        (intgrnd[:, :, 1:] + intgrnd[:, :, :-1]) * ddr).sum(
                            axis=-1)  # integrate over directions
                else:
                    intgrnd = intgrnd[:, :, 0]
                if Nfr == 1:
                    power[:, i_Tp, i_Hs, i_Dir] = .5 * (intgrnd  * dfr).sum(axis=-1)  # integrate over frequencies
                else:
                    power[:, i_Tp, i_Hs, i_Dir] = .5 * (
                        (intgrnd[:, 1:] + intgrnd[:, :-1]) * dfr).sum(
                        axis=-1)  # integrate over frequencies
    if len(spreading_weights)>1:
        spreading_sum_power = power.dot(spreading_weights)
        power = spreading_sum_power.reshape(NBodies, NTp, NHs, 1)
    return spec_val, pwr_reg, power


def power_regular(NBodies, freqs, dirs, Fex, Madd, Crad, M, Khst, Cpto, Kex,
                  Cex, convention):
    """
    power per unit squared amplitude wave in regular waves

    M, Khst, Cpto, Kex and Cex should be given as numpy.array(NDOF, NDOF)
    """
    NDOF, Nfr, Ndr = int(Madd.shape[-1] / NBodies), len(freqs), len(dirs)
    RAO = np.zeros(Fex.shape, complex)
    power = np.zeros((NBodies, Nfr, Ndr), float)
    for i_fr, freq in enumerate(freqs):
        RAO[i_fr] = MotionFreq(
            block_diag(*[M] * NBodies) + Madd[i_fr],
            Crad[i_fr] + block_diag(*[Cpto + Cex] * NBodies),
            block_diag(*[Khst + Kex] * NBodies), Fex[i_fr].T, freq,
            convention).T
        for body in range(NBodies):
            for i_dr in range(Ndr):
                vel = 1j * freq * RAO[i_fr, i_dr, NDOF * body:NDOF *
                                      (body + 1)]
                if convention == 'N':
                    vel *= -1
                power[body, i_fr, i_dr] = 0.5 * np.dot(vel.real.T,
                                                       np.dot(Cpto, vel.real))
                power[body, i_fr, i_dr] += 0.5 * np.dot(
                    vel.imag.T, np.dot(Cpto, vel.imag))
    return RAO, power


# TODO: check why this is not used so far
# def raos(NBodies, freqs, Force, Mass, Damping, Stiffness, convention):
#     """
#     """
#     RAO = np.zeros(Force.shape, complex)
#     for i_fr, freq in enumerate(freqs):
#         RAO[i_fr] = MotionFreq(Mass[i_fr], Damping[i_fr], Stiffness,
#                                Force[i_fr].T, freq, convention).T
#     NDOF = Force.shape[-1] / NBodies
#     Ndr = Force.shape[1]
#     RAO_bod = np.zeros((NBodies, NDOF, len(freqs), Ndr), complex)
#     for body in range(NBodies):
#         for dof in range(NDOF):
#             RAO_bod[body, dof] = RAO[:, :, body * NDOF + dof]
#     return RAO, RAO_bod

# if __name__ == "__main__":
#     from dtop_energycapt.business.arraywecstg3.perf_metrics.utils.spectrum import jonswap

#     NBo=1 
#     B= np.array([0,180.])/180.*np.pi 
#     Hs = np.array([0.5,1,1.5,2.]) 
#     Tp = np.array([3.5,4.5,5.5,6.5,7.5,8.5,9.5]) 
#     wdir = np.linspace(0,360,30,endpoint=False)/180.*np.pi 
#     period = np.linspace(2,15,50) 
#     spectrum = (jonswap, (3.3,)) 
#     M = np.eye(3) 
#     Madd = np.array([np.eye(3)]*50) 
#     Cpto = np.array([[[np.eye(3)]*2]*4]*7) 
#     Crad = np.array([np.eye(3)]*50) 
#     Khst = np.eye(3) 
#     Fex = np.ones((50,30,3)) 
#     Kfit = np.array([[[np.eye(3)]*2]*4]*7) 
#     Cfit = np.array([[[np.eye(3)]*2]*4]*7) 
#     convention = "N"

#     power_irregular(NBo, B, Hs, Tp, spectrum, 1/period, wdir, Fex, Madd,
#                     Crad, M, Khst, Cpto, Kfit, Kfit, convention)

#     power_irregular(NBo, B, Hs, Tp, spectrum, 1/period, wdir, Fex, Madd,
#                     Crad, M, Khst, Cpto[0,:], Kfit, Kfit, convention)
