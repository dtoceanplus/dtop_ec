# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
from numpy import (array,
                   zeros,
                   ones,
                   transpose,
                   dot,
                   sqrt,
                   arctan2,
                   exp,
                   cos,
                   sin,
                   linspace,
                   eye,
                   reshape,
                   meshgrid,
                   real,
                   imag)

from math import pi
from numpy.linalg import solve
from scipy.special import jv, yv

from dtop_energycapt.business.stg3utils.utils.watwaves import len2
from .utils.StrDyn import energyProduction
from .utils.spec_class import wave_spec
from .direct_matrix import DirectMatrix

from dtop_energycapt.business.stg3utils.utils.set_wdirs_multibody import set_wdirs_multibody, directional_wave_distribution

from .utils.power import power_irregular
from .utils.spectrum import jonswap

import logging
module_logger = logging.getLogger(__name__)


class FarmDM(object):
    def __init__(self, iSiteConditions, iConverter):
        self.gather_inputs(iSiteConditions, iConverter)

    def gather_inputs(self, iSiteConditions, iConverter, cylamplitude=True,
                   _debug=False):
        # coordinates of WECs
        self.coord = None
        self.debug = _debug
        # cylamplitude = True ==> will save cylindrical amplitude coefficients
        self.cylamplitude = cylamplitude
        self.depth = iSiteConditions.depth
        # Sea States(Hs, Tp, B)
        self.Hs = iSiteConditions.Hs
        self.Tp = iSiteConditions.Tp
        self.B = iSiteConditions.B
        # Probability of Occurrence of each sea state
        self.ScatDiag = iSiteConditions.scatDiag
        self.ScatDiag_spec = iSiteConditions.specType
        # Analysis of regular waves or
        # Discretization of sea states for irregular waves analysis
        self.period = iSiteConditions.period
        self.wnumber = iSiteConditions.wnumber

        # TODO: to mitigate a bug in the excitation force rotation, force yawAngle to be 360
        # this is a temporary patch. The error might be related to the number of
        # wave angles used to discretize the cylinder or in the MC module.
        iConverter.yawAngle = np.deg2rad(360)
        self.dir = set_wdirs_multibody(self.B.copy(), self.ScatDiag_spec[-1],
                                    iConverter.orientation_angle,
                                    iConverter.yawAngle)

        self.iconverter = iConverter
        (self.cpto,
         self.cfit,
         self.kmooring,
         self.kfit) = self.iconverter.matrix_zoh_interp(iSiteConditions)

    def energy(self, coord):

        # TODO: check the spreading angle estimation
        # add a body far from the farm to estimate the power for the isolated machine
        isolated_wec = [coord.min(0)-1000000000]
        coord = np.r_[coord, isolated_wec]
        Nbo = len2(coord)
        WEC = self.iconverter
        ratedCapacity = WEC.ratedPower*1000.0
        WECArr = DirectMatrix(WEC, cylamplitude=True)
        w = 2*np.pi/WECArr.period
        spectrum = (jonswap, (self.ScatDiag_spec[1],))
        beta_index = 0
        spreading = self.ScatDiag_spec[-1]
        power_matrix = np.zeros((Nbo-1,)+self.ScatDiag.shape)
        # power_matrix = np.zeros((Nbo,)+self.ScatDiag.shape)
        power_matrix_iso = np.zeros(self.ScatDiag.shape)
        mask_site = self.scatterDiagramThreshold()

        for body_orientation, wave_angles in self.dir.items():
            WECArr.orientation = body_orientation + np.pi
            WECArr.apply_body_rotation()
            # Available: WECArr.Madd and WECArr.Crad, which stand for added mass and radiation damping, 
            # respectively: WECArr.Madd.shape (Num freq, Num dof, Num dof)
            WECArr.Radiation(coord)
            # iterate each wave angle and solve the excitation force problem
            for beta in wave_angles:
                if (self.ScatDiag[:,:,beta_index].sum() < 0.01):
                    # if the cummulative probability of this angle is less then 
                    # 1% the calculation is not performed
                    beta_index += 1
                    continue
                beta_list, weights = directional_wave_distribution(spreading, 
                                                                   beta)
                WECArr.set_wave_directions(beta_list)
                # Available: WECArr.Fex, which stands for excitation force: WECArr.Fex.shape (Num freq, Num dir, Num dof)
                WECArr.Scattering(coord)

                K = self.kmooring[:,:,beta_index,:,:]+WEC.Khyd
 
                (spec_val,
                pwr_reg,
                power) = power_irregular(Nbo, beta_list,
                                        self.Hs, self.Tp, spectrum,
                                        w, beta_list, WECArr.Fex,
                                        WECArr.Madd, WECArr.Crad, WEC.M,
                                        K, self.cpto[:,:,beta_index,:,:], 
                                        self.kfit[:,:,beta_index,:,:],
                                        self.cfit[:,:,beta_index,:,:],'W',
                                        weights, mask=mask_site[:,:,beta_index])
                
                power[power>ratedCapacity] = ratedCapacity
                power_matrix[:,:,:,beta_index] = power[:-1,:,:,0]
                # power_matrix[:,:,:,beta_index] = power[:,:,:,0]
                # (spec_val,
                # pwr_reg,
                # power_iso) = power_irregular(1, beta_list,
                #                         self.Hs, self.Tp, spectrum,
                #                         w, beta_list, WEC.Fex,
                #                         WEC.Madd, WEC.Crad, WEC.M,
                #                         K, self.cpto[:,:,beta_index,:,:], 
                #                         self.kfit[:,:,beta_index,:,:],
                #                         self.cfit[:,:,beta_index,:,:],'W',
                #                         weights)
                power_matrix_iso[:,:,beta_index] = power[:-1,:,:,0].mean(0) #power[-1,:,:,0]
                # power_matrix_iso[:,:,beta_index] = power[:,:,:,0]
                beta_index += 1

        aep_dev = (self.ScatDiag*power_matrix).sum((1, 2, 3)) * 365 * 24
        aep_single = (self.ScatDiag*power_matrix_iso).sum((0, 1, 2)) * 365 * 24
        aep_farm = aep_dev.sum()

        # TODO: estimate the resource reduction correctly
        res = (aep_dev,
               aep_dev / (aep_single + 1e-10),
               aep_dev/(365*24),
               power_matrix)

        return res

    def scatterDiagramThreshold(self):
        """scatterDiagramThreshold remove the elements from the 
        scatter diagram with a low propability of occunrece.
        This is due to the HUGE amount of bins provided by the SC module, 
        in an attempt to reduce the computational cost.
        The function enforce the sum of the scatter diagram to be 1.
        """
        mask = self.ScatDiag < self.ScatDiag.max()/100  # cut every point smaller than 1% of the max value
        self.ScatDiag[mask] = 0.0
        self.ScatDiag /= self.ScatDiag.sum()

        return mask


    def EnergyBalance(self, power_prod_perD_perS, coord):
        """
        EnergyBalance: assess the ratio between outgoing and incoming energy
        Args:
            Nt: unused

        Returns:
            (float): max normalised resource reduction
        """

        # Handy names
        NB = len2(self.B)
        NHs = len2(self.Hs)
        NTp = len2(self.Tp)

        # Initialization
        e = zeros((NTp*NHs*NB), dtype = float)
        eP = zeros(e.shape, dtype = float)

        Spec_ = wave_spec(1./self.period,1.,1.)

        Spec_.s = self.ScatDiag_spec[2]
        Spec_.gamma = self.ScatDiag_spec[1]
        Spec_.spec_type = self.ScatDiag_spec[0]
        Spec_.add_spectrum()
        self.Spectrum = Spec_
        # loop over sea states
        ind_SS = -1
        for i0 in range(NB):
            for i1 in range(NHs):
                for i2 in range(NTp):
                    ind_SS +=1

                    eP[ind_SS] = (1025. * 9.81 ** 2 / 64. / np.pi *
                                 self.Hs[i1] ** 2 *
                                 self.Tp[i2] *
                                 np.sqrt((max(coord[:, 0]) -
                                          min(coord[:, 0])) ** 2 +
                                         (max(coord[:, 1]) -
                                          min(coord[:, 1])) ** 2))
                    e[ind_SS] = eP[ind_SS]-power_prod_perD_perS[:,ind_SS].sum()

        self.e = e
        self.eP = eP

        EnergyWarray = e # energy per sea state
        EnergyWOarray = eP # energy per sea state

        Balance = EnergyWOarray-EnergyWarray # balance per sea state

        return np.max(Balance/EnergyWOarray)
