# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energycapt.business.optimisation.optstrategy import dictfunc
from dtop_energycapt.business.arraywecstg3.perf_metrics import FarmDM, MildSlope
from dtop_energycapt.business.stg3utils.site_conditions import SiteConditions
from dtop_energycapt.business.arraywecstg3.converter import Converter
from dtop_energycapt.business.stg3utils.utils.bathymetry_utility import get_unfeasible_regions
# from shapely.geometry import Polygon

import numpy as np
import types
import os


def get_inputs(self, name, ecID, **kwargs):
    """
        userOutputTable (dict, optional): dictionary of dictionaries where all 
                        the array layouts inputed and analysed by the user are
                        collected. Using this option the internal WP2 
                        calculation is skipped, and the optimisaton is performed
                        in the given data.

        optSpecs (dict): dictionary containing the description of the array 
                        layout to be optimise. 
                        Keys:
                        'opt' (int): 1 - optimisation over the internal 
                                            parametric array layouts
                                        2 - fixed array layout specified by 
                                            the user not subject to 
                                            optimisation
                                        3 - array layout specified by the 
                                            user subject to optimisation via 
                                            expansion of the array
                        'grid' options:
                            (str): 'rectangular'
                            (str): 'staggered'
                            (str): 'full'
                            (numpy.ndarray) [m]: [X(Northing),Y(Easting)] 
                                                 coordinates of the devices
    """
    self.name = name
    self.ecID = ecID

    if 'nb_devices' in kwargs:
        self.nb_devices = kwargs.get("nb_devices")
    else:
        print('Number of devices input missing!')
        raise
    
    self.layout_method = 'verification'  # fallback option
    if 'layout_type' in kwargs:
        self.layout_method = kwargs['layout_type']        

    if self.layout_method == 'optimization':
        # try to fetch the method from the optimization module, default brute force
        optfunc = dictfunc.get(kwargs.get('opt_method', 'brute_force'), dictfunc['brute_force'])
        self.optimisation_threshold = kwargs.get('optimisation_threshold', 0.85)
        self.array_generic_type = kwargs.get('array_generic_type', 'staggered')
        
        self.optfunname = '{}_{}'.format(self.__class__.__name__, optfunc.__name__)
    else:  # if the system is verified, the layout must be given
        self.layout = kwargs.get("array_layout", [])
        if self.layout == []:
            raise ValueError('Invalid inputs: either specify a valid array layout or a valid optimization method')
    
    # only temporary will be removed in the checkinput
    kwargs['ver'] = 1
    self.__kwargs = kwargs
    # print(self.__kwargs)
    # print(self._site_keys)
    self.check_inputs()

    # TODO: this shoudl be refactored
    site = {k: kwargs.get(k, None) for k in self._site_keys}
    site['sea_state_directions'] = np.array(site['sea_state_directions'])
    site['sea_state_wave_height'] = np.array(site['sea_state_wave_height'])
    site['sea_state_wave_period'] = np.array(site['sea_state_wave_period'])
    site['scat_diagram'] = np.array(site['scat_diagram'])
    site['bathymetry'] = np.array(site['bathymetry'])
    site['lease'] = np.array(site['lease'])
    site['nogo_areas'] = np.array(site['nogo_areas'])
    

    model = None
    missing_k_v1 = set(self._converter_keys)-set(kwargs)
    if missing_k_v1:
        kwargs['ver'] = 2
        model = {k: kwargs[k] for k in self._converter_keys_v2}

    converter = {k: kwargs.get(k, None) for k in self._converter_keys}

    converter['orientation_angle'] = kwargs.get("orientation_angle")
    self.i_converter = Converter(**converter)
    self.i_converter.load_single_machine_model(site['spectral_type'], 
                                               model=model)

    site['wave_dirs'] = self.i_converter.directions
    site['frequencies'] = self.i_converter.frequency

    self.i_site_conditions = SiteConditions(**site)

    (NoGo, unfeasible_points_mask) = get_unfeasible_regions(
        self.i_site_conditions.bathy,
        self.i_converter.installDepth,
        debug=True,
        debug_plot=True)
    self.nogo_areas_bathymetry = NoGo

    self.set_opt_parameters()
    self._normalisation_point = max(self.i_converter.min_distance)
    self._min_dist = min(self.i_converter.min_distance)
    self._min_q_factor = self.optimisation_threshold

    self.max_num_devices = self.nb_devices
    # TODO: the Array rated power has been replaced by the device rated power and the number of devices
    # self.max_num_devices, status, errStr = self.i_converter.checkPowerConsistency(
    #     self.ratedPowerArray)

    if 'perf_method' in kwargs:
        if kwargs.get('perf_method') == 'const_bath':
            self.PerfMetrics = FarmDM(self.i_site_conditions, self.i_converter)
        elif kwargs.get('perf_method') == 'var_bath':
            self.PerfMetrics = MildSlope()
        else:
            raise AssertionError('Performance method not found')
    else:
        self.PerfMetrics = FarmDM(self.i_site_conditions, self.i_converter)

    return {"type": self.type, "stage": self.stage}


def set_opt_parameters(self):
    """
    __set_opt_parameters: method used to set up the problem order based
        on the __Opt and _Grid attributes
        
    Returns:

    """
    self.opt_dim = 4  # default case
    self.par_a = np.eye(self.opt_dim)
    self.par_b = np.zeros(self.opt_dim)

    if self.array_generic_type == 'rectangular':
        self.opt_dim = 2
        self.par_a = self.par_a[:, :2]
        self.par_b[-1] = -np.pi / 2
    elif self.array_generic_type == 'staggered':
        self.opt_dim = 2
        self.par_a = self.par_a[:, :2] * 0.
        self.par_a[0, 0] = 1.
        self.par_a[1, 0] = 1.
        self.par_a[2, 1] = 1.
        self.par_a[3, 1] = -1.
    elif self.array_generic_type == 'full':
        self.par_a[-1, -1] = -1.


def check_inputs(self):
    """Checks the inputs for the WEC Stage 3 array object.

    Returns:
        status (bool): True if successful, False otherwise.
        errStr (string): Message explaning the error encountered

    """

    missing_k_v1 = set(self._converter_keys)-set(self.__kwargs)
    missing_k_v2 = set(self._converter_keys_v2)-set(self.__kwargs)

    if missing_k_v1 and missing_k_v2:
        version = missing_k_v1
        if len(missing_k_v1) > len(missing_k_v2): version = missing_k_v2 
        raise IOError(f'The converter is missing the following variables: {", ".join(version)}')

    if any(el is None
           for el in [self.__kwargs.get(k, None) for k in self._site_keys]):
        raise IOError('The site condition is missing one or mulitple entries')

    # if any(el is None for el in
    #        [self.__kwargs.get(k, None) for k in self._converter_keys]):
    #     raise IOError('The converter is missing one or mulitple entries')


    del self.__kwargs

    errStr = []
    status = True
    """ self.name should be a string """
    try:
        if not isinstance(self.name, str):
            raise TypeError("Must be a string!")
    except TypeError:
        print('The name must be a string.')
        status = False
        errStr.append('The name must be a string.')
        raise
    """ self.ecID should be an integer """
    try:
        if not isinstance(self.ecID, int):
            raise TypeError("The ID must be of type integer.")
    except TypeError:
        print('The ID must be of type integer.')
        status = False
        errStr.append('The ID must be of type integer.')
        raise
    """ self.nb_devices should be a positive non null integer """
    try:
        if not isinstance(self.nb_devices, int):
            raise TypeError
    except TypeError:
        print('Number of devices must be an integer number.')
        status = False
        errStr.append('Number of devices must be an integer number.')
        raise
    try:
        if self.nb_devices <= 0:
            raise ValueError("Must be a positive non null integer!")
    except ValueError:
        print("The number of devices must be a positive non null integer.")
        status = False
        errStr.append("The number of devices must be a positive non null \
                      integer.")
        raise

    """ self.optimisation_threshold is a float in the range ]0.,1.]"""
    if not self.optimisation_threshold is None:
        try:
            if not isinstance(self.optimisation_threshold, (float)):
                raise TypeError
        except TypeError:
            status = False
            errStr.append(f'The input format of the optimisation threshold '
                          f'is incorrect.\n Accepted format are: float')
            print(f'The input format of the optimisation threshold is'
                  f' incorrect.\n  Accepted format are: float.')
        try:
            if not (self.optimisation_threshold >= 0 and self.optimisation_threshold <= 1):
                raise ValueError(f'The value of the optimisation threshold is incorrect.\n Range of values accepted: ]0,1]')
        except ValueError:
            status = False
            errStr.append(f'The value of the optimisation threshold '
                          f'is incorrect.\n Range of values accepted: ]0,1]')
            print(f'The value of the optimisation threshold is'
                  f' incorrect.\n  Range of values accepted: ]0,1]')
            raise
    else:
        if self.layout is None:
            status = False
            errStr.append(f'Missing input value for the optimisation '
                        f'threshold.')
            raise ValueError(f'Missing input value for the optimisation '
                            f'threshold.')

    if not self.array_generic_type is None:
        try:
            if not isinstance(self.array_generic_type, str):
                raise TypeError
        except TypeError:
            status = False
            errStr.append(f'The input format of the array generic type '
                          f'is incorrect.\n Accepted format are: string')
            print(f'The input format of the array generic type is'
                  f' incorrect.\n  Accepted format are: string.')
            raise
    # cast the array layout into a Nuply Array
    if self.layout is not None:
        if isinstance(self.layout, list):
            self.layout = np.array(self.layout)
    # """ self.nogoAreas if provided should be a list of numpy.ndarray """
    # if not self.nogoAreas is None:
    #     try:
    #         if not isinstance(self.NogoAreas, list):
    #             raise TypeError
    #     except TypeError:
    #         print('The input format of the nogo area is incorrect.\n \
    #                Accepted format are: None or list.')
    #         status = False
    #         errStr.append('The input format of the nogo area is incorrect.\n'
    #                       'Accepted format are: None or list.')
    #     try:
    #         if not all(isinstance(x, np.ndarray) for x in self.nogoAreas):
    #             raise TypeError
    #     except TypeError:
    #         print('The elements in the list of no go areas do not have the \
    #                right format.\n Accepted format is: numpy.ndarray.')
    #         status = False
    #         errStr.append('The elements in the list of no go areas do not have \
    #                        the right format.\n Accepted format \
    #                        is: numpy.ndarray.')

    errStr = "\n".join(errStr)

    # """ This is temporary. Check if there is a better way of doing this """
    # opt = self.optOptions
    # value = self.userArray
    # try:
    #     if opt == 2:
    #         if value.shape == (2L,):
    #             value = value.reshape((1,2))
    #             self.userArray = value
    #         if not value.shape[1]==2:
    #             if value.shape[0]==2:
    #                 value = value.T
    #                 self.userArray = value

    #             else:
    #                 raise ValueError

    # except ValueError:
    #     print(f"The fixed array layout need to have a shape of nbodyx2. The"
    #           f" given array shape is: ({value.shape[0]},{value.shape[1]})")
    #     status = False
    #     errStr.append(f"The fixed array layout need to have a shape of "
    #                   f"nbodyx2. The given array shape is: "
    #                   f"({value.shape[0]},{value.shape[1]})")

    return status, errStr


# def compress_lease_area(self):

#     overall_buffer = abs(max(0, self.i_site_conditions.boundaryPad))

#     lease_pol = Polygon(self.i_site_conditions.leaseArea)

#     # always contraction
#     lease_pol_buffer = lease_pol.buffer(-overall_buffer)

#     #        pol_max = pol.max(0)
#     #        pol_min = pol.min(0)
#     #        scale = (pol_max-pol_min)
#     #        offset = np.array([0.5, 0.5])
#     #        pol_norm = (pol-pol_min)/scale-offset
#     #        comp_norm = 1-(2*compression*1.05)/scale
#     #        pol_new = (pol_norm*comp_norm+offset)*scale+pol_min

#     self.i_site_conditions = np.c_[lease_pol_buffer.exterior.xy]
#     self.i_site_conditions._leaseArea = np.c_[lease_pol.exterior.xy]


#---------- from previous dtocean --------------
# def checkInput(self):
#         """
#         checkInput: used to assess the validity of the input given to the WP2 prior to perform any calculation

#         Args:
#             self (class)

#         Returns:
#             status (int): identify whether an error is found (status<0) or not (status=0)
#             errStr (list): error strings appended during the error occurence.
#         """

#         Opt = self.M_data.UserArray['Option']
#         Value = self.M_data.UserArray['Value']

#         if Opt==2:

#             if not Value.shape[1]==2:
#                 if Value.shape[0]==2:
#                     Value = Value.T
#                     self.M_data.UserArray['Value'] = Value

#                 else:
#                     raise ValueError("The fixed array layout need to have",
#                                      "a shape of nbodyx2.",
#                                      "The given array shape is: ",
#                                      "{}x{}".format(Value.shape[0], Value.shape[1]))

#         return status, errStr
