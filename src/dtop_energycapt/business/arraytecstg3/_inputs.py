# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energycapt.business.optimisation.optstrategy import dictfunc
from dtop_energycapt.business.stg3utils.site_conditions import SiteConditions
from dtop_energycapt.business.arraytecstg3.converter import Converter
from dtop_energycapt.business.stg3utils.utils.bathymetry_utility import get_unfeasible_regions
from dtop_energycapt.business.arraytecstg3.create_wake_db import create_wake_db
from shapely.geometry import Polygon

import numpy as np
import types
import os


def get_inputs(self, name, ecID, **kwargs):
    """

    """
    self.name = name
    self.ecID = ecID

    if 'nb_devices' in kwargs:
        self.nb_devices = kwargs.get("nb_devices")
    else:
        print('Number of devices input missing!')
        raise

    self.layout_method = 'verification'  # fallback option
    if 'layout_type' in kwargs:
        self.layout_method = kwargs['layout_type']        

    if self.layout_method == 'optimization':
        # try to fetch the method from the optimization module, default brute force
        optfunc = dictfunc.get(kwargs.get('opt_method', 'brute_force'), dictfunc['brute_force'])
        self.optimisation_threshold = kwargs.get('optimisation_threshold', 0.85)
        self.array_generic_type = kwargs.get('array_generic_type', 'staggered')
        
        self.optfunname = '{}_{}'.format(self.__class__.__name__, optfunc.__name__)
    else:  # if the system is verified, the layout must be given
        self.layout = kwargs.get("array_layout", [])
        if self.layout == []:
            raise ValueError('Invalid inputs: either specify a valid array layout or a valid optimization method')

    # only temporary will be removed in the checkinput
    self.__kwargs = kwargs
    self.check_inputs()
    site = {k: np.array(kwargs.get(k, None)) for k in self._site_keys}
    # TODO: Cp and CT should be a list of numpy array
    # check if converting to a np.array have influence of the results
    missing_k_v1 = set(self._converter_keys)-set(kwargs)
    if missing_k_v1:
        for k, v in self._compatibility_dict.items():
            if isinstance(v, str):
                kwargs[k]=kwargs[v]
            else:
                kwargs[k]=[kwargs[ki] for ki in v]
        

    converter = {
            k: np.array(kwargs.get(k, None))
            for k in self._converter_keys
        }
    self.i_site_conditions = SiteConditions(**site)
    self.i_converter = Converter(**converter)
    self.cfd_data = create_wake_db()

    if self.layout_method == 'optimization':
        self.set_opt_parameters()
        # self._normalisation_point = max(self.machine.minDist)
        # self._min_dist = min(self.machine.minDist)
        self._min_q_factor = self.optimisation_threshold

        self._normalisation_point = max(self.i_converter.min_distance)
        # TODO: implement a way to determine the max number of devices for the farm.
        self.max_num_devices = self.nb_devices
        # self.max_num_devices, status, errStr = self.machine.checkPowerConsistency(self.ratedPowerArray)

    number_rotor = kwargs.get('number_rotor', 1)
    self.i_converter.number_rotor = number_rotor
    self.i_converter.rotor_interdistance = kwargs.get('rotor_interdistance', 0)
    if (self.i_converter.rotor_interdistance == 0):
        self.i_converter.rotor_interdistance = self.i_converter.rotor_diameter*1.05
    # if (number_rotor > 1 and self.layout):
    #     n_device = len(self.layout)
    #     rotor_layout = np.array([el for el in self.layout]*number_rotor, dtype='float')

    #     rotor_dimeter = self.i_converter.rotor_diameter
    #     rotor_interdistance = kwargs.get('rotor_interdistance', 0)
    #     if (rotor_interdistance == 0):
    #         rotor_interdistance = rotor_dimeter*1.05
    #     rotor_pos = np.array([el*rotor_interdistance for el in range(number_rotor)], dtype='float')
    #     rotor_pos -= np.mean(rotor_pos)
    #     rotors_pos = np.tile(rotor_pos, (n_device,1)).flatten('F')
    #     angles = np.deg2rad(np.repeat(np.array(self.i_converter.orientation_angle), number_rotor))
    #     rotor_layout[:, 0] += np.cos(angles) * rotors_pos 
    #     rotor_layout[:, 1] += np.sin(angles) * rotors_pos 

    #     self.layout = rotor_layout.tolist()

    return {"type": self.type, "stage": self.stage}


def set_opt_parameters(self):
    """
    __set_opt_parameters: method used to set up the problem order based
        on the __Opt and _Grid attributes
        
    Returns:

    """
    self.opt_dim = 4  # default case
    self.par_a = np.eye(self.opt_dim)
    self.par_b = np.zeros(self.opt_dim)

    if self.array_generic_type == 'rectangular':
        self.opt_dim = 2
        self.par_a = self.par_a[:, :2]
        self.par_b[-1] = -np.pi / 2
    elif self.array_generic_type == 'staggered':
        self.opt_dim = 2
        self.par_a = self.par_a[:, :2] * 0.
        self.par_a[0, 0] = 1.
        self.par_a[1, 0] = 1.
        self.par_a[2, 1] = 1.
        self.par_a[3, 1] = -1.
    elif self.array_generic_type == 'full':
        self.par_a[-1, -1] = -1.

def check_inputs(self):
    """Checks the inputs for the WEC Stage 3 array object.

    Returns:
        status (bool): True if successful, False otherwise.
        errStr (string): Message explaning the error encountered

    """

    missing_k_v1 = set(self._converter_keys)-set(self.__kwargs)
    missing_k_v2 = set(self._converter_keys_v2)-set(self.__kwargs)

    if missing_k_v1 and missing_k_v2:
        version = missing_k_v1
        if len(missing_k_v1) > len(missing_k_v2): version = missing_k_v2 
        raise IOError(f'The converter is missing the following variables: {", ".join(version)}')
    
    if any(el is None
           for el in [self.__kwargs.get(k, None) for k in self._site_keys]):
        raise IOError('The site conidtion is missing one or mulitple entries')

    errStr = []
    status = True
    """ self.name should be a string """
    print("This will check the inputs")
    try:
        # at this point the data might be a list 
        # so we use the np.shape function and not the 
        # inherite numpy var.shape function
        nx = np.shape(self.__kwargs['X'])
        ny = np.shape(self.__kwargs['Y'])
        if len(nx)==2 and nx!=ny:
            raise ValueError(f'X, Y arguments have different dimensions: X ({nx}) - Y ({ny})')
        # this check is made to allow to use both array and vector for X and Y
        if len(nx)==1 and len(ny)==1:
            nx = nx+ny

        
        nu = np.shape(self.__kwargs['U'])
        nv = np.shape(self.__kwargs['V'])
        nti = np.shape(self.__kwargs['TI'])
        nssh = np.shape(self.__kwargs['SSH'])
        npr = np.shape(self.__kwargs['probability'])
        if not (nu==nv or nu==nti or nu==nssh):
            raise ValueError(f'U, V, TI and SSH arguments have different dimensions: U ({nu}) - V ({nv}) - TI ({nti}) - SSH ({nssh})')
        
        # TODO: relaxed due to inconsistency but the data format must be correct at this point
        if not (nu[1] in nx or nu[2] in nx or nu[0] == npr[0]):
            raise ValueError(f'U, V, TI and SSH dimension must be {npr[0]}x{nx[0]}x{nx[1]}. Actual dimensions: U ({nu}) - V ({nv}) - TI ({nti}) - SSH ({nssh})')
        nple = np.shape(self.__kwargs['PLE'])
        nsc = np.shape(self.__kwargs['geophy'])
        nb = np.shape(self.__kwargs['bathy'])
        
        # TODO: relaxed due to inconsistency but the data format must be correct at this point
        if not (nx[0] in nb or nx[0] in nple or nx[0] in nsc):
            raise ValueError(f'Bathymetry (bathy), Power Law Exponent (PLE), and Soil Characteristic (geophy) dimension must be {npr}x{nx[0]}x{nx[1]}. Actual dimensions: PLE ({nple}) - Geophy ({nsc}) - Bathy ({nb})')

        nl = np.shape(self.__kwargs['lease'])
        if len(nl)!=2:
            raise ValueError(f'Lease area vertex definition incorrect. The argument must be a 2D array')
        if nl[0]<3:
            raise ValueError(f'Lease area vertex definition incorrect. The minimum number of vertex is 3: actual number {nl[0]}')
        if nl[1]!=2:
            raise ValueError(f'Lease area vertex definition incorrect. Each vertex should be defined by 2 numbers (Easting, Norhting): actual vertex size {nl[1]}')
    except ValueError as e:
        print(e)
        raise ValueError(e)
    except Exception as e:
        raise RuntimeError(e)
    
    # check hub_height
    if (self.__kwargs['hub_height'] + self.__kwargs['rotor_diameter']/2.0) > 1.2*np.abs(np.mean(self.__kwargs['SSH']) + np.mean(self.__kwargs['bathy'])):
        rna = self.__kwargs['hub_height'] + self.__kwargs['rotor_diameter']/2.0
        bathy = np.mean(self.__kwargs['bathy']) + np.mean(self.__kwargs['SSH'])
        raise ValueError(f'The sum of Hub Height and Rotor Radius ({rna} m) is larger than the average water depth ({bathy} m)')
    # verify that the layout and the lease are are (fully) inside the region specified in the site condition X and Y
    try:
        lease = np.array(self.__kwargs['lease'])
        x = np.array(self.__kwargs['X'])
        y = np.array(self.__kwargs['Y'])
        # the actual check does not work, substitute after discussion with FEM with the one at line 199.
        # all([x.min() <= lease.min(0)[0], x.max() >= lease.max(0)[0], y.min() <= lease.min(0)[1], y.max() >= lease.max(0)[1]])
        if not (x.min() <= lease.min(0)[0] or x.max() >= lease.max(0)[0]
            or y.min() <= lease.min(0)[1] or y.max() >= lease.max(0)[1]):
            raise ValueError(f'Lease Area vertex must be inside the site characteristic points: Lease Bounds {list(lease.min(0)),list(lease.max(0))} - X {x.min(),x.max()} - Y {y.min(),y.max()}')
        

        if self.layout_method == 'verification':
            layout = np.array(self.layout)
            if not (x.min() <= layout.min(0)[0] or x.max() >= layout.max(0)[0]
            or y.min() <= layout.min(0)[1] or y.max() >= layout.max(0)[1]):
                raise ValueError(f'Device layout must be inside the site characteristic points: Layout Bounds {list(layout.min(0)),list(layout.max(0))} - X {x.min(),x.max()} - Y {y.min(),y.max()}')
            del layout

        del lease, x, y
    except ValueError as e:
        print(e)
        raise ValueError(e)
    except Exception as e:
        raise RuntimeError(e)
    del self.__kwargs
    try:
        if not isinstance(self.name, str):
            raise TypeError("Must be a string!")
    except TypeError:
        print('The name must be a string.')
        status = False
        errStr.append('The name must be a string.')
        raise
    """ self.ecID should be an integer """
    try:
        if not isinstance(self.ecID, int):
            raise TypeError("The ID must be of type integer.")
    except TypeError:
        print('The ID must be of type integer.')
        status = False
        errStr.append('The ID must be of type integer.')
        raise
    """ self.nb_devices should be a positive non null integer """
    try:
        if not isinstance(self.nb_devices, int):
            raise TypeError('Number of devices must be an integer number.')
    except TypeError:
        print('Number of devices must be an integer number.')
        status = False
        errStr.append('Number of devices must be an integer number.')
        raise
    try:
        if self.nb_devices <= 0:
            raise ValueError("Must be a positive non null integer!")
    except ValueError:
        print("The number of devices must be a positive non null integer.")
        status = False
        errStr.append("The number of devices must be a positive non null \
                      integer.")
        raise
    """ self.optimisation_threshold is a float in the range ]0.,1.]"""
    if not self.optimisation_threshold is None:
        try:
            if not isinstance(self.optimisation_threshold, (float)):
                raise TypeError
        except TypeError:
            status = False
            errStr.append(f'The input format of the optimisation threshold '
                          f'is incorrect.\n Accepted format are: float')
            print(f'The input format of the optimisation threshold is'
                  f' incorrect.\n  Accepted format are: float.')
        print('Optimisation threshol', self.optimisation_threshold)
        try:
            print('Optimisation threshol', self.optimisation_threshold)
            if not ((self.optimisation_threshold >= 0) and (self.optimisation_threshold <= 1)):
                raise ValueError(f'The value of the optimisation threshold is incorrect.\n Range of values accepted: ]0,1]')
        except ValueError:
            status = False
            errStr.append(f'The value of the optimisation threshold '
                          f'is incorrect.\n Range of values accepted: ]0,1]')
            print(f'The value of the optimisation threshold is'
                  f' incorrect.\n  Range of values accepted: ]0,1]')
            raise
    else:
        if self.layout is None:
            status = False
            errStr.append(f'Missing input value for the optimisation '
                        f'threshold.')
            raise ValueError(f'Missing input value for the optimisation '
                            f'threshold.')
    
    if self.layout_method == 'optimization':
        if not self.array_generic_type is None:
            try:
                if not isinstance(self.array_generic_type, (str)):
                    raise TypeError
            except TypeError:
                status = False
                errStr.append(f'The input format of the array generic type '
                            f'is incorrect.\n Accepted format are: string')
                print(f'The input format of the array generic type is'
                    f' incorrect.\n  Accepted format are: string.')
                raise

    errStr = "\n".join(errStr)

    return status, errStr


def compress_lease_area(self):
    """
    Uses the boundary pad to compress the given lease area.
    This will ensure that every installation element will 
    stay within the given lease area, including mooring and
    anchors
    """
    if self.i_site_conditions.boundaryPad == None:
        overall_buffer = 0
    else:
        overall_buffer = abs(max(0, self.i_site_conditions.boundaryPad))

    lease_pol = Polygon(self.i_site_conditions.leaseArea)

    # always contraction
    lease_pol_buffer = lease_pol.buffer(-overall_buffer)

    # TODO verify is this function is needed
    # self.i_site_conditions = np.c_[lease_pol_buffer.exterior.xy]
    # self.i_site_conditions._leaseArea = np.c_[lease_pol.exterior.xy]
    self.i_site_conditions.leaseArea = np.c_[lease_pol.exterior.xy]

