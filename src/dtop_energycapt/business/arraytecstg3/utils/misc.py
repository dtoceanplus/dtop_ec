# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

#    Copyright (C) 2016 Thomas Roc
#    Copyright (C) 2017-2018 Mathew Topper
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division

import re
import math
import logging

import numpy as np
from numpy.linalg import LinAlgError
from shapely.geometry import LineString

# local imports
from dtop_energycapt.business.arraytecstg3.utils.interpolation import interp_at_point

# Start logging
module_logger = logging.getLogger(__name__)


def closest_point( pt, pts, debug=True):
    """
    Finds the closest point in 'pts'
    and its associated distance
    to any given [x,y] 'pt'.

    Args:
      pt (list): [x,y]
      pts (list): points, [[x1,y1],...[xn,yn]]

    Kwargs:
      debug (bool): debug flag

    Returns:
      closest_point (list): [xi,yi]
      dist (float): distance, (m)

    """
    if debug: module_logger.info("Computing closest point index...")

    dist = np.inf
    ind = 0
    for test in pts:
        d = np.sqrt((test[0] - pt[0])**2.0 + (test[1] - pt[1])**2.0)
        if d < dist:
            closest_point = test
            dist = d
            index = ind
        ind += 1

    if debug: module_logger.info("index: {}".format(closest_point))
    if debug: module_logger.info("distance: {}".format(dist))

    return closest_point, index, dist


def line(p1, p2):
    """
    Defines line.

    Args:
      p1 (list): points, [x1,y1]
      p2 (list): points, [x2,y2]

    Returns:
      list: [A, B, C]
    """
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0]*p2[1] - p2[0]*p1[1])
    return [A, B, -C]


def intersection(L1, L2):
    """
    Computes the intersection point x,y between two lines

    Args:
      L1 (list): output of the dtocean_tidal.utils.line function
      L2 (list): output of the dtocean_tidal.utils.line function

    Returns:
      x (float): x coordinate of the intersection between L1 & L2
      y (float): y coordinate of the intersection between L1 & L2
    """
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x,y
    else:
        return False


def natural_sort(l):
    """Sort string list based on number in string

    Args:
      l (list): list strings with numbers

    Returns:
      list: sorted list
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)


def radians_to_bearing(x):

    initial_bearing = 90 - math.degrees(x)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing


def vector_to_bearing(x, y):

    initial_bearing = math.atan2(y, x)
    compass_bearing = radians_to_bearing(initial_bearing)

    return compass_bearing


def pi2pi(angle):
    """ Ensures angle is in the interval (-pi, pi]

    Args:
      angle (float): angle in radians

    Returns:
      newDir (float): angle in radians in interval (-pi, pi]
    """
    
    while True:
        
        if angle > np.pi:
            
            angle -= 2 * np.pi
            continue
        
        elif angle < -np.pi or np.isclose(angle, -np.pi):
            
            angle += 2 * np.pi
            continue
        
        break

    return angle


def deg360_to_radpi(bearing):
    """converts from 0-to-360 deg (North = 0, East = 90) to -pi-to-pi radian
    (North = pi/2; East = 0)

    Args:
      dir (float): angle in degree

    Returns:
      newDir (float): angle in radian

    """
    angle = np.radians(np.mod(90 - bearing, 360))
    angle = pi2pi(angle)
    
    return angle


def transec_surf(hydro, array, debug=True):
    """
    Computes transect surface (m2) and first row of the array

    Args:
      hydro (dtocean_tidal.main.Hydro): dtocean_tidal's Hydro object
      array (dtocean_tidal.main.Array): dtocean_tidal's Array object

    Kwargs:
      debug (bool): debug flag

    Returns:
      transect (float): lease's transect surface (m2), float
      first_row (list): first row of the array, list of ID numbers
      speed (float): averaged speed over transect (m/s), float

    """
    if debug: module_logger.info("Computing relative blockage ratio RBR...")
    Nturb = len(array.positions.keys())
    (xm, ym, xM, yM) = hydro.lease.bounds
    l = []
    ref = np.arange(Nturb)

    # In case there is only one turbine
    if Nturb == 1:
        l = list(array.positions.keys())[7:]  # removing 'turbine' from key
        # TODO: code not working when only one turbine in the array
    else:
        # TODO: check the distance from stream line calculation
        for k1 in array.distances.keys():
            for k2 in array.distances[k1].keys():
                # find turbine away from 1 diam
                diam = array.features[k1]['Diam']
                if array.distances[k1][k2][0] > diam:  # check distance along streamline
                    l.append(k2)

    # unique occurrence
    L = np.asarray(l).astype(int)
    un = np.unique(L)
    # first row composed of
    first_row = list(set(ref)-set(un))
    # print('first row', first_row, 'L', L)

    # Fix for odd cases/flows where no obvious first raw
    # First_row = to turbine with the least interactions (n.b.: first raw should not have any)
    if first_row == []:
        unique, counts = np.unique(L, return_counts=True)
        first_row = list(unique[np.where(counts == counts.min())])

    # check for missing turbine in the first row within a diam radius
    iterlist = first_row[:]
    for ii in iterlist:
        for jj in range(Nturb):
            diam = array.features['turbine'+str(ii)]['Diam']
            xdist = np.abs(array.positions['turbine'+str(ii)][0] - array.positions['turbine'+str(jj)][0])
            ydist = np.abs(array.positions['turbine'+str(ii)][1] - array.positions['turbine'+str(jj)][1])
            if np.sqrt(xdist**2.0 + ydist**2.0) < diam:
                first_row.append(jj)
    first_row = np.unique(first_row)

    # define linear function representative on the 1st row's ray line
    x1 = array.positions['turbine'+str(first_row[0])][0]
    y1 = array.positions['turbine'+str(first_row[0])][1]
    x2 = array.positions['turbine'+str(first_row[-1])][0]
    y2 = array.positions['turbine'+str(first_row[-1])][1]
    a = np.array([[x1,1], [x2,1]])
    b = np.array([y1,y2])
    try:
        coeffs = np.linalg.solve(a, b)
        ybm = coeffs[0] * xm + coeffs[1]
        ybM = coeffs[0] * xM + coeffs[1]
        xbm = xm
        xbM = xM
    except LinAlgError:  # error when row parallel of perpendicular to x axis
        if x1 == x2:
            ybm = ym
            ybM = yM
            xbm = x1
            xbM = x1
        if y1==y2:
            ybm = y1
            ybM = y1
            xbm = xm
            xbM = xM

    # define lineString within lease
    box = hydro.lease
    line = LineString([[xbm,ybm],[xbM,ybM]])
    inter_line = box.intersection(line)

    # calculate ratio between rotors surface and transect
    diam = array.features['turbine'+str(first_row[-1])]['Diam']
    n = inter_line.length//diam
    if n > 100: n = 100  # PATCH: if the lease area is large the number of points become to high. The calculation become then unfeasible.
    dl = inter_line.length / (n-1)
    pts = map(inter_line.interpolate, np.arange(n) * dl)  # define points uniformly along line
    #  interpolate bathymetry at those points
    Q = [hydro.SSH, hydro.bathy, hydro.U, hydro.V]
    wh = 0.0  # water column height
    speed = 0.0  # speed over transect
    for pt in pts:
        [el, h, u, v] = interp_at_point(pt.x, pt.y, hydro.X, hydro.Y, Q)
        # Don't allow NaNs
        if np.isnan([el, h, u, v]).any(): continue
        # quantity formatting
        if h < 0.0: h *= -1.0
        wh += (h + el)
        speed += np.sqrt(u**2.0 + v**2.0)
    # relative blockage ratio
    transect = (wh / n) * inter_line.length  # average transect surface
    speed = speed / n

    return transect, first_row, speed


# if __name__ == "__main__":
#     from dtop_energycapt import business
#     ecID = 5
#     name = "test_project"

#     array_layout = [[0, 0], [100, 50], [0, 100]]
#     inputs = {
#         'nb_devices': 3,
#         'opt_method': 'cmaes',
#         'optimisation_threshold': 0.8,
#         'array_generic_type': 'rectangular',
#         'ratedPowerArray': 5000,
#         'distance_min': (10, 10),
#         'nogo_areas': None,
#         'array_layout': array_layout
#         } 
#     bathy = -30.0
#     xmax = 840
#     ymax = 350
#     lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

#     x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
#     y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
#     X, Y = np.meshgrid(x, y)
#     nx = len(x)
#     ny = len(y)
#     BR = 1.0  # blockage ratio

#     umax = 5.0  # maximum velocity in the X direction
#     vmax = 0.0  # maximum velocity in the Y direction
#     sshmax = 0.0  # maximum sea surface elevation
#     timax = 0.1
#     bathy = -30.0

#     U = np.ones(X.shape) * umax
#     V = np.ones(X.shape) * vmax
#     SSH = np.ones(X.shape) * sshmax
#     TI = np.ones(X.shape) * timax
#     BATHY = np.ones(X.shape) * bathy
#     PLE = 1.0 * np.ones(X.shape)
#     # bed roughness coefficient here
#     manning = np.ones(X.shape) * 0.001
#     site = {}
#     site['TI'] = [TI.tolist()]
#     site['PLE'] = PLE.tolist()
#     site['geophy'] = manning.tolist()
#     site['X'] = x.tolist()  # save only 1D array as structured grid assumed
#     site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
#     site['U'] = [U.tolist()]
#     site['V'] = [V.tolist()]
#     site['SSH'] = [SSH.tolist()]
#     site['bathy'] = BATHY.tolist()
#     site['BR'] = BR
#     site['lease'] = lease.tolist()
#     site['probability'] = [1.0]

#     ## Turbines positions
#     bathy = -30.0
#     z = bathy / 2.0  # hub height/depth
#     diam = 5
#     array_layout = [[0, 0], [100, 50], [0, 100], [300,70]]

#     ## Turbines features
#     cut_in = 0.0  # cut-in speed
#     cut_out = 10.0  # cut-out speed
#     # actual turbine features
#     speed = np.arange(0.0, 10.0, 0.2)

#     CT = np.ones(speed.shape) * 0.76
#     Ct = [speed.tolist(), CT.tolist()]  # thrust curve

#     CP = np.ones(speed.shape) * 0.3
#     Cp = [speed.tolist(), CP.tolist()]  # Power curve
#     feat = {}

#     feat = {}
#     feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
#     feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
#     feat['Cp'] = Cp[:]  # Power curve
#     feat['Ct'] = Ct[:]  # thrust curve
#     feat['Diam'] = diam  # Diam = rotor diameter (m)
#     feat['cutIO'] = [cut_in, cut_out]
#     feat[
#         'floating'] = False  # so hub height will be considered from the seabottom upwards
#     feat['2way'] = False  # turbines work in both direction.
#     feat['Rating'] = 5e6  # turbines work in both direction.

#     inputs.update(site)
#     inputs.update(feat)
#     ARRAY = business.ArrayFactory.get_array('TEC', "3")
#     ARRAY.get_inputs(name, ecID, **inputs)
#     inputs['nb_devices'] = 1
#     inputs['array_layout'] = [[0,0]]
#     inputs['floating'] = True
#     inputs['2way'] = True
#     ARRAY.get_inputs(name, ecID, **inputs)

#     layout = array_layout
#     n_bodies = len(layout)
#     z = 15
#     coords = {}

#     for ii in range(n_bodies):
#         coords[f'turbine{ii}'] = {}
#         coords[f'turbine{ii}']['position'] = np.asarray(
#             (layout[ii][0], layout[ii][1], -z))
#     features = {}
#     for key in coords.keys():
#         features[key] = feat



#     hydro = business.arraytecstg3.ArrayTECstg3.Hydro(ARRAY.i_site_conditions, 0)
#     array = business.arraytecstg3.array.Array(hydro, coords, features,debug=True,debug_plot=True)

#     business.arraytecstg3.utils.misc.transec_surf(hydro, array)

#     single_coords = {} 
#     single_coords['turbine0'] = coords['turbine0']
#     array = business.arraytecstg3.array.Array(hydro, single_coords, features,debug=True,debug_plot=True)

#     business.arraytecstg3.utils.misc.transec_surf(hydro, array)

#     layout = [[-100, 0], [100, 100], [300, -1000]]
#     n_bodies = len(layout)
#     coords = {}

#     for ii in range(n_bodies):
#         coords[f'turbine{ii}'] = {}
#         coords[f'turbine{ii}']['position'] = np.asarray(
#             (layout[ii][0], layout[ii][1], -z))
#     features = {}
#     for key in coords.keys():
#         features[key] = feat
#     array = business.arraytecstg3.array.Array(hydro, coords, features,debug=True,debug_plot=True)
#     business.arraytecstg3.utils.misc.transec_surf(hydro, array)