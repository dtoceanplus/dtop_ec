# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#!/usr/bin/python2.7
# encoding: utf-8

from __future__ import division
from pkg_resources import get_distribution

#Local import
from .array_yield import ArrayYield
from .hydro_impact import HydroImpact
from .streamline import Streamlines
from .vertical_velocity_profile import vvpw

#Credentials
__version__ = 'v0.1'
__authors__ = ['DTOcean Tidal Developers']
__licence__ = 'GNU Affero GPL v3.0'
__copyright__ = 'Copyright (c) 2014 DTOcean'

