# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from __future__ import division

# Start logging
import logging
module_logger = logging.getLogger(__name__)

import time
# from shapely.geometry import Polygon
import numpy as np

# Local import
from dtop_energycapt.business.arraytecstg3.utils import distance_from_streamline
from dtop_energycapt.business.arraytecstg3.utils.interpolation import interpol_scatter2grid, interp_at_point
from dtop_energycapt.business.arraytecstg3.utils.misc import pi2pi, deg360_to_radpi, vector_to_bearing
from dtop_energycapt.business.arraytecstg3.modules.vertical_velocity_profile import vvpw

from dtop_energycapt.business.arraytecstg3.modules import Streamlines, ArrayYield, HydroImpact
from dtop_energycapt.business.arraytecstg3.submodel.WakeInteraction import WakeInteraction

class Array:
    """
    Gathers site data & Hydrodynamic conditions, turbine coordinates & features
    Computes streamlines, vertical profiles & yaw angles at hub heights

    Args:
      hydro(dtocean_tidal.main.Hydro) hydro class object
      turbines (dict): dictionnary gathering all turbines' positions
      features (dict): dictionnary gathering all turbines' features

    kwargs:
      debug (bool): output additional info. during runtime
      debug_plot (bool): plot additional info. during runtime

    Attributes:
      positions (dict): dictionary with turbine's ID as key, gathers turbine's coordinates
        ex: Array.positions[ID] = [x,y,z], 1d numpy array
      features (dict): dictionary gathering turbine's coordinates with the following keys:
          . HAS = heading angle span (deg.), float
          . RY = yaw angle relative to incoming flow (deg.), float
          . OA = orientation angle (deg.), float
          . Cp = power curve, velocity vs Cp, (2,n) array, [vel., Cp]
          . Ct = thrust curve, velocity vs Ct, (2,n) array, [vel., Ct]
          . Diam = turbine diameter, m, float
          . cutIO = cut-in and cut-out speed, m/s, [float,float]
          . floating = floating or not, boolean
          . 2way = working both ways or not, boolean
      streamlines (dict): dictionary with turbine's ID as key, gathers turbine's streamline
        ex: Array.streamlines[ID] = [X,Y] where X and Y are lists of coordinates
      distances (dict): dictionary with turbine's ID as key, gathers turbine's Relative
        distances from streamline to one another
      velHub (dict): dictionary with turbine's ID as key, gathers turbine's flow speed at hub

    """
    def __init__(self, hydro,
                       turbines,
                       features,
                       debug=True,
                       debug_plot=False):
        """ Initialise Array class"""
        self._turbine_count = len(turbines.keys())
        #Turn turbine positions into one big matrix
        self.positions = {}
        # print('DEBUG')
        for i in range(self._turbine_count):
            p = turbines['turbine' + str(i)]['position']
            if i == 0:
                positions = p
            else:
                positions = np.vstack((positions, p))
            self.positions['turbine' + str(i)] = p            
        # print('DEBUG')
        #Turn turbine features into one big matrix
        self.features = {}
        for i in range(self._turbine_count):
            self.features['turbine' + str(i)] = features['turbine' + str(i)]
        # print('DEBUG')
        # Streamlines
        #  generate regular structured grid U, V, X, Y to pass on to Streamlines
        data = {}
        x, y = np.meshgrid(hydro.X, hydro.Y)
        # x, y = hydro.X, hydro.Y
        xn = hydro.X.shape[0]
        yn = hydro.Y.shape[0]
        # print('DEBUG')
        data['X'], data['Y'], data['U'] = interpol_scatter2grid(
                                                            x.flatten(),
                                                            y.flatten(),
                                                            hydro.U.flatten(),
                                                            xn,
                                                            yn, 
                                                            debug=debug)
        # print('DEBUG')
        data['X'], data['Y'], data['V'] = interpol_scatter2grid(
                                                            x.flatten(),
                                                            y.flatten(),
                                                            hydro.V.flatten(),
                                                            xn,
                                                            yn,
                                                            debug=debug)
        # In case there is only one turbine in the array
        # print('DEBUG')
        if not self._turbine_count == 1:
            #  computes streamlines
            SLs = Streamlines(data,
                              positions,
                              self._turbine_count,
                              debug=debug)
            # if debug_plot: 
            #     SLs.plot(self._turbine_count)
            #     plt.show()

            #Relative distance from streamlines
            self.streamlines={}
            self.distances={}
            # print('DEBUG')
            for i in range(self._turbine_count):
                if debug:
                    module_logger.info("Streamline nb: {}".format(i))
                streamline = SLs.streamlines[i]
                self.streamlines['turbine' + str(i)] = streamline
                self.distances['turbine' + str(i)] = \
                            distance_from_streamline(streamline,
                                                     positions,
                                                     debug=debug,
                                                     debug_plot=debug_plot)
            
            # if debug_plot:
            #     SLs.plot(self._turbine_count)
            #     plt.show()
                
        else:
            
            if debug: module_logger.info("Single turbine...no streamlines")

        # Velocity and TI at hub
        # print('DEBUG')
        self.velHub = {}
        self.velHubIni = {}  # save initial velocity for computing Q factor
        if debug: module_logger.info("Computing Hub velocities...")
        for i in range(self._turbine_count):
            if debug: module_logger.info("Interpolating quantities...")
            Q = [hydro.U,
                 hydro.V,
                 hydro.SSH,
                 hydro.bathy,
                 hydro.geophy,
                 hydro.PLE,
                 hydro.TI]
            [u, v, el, h, n, ple, ti] = interp_at_point(
                                        self.positions['turbine' + str(i)][0],
                                        self.positions['turbine' + str(i)][1],
                                        hydro.X, hydro.Y, Q)
            # quantity formatting
            if h < 0.0: h *= -1.0
            #  Hub height account for floating options
            hh = self.positions['turbine' + str(i)][2]  # hub height, negative by convention
            if hh > 0.0: hh *= -1.0
            if self.features['turbine' + str(i)]['floating']:
                z = (el + h) + hh
            else:
                z = -hh
            # Computing velocity vertical profile weight
            radius = self.features['turbine' + str(i)]['Diam'] / 2.0
            wTop = vvpw(u, v, z+radius, el, h, n, debug=debug)
            wBottom = vvpw(u, v, z-radius, el, h, n, debug=debug)
            # TR: alternative using Soulsby formulation
#            wTop = vvpw_soulsby(z+radius, el, h, n, ple, debug=debug)
#            wBottom = vvpw_soulsby(z-radius, el, h, n, ple, debug=debug)
            w = (wTop + wBottom) / 2.0  # Linear interpolation along the vertical
            self.velHub['turbine' + str(i)] = np.array([u * w, v * w])
            self.velHubIni['turbine' + str(i)] = np.array([u * w, v * w])
            # Compute TIH, tubulence intensity at hub (%)
            # TR: assuming TKE uniform throughout the water column height and TI = sqrt(2/3 * k) / U
            wTI = 1/w # TR: due to above assumption
            self.features['turbine' + str(i)]['TIH'] = wTI * ti

        # Computes actual yawing angles
        # Convention: angle between -pi and pi, 0 of trigonometric circle coincides with East
        if debug: module_logger.info("Computing yawing angles...")
        for i in range(self._turbine_count):
            # absolute angle
            bearing = vector_to_bearing(u, v)
            absAngle = pi2pi(deg360_to_radpi(bearing) - np.pi)
            # -pi in order to test turbine aligned with flow rather than facing flow
            turbDir = deg360_to_radpi(self.features['turbine' + str(i)]['OA'])
            turbSpan = np.abs(
                    np.radians(self.features['turbine' + str(i)]['HAS']))/2.0
            if turbSpan < np.pi:  # case where turbine rotates 360.0 degrees
                # angle between -pi and pi
                turbDir = pi2pi(turbDir)
                # define intervals
                inter1 = self._interval(turbDir, turbSpan)
                # check if 2way turbine and define additional interval if needed
                if self.features['turbine' + str(i)]['2way']:
                    turbDir2 = pi2pi(turbDir + np.pi)
                    inter2 = self._interval(turbDir2, turbSpan)
                # check if flow direction with interval
                inFlag = False
                if type(inter1)==list:
                    if inter1[0][0]<=absAngle<=inter1[0][1]: inFlag=True
                    if inter1[1][0]<=absAngle<=inter1[1][1]: inFlag=True
                else:
                    if inter1[0]<=absAngle<=inter1[1]: inFlag=True
                if self.features['turbine' + str(i)]['2way']:
                    if type(inter2)==list:
                        if inter2[0][0]<=absAngle<=inter2[0][1]: inFlag=True
                        if inter2[1][0]<=absAngle<=inter2[1][1]: inFlag=True
                    else:
                        if inter2[0]<=absAngle<=inter2[1]: inFlag=True
            else:
                inFlag=True
            # compute relative yaw, RY
            if inFlag:
                self.features['turbine' + str(i)]['RY'] = 0.0
            else:
                bounds = np.array(inter1).flatten()
                if self.features['turbine' + str(i)]['2way']:
                    bounds = np.hstack((bounds, np.array(inter2).flatten()))
                #  find closest bound
                ry = self._diff_rads(absAngle, bounds)
                # relative yaw angle in deg., RY
                self.features['turbine' + str(i)]['RY'] = np.degrees(ry.min())
            if debug:
                logMsg = ("Relative yawing angle for turbine{} = "
                          "{}").format(i, 
                                       self.features['turbine' + str(i)]['RY'])                                       
                module_logger.info(logMsg)

    def _interval(self, turbDir, turbSpan):
        """
        define angle interval

        Args:
          turbDir (float): turbine's orientation
          turbSpan (float): turbine's yawing span

        Returns:
          inter1 (list): yawing angle's interval

        """
        inter1 = np.array([turbDir - turbSpan, turbDir + turbSpan])
        # angle between -pi and pi
        if np.any(inter1 >= np.pi):
            inter1 = [np.array([turbDir - turbSpan, np.pi]),
                     np.array([-np.pi, (turbDir + turbSpan) - 2.0*np.pi])]
        elif np.any(inter1 <= -np.pi):
            inter1 = [np.array([-np.pi, turbDir + turbSpan]),
                     np.array([(turbDir - turbSpan) + 2.0*np.pi, np.pi])]
        return inter1
    
    def _diff_rads(self, angle1, angles):
        
        if angle1 < 0: angle1 += 2 * np.pi
        
        diff = []
        
        for angle2 in angles:
            
            if angle2 < 0: angle2 += 2 * np.pi
            diff.append(abs(angle1 - angle2))
        
        return np.array(diff)
