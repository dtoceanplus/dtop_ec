# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

#    Copyright (C) 2016 Thomas Roc, Mathew Topper
#    Copyright (C) 2017-2018 Mathew Topper
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division

import logging

import numpy as np
from shapely.geometry import Polygon, Point, LineString
from shapely.geos import TopologicalError
from scipy.interpolate import interp1d

# Local import
from dtop_energycapt.business.arraytecstg3.utils.misc import line, intersection, closest_point
from dtop_energycapt.business.arraytecstg3.utils.interpolation import interp_at_point

# Start logging
module_logger = logging.getLogger(__name__)


class Wake:
    """
    Wake class computes the induction factor for any distance (x,y) and any set of turbine parameters

    Args:
      data_reader (fortran): database reader
      turbParams (dict): turbine's parameters
      BR (float): blockage ratio, float

    Attributes:
      indFac (numpy.array or list): induction factor, either 3D array (Ny, Nx, 2) or 2D array (Nturb, 2) or list [x,y]
      VBR (float): vertical blockage ratio
      TIH (float): turbulence intensity at hub (%)
      YA (float): yaw angle (deg.)
      Ct (numpy.array): thrust coefficient
      A (float): rotor diameter (m2)
      velHub (list): velocity components at hub, [u, v], float list

    """
    #Class counter to zero
    #ID = 0

    def __init__(self, dataframe, turbParams, BR, debug=True):
        self._debug = debug
        # Sanity checks on...
        # ...blockage ratio
        if BR < 0.0:
            self.BR = 0.0
        elif BR > 0.7:
            self.BR = 0.7
            module_logger.debug("Blockage ratio > 70%... reaching model's "
                                "theoretical limit.")
        else:
            self.BR = BR
        # ...relative yawing angle
        ry = np.radians(turbParams['RY'])
        if ry < 0.0:
            self.RY = ry * -1.0
        elif ry > np.radians(89.0):
            module_logger.info("Relative angle with inflow > 90.0 deg... "
                                "reaching model's theoretical limit.")
            self.RY = ry
        else:
            self.RY = ry
        self.TIH = turbParams['TIH']
        self.Ct = interp1d(turbParams['Ct'][0], turbParams['Ct'][1], bounds_error=False, fill_value=0.0)
        self.Diam = turbParams['Diam']
        self.cutIn = turbParams['cutIO'][0]
        self.cutOut = turbParams['cutIO'][1]
        self._df = dataframe
        # database values
        self._cts = np.copy(self._df['cts'][:])
        self._tis = np.copy(self._df['tis'][:])
        self._cts.sort()
        self._tis.sort()
        # Real distance Empirical relationship for vertical blockage ratio and yawing
        ratio = 0.489*self.BR**2.0 - 1.472*self.BR + 1.0
        self._dfx = self._df['dfX'] * ratio * self.Diam
        self._dfy = self._df['dfY'] * np.cos(ry) * self.Diam

        return

    def read_at_point(self, x, y, ct, ti, debug=True):
        """
        Interpolates CFD datasets stored in self._df based on Ct and TI values
        Args:
          x (float): relative distance along streamline, m
          y (float): relative distance across streamline, m
          ct (float): turbine's thrust coefficient, dimensionless
          ti (float): turbulence intensity at hub's location, dimensionless [0 ; 1]

        Returns:
          u (numpy array): 2D array, flow field, m/s
          tke (numpy array): 2D array, TKE filed, m2/s2
        """

        debug = debug or self._debug
        if debug: module_logger.info("Picking CFD datasets...")

        # Looking through available Ct's and TI's values
        ctbounds = []
        for ss, ee in zip(self._cts[:-1], self._cts[1:]):
            if ss <= ct <= ee:
                ctbounds = [ss, ee]

        tibounds = []
        for ss, ee in zip(self._tis[:-1], self._tis[1:]):
            if ss <= ti <= ee:
                tibounds = [ss, ee]

        # If requested values outside of range
        if ctbounds == []:
            module_logger.debug(("Requested Ct value {} is not covered by the "
                                 "current CFD database.").format(ct))
            
            if ct > self._cts.max():
                ctbounds =  [self._cts[-2], self._cts[-1]]
            else:
                ctbounds =  [self._cts[0], self._cts[1]]

            # idx = (np.abs(self._cts - ct)).argmin()
            # try:
            #     ctbounds = [self._cts[idx-1], self._cts[idx]]
            # except IndexError:
            #     ctbounds = [self._cts[idx], self._cts[idx + 1]]
            module_logger.debug(("Ct value of {} will be "
                                 "used.").format(ctbounds[0]))

        if tibounds == []:
            module_logger.debug(("Requested TI value {} is not covered by "
                                 "the current CFD database.").format(ti))
            # idx = (np.abs(self._tis - ti)).argmin()

            if ti > self._tis.max():
                tibounds =  [self._tis[-2], self._tis[-1]]
            else:
                tibounds =  [self._tis[0], self._tis[1]]
            # try:
            #     tibounds = [self._tis[idx-1], self._tis[idx]]
            # except IndexError:
            #     tibounds = [self._tis[idx], self._tis[idx+1]]
            module_logger.debug(("TI value of {} will be "
                                 "used.").format(tibounds[0]))

        # compute distance
        dist = np.zeros(4)
        dist[0] = (ct - ctbounds[0])**2.0 + (ti - tibounds[0])**2.0
        dist[1] = (ct - ctbounds[0])**2.0 + (ti - tibounds[1])**2.0
        dist[2] = (ct - ctbounds[1])**2.0 + (ti - tibounds[0])**2.0
        dist[3] = (ct - ctbounds[1])**2.0 + (ti - tibounds[1])**2.0
        mloc = dist.argmax()

        # computing weights
        nn = 0
        atmp = np.zeros(3)
        btmp = np.zeros(3)
        for n in range(4):
            if not n == mloc:
                if n==0:
                    atmp[nn] = ctbounds[0]
                    btmp[nn] = tibounds[0]
                elif n==1:
                    atmp[nn] = ctbounds[0]
                    btmp[nn] = tibounds[1]
                elif n==2:
                    atmp[nn] = ctbounds[1]
                    btmp[nn] = tibounds[0]
                elif n==3:
                    atmp[nn] = ctbounds[1]
                    btmp[nn] = tibounds[1]
                nn += 1

        wght = np.ones(3)
        wght[0] =(btmp[1]-btmp[2])*(ct-atmp[2]) + (atmp[2]-atmp[1])*(ti-btmp[2])
        wght[1]=(btmp[2]-btmp[0])*(ct-atmp[2]) + (atmp[0]-atmp[2])*(ti-btmp[2])
        wght[0:1] = wght[0:1] / ((btmp[1]-btmp[2])*(atmp[0]-atmp[2]) + (atmp[2]-atmp[1])*(btmp[0]-btmp[2]))
        wght[2] = 1.0 - wght[0] - wght[1]

        X = self._dfx
        Y = self._dfy

        if debug: module_logger.info("...interpolation...")
        u = 0.0
        v = 0.0
        tke = 0.0
        for n in range(3):
            tmpCt = atmp[n]
            tmpTi = btmp[n]
            umat = self._df['dfU']['ti'+str(tmpTi)]['ct'+str(tmpCt)] * wght[n]
            vmat = self._df['dfV']['ti'+str(tmpTi)]['ct'+str(tmpCt)] * wght[n]
            tkemat = self._df['dfTKE']['ti'+str(tmpTi)]['ct'+str(tmpCt)] * wght[n]
            tmp = interp_at_point(x, y, X, Y, [umat.T, vmat.T, tkemat.T])
            u += tmp[0]
            v += tmp[1]
            tke += tmp[2]

        return u, v, tke

    def ind_fac(self, distance, velHub, tiHub, debug=True):
        """Computing induction factor 1-(U/Uinf) and T.I.

        Args:
          distance (numpy.array or list): along and across distances to hub axis (m), either 3D array (Ny, Nx, 2)
          or 2D array (Nturb, 2) or list [x,y]
          velHub (list): velocity components at hub, [u, v], float list
          tiHub (float): turbulence intensity at hub (%)

        Kwargs:
          debug (bool): debug flag

        Returns:
          ind (float): induction factor
          newTI (float): new turbulence intensity

        """

        debug = debug or self._debug

        # Various param.
        #  Distance variables
        x = np.asarray(distance[0])
        y = np.asarray(distance[1])
        #  Velocity norm
        norm = np.sqrt((velHub[0]**2.0) + (velHub[1]**2.0))
        Ct = self.Ct(norm)
        # TODO: this was removed because the interpolant error is set to False
        # try:
        #     Ct = self.Ct(norm)
        # except ValueError:
        #     if debug: module_logger.info("Flow speed value outside of Ct's "
        #                                  "curve range. Ct will be set to 0.0")
        #     # Use nearest value
        #     # idSpeed = np.argmin(np.abs(self.Ct.x - norm))
        #     # Ct = self.Ct.y[idSpeed]
        #     Ct = 0.0

        ry = np.radians(self.RY)

        # Checking bounds
        if Ct > self._cts[-1]:
            Ct = self._cts[-1]
            module_logger.debug(("Thrust coefficient > {}... reaching "
                                 "parametric database's "
                                 "limit.").format(self._cts[-1]))
        if Ct < self._cts[0]:
            module_logger.debug(("Thrust coefficient < {}... reaching "
                                 "parametric database's "
                                 "limit.").format(self._cts[0]))
        if tiHub < self._tis[0]:
            tiHub = self._tis[0]
            module_logger.debug(("T.I. < {}... reaching "
                                 "parametric database's "
                                 "limit.").format(self._tis[0]))
        if tiHub > self._tis[-1]:
            tiHub = self._tis[-1]
            module_logger.debug(("T.I. > ... reaching "
                                 "parametric database's "
                                 "limit.").format(self._tis[-1]))

        # call fortran routine to average the nearby solutions
        X = self._dfx
        Y = self._dfy
        if (Ct > np.min(self._cts) and ry < np.radians(89.0)) and \
           (X.min() < x < X.max()) and (Y.min() < y < Y.max()):  # test if within values and spatial bounds
            # Linear interpolation
            # Retrieve u, tke, x, y at point
            u, v, tke = self.read_at_point(x, y, Ct, tiHub, debug=debug)
            # Induction factor
            # TR: scheme 1
            indFac = np.sqrt(u**2.0 + v**2.0)
            newTKE = tke
        else:  # need to keep the option to set Ct = 0 to "mute" device
            if debug: module_logger.debug("Turbine is mute")
            indFac = 1.0
            newTKE = np.nan

        return indFac, newTKE

# if __name__ == "__main__":
#     from dtop_energycapt import business
#     from dtop_energycapt.business.arraytecstg3.create_wake_db import create_wake_db

#     ecID = 5
#     name = "test_project"

#     array_layout = [[0, 0], [100, 50], [0, 100]]
#     inputs = {
#         'nb_devices': 3,
#         'opt_method': 'cmaes',
#         'optimisation_threshold': 0.8,
#         'array_generic_type': 'rectangular',
#         'ratedPowerArray': 5000,
#         'distance_min': (10, 10),
#         'nogo_areas': None,
#         'array_layout': array_layout
#         } 
#     bathy = -30.0
#     xmax = 840
#     ymax = 350
#     lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

#     x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
#     y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
#     X, Y = np.meshgrid(x, y)
#     nx = len(x)
#     ny = len(y)
#     BR = 1.0  # blockage ratio

#     umax = 5.0  # maximum velocity in the X direction
#     vmax = 0.0  # maximum velocity in the Y direction
#     sshmax = 0.0  # maximum sea surface elevation
#     timax = 0.1
#     bathy = -30.0

#     U = np.ones(X.shape) * umax
#     V = np.ones(X.shape) * vmax
#     SSH = np.ones(X.shape) * sshmax
#     TI = np.ones(X.shape) * timax
#     BATHY = np.ones(X.shape) * bathy
#     PLE = 1.0 * np.ones(X.shape)
#     # bed roughness coefficient here
#     manning = np.ones(X.shape) * 0.001
#     site = {}
#     site['TI'] = [TI.tolist()]
#     site['PLE'] = PLE.tolist()
#     site['geophy'] = manning.tolist()
#     site['X'] = x.tolist()  # save only 1D array as structured grid assumed
#     site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
#     site['U'] = [U.tolist()]
#     site['V'] = [V.tolist()]
#     site['SSH'] = [SSH.tolist()]
#     site['bathy'] = BATHY.tolist()
#     site['BR'] = BR
#     site['lease'] = lease.tolist()
#     site['probability'] = [1.0]

#     ## Turbines positions
#     bathy = -30.0
#     z = bathy / 2.0  # hub height/depth
#     diam = 5
#     array_layout = [[0, 0], [100, 50], [0, 100], [300,70]]

#     ## Turbines features
#     cut_in = 0.0  # cut-in speed
#     cut_out = 10.0  # cut-out speed
#     # actual turbine features
#     speed = np.arange(0.0, 10.0, 0.2)

#     CT = np.ones(speed.shape) * 0.76
#     Ct = [speed.tolist(), CT.tolist()]  # thrust curve

#     CP = np.ones(speed.shape) * 0.3
#     Cp = [speed.tolist(), CP.tolist()]  # Power curve
#     feat = {}

#     feat = {}
#     feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
#     feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
#     feat['Cp'] = Cp[:]  # Power curve
#     feat['Ct'] = Ct[:]  # thrust curve
#     feat['Diam'] = diam  # Diam = rotor diameter (m)
#     feat['cutIO'] = [cut_in, cut_out]
#     feat[
#         'floating'] = False  # so hub height will be considered from the seabottom upwards
#     feat['2way'] = False  # turbines work in both direction.
#     feat['Rating'] = 5e6  # turbines work in both direction.

#     inputs.update(site)
#     inputs.update(feat)
#     ARRAY = business.ArrayFactory.get_array('TEC', "3")
#     ARRAY.get_inputs(name, ecID, **inputs)
#     inputs['nb_devices'] = 1
#     inputs['array_layout'] = [[0,0]]
#     inputs['floating'] = True
#     inputs['2way'] = True
#     ARRAY.get_inputs(name, ecID, **inputs)

#     layout = array_layout
#     n_bodies = len(layout)
#     z = 15
#     coords = {}

#     for ii in range(n_bodies):
#         coords[f'turbine{ii}'] = {}
#         coords[f'turbine{ii}']['position'] = np.asarray(
#             (layout[ii][0], layout[ii][1], -z))
#     features = {}
#     for key in coords.keys():
#         features[key] = feat



#     hydro = business.arraytecstg3.ArrayTECstg3.Hydro(ARRAY.i_site_conditions, 0)
#     array = business.arraytecstg3.array.Array(hydro, coords, features)

    
#     db = create_wake_db()
#     wake = business.arraytecstg3.submodel.ParametricWake.wakeClass.Wake(db, features['turbine0'], 0.5)
#     wake.read_at_point(10, 30, 1.001, 100)

    # wake.ind_fac(array.distances['turbine1']['3'][:],
    #              [2,0],
    #              array.features['turbine1']['TIH'])

    # wake.ind_fac(array.distances['turbine1']['3'][:],
    #              [1000,10000],
    #              array.features['turbine1']['TIH'])

    # feat = features['turbine0'].copy()
    # feat['Ct'][1] = [el+100 for el in feat['Ct'][1]]
    # wake = business.arraytecstg3.submodel.ParametricWake.wakeClass.Wake(db, feat, 0.5)
    # wake.ind_fac(array.distances['turbine1']['3'][:],
    #              [3,1],
    #              array.features['turbine1']['TIH'])     

    # wake.ind_fac(array.distances['turbine1']['3'][:],
    #              [3,1],
    #              -10)   
    # wake.ind_fac(array.distances['turbine1']['3'][:],
    #              [3,1],
    #              100)                                       