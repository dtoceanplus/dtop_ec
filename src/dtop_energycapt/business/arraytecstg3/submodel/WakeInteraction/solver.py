# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

#    Copyright (C) 2016 Thomas Roc
#    Copyright (C) 2017-2018 Mathew Topper
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division

import time
import logging

import numpy as np

# Local import
from dtop_energycapt.business.arraytecstg3.submodel.ParametricWake import Wake
from dtop_energycapt.business.arraytecstg3.modules.blockage_ratio import blockage_ratio

# Start logging
module_logger = logging.getLogger(__name__)


class WakeInteraction:
    """
    -WakeInteraction class-

    Computes wake interaction and their impacts of flow field

    Args:
      hydro (dtocean_tidal.main.Hydro): Hydro class/object
      array (dtocean_tidal.main.Array): Array class/object

    Kwargs:
      debug (bool): debug flag
      debug_plot (bool): debug plot flag

    Attributes:
      indMat (numpy.array): matrix of induction factors, float, (nb. device, nb. device)
      tiMat (numpy.array): matrix of turbulence intensity, float, (nb. device, nb. device)
      induction (numpy.array): turbines' resultant induction factors, float, (nb. device)
      inducedTI (numpy.array): turbines' resultant turbulence intensities, float, (nb. device)
      wake (dict): collection of dtocean_tidal.submodel.ParametricWake.Wake class objects for each device
      wakeShape (dict):  collection of dtocean_tidal.submodel.ParametricWake.WakeShape class objects for each device

    """

    def __init__(self, hydro, array, cfd_data, debug=True, debug_plot=False):

        self._turbine_count = len(array.positions.keys())
        self._debug = debug
        self._debug_plot = debug_plot
        self._hydro = hydro
        self._bounding_box = hydro.bounding_box
        self._array = array
        self.indMat = np.ones((self._turbine_count, self._turbine_count))
        self.tkeMat = np.zeros((self._turbine_count, self._turbine_count))
        self.induction = np.zeros(self._turbine_count)
        self.inducedTKE = np.zeros(self._turbine_count)
        self.wake = {}
        self.wakeShape = {}
        # compute global blockage ratio
        rbr = blockage_ratio(hydro, array, debug=debug)
        self.blockage = rbr * hydro.BR
        # Load the dataframe.
        self._df = cfd_data

        return

    def solv_induction(self, speed_superimpo='renkema',
                             tke_superimpo='max',
                             debug=True):
        """
        Compute the induction factor at each turbine hub
        based on each others interactions

        Kwargs:
          speed_superimpo (str): wake speed superimposition method
                                 it can be 'linear', 'geometric', 'rss', 'linrss', 'max', 'mean', 'renkema', 'prod', 'geosum'
          tke_superimpo (str): wake tke superimposition method
                                 it can be 'max', 'min', 'mean', 'sum', 'prod', 'rss'
          debug (bool): debug flag
          debug_plot (bool): debug plot flag
        """
        # Compute velocities at each turbine hub position
        debug = debug or self._debug

        # Initial speed and T.I. into two big matrix
        for i in range(self._turbine_count):
            p = self._array.velHub['turbine' + str(i)]
            speed = np.sqrt(p[0]**2.0 + p[1]**2.0)
            u = np.abs(p[0])
            v = np.abs(p[1])
            tke = 1.5 * (self._array.features['turbine' + str(i)]['TIH'] * speed)**2.0
            if i == 0:
                iniSpeed = speed
                iniTKE = tke
            else:
                iniSpeed = np.hstack((iniSpeed, speed))
                iniTKE = np.hstack((iniTKE, tke))

        # initialization Convergence criteria and other matrices
        newVel = np.zeros((2, self._turbine_count))
        newTKE = np.zeros(self._turbine_count)
        self.tkeMat[:] = np.resize(iniTKE, (self._turbine_count, self._turbine_count))

        if debug:
           module_logger.info("Querying parametric wake model...")
           start = time.time()
        for i in range(self._turbine_count):
            turb = 'turbine' + str(i)
            self.wake[turb] = Wake(self._df,
                                   self._array.features[turb],
                                   self.blockage,
                                   debug=self._debug)

            for turbNb in self._array.distances[turb].keys():
                self.indMat[i, int(turbNb)], self.tkeMat[i, int(turbNb)] = \
                self.wake[turb].ind_fac(self._array.distances[turb][turbNb][:],
                                        self._array.velHub[turb][:],
                                        self._array.features[turb]['TIH'],
                                        debug=self._debug)
        if debug:
            end = time.time()
            module_logger.info("...done after " + str(end - start) + " seconds.")
        # Cumulative induction: Sigma (1-U/Uinf)
        # TODO: perform validation against CFD to determine which method to choose
        speed_superimpo = speed_superimpo.lower()
        if not speed_superimpo in ['prod', 'linear', 'geometric', 'rss', 'linrss', 'max', 'renkema', 'geosum', 'mean']:
            module_logger.warning("Wrong superimposition method for momentum. Default value used")
            speed_superimpo = 'renkema'  # default method
        # ## Wake Interaction methods for intersection wakes cf. Palm 2011
        if speed_superimpo == 'linear':
            # Wake Inter. Method 1: linear superposition
            self.induction = np.sum(self.indMat,0)
            # filtering induction = 1.0 for summation
            indMat = self.indMat[:]
            indMat[self.indMat == 1.0] = 0.0
            # summation
            self.induction = np.sum(indMat, 0)
            # filtering back
            self.induction[np.where(self.induction == 0.0)] = 1.0
        elif speed_superimpo == 'geometric':
            # Wake Inter. Method 2: geometric superposition
            ratio = 1.0 - self.indMat
            # Filtering value = 0.0
            ratio[ratio == 0.0] = 1.0
            self.induction = 1.0 - np.prod(ratio,0)
            # filtering back
            self.induction[np.where(self.induction == 0.0)] = 1.0
        elif speed_superimpo == 'rss':
            # Wake Inter. Method 3: RSS of velocity deficits
            # filtering induction = 1.0 for summation
            indMat = self.indMat[:]
            indMat[self.indMat == 1.0] = 0.0
            # summation
            self.induction = np.sqrt(np.sum(indMat**2.0, 0))
            # filtering back
            self.induction[np.where(self.induction == 0.0)] = 1.0
        elif speed_superimpo == 'linrss':
            # Wake Inter. Method 4: average of RSS & linear superposition
            # filtering induction = 1.0 for summation
            indMat = self.indMat[:]
            indMat[self.indMat == 1.0] = 0.0
            # summation
            urss = np.sqrt(np.sum(indMat**2.0,0))
            uls = np.sum(indMat,0)
            self.induction = (urss + uls) / 2.0
            # filtering back
            self.induction[np.where(self.induction == 0.0)] = 1.0
        elif speed_superimpo == 'max':
            # Wake Inter. Method 5: Maximum wake deficit
            self.induction = self.indMat.max(axis=0)
        # ## Wake Superposition method cf. Renkema 2007
        elif speed_superimpo == 'renkema':
            # Wake Inter. Method 3: RSS of velocity deficits
            c = 1.0 - self.indMat
            self.induction = 1.0 - np.sqrt(np.sum(c**2.0, 0))
        # ## Wake Interaction methods for wake superimposition Thomas Roc
        elif speed_superimpo == 'prod':
            # Wake Inter. Method 1: simple multiplication of induction
            self.induction = np.prod(self.indMat, 0)
        elif speed_superimpo == 'geosum':
            # Wake Inter. Method 2: geometric superposition type
            ratio = 1.0 - self.indMat
            self.induction = 1.0 - np.sum(ratio, 0)
        elif speed_superimpo == 'mean':
            # Wake Inter. Method 6: Mean wake deficit# filtering induction = 1.0 for summation
            indMat = self.indMat[:]
            indMat[indMat == 1.0] = np.nan
            # summation
            self.induction = np.nanmean(self.indMat, axis=0)
            # filtering back
            self.induction[np.where(np.isnan(self.induction))] = 1.0

        tke_superimpo = tke_superimpo.lower()
        if not tke_superimpo in ['prod', 'min', 'rss', 'max', 'sum', 'mean']:
            module_logger.warning("Wrong superimposition method for tke. Default value used")
            tke_superimpo = 'max'  # default method
        # ##Wake Interaction methods for TKE superimposition Thomas Roc
        if tke_superimpo == 'max':
            # Wake Inter. Method 1: simple maximum of TKE
            self.inducedTKE = np.nanmax(self.tkeMat, axis=0)
        elif tke_superimpo == 'min':
            # Wake Inter. Method 2: simple average of TKE
            self.inducedTKE = np.nanmin(self.tkeMat, axis=0)
        elif tke_superimpo == 'mean':
            # Wake Inter. Method 2: simple average of TKE
            self.inducedTKE = np.nanmean(self.tkeMat, axis=0)
        elif tke_superimpo == 'sum':
            # Wake Inter. Method 3: simple sum of TKE
            self.inducedTKE = np.nansum(self.tkeMat, axis=0)
            self.inducedTKE[np.where(self.inducedTKE == 0.0)] = np.nan  # empty slice case
        elif tke_superimpo == 'prod':
            # Wake Inter. Method 4: simple prod of TKE
            self.inducedTKE = 1.0 - np.nanprod(1.0 + self.tkeMat, axis=0)
            self.inducedTKE[np.where(self.inducedTKE == 1.0)] = np.nan  # empty slice case
        elif tke_superimpo == 'rss':
            # Wake Inter. Method 3: RSS of tke
            self.inducedTKE = np.sqrt(np.nansum(self.tkeMat**2.0, 0))
            self.inducedTKE[np.where(self.inducedTKE == 0.0)] = np.nan  # empty slice case

        # iteratively compute and re-assign velocity and T.I. at hub
        for i in range(self._turbine_count):
            turb = 'turbine' + str(i)
            newVel[:,i] = self._array.velHub[turb][:] * self.induction[i]
            self._array.velHub[turb][:] = newVel[:, i]
            speed = np.sqrt(newVel[0, i]**2.0 + newVel[1, i]**2.0)
            if not np.isnan(self.inducedTKE[i]):
                newTKE[i] = self.inducedTKE[i]
            else:
                newTKE[i] = 1.5 * (self._array.features[turb]['TIH'] * speed)**2.0
            self._array.features[turb]['TIH'] = np.sqrt((2.0/3.0) * newTKE[i]) / speed

        # Recompute induction factor based on result speed / initial speed
        for i in range(self._turbine_count):
            p = self._array.velHub['turbine' + str(i)]
            speed = np.sqrt(p[0]**2.0 + p[1]**2.0)
            if i == 0:
                resSpeed = speed
            else:
                resSpeed = np.hstack((resSpeed, speed))
        self.induction = resSpeed / iniSpeed

# if __name__ == "__main__":
#     from dtop_energycapt import business
#     from dtop_energycapt.business.arraytecstg3.create_wake_db import create_wake_db
#     ecID = 5
#     name = "test_project"

#     array_layout = [[0, 0], [100, 50], [0, 100]]
#     inputs = {
#         'nb_devices': 3,
#         'opt_method': 'cmaes',
#         'optimisation_threshold': 0.8,
#         'array_generic_type': 'rectangular',
#         'ratedPowerArray': 5000,
#         'distance_min': (10, 10),
#         'nogo_areas': None,
#         'array_layout': array_layout
#         } 
#     bathy = -30.0
#     xmax = 840
#     ymax = 350
#     lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

#     x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
#     y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
#     X, Y = np.meshgrid(x, y)
#     nx = len(x)
#     ny = len(y)
#     BR = 1.0  # blockage ratio

#     umax = 5.0  # maximum velocity in the X direction
#     vmax = 0.0  # maximum velocity in the Y direction
#     sshmax = 0.0  # maximum sea surface elevation
#     timax = 0.1
#     bathy = -30.0

#     U = np.ones(X.shape) * umax
#     V = np.ones(X.shape) * vmax
#     SSH = np.ones(X.shape) * sshmax
#     TI = np.ones(X.shape) * timax
#     BATHY = np.ones(X.shape) * bathy
#     PLE = 1.0 * np.ones(X.shape)
#     # bed roughness coefficient here
#     manning = np.ones(X.shape) * 0.001
#     site = {}
#     site['TI'] = [TI.tolist()]
#     site['PLE'] = PLE.tolist()
#     site['geophy'] = manning.tolist()
#     site['X'] = x.tolist()  # save only 1D array as structured grid assumed
#     site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
#     site['U'] = [U.tolist()]
#     site['V'] = [V.tolist()]
#     site['SSH'] = [SSH.tolist()]
#     site['bathy'] = BATHY.tolist()
#     site['BR'] = BR
#     site['lease'] = lease.tolist()
#     site['probability'] = [1.0]

#     ## Turbines positions
#     bathy = -30.0
#     z = bathy / 2.0  # hub height/depth
#     diam = 5
#     array_layout = [[0, 0], [100, 50], [0, 100], [300,70]]

#     ## Turbines features
#     cut_in = 0.0  # cut-in speed
#     cut_out = 10.0  # cut-out speed
#     # actual turbine features
#     speed = np.arange(0.0, 10.0, 0.2)

#     CT = np.ones(speed.shape) * 0.76
#     Ct = [speed.tolist(), CT.tolist()]  # thrust curve

#     CP = np.ones(speed.shape) * 0.3
#     Cp = [speed.tolist(), CP.tolist()]  # Power curve
#     feat = {}

#     feat = {}
#     feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
#     feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
#     feat['Cp'] = Cp[:]  # Power curve
#     feat['Ct'] = Ct[:]  # thrust curve
#     feat['Diam'] = diam  # Diam = rotor diameter (m)
#     feat['cutIO'] = [cut_in, cut_out]
#     feat[
#         'floating'] = False  # so hub height will be considered from the seabottom upwards
#     feat['2way'] = False  # turbines work in both direction.
#     feat['Rating'] = 5e6  # turbines work in both direction.

#     inputs.update(site)
#     inputs.update(feat)
#     ARRAY = business.ArrayFactory.get_array('TEC', "3")
#     ARRAY.get_inputs(name, ecID, **inputs)
#     inputs['nb_devices'] = 1
#     inputs['array_layout'] = [[0,0]]
#     inputs['floating'] = True
#     inputs['2way'] = True
#     ARRAY.get_inputs(name, ecID, **inputs)

#     layout = array_layout
#     n_bodies = len(layout)
#     z = 15
#     coords = {}

#     for ii in range(n_bodies):
#         coords[f'turbine{ii}'] = {}
#         coords[f'turbine{ii}']['position'] = np.asarray(
#             (layout[ii][0], layout[ii][1], -z))
#     features = {}
#     for key in coords.keys():
#         features[key] = feat



#     hydro = business.arraytecstg3.ArrayTECstg3.Hydro(ARRAY.i_site_conditions, 0)
#     # array = business.arraytecstg3.array.Array(hydro, coords, features)
    
#     feat['2way'] = True  # turbines work in both direction.
#     feat['OA'] = 0  # turbines work in both direction.
#     feat['HAS'] = 180  # turbines work in both direction.
#     for ii in range(n_bodies):
#         coords[f'turbine{ii}'] = {}
#         coords[f'turbine{ii}']['position'] = np.asarray(
#             (layout[ii][0], layout[ii][1], -z))
#     features = {}
#     for key in coords.keys():
#         features[key] = feat
#     array = business.arraytecstg3.array.Array(hydro, coords, features)
#     array._interval(0, 0)
    
#     db = create_wake_db()
#     solver = WakeInteraction(hydro, array, db)
#     solver.solv_induction(speed_superimpo="wrong_key")
#     solver.solv_induction(speed_superimpo="linear")
#     solver.solv_induction(speed_superimpo="geometric")
#     solver.solv_induction(speed_superimpo="rss")
#     solver.solv_induction(speed_superimpo="linrss")
#     solver.solv_induction(speed_superimpo="max")
#     solver.solv_induction(speed_superimpo="prod")
#     solver.solv_induction(speed_superimpo="geosum")
#     solver.solv_induction(speed_superimpo="mean")
