# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np

class Converter(object):
    """
    """

    def __init__(self, **kwargs):
    
        self.rotor_diameter = None
        self.floating = False
        self.alpha = None
        self.hub_height = kwargs.pop('hub_height', None)
        self.cut_in = None
        self.cut_out = None
        self.cp = None
        self.ct = None
        self.rated_pow_device = None
        
        
        self.orientation_angle = None
        self.area = None
        self.velocity_vector_angle = None
        self.rotation_matrix = None
        
        self.min_distance = None
        self.bidirectional = False
        self.number_rotor = 1
        self.rotor_interdistance = 0.0

        if 'OA' in kwargs:
            self.orientation_angle = kwargs.get("OA")
            if self.orientation_angle is None:
                self.orientation_angle = 0
                kwargs['OA'] = 0
        self.features = kwargs

        if 'Rating' in kwargs:
            self.rated_pow_device = kwargs.get("Rating")
        if 'Diam' in kwargs:
            self.rotor_diameter = kwargs.get("Diam")
            self.min_distance = (2*self.rotor_diameter, 
                                5*self.rotor_diameter)
        if 'HAS' in kwargs:
            self.heading_angle_span = kwargs.get("HAS")
        if '2way' in kwargs:
            self.bidirectional = kwargs.get("2way")

        if 'Ct' in kwargs:
            self.ct = kwargs.get("Ct")
        if 'Cp' in kwargs:
            self.cp = kwargs.get("Cp")
        if 'cutIO' in kwargs:
            self.cut_in = min(kwargs.get("cutIO"))
            self.cut_out = max(kwargs.get("cutIO"))
        if 'floating' in kwargs:
            self.floating = kwargs.get("floating")
        if 'hub_height' in kwargs:
            self.hub_height = kwargs.get("hub_height")
        
