# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# create the tidal dummy database of wakes
import pandas as pd
import numpy as np
import os
import msgpack
import logging

LOG = logging.getLogger(__name__)

FULL_WAKE_DB = './wake_database/dtop_tidal_wakes_db.msgpack'
REDUCED_WAKE_DB = './wake_database/dtop_tidal_wakes_db_red.msgpack'
def create_wake_db():
        # try to load the full database first
        if os.path.isfile(FULL_WAKE_DB):
                LOG.info('Loading the full tidal DB')
                fn = FULL_WAKE_DB
                f = open(fn, 'r+b')
                binary_format = f.read()
                f.close()

                data = msgpack.unpackb(binary_format)

                data['dfU'] = pd.DataFrame.from_dict(list2array(data['dfU']))
                data['dfV'] = pd.DataFrame.from_dict(list2array(data['dfV']))
                data['dfTKE'] = pd.DataFrame.from_dict(list2array(data['dfTKE']))
                data['dfY'] = np.array(data['dfY'])
                data['dfX'] = np.array(data['dfX'])
                data['cts'] = np.array(data['cts'])
                data['tis'] = np.array(data['tis'])

                return data
                
        elif os.path.isfile(REDUCED_WAKE_DB):
                LOG.info('Loading the reduced tidal DB')
                fn = REDUCED_WAKE_DB
                f = open(fn, 'r+b')
                binary_format = f.read()
                f.close()

                data = msgpack.unpackb(binary_format)

                data['dfU'] = pd.DataFrame.from_dict(list2array(data['dfU']))
                data['dfV'] = pd.DataFrame.from_dict(list2array(data['dfV']))
                data['dfTKE'] = pd.DataFrame.from_dict(list2array(data['dfTKE']))
                data['dfY'] = np.array(data['dfY'])
                data['dfX'] = np.array(data['dfX'])
                data['cts'] = np.array(data['cts'])
                data['tis'] = np.array(data['tis'])

                return apply_symmetry(data)


        LOG.info('cannot find the tidal wakes databases. Loading a dummy database')
        return create_dummy_wake_db()
        
def apply_symmetry(wake_db):
        # the modification in place of the dictionary gave problem 
        # so the code has been made more explicit
        full_db = {}
        full_db['cts'] = wake_db['cts']
        full_db['dfX'] = wake_db['dfX']
        full_db['tis'] = wake_db['tis']
        full_db['dfY'] = np.r_[-wake_db['dfY'][::-1], wake_db['dfY']]
        full_db['dfU'] = {}
        full_db['dfV'] = {}
        full_db['dfTKE'] = {}
        for ti in wake_db['tis']:
                full_db['dfU'][f'ti{ti}'] = {}
                full_db['dfV'][f'ti{ti}'] = {}
                full_db['dfTKE'][f'ti{ti}'] = {}
                for ct in wake_db['cts']:
                        full_db['dfU'][f'ti{ti}'][f'ct{ct}'] = np.c_[wake_db['dfU'][f'ti{ti}'][f'ct{ct}'][:,::-1], wake_db['dfU'][f'ti{ti}'][f'ct{ct}']] 
                        full_db['dfV'][f'ti{ti}'][f'ct{ct}'] = np.c_[wake_db['dfV'][f'ti{ti}'][f'ct{ct}'][:,::-1], wake_db['dfV'][f'ti{ti}'][f'ct{ct}']]       
                        full_db['dfTKE'][f'ti{ti}'][f'ct{ct}'] = np.c_[wake_db['dfTKE'][f'ti{ti}'][f'ct{ct}'][:,::-1], wake_db['dfTKE'][f'ti{ti}'][f'ct{ct}']] 
        return full_db                              

def list2array(l_dic):
    a_dict = {}
    for key in l_dic.keys():
        if isinstance(l_dic[key], dict):
            a_dict[key] = list2array(l_dic[key])
        else:
            a_dict[key] = np.array(l_dic[key])    
        
    return a_dict

def create_dummy_wake_db():
        LOG.info('Generating a dummy DB')
        cts = [0.1,1.0]
        tis = [0.1,1.0]
        nx = 10
        ny = 10

        ctsnames = ['ct'+str(i) for i in cts]
        tisnames = ['ti'+str(i) for i in tis]

        #  Generate dataframe
        dfU = pd.DataFrame(index=ctsnames, columns=tisnames)
        dfV = pd.DataFrame(index=ctsnames, columns=tisnames)
        dfTKE = pd.DataFrame(index=ctsnames, columns=tisnames)

        for ii in tis:
                for cc in cts:
                        # reader.read_u_v_tke(cc, ii)
                        dfU['ti'+str(ii)]['ct'+str(cc)] = np.ones((nx, ny))
                        dfV['ti'+str(ii)]['ct'+str(cc)] = np.zeros((nx, ny))
                        dfTKE['ti'+str(ii)]['ct'+str(cc)] = np.zeros((nx, ny))
                        dfX = np.array(range(nx))
                        dfY = np.array(range(ny))
        
        # Build the dictionaries        
        database = {'dfU'   : dfU,
                'dfV'   : dfV,
                'dfTKE' : dfTKE,
                'dfX'   : dfX,
                'dfY'   : dfY,
                'cts'   : np.asarray(cts),
                'tis'   : np.asarray(tis)}


        return database

