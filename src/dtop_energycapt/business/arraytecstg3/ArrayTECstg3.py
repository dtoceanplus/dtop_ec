# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
from dtop_energycapt.business.arraytecstg3.array import Array
from dtop_energycapt.business.iarray.IArray import IArray
# from dtop_energycapt.business.arraytecstg3.hydro import Hydro

# Start logging
import logging
module_logger = logging.getLogger(__name__)

import time
from shapely.geometry import Polygon
import numpy as np

# Local import
from dtop_energycapt.business.arraytecstg3.utils import distance_from_streamline
from dtop_energycapt.business.arraytecstg3.utils.interpolation import interpol_scatter2grid, interp_at_point
from dtop_energycapt.business.arraytecstg3.utils.misc import pi2pi, deg360_to_radpi, vector_to_bearing
from dtop_energycapt.business.arraytecstg3.modules.vertical_velocity_profile import vvpw
# TR: alternative using Soulsby formulation
#from .modules.vertical_velocity_profile import vvpw_soulsby
from dtop_energycapt.business.arraytecstg3.modules import Streamlines, ArrayYield, HydroImpact
from dtop_energycapt.business.arraytecstg3.submodel.WakeInteraction import WakeInteraction
# from submodel.ParametricWake import read_database

class ArrayTECStg3(IArray):
    _site_keys = ('TI', 'PLE', 'geophy', 'X', 'Y', 'U', 'V', 'SSH', 'bathy',
                  'BR', 'lease', 'probability')
    _converter_keys = ('OA', 'HAS', 'Cp', 'Ct', 'Diam', 'cutIO', 'floating',
                       '2way', 'Rating', 'hub_height')
    _converter_keys_v2 = ('orientation_angle','heading_angle_span','cp','ct',
                          'rotor_diameter','cut_in_velocity','cut_out_velocity',
                          'floating','bidirectional','rated_pow_device','cp_ct_velocity',
                          'hub_height')
    _compatibility_dict = {'HAS': 'heading_angle_span',
                           '2way': 'bidirectional',
                           'Rating': 'rated_pow_device',
                           'Diam': 'rotor_diameter',
                           'Cp': ['cp_ct_velocity','cp'],
                           'Ct': ['cp_ct_velocity','ct'],
                           'cutIO': ['cut_in_velocity','cut_out_velocity'],
                           'OA': 'orientation_angle',
                           'hub_height': 'hub_height'}

    def __init__(self):
        super().__init__()
        self.type = "TEC"
        self.stage = "Stage 3"
        self.results = None
        self.cfd_data = None
        self.i_site_conditions = None  # this will be an instance of the SiteConditions class
        self.i_converter = None
        self.array = None  # this will be an instance of the Array class
        self.layout = None
        self.optimisation_threshold = None
        # self.bathyemtry = None
        # self.geophysics = None
        # self.ple = None  # power law exponent for vertical velocity profile
        # self.ssh = None  # sea surface elevation
        # self.ti = None  # turbulence intensity
        # self.v = None  # depth averaged South-North velocity component (m/s)
        # self.u = None  # depth averaged West-East velocity component (m/s)
        # self.x = None  # West-East corrdinates (m)
        # self.y = None  # South-North corrdinates (m)

    from ._inputs import get_inputs, check_inputs, set_opt_parameters, compress_lease_area
    from ._digrep import read_digrep, write_digrep

    # def get_inputs(self, data, features):
    #     self.site = Hydro(data)
    #     self.machine = features
    #     self.cfd_data = create_dummy_wake_db()
    #     return {"type": self.type, "stage": self.stage}

    def verify_farm_layout(self, debug=True, debug_plot=False):
        """
        DTOcean tidal model

        Args:
        data (dict) dictionary gathering site information with the following entries:
            . 'bathy' = bathymetry (m), 2D numpy array, dimension: (ny,nx)
            . 'geophy' = Manning's roughness coeff., 2D numpy array, dimension: (ny,nx)
            . 'PLE' = Power law exponent for velocity vertical profile,
                    2D numpy array, dimension: (ny,nx)
            . 'SSH' = sea surface elevation (m), 2D numpy array, dimension: (ny,nx)
            . 'TI' = depth averaged turbulence intensity (m), 2D numpy array, dimension: (ny,nx)
            . 'U' = depth averaged West-East velocity component (m/s),
                2D numpy array, dimension: (ny,nx)
            . 'V' = depth averaged South-North velocity component (m/s),
                2D numpy array, dimension: (ny,nx)
            . 'X' = West-East corrdinates (m), 1D numpy array, dimension: (nx)
            . 'Y' = South-North corrdinates (m), 1D numpy array, dimension: (ny)
        turbines (dict): dictionary gathering turbines' locations (m), 1D numpy array [x,y,z].
        features (dict): dictionary gathering individual turbine's features with the following entries:
            . 'Cp' = Power curve, Cp vs flow speed, list of two 1D numpy arrays [speed, Cp] where
                speed's and Cp's dimensions are identical
            . 'Cp' = Thrust curve, Ct vs flow speed, list of two 1D numpy arrays [speed, Ct] where
                speed's and Ct's dimensions are identical
            . 'cutIO' = cut-in and cut-out speeds (m/s), 1D numpy array [cut-in,cut-out]
            . 'Diam' = rotor diameter (m), float
            . 'floating' = floating-turbine-or-not info., boolean
            . 'HAS' = heading angle span (deg.), float

        Kwargs:
        debug (bool): debug flag
        debug_plot (bool): debug plot flag

        Returns:
        pow_perf_dev_no_int (dict): dictionary gathering turbines' power performance (MW) without interaction, float.
        pow_perf_dev )dict): dictionary gathering turbines' power performance (MW), float.
        pow_perf_array_no_int (float): Array power performance (MW) without interaction, float
        pow_perf_array (float): Array power performance (MW), float
        impact (float): ratio between dissipated and available mass flow rate from the flow
        for the considered statistical bin, [0 to 1]
        ti (dict) turbulence intensities at hub height for each turbine

        Notes: the 'turbines', 'features' and output dictionaries have the following structure:
        turbines[turbine's ID]['position'] where turbine's ID = 'turbine'+str(integer)
        for integers going from 0 to infinity


        """
        if debug: start = time.time()

        if not self.layout is None:
            z = self.i_site_conditions.bathy.mean() / 2
            if not self.i_converter.hub_height is None:
                z = abs(self.i_converter.hub_height)
            coords, features = self.__set_machine_coordinates(
                self.layout, z, self.i_converter.features)
            hubs_velocity = []
            pow_perf_dev = []
            pow_perf_dev_no_int = []
            pow_perf_array = []
            pow_perf_array_no_int = []
            ratio = []
            for condition_id, probability in enumerate(self.i_site_conditions.probability):
                # TODO: use only the items associated with the condition
                hydro = Hydro(self.i_site_conditions, condition_id)
                if debug: module_logger.info("initiating classes...")
                array = Array(hydro,
                              coords,
                              features,
                              debug=debug,
                              debug_plot=debug_plot)
                # In case there is only one turbine in the array
                NbTurb = len(array.positions.keys())
                if not NbTurb == 1:
                    if debug: module_logger.info("Starting solver...")
                    interaction = WakeInteraction(hydro,
                                                array,
                                                self.cfd_data,
                                                debug=debug,
                                                debug_plot=debug_plot)
                    interaction.solv_induction(debug=debug)
                else:
                    if debug:
                        module_logger.info("Single turbine...no interactions")

                if debug: module_logger.info("Computing performances...")
                arrayYield = ArrayYield(array, debug=debug, debug_plot=debug_plot)
                arrayYield.performance()

                if debug: module_logger.info("Computing hydrodynamic impacts...")
                # In case there is only one turbine in the array
                if not NbTurb == 1:
                    impacts = HydroImpact(array,
                                        hydro,
                                        interaction,
                                        arrayYield,
                                        debug=debug,
                                        debug_plot=debug_plot)
                    impacts.dissipated_over_available_flux_ratio(debug=debug)
                else:
                    impacts = HydroImpact(array,
                                        hydro,
                                        None,
                                        arrayYield,
                                        debug=debug,
                                        debug_plot=debug_plot)
                    impacts.dissipated_over_available_flux_ratio(debug=debug)
                # print(f'CONDITION ID: {condition_id}')
                # print(arrayYield.turbine_capacity)
                pow_perf_dev.append(arrayYield.turbine_capacity)
                pow_perf_dev_no_int.append(arrayYield.turbine_capacity_no_interaction)
                ratio.append(impacts.diss_avai_mass_flow_rate)
                hubs_velocity.append(array.velHub)
                
                # print('TEC complexity 3')
                # print(array.velHub)


            #exit function
            if debug:
                end = time.time()
                logMsg = "Overall computation time: {} seconds.".format(end -
                                                                        start)
                module_logger.info(logMsg)

            hub_velocity_device = {}
            # TODO: refactor this lines usign dictionary.items()
            for k,v in [(key,d[key]) for d in hubs_velocity for key in d]:
                if k not in hub_velocity_device: hub_velocity_device[k]=[np.linalg.norm(v)]
                else: hub_velocity_device[k].append(np.linalg.norm(v))

            power_perf_device = {}
            for k,v in [(key,d[key]) for d in pow_perf_dev for key in d]:
                if k not in power_perf_device: power_perf_device[k]=[float(v)]
                else: power_perf_device[k].append(float(v))

            power_perf_device_iso = {}
            for k,v in [(key,d[key]) for d in pow_perf_dev_no_int for key in d]:
                if k not in power_perf_device_iso: power_perf_device_iso[k]=[float(v)]
                else: power_perf_device_iso[k].append(float(v))

            output = {}
            for k,v in power_perf_device.items():
                output[k] = {
                    'p_avg': np.mean(v),
                    'p_avg_condition': v,
                    'aep': np.mean(v) * 24 * 365,
                    'pos': coords[k]['position'],
                    'q_factor': np.mean(v) / np.mean(power_perf_device_iso[k]),
                    'hub_velocity': hub_velocity_device.get(k)
                }

            # TODO: check with ESA if the resource reduction is expecrted for each sea state
            output['resource_reduction'] = ratio[0]
            self.results = output
        else:
            raise Warning(
                'The layout attribute has not been set, therefore the power performance assessment cannot be carried out'
            )

    @staticmethod
    def __set_machine_coordinates(layout, z, features):
        """ convert the given array layout to the 
        shape used in the core module

        Attributes:
            layout: numpy array
        """
        n_bodies = np.shape(layout)[0]
        individual_orientation = False
        if np.shape(features['OA'])[0] == n_bodies:
            individual_orientation = True

        coords = {}
        turbine_orientation = {}

        for ii in range(n_bodies):
            coords[f'turbine{ii}'] = {}
            coords[f'turbine{ii}']['position'] = np.asarray(
                (layout[ii][0], layout[ii][1], z))
            if individual_orientation:
                turbine_orientation[f'turbine{ii}'] = features['OA'][ii]
            else:
                turbine_orientation[f'turbine{ii}'] = features['OA'][0]

        feat = {}
        for key in coords.keys():
            feat[key] = {k:v for k,v in features.items()}
            feat[key]['OA'] = turbine_orientation[key]
        return coords, feat


class Hydro():
    """
    Site data & Hydrodynamic conditions

    Args:
      site (dict): dictionary gathering all site's information
      site_id (int): ID of the current condition to analyze

    Attributes:
      U (numpy.array): depth-averaged u velocity component (West-East), 2d numpy array, m/s
      V (numpy.array): depth-averaged v velocity component (South-North), 2d numpy array, m/s
      TI (numpy.arra): turbulence intensity, 2d numpy array
      SSH (numpy.array): sea surface elevation, 2d numpy array, m
      bathy (numpy.array): bathymetry, 2d numpy array, m
      geophy (numpy.array): geophysic info, 2d numpy array, Manning coefficient or bed roughness if using Soulsby formulation
      BR (float): blockage ratio, defined as lease surface / site area, user input, float
      X (numpy.array): x data positions (West-East), assuming regular grid, 1d array, m
      Y (numpy.array): y data positions (South-North), assuming regular grid, 1d array, m
      dx (float): x spatial step, assuming regular grid, float, m
      dy (float): y spatial step, assuming regular grid, float, m

    """
    def __init__(self, site, site_id):
        self.U = site.U[site_id,:,:]
        self.V = site.V[site_id,:,:]
        self.SSH = site.SSH[site_id,:,:]
        self.bathy = site.bathy
        self.geophy = site.geophy
        self.PLE = site.PLE
        # TODO: use flatten array of X and Y and not mgrid
        self.X = np.unique(site.X)
        self.Y = np.unique(site.Y)
        self.TI = site.TI[site_id,:,:] 
        self.BR = site.BR
        self.bounding_box = site.bounding_box
        self.lease = site.lease
