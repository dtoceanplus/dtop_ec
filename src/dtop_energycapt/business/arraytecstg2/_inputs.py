# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energycapt.business.arraytecstg2.site_conditions import SiteConditions
from dtop_energycapt.business.arraytecstg2.converter import Converter
from dtop_energycapt.business.optimisation.optstrategy import dictfunc
from dtop_energycapt.business.arraytecstg2.perf_metrics.farm import Farm
import types
import numpy as np

def get_inputs(self, name, ecID, **kwargs):
    """Gets the inputs from the user for the TEC Stage 1 array type.
    Args:
        name (str): Name of the EC project
        ecID (int): Unique identifier for the EC project.
    Kwargs:
        nb_devices (int): Number of devices in the array.


        array_layout (List[[float,float]]): Suggested array layout by the 
                                            user (mainly for the optimisation 
                                            functionality of the module). 
                                            Given as a list of [Easting [m], 
                                            Northing [m]] coordinates.
        opt_method (string):
        optimisation_threshold (float):
        rated_pow_array (float):
        array_generic_type (string):

    """
    self.name = name
    self.ecID = ecID

    if 'nb_devices' in kwargs:
        self.nb_devices = kwargs.get("nb_devices")
    else: 
        print('Number of devices input missing!')
        raise

    self.layout_method = 'verification'  # fallback option
    if 'layout_type' in kwargs:
        self.layout_method = kwargs['layout_type']        

    if self.layout_method == 'optimization':
        # try to fetch the method from the optimization module, default brute force
        optfunc = dictfunc.get(kwargs.get('opt_method', 'brute_force'), dictfunc['brute_force'])
        self.optimisation_threshold = kwargs.get('optimisation_threshold', 0.85)
        self.array_generic_type = kwargs.get('array_generic_type', 'staggered')
        
        self.optfunname = '{}_{}'.format(self.__class__.__name__, optfunc.__name__)
    else:  # if the system is verified, the layout must be given
        self.layout = kwargs.get("array_layout", [])
        if self.layout == []:
            raise ValueError('Invalid inputs: either specify a valid array layout or a valid optimization method')
    
    if 'ratedPowerArray' in kwargs:
        self.rated_pow_array = kwargs.get('ratedPowerArray')
    
    self.check_inputs()

    # check hub_height
    if (kwargs['hub_height'] + kwargs['rotor_diameter']/2.0) > kwargs['bathymetry']:
        rna = kwargs['hub_height'] + kwargs['rotor_diameter']/2.0
        bathy = kwargs['bathymetry']
        raise ValueError(f'The sum of Hub Height and Rotor Radius ({rna} m) is larger than the average water depth ({bathy} m)')

    self.i_site_conditions = SiteConditions(**kwargs)
    self.i_converter = Converter(self.i_site_conditions.velocity, **kwargs)
    
    self.i_converter.orientation_angle = kwargs.get('orientation_angle', [0])
    # TODO: pass the bathyemtry (100) and manning number (0.3) from the site
    self.PerfMetrics = Farm(self.i_site_conditions.velocity,
                            self.i_site_conditions.bathymetry, 0.3, 
                            self.layout, self.i_converter)

    self._normalisation_point = max(self.i_converter.min_distance)
    
    self.max_num_devices = self.nb_devices
    
    number_rotor = kwargs.get('number_rotor', 1)
    self.i_converter.number_rotor = number_rotor
    self.i_converter.rotor_interdistance = kwargs.get('rotor_interdistance', 0)
    if (self.i_converter.rotor_interdistance == 0):
        self.i_converter.rotor_interdistance = self.i_converter.rotor_diameter*1.05

    # if (number_rotor > 1 and self.layout):
    #     n_device = len(self.layout)
    #     rotor_layout = np.array([el for el in self.layout]*number_rotor, dtype='float')

    #     rotor_dimeter = self.i_converter.rotor_diameter
    #     rotor_interdistance = self.i_converter.rotor_interdistance
    #     rotor_pos = np.array([el*rotor_interdistance for el in range(number_rotor)], dtype='float')
    #     rotor_pos -= np.mean(rotor_pos)
    #     rotors_pos = np.tile(rotor_pos, (n_device,1)).flatten('F')
    #     angles = np.deg2rad(np.array(self.i_converter.orientation_angle*number_rotor))
    #     rotor_layout[:, 0] += np.cos(angles) * rotors_pos 
    #     rotor_layout[:, 1] += np.sin(angles) * rotors_pos 

    #     self.layout = rotor_layout.tolist()

def check_inputs(self):
    """Checks the inputs for the TEC Stage 1 array type.

    Returns:
        True if successful, False otherwise.

    """

    """ self.name should be a string """
    try:
        if not isinstance(self.name, str):
            raise TypeError("Must be a string!")
    except TypeError:
        print('The name must be a string.')
        raise

    """ self.ecID should be an integer """
    try:
        if not isinstance(self.ecID, int):
            raise TypeError("The ID must be of type integer.")
    except (TypeError):
        print('The ID must be of type integer.')
        raise

    """ self.nb_devices should be a positive non null integer """
    try:
        if not isinstance(self.nb_devices, int):
            raise TypeError
    except TypeError:
        print('Number of devices must be an integer number.')
        raise
    try:
        if self.nb_devices <= 0:
            raise ValueError("Must be a positive non null integer!")
    except ValueError:
        print("The number of device must be a positive non null integer.")
        raise

    """ self.optimisation_threshold should be a float """
    if not self.optimisation_threshold is None:
        try:
            if not isinstance(self.optimisation_threshold, float):
                raise TypeError("The optimisation threshold must be of type float.")
        except (TypeError):
            print('The optimisation threshold must be of type float.')
            raise

    """ self.rated_pow_array should be a float """
    if not self.rated_pow_array is None:
        try:
            if not isinstance(self.rated_pow_array, float):
                raise TypeError("The rated power of the array must be of type float.")
        except (TypeError):
            print('The rated power of the array must be of type float.')
            raise

    if not self.array_generic_type is None:
        try:
            if not isinstance(self.array_generic_type, str):
                raise TypeError
        except TypeError:
            status  = False
            print(f'The input format of the array generic type is'
                    f' incorrect.\n  Accepted format are: string.') 
            raise   