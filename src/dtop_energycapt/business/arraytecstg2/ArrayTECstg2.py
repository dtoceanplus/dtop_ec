# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
from dtop_energycapt.business.iarray.IArray import IArray

from dtop_energycapt.business.arraytecstg2.perf_metrics.farm import Farm
import numpy as np

class ArrayTECStg2(IArray):

    def __init__(self):
        super().__init__()
        self.type = "TEC"
        self.stage = "Stage 2"
        self.name = None
        self.ecID = None
        self.nb_devices = None
        self.optimisation_threshold = None
        self.layout = None
        self.rated_pow_array = None
        self.array_generic_type = None
        self.i_converter = None
        self.i_site_conditions = None
        self.PerfMetrics = None
        self.nogoAreas_bathymetry = None
        self.max_num_devices = None





    from ._digrep import read_digrep, write_digrep
    from ._inputs import get_inputs, check_inputs

    # def get_inputs(self, velocity, layout, device):
    #     self.PerfMetrics = Farm(velocity, None, None, layout, device)
    #     return {"type": self.type, "stage": self.stage}        


    def verify_farm_layout(self):
        """Calculates power metrics for a single device and for the array.

        Returns:
            True if successful, False otherwise.

        """
        if not self.layout is None:
            self.PerfMetrics.set_device_layout(self.layout)
            self.PerfMetrics.set_device_distances()
            # print(farm.dij)
            # print(farm.xij)
            self.PerfMetrics.set_tubine_overlaps()
            # print(farm.aij)
            # print(farm.rij)
            self.PerfMetrics.velocity_at_devices_location()
            # print(farm.hubs_velocity)
            self.PerfMetrics.power_performance()
            # print("----- The outputs -----")
            # print(self.PerfMetrics.outputs)
            # self.PerfMetrics.plot_farm()
            self.results = self.PerfMetrics.outputs 
        else:
            raise Warning(
                'The layout attribute has not been set, therefore the power performance assessment cannot be carried out'
            )