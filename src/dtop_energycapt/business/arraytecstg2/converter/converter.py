# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np
from math import acos, sin, cos
# TODO: check is the import of the functions above from the math package is causing problems.

class Converter(object):
    """
    Converter class: for the level 2 complexity of the DTO+ project
    created: 2019/12/03
    author: francesco ferri, AAU (ff@civil.aau.dk)
    last modified: 2019/12/04
    ...
    Attributes
    ----------
    rotor_diameter : float, [m]
        tidal converter rotor diameter
    hub_height : float, [m]
        tidal converter hub height, if floating is False the hub height is given wrt the sea bottom
                                    if floating is True the hub height is given wrt the machine water line
    undisturbed_hub_speed : array, [m/s]
        west-east and south-north components of the velocity vector field at the hub height.
    cut_in : float, [m/s]
        tidal converter cut in velocity of the incoming hub particle speed
    cut_out : float, [m/s]
        tidal converter cut out velocity of the incoming hub particle speed
    cp : float, []
        tidal converter power coefficient
    ct : float, []
        tidal converter trust coefficient
    alpha : float, []
        wake expansion factor
    area : float, [m^2]
        tidal converter rotor area
    floating: bool, []
        tidal converter floating flag, default is False

    Methods
    -------
    tec_yield(in_hub_speed_mag)
        assess the tec annual energy yield and average power production in MWh and MW respectivelly,
        given the speed magnitude at the hub height

    disk_overalp(x_dist, y_dist):
        assess the wake overlap given the down-flow and across distances between two interacting wakes
    
    tec_yield(self, in_hub_speed_mag):
        assess the tec annual energy yield and average power production in MWh and MW respectivelly,
        given the speed magnitude at the hub height
    
    plot_wake(self, ax, hub_location, id=None):
        plot a representation of the machine rotor, the machine rotation axis
        and the machine wake.
        TODO: this function is only temporary

    unit_vector(vector):
        Returns the unit vector of the vector
    
    angle_between(self, v2):
        Returns the angle in radians between vectors reference speed vector and 'v2'

    Example
    -------

    >>> Converter(rotor_diameter, hub_height,
                                incoming_hub_speed, cp, ct, 
                                alpha=0.07, cut_IO=[0,np.inf], floating=False)
    """
    __ns = np.array((1,0))
    DISK = np.array([[0,0],[-1,1]])
    AXIS = np.array([[0,1],[0,0]])
    WAKE = np.array([[0,0,1,1],[-1,1,1,-1]])
    wake_length = 10

    def __init__(self, undisturbed_hub_speed, **kwargs):
    
        self.rotor_diameter = None
        self.floating = None
        self.alpha = None
        self.hub_height = None
        self.cut_in = None
        self.cut_out = None
        self.cp = None
        self.ct = None
        self.rated_pow_device = None
        
        self.area = None
        self.velocity_vector_angle = None
        self.rotation_matrix = None
        self.number_rotor = 1
        self.rotor_interdistance = 0.0
        self.orientation_angle = [0.0]

        self.min_distance = None

        if 'rated_pow_device' in kwargs:
            self.rated_pow_device = kwargs.get("rated_pow_device")
        else: 
            print('Rated power of the device input missing!')
            raise
        if 'rotor_diameter' in kwargs:
            self.rotor_diameter = kwargs.get("rotor_diameter")
            self.min_distance = (2*self.rotor_diameter, 
                                5*self.rotor_diameter)
        else: 
            print('Rotor diameter input missing!')
            raise
        if 'hub_height' in kwargs:
            self.hub_height = kwargs.get("hub_height")
        else: 
            print('Tidal converter hub height input missing!')
            raise
        if 'ct' in kwargs:
            self.ct = kwargs.get("ct")
        else: 
            print('Tidal converter trust coefficient input missing!')
            raise
        if 'cp' in kwargs:
            self.cp = kwargs.get("cp")
        else: 
            print('Tidal converter power coefficient input missing!')
            raise
        
        if 'cut_IO' in kwargs:
            self.cut_in = min(kwargs.get("cut_IO"))
            self.cut_out = max(kwargs.get("cut_IO"))
        else: 
            if 'cut_in_velocity' in kwargs:
                self.cut_in = kwargs.get("cut_in_velocity")
            else: 
                print('Tidal converter cut in input missing!')
                raise
            if 'cut_out_velocity' in kwargs:
                self.cut_out = kwargs.get("cut_out_velocity")
            else: 
                print('Tidal converter cut out input missing!')
                raise

        if 'floating' in kwargs:
            self.floating = kwargs.get("floating")
        else: 
            self.floating = False

        if 'alpha' in kwargs:
            self.alpha = kwargs.get("alpha")
        else: 
            self.alpha = 0.07

        self.check_inputs()    
        self.velocity_vector_angle = self.angle_between(undisturbed_hub_speed)
        self.get_rotation_matrix()         
        self.area = .25*np.pi*self.rotor_diameter**2

    def printInput(self, indent=4):
        """print the Converter class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """
        
        print('Print all arguments of this instance of Converter')


        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)
        
        return

    def check_inputs(self):
        
        status = True
        errStr = []

        """ self.rated_pow_device should be a positive non null float """
        try:
            if not isinstance(self.rated_pow_device, (int, float)):
                raise TypeError
        except TypeError:
            print('Rated power of the device must be a float value.')
            raise
        try:
            if self.rated_pow_device <= 0:
                raise ValueError
        except ValueError:
            print("The rated power of the device must be a positive non null \
                    value.")
            raise




    def checkPowerConsistency(self, rPowArray):
        
        status = True
        errStr = []
        
        """ self.maxNumDevices should be a positive non null integer """
        try:
            maxNumDevices = int(rPowArray / self.rated_pow_device)
        except TypeError:
            print('Maximum number of device must be an integer number.')
            status = False
            errStr.append(
                'Maximum number of device must be an integer number.')
            raise
        try:
            if maxNumDevices <= 0:
                raise ValueError("Must be a positive non null integer!")
        except ValueError:
            print(
                "The ratio between array and machine rated powers is below 1.\n\
                Verify the rated power inputs.")
            status = False
            errStr.append(
                "The ratio between array and machine rated powers is \
                        below 1.'\n' Verify the rated power inputs. ")
            raise

        return maxNumDevices, status, errStr

    def disk_overlap(self, x_dist, y_dist):
        """ assess the wake overlap given the down-flow and across distances 
        between two interacting wakes

        Inputs
        ------
            x_dist : float, [m]
                distance between turbines hubs in the down-flow direction
            y_dist : float, [m]
                distance between turbines hubs in the downw-flow perpendicular direction
        
        Outputs
        -------
            a : float, [m^2]
                wake overalp area in m^2
        """
        r = self.rotor_diameter/2
        if (y_dist==0): return np.pi*r**2
        Rij = r+self.alpha*x_dist
        theta_arg = (r**2+y_dist**2-Rij**2)/(2*y_dist*r)
        
        if theta_arg < -1 or theta_arg > 1: 
            if Rij > r+y_dist:
                return np.pi*r**2
            return 0
        theta = acos(theta_arg)
        beta = acos((Rij**2+y_dist**2-r**2)/(2*y_dist*Rij))
        a = r**2*(theta-(sin(theta)*cos(theta))) + Rij**2*(beta-sin(beta)*cos(beta))

        return a

    def get_rotation_matrix(self):
        """ estimates the rotation matrix associated with the undisturbed wind direction
        """
        theta = np.radians(self.velocity_vector_angle)
        c, s = np.cos(theta), np.sin(theta)
        R = np.array(((c,-s), (s, c)))

        self.rotation_matrix = R

    def tec_yield(self, in_hub_speed_mag):
        """assess the tec annual energy yield and average power production in MWh and MW respectivelly,
        given the speed magnitude at the hub height
        
        Inputs
        ------
            in_hub_speed_mag : float, [m/s]
                velocity speed magnitude at the hub height
        
        Outputs
        -------
            average power : float, [MW]
                average power production of the given machine
            annual energy yield : float, [MWh]
                annual energy production of the given machine 
        """
        rho = 1024
        average_power = 0
        # print(self.area)
        # print(in_hub_speed_mag)
        # print(self.cp)
        if in_hub_speed_mag >= self.cut_in or in_hub_speed_mag <= self.cut_out:
            average_power = 0.5*rho*self.cp*self.area*(in_hub_speed_mag**3.0)

        return (average_power, average_power*24*365)
    
    # def plot_wake(self, ax, hub_location):#, id=None):
    #     """plot a representation of the machine rotor, the machine rotation axis
    #     and the machine wake.
    #     TODO: this function is only temporary
        
    #     Inputs
    #     ------
    #         ax : matplotlib.pyplot.axes, []
    #             plot axes, where to stack the rotor and wake shape
    #         hub_location : array, [?]
    #             absolute location of the machine hub
    #         id : int, []
    #             id of the machine, used only for graphycal purposes

    #     """
    #     x = self.wake_length*self.rotor_diameter
    #     pos = np.array([hub_location,hub_location]).T

    #     disk = np.dot(self.rotation_matrix,self.DISK*self.rotor_diameter/2) + pos
    #     axis = np.dot(self.rotation_matrix,self.AXIS*10*self.rotor_diameter/2) + pos

    #     R = self.rotor_diameter/2+self.alpha*x
    #     wake = np.dot(self.rotation_matrix,
    #                   (self.WAKE*np.array([[1,1,20*self.rotor_diameter/2,20*self.rotor_diameter/2],
    #                                        [self.rotor_diameter/2,self.rotor_diameter/2,R,R]]))).T + pos[:,0]

    #     ax.plot(disk[0,:],disk[1,:],'k',linewidth=5)
    #     ax.plot(axis[0,:],axis[1,:],'k--',linewidth=2)
    #     ax.add_patch(Polygon(wake, closed=True, fill=False, hatch='/',linewidth=0.5))
        
        # if id:
        #     ax.text(hub_location[0]*1.1,hub_location[1]*1.1,str(id))
        
    def angle_between(self, v2):
        """ Returns the angle in radians between vectors reference speed vector and 'v2':

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
        """
        v2_u = self.unit_vector(v2)
        # v0 = self.__ns
        # v1 = v2_u
        return np.degrees(np.math.atan2(np.linalg.det([self.__ns,v2_u]),np.dot(self.__ns,v2_u)))
        # return np.arccos(np.clip(np.dot(self.__ns, v2_u), -1.0, 1.0))*180/np.pi

    @staticmethod
    def unit_vector(vector):
        """ Returns the unit vector of the vector.  """
        return vector / np.linalg.norm(vector)

    def __str__(self):
        return (f"{self.__class__.__name__} (objID: {id(self)})\n"
                f"attributes:\n"
                f" rotor_diameter: {self.rotor_diameter:.1f}\n"
                f" hub_height: {self.hub_height:.1f}\n"
                f" floating: {self.floating}\n"
                f" cut IN/OUT velocity: {self.cut_in, self.cut_out}\n")



