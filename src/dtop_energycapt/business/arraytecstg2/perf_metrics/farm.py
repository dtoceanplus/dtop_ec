# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np

# from .tidal_energy_converter import TidalEnergyConverter

class Farm():
    """Farm class for the level 2 complexity of the DTO+ project
    created: 2019/12/03
    author: francesco ferri, AAU (ff@civil.aau.dk)
    last modified: 2019/12/04
    ...
    Attributes
    ----------
    undisturbed_velocity_field : array, [m/s]
        west-east and south-north components of the velocity vector field at the specified point in the vertical direction,
        only the average vector is used.

    water_depth : float, [m]
        TBI --> vertical distance between the measurement point and the sea bottom at the considered location
    
    manning_number : float, [-]
        TBI --> average manning number at the given site, sued to calculate the vertical velocity profile

    device_layout : array, [m]
        TBD --> position of the devices' hubs in the global coordinate system?????
    
    device : dict, []
        comprises all the machine information, items:
            rotor_diamter : float, [m], tidal converter rotor diameter
            hub height : float, [m], tidal converter hub height
                            if floating is False the hub height is given wrt the sea bottom
                            if floating is True the hub height is given wrt the machine water line
            cp : float, [], tidal converter power coefficient
            ct : float, [], tidal converter trust coefficient
            alpha : float, [], wake expansion factor
            floating : bool, [], tidal converter floating flag, default is False
            cut_IO : float, [m/s], tidal converter cut in/out velocities
        
    Methods
    -------
    set_device_layout(device_layout)
        set the given farm layout and reset the farm outputs
    
    __reset_farm_output()
        reset the farm outputs, truggered by a change in the farm layout
    
    set_device_distances()
        estimate the pairwise distances between devices in the rotate and ranked layout

    rotate_layout()
        rotate the farm layout according to the wind direction
        
    sort_layout()
        sort the devices starting from the first row, in the up-current direction

    set_tubine_overlaps()
        estimates the area averlap between all the turbines in the farm
    
    power_performance()
        estimates the power performance of each device in the farm
    
    velocity_at_devices_location()
        solves the wake interaction and estiamtes the velocity at the given device locations
    
    plot_farm()
        plot the farm layout, including individual wakes from each machine
        
    """
    def __init__(self, undisturbed_velocity_field,
                 water_depth, manning_number, 
                 device_layout, device):
        self.water_depth = water_depth
        self.manning_number = manning_number         
        self.device_layout = None
        self.rotated_layout = None
        self.ranked_layout = None
        self.device_number = None
        self.xij = None
        self.dij = None
        self.hubs_velocity = None
        self.aij = None
        self.rij = None
        self.outputs = None
        self._sorted_indexes = None

        self.tec = device
        # estimate the velocity vector at the 
        self.velocity_vector = self.vertical_profile(undisturbed_velocity_field)
        # done in this way to make clear the dynamic nature of this attribute
        if not device_layout is None:
            self.set_device_layout(device_layout)

    def set_device_layout(self, device_layout):
        """set the given farm layout in the farm attribute
        """
        self.device_layout = np.array(device_layout)
        self.rotate_layout()
        self.sort_layout()
        self.device_number = len(self.device_layout)
        self.__reset_farm_output()

    def __reset_farm_output(self):
        """reset the farm outputs, truggered by a change in the farm layout
        """
        self.u = None
        self.xij = None
        self.dij = None
        self.hubs_velocity = None
        self.aij = None
        self.rij = None
        self.outputs = None

    def set_device_distances(self):
        """estimate the pairwise distances between devices in the rotate and ranked layout.
        """
        # hub distance in the y-direction
        y = self.ranked_layout[:,1]
        x = self.ranked_layout[:,0]
        self.dij = np.abs(y - y[:, None])

        # hub distance in the x-direction
        self.xij = x - x[:, None]

    def rotate_layout(self):
        """rotate the farm layout according to the wind direction

        layout (numpy array of size Nx2) describes the position of the N devices in the farm
        wind_direction (float) is the angle in DEG with respect to the North-South direction

        wind_direction for North to South wind is 0deg
        wind_direciton for West to East is 90deg
        """
        n,m = np.shape(self.device_layout)
        if not m==2:
            if n==2: 
                self.device_layout = self.device_layout.T
            else:
                print('The layout array should be a Nx2 matrix')
                raise ValueError

        self.rotated_layout = np.dot(self.tec.rotation_matrix.T, self.device_layout.T).T

    def sort_layout(self):
        """sort the devices starting from the first row, in the up-current direction
        """
        d,i = self.rotated_layout[:,0].min(0),self.rotated_layout[:,0].argmin(0)
        shifted_layout = self.rotated_layout-np.array([d,0])
        self._sorted_indexes = shifted_layout[:,0].argsort()
        self.ranked_layout = shifted_layout[self._sorted_indexes]
        
    def set_tubine_overlaps(self):
        """estimates the area averlap between all the turbines in the farm
        """
        a = np.zeros(self.xij.shape)
        for i in range(self.device_number):
            for j in range(self.device_number):
                if i!=j:
                    a[i,j] = self.tec.disk_overlap(self.xij[i,j], self.dij[i,j])
        rij = self.tec.rotor_diameter + self.tec.alpha*self.xij
        self.aij = a
        self.rij = rij

    def power_performance(self):
        """estimates the power performance of each device in the farm
        """
        output = {}
        undisturbed_velocity = np.linalg.norm(self.velocity_vector)
        isolated_power, isolated_aep = self.tec.tec_yield(undisturbed_velocity)
        # TODO: check power performances values
        for dev in range(self.device_number):
            # print('Power performance of dev:', self.hubs_velocity[dev])
            p,aep = self.tec.tec_yield(self.hubs_velocity[dev])
            original_id = self._sorted_indexes[dev]
            output[original_id] = {'p_avg': p, 'aep': aep,
                                'p_avg_condition': [p],
                                'pos': self.device_layout[original_id],
                                'q_factor': p/isolated_power,
                                'hub_velocity': [self.hubs_velocity[dev]]}
        
        out = list(output.items())
        out.sort()
        output = dict(out)

        # TODO: improve estimation of the resource reduction
        output['resource_reduction'] = p/isolated_power

        self.outputs = output

    def velocity_at_devices_location(self):
        """solves the wake interaction and estiamtes the velocity at the given device locations
        """
        a = 1.0/3.0
        Ct = 4*a*(1-a)
        Cr = 1-np.sqrt(1-Ct)
        D = self.rij[0,0]
        A = np.pi*D**2/4
        self.aij *= np.triu(np.ones((self.device_number,self.device_number), dtype=int))
        u = np.linalg.norm(self.velocity_vector)*(1-Cr*D**2/A*np.dot(self.aij.T, 1/(2*self.rij)**2))
        
        self.hubs_velocity = u[np.eye(self.device_number, dtype=bool)]

    def vertical_profile(self, average_velocity_vector):
        # TODO: check if the method is correct
        velocity_mag = np.linalg.norm(average_velocity_vector)
        velocity_dir = average_velocity_vector/velocity_mag

        if self.tec.floating:
            z = self.water_depth-self.tec.hub_height
        else:
            z = self.tec.hub_height

        beta = self.manning_number
        return velocity_dir*((z/(beta*self.water_depth))**(1/7)*velocity_mag)

    # def plot_farm(self):
    #     """plot the farm layout, including individual wakes from each machine
    #     """
    #     fig, ax = plt.subplots()
    #     for mac in self.device_layout:
    #         self.tec.plot_wake(ax, mac)

    #     plt.show()

# if __name__ == "__main__":
#     device = {'rotor diameter': 10,
#                 'hub height': 20,
#                 'floating': False,
#                 'cut_IO': [1,10],
#                 'cp': 1,
#                 'ct': 1,
#                 'alpha': 0.07}
#     layout = [[0,0],[100,50],[0,100]]
#     velocity = ((0,5),)

#     farm = Farm(velocity, layout, device)

#     farm.set_device_distances()
#     print(farm.dij)
#     print(farm.xij)
#     farm.set_tubine_overlaps()
#     print(farm.aij)
#     print(farm.rij)
#     farm.velocity_at_devices_location()
#     print(farm.hubs_velocity)
#     farm.power_performance()
#     print(farm.outputs)
#     farm.plot_farm()

    
