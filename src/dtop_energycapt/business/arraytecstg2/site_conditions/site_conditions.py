# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import numpy as np
from shapely.geometry import Polygon

class SiteConditions(object):
    """
    SiteConditions class: The class contains all the information relative to the 
    site conditions at the deployment location of the array.
    
    Args:
        velocity (numpy.ndarray): west-east and south-north components of the 
                                  velocity vector field at the specified point 
                                  in the vertical direction, only the average 
                                  vector is used [m/s].
        manning_number (float): TBI --> average manning number at the given 
                                site, sued to calculate the vertical velocity 
                                profile [-]
        speed_vertical_meas_pt (float) : TBI --> vertical distance between the 
                                         measurement point and the sea bottom at
                                         the considered location [m]
        loc_position (List[[float,float]]): Position of the deployment site 
                                            [Easting [m], Northing [m]].
        central_deploy_pos(List[[float,float]]): Central position of the array 
                                                 layout.
    """
    
    def __init__(self, **kwargs):
        # TODO: identify if SC can provide the vertical position of the measurement point
        self.velocity = None
        self.loc_position = None
        self.central_deploy_pos = None
        self.bathymetry = None

        self.main_angle = 0
        
            
        if 'velocity_field' in kwargs:
            self.velocity = kwargs.get("velocity_field")
        else: 
            print('Velocity field for the deployment site missing!')
            raise
        if 'bathymetry' in kwargs:
            self.bathymetry = kwargs.get("bathymetry")
        else: 
            print('Bathymetry for the deployment site missing!')
            raise
        if 'loc_position' in kwargs:
            self.loc_position = kwargs.get("loc_position")
        else: 
            print('Position of the location input missing!')
            raise
        self.check_inputs()

        self.lease_reduced = Polygon(self.loc_position)

        # Get the mean position of the deployment area from
        # self.isiteconditions.loc_postion
        x = [t[0] for t in self.loc_position]
        y = [t[1] for t in self.loc_position]
        self.central_deploy_pos = [sum(x)/len(x), sum(y)/len(y)]          
                
    def printInput(self, indent=4):
        """print the SiteConditions class input arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """
        
        print('Print all arguments of this instance of SiteConditions')


        # vars_string = pformat(vars(self), indent=indent)
        # logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)
        
        return

    def check_inputs(self):
        
        status = True
        errStr = []
        # """ self.velocity should be a numpy.ndarray of positive float """
        try:
            if not isinstance(self.velocity, (tuple, list)):
                raise TypeError
            # vel = np.array(self.velocity)
            n = np.shape(self.velocity)
            if n != (2,):
                v = np.mean(self.velocity, 0)
                if v.shape == (2,):
                    self.velocity = v.tolist()
                else:
                    raise TypeError('Invalid shape for the velocity field')
        except TypeError:
            print('Invalid format for the velocity field')
            raise

        """ self.loc_position should be a list """
        try: 
            list_ = isinstance(self.loc_position, list)
            if not list_:
                raise TypeError
        except TypeError:
            print("Deployment location should be a list of points.")
            raise

        """ All elements in self.loc_position should be a list """
        try:
            if not all(isinstance(x, list) for x in self.loc_position):
                raise TypeError
        except TypeError: 
            print("Each element in loc_position should be a list of points.")
            raise                   

        """ All elements in the elements in self.loc_position should be a \
            float (or int) """
        try:
            if not all(isinstance(x, float) for sublist in self.loc_position 
            for x in sublist) and not all(isinstance(x, int) for sublist 
            in self.loc_position for x in sublist):
                raise TypeError
        except TypeError: 
            print("The easting and northing position should be float.")
            raise                 

        """ All elements in the elements in self.loc_position should be \
            positive non null float (or int) 
            This is an overly tight constraint. As described here 
            (https://gis.stackexchange.com/questions/80680/negative-utm-easting)
            wihtin 20deg the reprojection error is marginal, therefore allowign for
            negative easting and northing. This could apply to an lease area between two UTM zones"""

        # try:
        #     if not all(x >= 0 for sublist in self.loc_position for x in sublist):
        #         raise ValueError
        # except ValueError: 
        #     print("The easting and northing position should be positive non \
        #         null float.")
        #     raise     

        """ All elements in self.loc_position should be pairs of points """
        try:
            if not all(len(sublist)==2 for sublist in self.loc_position):
                raise ValueError
        except ValueError: 
            print("The number of easting values does not match the number of \
                northing values. Example: [[3440, 4000],[3550, 4550]]")
            raise  
            
        
