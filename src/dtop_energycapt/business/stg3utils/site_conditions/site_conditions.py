# This is the Energy Capture (EC) module for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 AAU-Build - Francesco Ferri
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

#    Copyright (C) 2016 Francesco Ferri
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
This module contains the class used to collect the parameters used in the
creation of the tidal or wave numerical models for the stage 3, and 
haracterisation of the sea conditions

.. module:: SiteConditions
   :platform: Windows
   :synopsis: SiteConditions module for the Energy Capture module of DTOceanPlus 

.. moduleauthor:: Francesco Ferri <ff@civil.aau.dk>
"""

from math import pi
from dtop_energycapt.business.stg3utils.utils.watwaves import WNumber
from dtop_energycapt.business.stg3utils.utils.set_wdirs_multibody import anglewrap, convertangle
import numpy as np
from numpy import average
from pprint import pformat
from scipy import interpolate
from shapely.geometry import MultiPoint, MultiPolygon, Polygon

spectral_type = [
    "Regular", "Bretschneider_Mitsuyasu", "Modified_Bretschneider_Mitsuyasu",
    "Pierson_Moskowitz", "Jonswap", "pscTMA", "pscFRF", "pscSwell"
]


class SiteConditions(object):
    """
    Args:
        The number of args is variable, depending on the machine type:
            B (numpy.ndarray): Sea state wave directions.
            Hs (numpy.ndarray): Sea state wave heights.
            Tp (numpy.ndarray): Sea state wave periods.
            scatDiag (numpy.ndarray): probability of occurence of the different 
                                      sea states (B, Hs, Tp).
            depth (float, numpy.ndarray): 
                        wave (float): Flattened water depth of the site.
                        tidal (numpy.ndarray): sea surface elevation at each 
                                               grid node for each sea state
            freqs (numpy.ndarray): frequencies (rad/s) used to deiscretise 
                                        the frequency domain model.
            dirs (numpy.ndarray): wave directions (rad) used to discretise the 
                              frequency domain model.
            specType (tuple): description of the wave spectrums, spectrum type, 
                              peak factor and spreading parameter
            V (numpy.ndarray): y-contribution (Northing) to the velocity field 
                               at each grid node
            U (numpy.ndarray): x-contribution (Easting) to the velocity field at 
                           each grid node
            p (numpy.ndarray): probability of occurence of the different sea 
                               states.
            TI (numpy.ndarray): turbulence intensity at each grid node for each 
                                sea state
            velocityShear (numpy.ndarray) [-]: Tidal velocity shear formula 
                                               (power law), used to evaluate the
                                               vertical velocity profile
            geophysics (numpy.ndarray) [-]: Describes the sea bottom geophysic 
                                            characteristic (Manning number) at 
                                            each (given) UTM coordinate. 
                                            Expressed as 
                                            [X(Northing),Y(Easting),Geo]
            BR (float) [-]: describes the ratio between the lease area surface 
                            over the site area surface enclosed in a channel. 
                                1. - closed channel
                                0. - open sea
            electricalConnectPoint (numpy.ndarray) [m]: UTM coordinates of 
                                    the electrical connection point at the shore
                                    line, expressed as [X(Northing),Y(Easting)].
            boundaryPad (float, optional) [m]: Padding added inside the 
                                               lease area in which devices may 
                                               not be placed. 
            x (numpy.ndarray): x-coordinates (Easting) of the grid nodes
            y (numpy.ndarray): y-coordinates (Northing) of the grid nodes
            mainDirection (numpy.ndarray, optional) [m]: xy vector defining the 
                                                         main orientation of the
                                                         array. If not provided
                                                         it will be assessed 
                                                         from the input site 
                                                         conditions, expressed 
                                                         as [X(Northing), 
                                                         Y(Easting)]. 

            leaseArea (numpy.ndarray): array of coordinates describing the lease
                                       area at the site
            nogoAreas (list): list containing the UTM coordinates of the nogo 
                              areas poligons expressed as 
                              [X(Northing),Y(Easting)].
            bathy (numpy.ndarray): bathymetry of the lease area at each grid 
                                   node
            type (bool): takes the value False for wave converter case and True
                         for tidal converter case

    Attributes:
        .period: calculated as 2*pi/cfrequency
        .wnumber: calculated from the period and the water depth
        .main_angle (numpy.ndarray) [rad]: defines the orientation angle of the 
                                          array. The angle is calculated from 
                                          the north in a clockwise direction
                                            0       - North
                                            pi/2    - East
                                            pi      - South
                                            3/2 pi  - West 
    Note:
        The attribute(s) are modified as follow:
            .geophysics: reshaped to the provided grid if a single float is 
                         given
    """
    def __init__(self, **kwargs):

        self.B = None  #wave
        self.Hs = None  #wave
        self.Tp = None  #wave
        self.depth = None  #wave and tidal
        self.scatDiag = None  #wave
        self.bathy = None  #wave and tidal
        self.freqs = None  #wave
        self.specType = None  #wave
        self.dirs = None  #wave
        self.V = None  #tidal
        self.U = None  #tidal
        self.probability = None  #tidal
        self.TI = None  #tidal
        self.x = None  #tidal
        self.y = None  #tidal
        self.geophysics = None  #tidal
        self.velocityShear = None  #tidal
        self.BR = None  #tidal
        self.mainDirection = None  #wave and tidal
        self.main_angle = 0.0  #wave and tidal
        self.leaseArea = None  #wave and tidal
        self.nogoAreas = None  #wave and tidal
        self.electricalConnectPoint = None  #wave and tidal
        self.boundaryPad = None  #wave and tidal

        self.central_deploy_pos = None
        self.loc_position = None
        self.lease_reduced = None

        self.period = None  #wave
        self.wnumber = None  #wave
        self.type = False  #wave

        if 'sea_state_directions' in kwargs:
            self.B = kwargs.get("sea_state_directions")
        else:
            self.type = True
        if 'sea_state_wave_height' in kwargs:
            self.Hs = kwargs.get("sea_state_wave_height")
        else:
            self.type = True
        if 'sea_state_wave_period' in kwargs:
            self.Tp = kwargs.get("sea_state_wave_period")
        else:
            self.type = True
        if 'scat_diagram' in kwargs:
            self.scatDiag = kwargs.get("scat_diagram")
            sd = self.scatDiag
            tp = self.Tp
            hs = self.Hs
            wdir = self.B

            x, y, z = (np.unique(tp), np.unique(hs), np.unique(wdir))

            sd_mat = np.zeros((len(x), len(y), len(z)))
            for ii in range(len(sd)):
                l, = np.where(x==tp[ii])
                m, = np.where(y==hs[ii])
                n, = np.where(z==wdir[ii])
                sd_mat[l,m,n] = sd[ii]
            self.scatDiag = sd_mat
            self.Tp = x
            self.Hs = y
            self.B = z
        else:
            self.type = True
        if 'spectral_type' in kwargs:
            self.specType = kwargs.get('spectral_type')
            # TODO: check the spreading error and remove this block
            self.specType = (self.specType[0], self.specType[1], 0.0)
        if 'wave_dirs' in kwargs:
            self.dirs = kwargs.get('wave_dirs')
        if 'frequencies' in kwargs:
            self.freqs = kwargs.get('frequencies')
        if 'V' in kwargs:
            self.V = kwargs.get('V')
        if 'U' in kwargs:
            self.U = kwargs.get('U')
        if 'PLE' in kwargs:
            self.PLE = kwargs.get('PLE')
        if 'SSH' in kwargs:
            self.SSH = kwargs.get('SSH')
        if 'probability' in kwargs:
            self.probability = kwargs.get('probability')
        if 'TI' in kwargs:
            self.TI = kwargs.get('TI')
        if 'geophy' in kwargs:
            self.geophy = kwargs.get('geophy')
        # if 'velocity_shear' in kwargs:
        #     self.velocityShear = kwargs.get('velocity_shear')
        if 'BR' in kwargs:
            self.BR = kwargs.get('BR')
        if 'X' in kwargs:
            self.X = kwargs.get('X')
        if 'Y' in kwargs:
            self.Y = kwargs.get('Y')
        if 'lease' in kwargs:
            self.lease = Polygon(kwargs.get('lease'))
            (xm, ym, xM, yM) = self.lease.bounds
            self.bounding_box = Polygon([[xm, ym], [xM, ym], [xM, yM],
                                         [xm, yM]])

        if 'water_depth' in kwargs:
            self.depth = kwargs.get("water_depth")

        # TODO: why the lease area is defined twice?
        if 'lease' in kwargs:
            self.leaseArea = kwargs.get('lease')
            self.centroidLease = average(self.leaseArea, axis=0)
            self.lease_P = Polygon(self.leaseArea)
            self.lease_reduced = self.lease_P
            self.central_deploy_pos = average(self.leaseArea, axis=0)
            self.loc_position = self.leaseArea
        if 'electrical_connection_point' in kwargs:
            self.electricalConnectPoint = kwargs.get(
                'electrical_connection_point')
        if 'boundary_padding' in kwargs:
            self.boundaryPad = kwargs.get('boundary_padding')
        if 'bathymetry' in kwargs:
            self.bathy = kwargs.get('bathymetry')
        if 'bathy' in kwargs:
            self.bathy = kwargs.get('bathy')
        if 'nogo_areas' in kwargs:
            self.nogoAreas = kwargs.get('nogo_areas')
        if 'main_direction' in kwargs:
            self.mainDirection = kwargs.get('main_direction')
        if self.type:
            # self.getmain_angle_t()
            pass
        else:
            self.getmain_angle_w()

        self.check_inputs()
        if self.type:
            self.check_inputs_t()
        else:
            self.check_inputs_w()
            self.period = 2 * pi / self.freqs
            self.wnumber = WNumber(self.period, self.depth)
            self.change_angle_convention()
        self.printInput()

    def check_inputs(self):
        """ check_inputs() will check that the inputs provided for the site 
            conditions are of the proper format

        Returns:
            status (bool): True if successful, False otherwise.
            errStr (string): Message explaining the error encountered
        """
        status = True
        errStr = []
        """ self.main_angle if provided should be a numpy.ndarray of two 
            elements """
        if self.mainDirection is None:
            self.main_angle = 0.0  # set the main angle to 0 if not provided
        # else:
        #     try:
        #         if not isinstance(self.mainDirection, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append('The input format of the main direction is\
        #                        incorrect.\n Accepted format are: None or \
        #                        numpy.ndarray')
        #         print('The input format of the main direction is incorrect.\n \
        #                Accepted format are: None or numpy.ndarray')
        #         raise
            # try:
            #     if not self.mainDirection.size == 2:
            #         raise ValueError
            # except ValueError:
            #     status = False
            #     errStr.append(f'The dimension of the main direction input is '
            #                   f' incorrect.\n Accepted dimension: 2.')
            #     print(
            #         'The dimension of the main direction input is incorrect.\n \
            #            Accepted dimension: 2.')
        # TODO: set the self.main_angle to the user provided main direction    
        # """ self.nogoAreas if provided a numpy.ndarray of two elements """
        # if not self.nogoAreas is None:
        #     try:
        #         if not isinstance(self.nogoAreas, np.ndarray):
        #             raise TypeError
        #     except TypeError:
        #         print(f'The input format of the nogo area is incorrect.\n'
        #               f'Accepted format are: None or list.')
        #         status = False
        #         errStr.append(
        #             'The input format of the nogo area is incorrect.\n'
        #             'Accepted format are: None or numpy.ndarray.')
        #         raise
        #     try:
        #         if not self.nogoAreas.shape[1] == 2:
        #             raise TypeError
        #     except TypeError:
        #         print(f'The dimension of the main direction input is '
        #               f' incorrect.\n Accepted dimension: 2.')
        #         status = False
        #         errStr.append(
        #             f'The dimension of the main direction input is '
        #             f' incorrect.\n The first dimension should be 2.')
        #         raise
        # """ self.leaseArea should be a numpy.ndarray of two elements """
        # if not self.leaseArea is None:
        #     try:
        #         if not isinstance(self.leaseArea, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append('The input format of the lease area is\
        #                        incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print('The input format of the lease area is incorrect.\n\
        #                Accepted format are: numpy.ndarray')
        #         raise
        #     try:
        #         if not self.leaseArea.shape[1] == 2:
        #             raise ValueError('Lease area shape incorrect!')
        #     except ValueError:
        #         status = False
        #         errStr.append(f'The dimension of the lease area input is'
        #                       f' incorrect.\nThe first dimension should be 2.')
        #         print(f'The dimension of thelease area input is incorrect.'
        #               f'\nThe first dimension should be 2.')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The lease area is not provided. Input lease area!')
        #     print(f'The lease area is not provided. Input lease area!')
        #     raise TypeError(f'The lease area is not provided. Input lease'
        #                     f'area!')

        # """ self.bathy if provided should be a numpy.ndarray of two
        #     elements """
        # if not self.bathy is None:
        #     try:
        #         if not isinstance(self.bathy, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append('The input format of the bathymetry is\
        #                        incorrect.\n Accepted format are: numpy.ndarray')
        #         print('The input format of the bathymetry is incorrect.\n\
        #                Accepted format are: numpy.ndarray')
        #         raise
        #     print(self.bathy.shape[1])
        #     try:
        #         if not self.bathy.shape[1]==3:
        #             raise ValueError('Bathymetry shape incorrect!')
        #     except ValueError:
        #         status = False
        #         errStr.append(f'The dimension of the bathymetry input is'
        #                       f'incorrect.\nThe first dimension should be 3.')
        #         print(f'The dimension of the bathymetry input is incorrect.'
        #               f'\nThe first dimension should be 3.')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The bathymetry is not provided. Input bathymetry!')
        #     print(f'The bathymetry is not provided. Input bathymetry!')
        #     raise TypeError(f'The bathymetry is not provided. Input bathymetry'
        #                     f'!')

        # """ self.electricalConnectPoint should be a numpy.ndarray of two
        #     elements """
        # if not self.electricalConnectPoint is None:
        #     try:
        #         if not isinstance(self.electricalConnectPoint, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append('The input format of the main direction is\
        #                        incorrect.\n Accepted format are: None or \
        #                        numpy.ndarray')
        #         print('The input format of the main direction is incorrect.\n \
        #                Accepted format are: None or numpy.ndarray')
        #     try:
        #         if not self.electricalConnectPoint.size==2:
        #             raise ValueError
        #     except ValueError:
        #         status = False
        #         errStr.append(f'The dimension of electrical connection point'
        #                       f' input is '
        #                       f' incorrect.\n Accepted dimension: 2.')
        #         print(f'The dimension of the electrical connection point input'
        #               f' is incorrect.\n '
        #               f' Accepted dimension: 2.')
        # else:
        #     status  = False
        #     errStr.append(f'The electrical connection point is not provided. '
        #                   f'Input electrical connection point!')
        #     print(f'The electrical connection point is not provided. '
        #           f'Input electrical connection point!')
        #     raise TypeError(f'The electrical connection point is not provided.'
        #                     f' Input electrical connection point!')

        # """ self.boundaryPad should be a float """
        # if not self.boundaryPad is None:
        #     try:
        #         if not isinstance(self.boundaryPad, (float)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of the padding is'
        #                       f' incorrect.\n Accepted format are: None or'
        #                       f' float')
        #         print('The input format of the padding is incorrect.\n \
        #                Accepted format are: None or float')

        errStr = "\n".join(errStr)

    def check_inputs_w(self):
        """ check_inputs_w() will check that the inputs provided for the site 
            conditions are of the proper format for wave type of devices

        Returns:
            status (bool): True if successful, False otherwise.
            errStr (string): Message explaining the error encountered
        """
        status = True
        errStr = []
        # """ self.B should be a numpy.ndarray """
        # if not self.B is None:
        #     try:
        #         if not isinstance(self.B, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(
        #             f'The input format of the sea state wave '
        #             f'directions is incorrect.\n Accepted format are:'
        #             f' numpy.ndarray')
        #         print(f'The input format of the sea state wave directions is '
        #               f'incorrect.\n Accepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status = False
        #     errStr.append(f'The sea state wave directions is not provided. '
        #                   f'Input sea state wave directions!')
        #     print(f'The sea state wave directions is not provided. Input sea '
        #           f'state wave directions!')
        #     raise TypeError(f'The sea state wave directions is not provided. '
        #                     f'Input sea state wave directions!')
        # """ self.Hs should be a numpy.ndarray """
        # if not self.Hs is None:
        #     try:
        #         if not isinstance(self.Hs, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The input format of the sea state wave '
        #                       f'heights is incorrect.\n Accepted format are:'
        #                       f' numpy.ndarray')
        #         print(f'The input format of the sea state waveheights is '
        #               f'incorrect.\n Accepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status = False
        #     errStr.append(f'The sea state wave heights is not provided. '
        #                   f'Input sea state wave heights!')
        #     print(f'The sea state wave heights is not provided. Input sea '
        #           f'state wave heights!')
        #     raise TypeError(f'The sea state wave heights is not provided. '
        #                     f'Input sea state wave heights!')
        # """ self.Tp should be a numpy.ndarray """
        # if not self.Tp is None:
        #     try:
        #         if not isinstance(self.Tp, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The input format of the sea state wave '
        #                       f'periods is incorrect.\n Accepted format are:'
        #                       f' numpy.ndarray')
        #         print(f'The input format of the sea state periods is '
        #               f'incorrect.\n Accepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status = False
        #     errStr.append(f'The sea state wave periods is not provided. '
        #                   f'Input sea state wave periods!')
        #     print(f'The sea state wave periods is not provided. Input sea '
        #           f'state wave periods!')
        #     raise TypeError(f'The sea state wave periods is not provided. '
        #                     f'Input sea state wave periods!')
        # """ self.scatDiag should be a numpy.ndarray """
        # if not self.scatDiag is None:
        #     try:
        #         if not isinstance(self.scatDiag, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The input format of the probability of '
        #                       f'occurence is incorrect.\n Accepted format are:'
        #                       f' numpy.ndarray')
        #         print(f'The input format of the probability of occurence is '
        #               f'incorrect.\n Accepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status = False
        #     errStr.append(f'The probability of occurence is not provided. '
        #                   f'Input probability of occurence!')
        #     print(f'The probability of occurence is not provided. Input '
        #           f'probability of occurence!')
        #     raise TypeError(f'The probability of occurence is not provided. '
        #                     f'Input probability of occurence!')
        # """ self.depth should be a float """
        # if not self.depth is None:
        #     try:
        #         if not isinstance(self.depth, float):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The water depth input should be a float.')
        #         print(f'The water depth input should be a float.')
        # else:
        #     status = False
        #     errStr.append(f'The water depth input is not provided. '
        #                   f'Input water depth !')
        #     print(f'The water depth input is not provided. Input '
        #           f'water depth!')
        #     raise TypeError(f'The water depth input is not provided. '
        #                     f'Input water depth!')
        # """ self.freqs should be a numpy.ndarray """
        # if not self.freqs is None:
        #     try:
        #         if not isinstance(self.freqs, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The frequency input should be a float.')
        #         print(f'The frequency input should be a float.')
        # else:
        #     status = False
        #     errStr.append(f'The frequency input is not provided. '
        #                   f'Input frequency !')
        #     print(f'The frequency input is not provided. Input ' f'frequency!')
        #     raise TypeError(f'The frequency input is not provided. '
        #                     f'Input frequency!')
        # """ self.dirs should be a numpy.ndarray """
        # if not self.dirs is None:
        #     try:
        #         if not isinstance(self.dirs, (np.ndarray, list, float)):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The wave directions input should be a float.')
        #         print(f'The wave directions input should be a float.')
        # else:
        #     status = False
        #     errStr.append(f'The wave directions input is not provided. '
        #                   f'Input wave directions !')
        #     print(f'The wave directions input is not provided. Input '
        #           f'wave directions!')
        #     raise TypeError(f'The wave directions input is not provided. '
        #                     f'Input wave directions!')
        # """ self.specType should be a numpy.ndarray, with three elements. The
        #     first element should be a string contained in the list 
        #     sprectal_type, the second and third should be positive floats.
        # """
        # if not self.specType is None:
        #     try:
        #         if not isinstance(self.specType, tuple):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The spectral type input should be a tuple.')
        #         print(f'The spectral type input should be a tuple.')
        #     try:
        #         if not len(self.specType) == 3:
        #             raise ValueError
        #     except ValueError:
        #         status = False
        #         errStr.append(f'The spectral type input should contain 3 '
        #                       f'elements.')
        #         print(f'The spectral type input should contain 3 elements.')
        #     try:
        #         if not (isinstance(self.specType[1], float)
        #                 and isinstance(self.specType[2], float)):
        #             raise ValueError
        #     except ValueError:
        #         status = False
        #         errStr.append(f'The spectral type peak factor and spreading'
        #                       f'parameter should be floats')
        #         print(f'The spectral type peak factor and spreading'
        #               f'parameter should be floats')
        #     try:
        #         if not self.specType[0] in spectral_type:
        #             raise ValueError
        #     except ValueError:
        #         status = False
        #         errStr.append(
        #             f'The spectral type description input '
        #             f'is not in the list of accepted types.\nAccepted'
        #             f' types are {spectral_type}.')
        #         print(
        #             f'The spectral type description input is not in the list '
        #             f'of accepted types.\n'
        #             f'Accepted types are {spectral_type}.')

        # else:
        #     status = False
        #     errStr.append(f'The spectral type input is not provided. '
        #                   f'Input spectral type !')
        #     print(f'The spectral type input is not provided. Input '
        #           f'spectral type!')
        #     raise TypeError(f'The spectral type input is not provided. '
        #                     f'Input spectral type!')

        errStr = "\n".join(errStr)
        print(errStr)

    def check_inputs_t(self):
        """ check_inputs_t() will check that the inputs provided for the site 
            conditions are of the proper format for tidal type of devices

        Returns:
            status (bool): True if successful, False otherwise.
            errStr (string): Message explaining the error encountered
        """
        print('This will check the inputs for the tidal case')
        status = True
        errStr = []
        # """ self.depth should be a float """
        # if not self.depth is None:
        #     try:
        #         if not isinstance(self.depth, np.ndarray):
        #             raise TypeError
        #     except TypeError:
        #         status = False
        #         errStr.append(f'The water depth input should be a numpy.ndarray.')
        #         print(f'The water depth input should be a numpy.ndarray.')
        # else:
        #     status  = False
        #     errStr.append(f'The water depth input is not provided. '
        #                   f'Input water depth !')
        #     print(f'The water depth input is not provided. Input '
        #           f'water depth!')
        #     raise TypeError(f'The water depth input is not provided. '
        #                     f'Input water depth!')

        # """ self.V should be a numpy.ndarray """
        # if not self.V is None:
        #     try:
        #         if not isinstance(self.V, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of y-contribution to the '
        #                       f'velocity field  is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the y-contribution to the velocity'
        #               f' field  is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The y-contribution to the velocity field is'
        #                   f' not provided.'
        #                   f' Input y-contribution to the velocity field!')
        #     print(f'The y-contribution to the velocity field is not provided. '
        #           f'Input the y-contribution to the velocity field!')
        #     raise TypeError(f'The y-contribution to the velocity field is not'
        #                     f' provided. Input the y-contribution to the '
        #                     f'velocity field')

        # """ self.U should be a numpy.ndarray """
        # if not self.U is None:
        #     try:
        #         if not isinstance(self.U, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of x-contribution to the '
        #                       f'velocity field  is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the x-contribution to the velocity'
        #               f' field  is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The x-contribution to the velocity field is'
        #                   f' not provided.'
        #                   f' Input x-contribution to the velocity field!')
        #     print(f'The x-contribution to the velocity field is not provided. '
        #           f'Input the x-contribution to the velocity field!')
        #     raise TypeError(f'The x-contribution to the velocity field is not'
        #                     f' provided. Input the x-contribution to the '
        #                     f'velocity field')

        # """ self.p should be a numpy.ndarray """
        # if not self.p is None:
        #     try:
        #         if not isinstance(self.p, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of the probability of '
        #                       f'occurence is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the probability of occurence'
        #               f' is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The probability of occurence is'
        #                   f' not provided.'
        #                   f' Input probability of occurence!')
        #     print(f'The probability of occurence is not provided. '
        #           f'Input the probability of occurence!')
        #     raise TypeError(f'The probability of occurence is not'
        #                     f' provided. Input the probability of occurence!')

        # """ self.x should be a numpy.ndarray """
        # if not self.x is None:
        #     try:
        #         if not isinstance(self.x, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of x-coordinates (Easting) '
        #                       f'is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the x-coordinates (Easting)'
        #               f' is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The x-coordinates (Easting) is'
        #                   f' not provided.'
        #                   f' Input x-coordinates (Easting)!')
        #     print(f'The x-coordinates (Easting) is not provided. '
        #           f'Input the x-coordinates (Easting)!')
        #     raise TypeError(f'The x-coordinates (Easting) is not'
        #                     f' provided. Input the x-coordinates (Easting)!')

        # """ self.y should be a numpy.ndarray """
        # if not self.y is None:
        #     try:
        #         if not isinstance(self.y, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of y-coordinates (Northing) '
        #                       f'is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the y-coordinates (Northing)'
        #               f' is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The y-coordinates (Northing) is'
        #                   f' not provided.'
        #                   f' Input y-coordinates (Northing)!')
        #     print(f'The y-coordinates (Northing) is not provided. '
        #           f'Input the y-coordinates (Northing)!')
        #     raise TypeError(f'The y-coordinates (Northing) is not'
        #                     f' provided. Input the y-coordinates (Northing)!')

        # """ self.TI should be a numpy.ndarray """
        # if not self.TI is None:
        #     try:
        #         if not isinstance(self.TI, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of the turbulence intensity '
        #                       f'is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the turbulence intensity'
        #               f' is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The turbulence intensity is'
        #                   f' not provided.'
        #                   f' Input turbulence intensity!')
        #     print(f'The turbulence intensity is not provided. '
        #           f'Input the turbulence intensity!')
        #     raise TypeError(f'The turbulence intensity is not'
        #                     f' provided. Input the turbulence intensity!')

        # """ self.BR should be a float with values 1 or 0 """
        # if not self.BR is None:
        #     try:
        #         if not isinstance(self.BR, (int,float)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of the BR ratio '
        #                       f'is'
        #                       f'incorrect.\n Accepted format are: float'
        #                       )
        #         print(f'The input format of the BR ratio'
        #               f' is '
        #               f'incorrect.\nAccepted format are: float')
        #         raise
        #     try:
        #         if not (self.BR == 0 or self.BR == 1):
        #             raise ValueError
        #     except ValueError:
        #         status  = False
        #         errStr.append(f'The BR ratio '
        #                       f'is'
        #                       f'incorrect.\n Accepted values are: 0 or 1'
        #                       )
        #         print(f'The BR ratio'
        #               f' is '
        #               f'incorrect.\nAccepted values are: 0 or 1')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The BR ratio is'
        #                   f' not provided.'
        #                   f' Input BR ratio!')
        #     print(f'The BR ratio is not provided. '
        #           f'Input the BR ratio!')
        #     raise TypeError(f'The BR ratio is not'
        #                     f' provided. Input the BR ratio!')

        # """ self.velocityShear should be a numpy.ndarray """
        # if not self.velocityShear is None:
        #     try:
        #         if not isinstance(self.velocityShear, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append(f'The input format of the tidal velocity shear is'
        #                       f'incorrect.\n Accepted format are: numpy.ndarray'
        #                       )
        #         print(f'The input format of the tidal velocity shear is '
        #               f'incorrect.\nAccepted format are: numpy.ndarray')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The tidal velocity shear is not provided.'
        #                   f' Input tidal velocity shear!')
        #     print(f'The tidal velocity shear is not provided. Input tidal'
        #           f' velocity shear!')
        #     raise TypeError(f'The tidal velocity shear is not provided. Input'
        #                     f' tidal velocity shear!')

        # """ self.geophysics should be a numpy.ndarray and the second dimension
        #     should be three """
        # if not self.geophysics is None:
        #     try:
        #         if not isinstance(self.geophysics, (np.ndarray)):
        #             raise TypeError
        #     except TypeError:
        #         status  = False
        #         errStr.append('The input format of the geophysics is\
        #                        incorrect.\n Accepted format are: numpy.ndarray')
        #         print('The input format of the geophysics is incorrect.\n\
        #                Accepted format are: numpy.ndarray')
        #         raise
        #     try:
        #         if not self.geophysics.shape[0]==3:
        #             raise ValueError
        #     except ValueError:
        #         status = False
        #         errStr.append(f'The dimension of the geophysics input is'
        #                       f'incorrect.\nThe first dimension should be 3.')
        #         print(f'The dimension of the geophysics input is incorrect.'
        #               f'\nThe first dimension should be 3.')
        #         raise
        # else:
        #     status  = False
        #     errStr.append(f'The geophysics is not provided. Input geophysics!')
        #     print(f'The geophysics is not provided. Input geophysics!')
        #     raise TypeError(f'The geophysics is not provided. Input geophysics'
        #                     f'!')

        # geophysics = self.geophysics
        # if len(geophysics) == 1:
        #         x = self.x
        #         y = self.y
        #         X, Y = np.meshgrid(x,y)
        #         G = np.ones(X.shape)*geophysics[0,0,0]
        #         self.geophysics = np.vstack((X.ravel(),Y.ravel(),G.ravel())).T

        # self.check_site_data_boundary()
        # self.irrotational_flow()

        # errStr = "\n".join(errStr)

    def getmain_angle_w(self):
        """
        getmain_angle: calculates the main angle used to orient the array layouts
                      for the wave case

        Args:
            self (class)

        Note:
            the method update the following attributes:
                .main_angle (float): orientation angle (rad) in the North-Easting
                                    global convention

        """
        ind_max_direction = np.argmax(np.sum(self.scatDiag, (0, 1)))
        angle = self.B[ind_max_direction]
        self.mainDirection = np.array(
            [np.cos(angle), np.sin(angle)], dtype=float)
        # print('The main direction is:', self.mainDirection)
        self.main_angle = anglewrap(np.arctan2(
            self.mainDirection[1],
            self.mainDirection[0],
        ),
                                    conversion='r2r')

        # strDEBUG = 'The main angle is \n'
        # strDEBUG += str(self.main_angle * 180 / np.pi)
        # strDEBUG += '\n' + '*' * 100
        # strDEBUG += 'The main direction is \n'
        # strDEBUG += str(self.mainDirection)
        # strDEBUG += '\n' + '*' * 100
        # module_logger.debug(strDEBUG)

    def getmain_angle_t(self):
        """
        getmain_angle: calculates the main angle used to orient the array layouts

        Args:
            self (class)

        Note:
            the method update the following self.S_data attributes:
                .main_angle (float): orientation angle (rad) in the North-Easting global convention

        """
        # for the tidal case the main direction is given by
        # the negative of the mean stream direction (U,V) of the most
        # representative sea condition
        ind_max_direction = np.argmax(self.probability)
        U = np.nanmean(self.U[:, :, ind_max_direction])
        V = np.nanmean(self.V[:, :, ind_max_direction])
        self.mainDirection = -np.array([U, V])

        self.main_angle = anglewrap(np.arctan2(
            self.mainDirection[1],
            self.mainDirection[0],
        ),
                                    conversion='r2r')

        # strDEBUG = 'The main angle is \n'
        # strDEBUG += str(self.S_data.main_angle * 180 / np.pi)
        # strDEBUG += '\n' + '*' * 100
        # strDEBUG += 'The main direction is \n'
        # strDEBUG += str(self.S_data.Main_Direction)
        # strDEBUG += '\n' + '*' * 100
        # module_logger.debug(strDEBUG)

    def printInput(self, indent=4):
        """print the Site conditions arguments

        Args:
            indent(int,Optional): define the indent level of the printed string
        """

        vars_string = pformat(vars(self), indent=indent)
        logMsg = '--> MACHINE INPUT SUMMARY:\n\n{}'.format(vars_string)
        # module_logger.info(logMsg)

        return

    def change_angle_convention(self):
        """
        change_angle_convention: convert the angles given in the metocean convention into
            the East-North (UTM) coordinate system

        Args:
            self (class)

        Note:
            the method update the following self.S_data attributes:
                .MeteoceanConditions (dic): wave directions converted from South-West to East-North

            the method update the following self.M_data attributes:
                .SingleMachine (dic): wave directions converted from South-West to East-North
        """
        old_beta_array = self.B
        new_beta_array = convertangle(old_beta_array)
        self.B = new_beta_array

    def irrotational_flow(self):
        U = self.U.copy()
        V = self.V.copy()
        x = self.X.copy()
        y = self.Y.copy()
        u_sh = U.shape
        if not len(u_sh) == 3:
            raise IOError("The shape of the velocity field is incorrect",
                          "The matrix need to be nx, ny, n_seastate")
        if not u_sh[0] == len(x):
            raise IOError("The shape of the velocity field is incorrect",
                              "The matrix need to be nx, ny, n_seastate")

        for el in range(u_sh[2]):
            curl = self.__curl_2d(U[:, :, el], V[:, :, el], x, y)
            if np.sqrt(curl.max()**2 + curl.min()**2) > 0.1:
                raise IOError("The input velocity field is highly rotational ",
                              "This violate the model assumption ",
                              "The execution is terminated")

    def __curl_2d(self, uu, vv, x, y):

        u = uu.copy() - np.nanmin(uu)
        if np.nanmax(u): u /= np.nanmax(u)
        u -= np.nanmean(u)

        v = vv.copy() - np.nanmin(vv)
        if np.nanmax(v): v /= np.nanmax(v)
        v -= np.nanmean(v)

        dx = np.mean(x[1:] - x[:-1])
        dy = np.mean(y[1:] - y[:-1])

        u = np.nan_to_num(u)
        v = np.nan_to_num(v)

        x_var = interpolate.interp2d(x, y, u.T, 'cubic')
        y_var = interpolate.interp2d(x, y, v.T, 'cubic')

        dvdx = (y_var(x + dx, y) - y_var(x - dx, y)) / (2 * dx)
        dudy = (x_var(x, y + dy) - x_var(x, y - dy)) / (2 * dy)

        return dvdx - dudy

# if __name__ == "__main__":
#     from dtop_energycapt import business
#     import numpy as np
    
#     obj = business.stg3utils.site_conditions.site_conditions.SiteConditions(boundary_padding=10)

#     x = np.linspace(0,10,20)
#     y = np.linspace(0,10,20)
#     obj.X, obj.Y = x, y

#     obj.U = np.array([1,1,1.])
#     obj.V = np.array([0,0,0.])
    
#     # with pytest.raises(IOError) as excinfo:
#     #     obj.irrotational_flow()

#     obj.U = np.random.rand(20,20,2)
#     obj.V = np.random.rand(20,20,2)

#     obj.U = np.random.rand(2,20,20)
#     obj.V = np.random.rand(2,20,20)
#     obj.irrotational_flow()