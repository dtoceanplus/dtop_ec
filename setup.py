# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop-energycapt",
    version="0.0.1",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    install_requires=[
        "flask",
        "flask-babel",
        "flask-cors",
        "requests",
        "Flask-SQLAlchemy",
        "flask-marshmallow",
        "marshmallow-sqlalchemy"
    ],
    include_package_data=True,
    zip_safe=False,
)
