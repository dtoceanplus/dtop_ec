SHELL := /bin/sh

.PHONY: $(wildcard x-*)

export DTOP_DOMAIN = dto.test
export DTOP_LOCAL = 0.0.0.0/

all: x-env x-dredd

run_q:
	docker-compose --f docker-compose.test.yml -f docker-compose.yml up nginx mm mc.dto.test sc.dto.test

run:
	docker-compose --f docker-compose.test.yml -f docker-compose.yml up nginx ec mm mc.dto.test sc.dto.test client ec_redis ec_worker

build-cypress-image:
	docker build --tag dtop:ec-cypress --file ./test/e2e-cypress/Dockerfile ./test/e2e-cypress

x-api:
	$(MAKE) --directory=api xtest
	cp -f ./api/dtop-ec/0.0.1/openapi.json ./src/dtop_energycapt/service/static

x-env:
	pip install pipenv
	npm install dredd sync-request

x-dredd:
	node_modules/dredd/bin/dredd --config ./tets/dredd/dredd-local.yml

x-be-img:
	docker build --tag ec --file ./backend.dockerfile .

x-be-dev: x-be-img
	docker run --tty --interactive ec bash

x-be-run: x-be-img
	docker run --detach -p 5000:5000 ec

x-be-test:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --exit-code-from=test-ec ec test-ec

x-pytest:
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	ec_worker pytest mc.dto.test sc.dto.test

x-test-task:
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=test-task \
	test-task ec_worker

build-dredd:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml build ec dredd ec_worker mc.dto.test sc.dto.test

build-backend:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml build ec

dredd:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --exit-code-from=dredd ec dredd ec_worker mc.dto.test sc.dto.test

verify:
	docker-compose --project-name ec -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name ec -f verifiable.docker-compose -f verifier.docker-compose stop ec
	docker cp `docker-compose --project-name ec -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify

test-create-task:
	apk add bash
	wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -P /tmp
	chmod +x /tmp/wait-for-it.sh
	bash /tmp/wait-for-it.sh ec:5000
	echo '{"title": "RM3 North Sea test","desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea","complexity": 1,"type": "TEC", "date": "2019-11-14", "status": 80,"tags": "RM3-NS"}' | http --print=HBhb --check-status ec:5000/ec
	echo '{"nb_devices": 1, "layout_type": "","opt_method": "","optimisation_threshold": null,"array_layout": "[[0,0]]","notes": "", "array_generic_type": ""}' | http --print=HBhb --check-status put ec:5000/ec/1/inputs/farm
	echo '{"loc_resource": 3,"loc_position": [[0,0],[500,500],[0,500]]}' | http --print=HBhb --check-status put ec:5000/ec/1/inputs/site/tec/complexity1
	echo '{"cp": 0.3,"main_dim_device": 10, "sec_dim_device": 15,"rated_pow_device": 500}' | http --print=HBhb --check-status put ec:5000/ec/1/inputs/machine/tec/complexity1
	echo '{"id": 1}' | http --print=HBhb --check-status ec:5000/ec/tasks

impose:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml run --rm contracts