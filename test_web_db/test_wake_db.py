from dtop_energycapt import business
import pytest
import pickle
import pandas as pd
import msgpack


def test_tidal_wake_db():
    f_n = './src/instance/dtop_tidal_wakes_db.msgpack'

    f = open(f_n, 'r+b')
    binary_format = f.read()
    f.close()

    data = msgpack.unpackb(binary_format)
    
    assert 'dfY' in data.keys()
