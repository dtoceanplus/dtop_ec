FROM apiaryio/dredd

RUN apk --no-cache add bash py3-pip
RUN pip3 --no-cache-dir install dredd-hooks requests

RUN wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -P /usr/local/bin
RUN chmod +x /usr/local/bin/wait-for-it.sh
