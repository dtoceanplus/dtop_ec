import json
import os
import traceback
from functools import partial

import dredd_hooks as hooks
from dredd_utils_data import *

import sys
import requests
import binascii

sys.stdout = sys.stderr = open("hooks-output.txt", "w")
def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction['skip']:
            func(transaction)
    return wrapper
# filename = "hooks-output.txt"
# if os.path.exists(filename):
#     os.remove(filename)

# file = open(filename, "w")
# print = partial(print, file=file)

# def if_not_skipped(func):
#     def wrapper(transaction):
#         try:
#             func(transaction)
#         except:
#             traceback.print_exc(file=file)
#         finally:
#             file.flush()
#     return wrapper

def clear_db(tr):
    base_url = f"{tr['protocol']}//{tr['host']}:{tr['port']}/ec"
    response = requests.get(base_url)
    if response.status_code == 200:
        existing_project = response.json()
        for project in existing_project:
            response = requests.delete(f'{base_url}/{project["id"]}')

def post_project(tr,mac='wec',cpx=1):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    body["complexity"]=cpx
    body["type"]=mac.upper()
    r = requests.post(f'{protocol}//{host}:{port}/ec', json=body)

def save_input_farm(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    
    r = requests.put(f'{protocol}//{host}:{port}/ec/1/inputs/farm', json=body_input_farm)

def save_input_site(tr):
    uri = tr['request']['uri']
    mac = uri.split('/')[-2]
    cpx = uri.split('/')[-1]
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    r = requests.put(f'{protocol}//{host}:{port}{uri}', json=body_input_site[mac][cpx])

def save_input_machine(tr):
    uri = tr['request']['uri']
    mac = uri.split('/')[-2]
    cpx = uri.split('/')[-1]
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    r = requests.put(f'{protocol}//{host}:{port}{uri}', json=body_input_machine[mac][cpx])

def post_task(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    r = requests.post(f'{protocol}//{host}:{port}/ec/tasks', json={"id":1})
    return r

def post_a_farm(tr, ecId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/ec/{ecId}/farm',
                      json=body_farm)

def post_devices(tr, ecId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/ec/{ecId}/devices',
                      json=[body_device, body_device])

# to be removed
def get_project_ids(tr):
    existing_project = check_project(tr)
    ids = [pr['id'] for pr in existing_project]
    return list(set(ids))


def get_devices_id(tr, ecId):
    existing_project = check_project(tr)
    prj = [pr for pr in existing_project if pr['id'] == ecId]
    if prj:
        print(prj)
        return prj[0]['devices']

    return []


def check_project(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    existing_project = requests.get(f'{protocol}//{host}:{port}/ec').json()
    return existing_project








def post_a_device_list(tr, ecId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/ec/{ecId}/devices',
                      json=[body_device, body_device])
    print('POST A LIST OF DEVICES')
    print([body_device, body_device])
    print(f'{protocol}//{host}:{port}/ec/{ecId}/devices')
    print(r.status_code)
    print(r)


def get_a_project(tr):
    existing_project = check_project(tr)
    if existing_project:  # if any project exist
        project = existing_project[0]
    else:
        post_project(tr)
        projects = check_project(tr)
        if projects:
            project = projects[0]
        else:
            print('ERROR NO PROJECT AVAILABLE IN THE DB')

    return project


def get_a_project_wo_farm(tr):
    print('get_a_project_wo_farm')
    existing_project = check_project(tr)
    if existing_project:  # if any project exist
        for project in existing_project:
            if not project['farm']:
                print('Found a project without a farm')
                return project

    print('No project without a farm')
    print('posting a new project')
    post_project(tr)
    project = get_a_project_wo_farm(tr)
    print(project)

    return project


def get_project_w_device(tr):
    existing_project = check_project(tr)
    if existing_project:  # if any project exist
        for project in existing_project:
            if project['device']:
                print('Found a project with a device')
                return project
        else:
            post_device()

    post_project(tr)

def submit_project_inputs(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']


    r = requests.put(f'{protocol}//{host}:{port}/ec/1/inputs/farm', json=body_inputs)
    r = requests.put(f'{protocol}//{host}:{port}/ec/1/inputs/site', json={"site_url": "/sc/1/site"})
    r = requests.put(f'{protocol}//{host}:{port}/ec/1/inputs/machine', json={"site_url": "/mc/1/hydrodynamic"})

def prepare_multipart_formdata(fields, filename):

    fn = filename
    if os.path.isfile(fn):
        print (filename + " exists")
    else:
        print (filename + " doesn't exist")
        exit()

    with open(fn, 'r') as f:
        content_dict = json.load(f)

    boundary = binascii.hexlify(os.urandom(16)).decode('ascii')

    body = (
        "".join("--%s\r\n"
                "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n"
                "Content-Type: application/json\r\n"
                "\r\n"
                "%s\r\n" % (boundary, field, filename, json.dumps(content_dict))
                for field, value in fields.items()) +
        "\r\n--%s--\r\n" % boundary
    )

    content_type = "multipart/form-data; boundary=%s" % boundary

    return content_type, body
