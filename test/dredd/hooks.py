import json
import os
import traceback
from functools import partial

import dredd_hooks as hooks
import requests

from dredd_utils import *

# @hooks.before_each
# @print_log
# def my_before_each_hook(transaction):
#   print(f'@hooks.before("{transaction["name"]}")')

# @hooks.before("/ec > Add an Energy Capture (EC) design > 201")
# @hooks.before("/ec > Add an Energy Capture (EC) design > 400")
# @hooks.before("/ec > Return the list of Energy Capture (EC) designs > 200 > application/json")
# @hooks.before("/ec > Return the list of Energy Capture (EC) designs > 404")
# @hooks.before("/ec/{ecId} > Return the Energy Capture (EC) design > 200 > application/json")
# @hooks.before("/ec/{ecId} > Return the Energy Capture (EC) design > 404")
# @hooks.before("/ec/{ecId} > Delete the Energy Capture (EC) design > 200")
# @hooks.before("/ec/{ecId} > Delete the Energy Capture (EC) design > 404")
# @hooks.before("/ec/{ecId} > Update the Energy Capture (EC) design > 201")
# @hooks.before("/ec/{ecId} > Update the Energy Capture (EC) design > 400")
# @hooks.before("/ec/{ecId}/inputs/farm > Farm inputs status > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/farm > Farm inputs status > 404")
# @hooks.before("/ec/{ecId}/inputs/farm > Save farm inputs > 201")
# @hooks.before("/ec/{ecId}/inputs/farm > Save farm inputs > 400")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity1 > Returns the description of the site inputs tec-1 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity1 > Returns the description of the site inputs tec-1 > 404")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity1 > Create the resource site input tec-1 > 201")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity1 > Create the resource site input tec-1 > 400")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity2 > Returns the description of the site inputs tec-2 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity2 > Returns the description of the site inputs tec-2 > 404")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity2 > Create the resource site input tec-2 > 201")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity2 > Create the resource site input tec-2 > 400")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity3 > Returns the description of the site inputs tec-3 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity3 > Returns the description of the site inputs tec-3 > 404")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity3 > Create the resource site input tec-3 > 201")
# @hooks.before("/ec/{ecId}/inputs/site/tec/complexity3 > Create the resource site input tec-3 > 400")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity1 > Returns the description of the site inputs wec-1 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity1 > Returns the description of the site inputs wec-1 > 404")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity1 > Create the resource site input wec-1 > 201")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity1 > Create the resource site input wec-1 > 400")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity2 > Returns the description of the site inputs wec-2 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity2 > Returns the description of the site inputs wec-2 > 404")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity2 > Create the resource site input wec-2 > 201")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity2 > Create the resource site input wec-2 > 400")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity3 > Returns the description of the site inputs wec-3 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity3 > Returns the description of the site inputs wec-3 > 404")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity3 > Create the resource site input wec-3 > 201")
# @hooks.before("/ec/{ecId}/inputs/site/wec/complexity3 > Create the resource site input wec-3 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity1 > Upload site inputs tec-1 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity1 > Upload site inputs tec-1 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity2 > Upload site inputs tec-2 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity2 > Upload site inputs tec-2 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity3 > Upload site inputs tec-3 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity3 > Upload site inputs tec-3 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity1 > Upload site inputs wec-1 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity1 > Upload site inputs wec-1 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity2 > Upload site inputs wec-2 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity2 > Upload site inputs wec-2 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity3 > Upload site inputs wec-3 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity3 > Upload site inputs wec-3 > 400")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity1 > Returns the description of the machine inputs tec-1 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity1 > Returns the description of the machine inputs tec-1 > 404")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity1 > Create the resource machine input tec-1 > 201")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity1 > Create the resource machine input tec-1 > 400")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity2 > Returns the description of the machine inputs tec-2 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity2 > Returns the description of the machine inputs tec-2 > 404")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity2 > Create the resource machine input tec-2 > 201")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity2 > Create the resource machine input tec-2 > 400")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity3 > Returns the description of the machine inputs tec-3 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity3 > Returns the description of the machine inputs tec-3 > 404")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity3 > Create the resource machine input tec-3 > 201")
# @hooks.before("/ec/{ecId}/inputs/machine/tec/complexity3 > Create the resource machine input tec-3 > 400")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity1 > Returns the description of the machine inputs wec-1 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity1 > Returns the description of the machine inputs wec-1 > 404")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity1 > Create the resource machine input wec-1 > 201")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity1 > Create the resource machine input wec-1 > 400")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity2 > Returns the description of the machine inputs wec-2 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity2 > Returns the description of the machine inputs wec-2 > 404")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity2 > Create the resource machine input wec-2 > 201")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity2 > Create the resource machine input wec-2 > 400")
@hooks.before("/ec/{ecId}/inputs/machine/wec/complexity3 > Returns the description of the machine inputs wec-3 > 200 > application/json")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity3 > Returns the description of the machine inputs wec-3 > 404")
@hooks.before("/ec/{ecId}/inputs/machine/wec/complexity3 > Create the resource machine input wec-3 > 201")
# @hooks.before("/ec/{ecId}/inputs/machine/wec/complexity3 > Create the resource machine input wec-3 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity1 > Upload machine inputs tec-1 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity1 > Upload machine inputs tec-1 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity2 > Upload machine inputs tec-2 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity2 > Upload machine inputs tec-2 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity3 > Upload machine inputs tec-3 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity3 > Upload machine inputs tec-3 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity1 > Upload machine inputs wec-1 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity1 > Upload machine inputs wec-1 > 400")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity2 > Upload machine inputs wec-2 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity2 > Upload machine inputs wec-2 > 400")
@hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity3 > Upload machine inputs wec-3 > 201")
# @hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity3 > Upload machine inputs wec-3 > 400")
# @hooks.before("/mc/tasks > add a task to RQ > 202")#
# @hooks.before("/mc/tasks > add a task to RQ > 400")
# @hooks.before("/ec/tasks/{taskId} > Task Id status > 200 > application/json")#
# @hooks.before("/ec/tasks/{taskId} > Task Id status > 404")
@hooks.before("/ec/tasks/{taskId} > Task Id status > 500") # SKIPPED
# @hooks.before("/ec/{ecId}/farm > Returns the description of the designed farm > 200 > application/json")
# @hooks.before("/ec/{ecId}/farm > Returns the description of the designed farm > 404")
# @hooks.before("/ec/{ecId}/farm > Create the resource farm > 201")
# @hooks.before("/ec/{ecId}/farm > Create the resource farm > 400")
# @hooks.before("/ec/{ecId}/farm > Delete the resource farm > 200")
# @hooks.before("/ec/{ecId}/farm > Delete the resource farm > 404")
# @hooks.before("/ec/{ecId}/devices > List devices > 200 > application/json")
# @hooks.before("/ec/{ecId}/devices > List devices > 404")
# @hooks.before("/ec/{ecId}/devices > Create list of devices > 201")
# @hooks.before("/ec/{ecId}/devices > Create list of devices > 400")
@hooks.before("/ec/{ecId}/devices/{deviceId} > Read device > 200 > application/json")  # SKIPPED DUE TO THE MODIFICATION ASSOCIATED WITH THE ROTOR
# @hooks.before("/ec/{ecId}/devices/{deviceId} > Read device > 404")
# @hooks.before("/ec/{ecId}/devices/{deviceId} > Delete a device > 200")
# @hooks.before("/ec/{ecId}/devices/{deviceId} > Delete a device > 404")
@hooks.before("/ec/{ecId}/results/tec/complexity1 > Additional result tec-1 > 200 > application/json") # SKIPPED
@hooks.before("/ec/{ecId}/results/tec/complexity2 > Additional result tec-2 > 200 > application/json") # SKIPPED
# @hooks.before("/ec/{ecId}/results/tec/complexity2 > Additional result tec-2 > 404")
@hooks.before("/ec/{ecId}/results/tec/complexity3 > Additional result tec-3 > 200 > application/json") # SKIPPED
# @hooks.before("/ec/{ecId}/results/tec/complexity3 > Additional result tec-3 > 404")
@hooks.before("/ec/{ecId}/results/wec/complexity3 > Additional result wec-2 > 200 > application/json") # SKIPPED
# @hooks.before("/ec/{ecId}/results/wec/complexity3 > Additional result wec-2 > 404")
def skip(transaction):
    """
    Skip the test.
    """
    transaction['skip'] = True

@hooks.before_each
@if_not_skipped
def before_each(transaction):
    clear_db(transaction)
    if transaction['expected']['statusCode'] == '200' or transaction['expected']['statusCode'] == '201' or transaction['expected']['statusCode'] == '202':
        uri = transaction['request']['uri'].split('/')
        try:
            mac = uri[-2].upper()
            cpx = int(uri[-1][-1])
            if mac.lower() in ['wec', 'tec'] and cpx in [1,2,3]:
                post_project(transaction, mac=mac.lower(), cpx=cpx)
            else:
                raise
        except:
            post_project(transaction)

@hooks.before("/ec/{ecId}/inputs/site/fetch > fetch/save site characterization data > 201")
def get_site_fetch(transaction):
    # in order to use the actual example in the openAPI the body is changed to match the mountbank new naming
    transaction['request']['body'] = json.dumps({"url":"http://sc.dto.test","entityID":1}) 

@hooks.before("/ec/{ecId}/inputs/machine/fetch > fetch/save machine characterization data > 201")
def get_machine_fetch(transaction):
    # in order to use the actual example in the openAPI the body is changed to match the mountbank new naming
    transaction['request']['body'] = json.dumps({"url":"http://mc.dto.test","entityID":1}) 

@hooks.before("/ec/{ecId}/results/tec/complexity1 > Additional result tec-1 > 200 > application/json")
@hooks.before("/ec/{ecId}/results/tec/complexity2 > Additional result tec-2 > 200 > application/json")
@hooks.before("/ec/{ecId}/results/tec/complexity3 > Additional result tec-3 > 200 > application/json")
def get_results(transaction):
    post_a_farm(transaction, 1)
    post_devices(transaction, 1)

@hooks.before("/ec/tasks/{taskId} > Task Id status > 200 > application/json")#
@if_not_skipped
def get_task_200(transaction):
    transaction['request']['body'] = json.dumps({"id": 1}) 
    fake_tr = {'request': {'uri': "/ec/1/inputs/machine/wec/complexity1"}}
    fake_tr['protocol'] = transaction['protocol']
    fake_tr['host'] = transaction['host']
    fake_tr['port'] = transaction['port']
    save_input_machine(fake_tr)
    fake_tr = {'request': {'uri': "/ec/1/inputs/site/wec/complexity1"}}
    fake_tr['protocol'] = transaction['protocol']
    fake_tr['host'] = transaction['host']
    fake_tr['port'] = transaction['port']
    save_input_site(fake_tr)
    save_input_farm(fake_tr)
    r = post_task(transaction)
    print(r.json)
    print(r.json())
    task_id = r.json()["data"]["task_id"]
    transaction['request']['uri'] = f'/ec/tasks/{task_id}'
    transaction['fullPath'] = f'/ec/tasks/{task_id}'


@hooks.before("/ec/tasks/{taskId} > Task Id status > 404")
@if_not_skipped
def get_task_404(transaction):
    print(transaction)
    transaction['request']['uri'] = '/ec/tasks/0'
    transaction['fullPath'] = '/ec/tasks/0'

@hooks.before("/ec/tasks > add a task to RQ > 202")
@if_not_skipped
def post_a_task(transaction):
    print(transaction)
    transaction['request']['body'] = json.dumps({"id": 1}) 
    fake_tr = {'request': {'uri': "/ec/1/inputs/machine/wec/complexity1"}}
    fake_tr['protocol'] = transaction['protocol']
    fake_tr['host'] = transaction['host']
    fake_tr['port'] = transaction['port']
    save_input_machine(fake_tr)
    fake_tr = {'request': {'uri': "/ec/1/inputs/site/wec/complexity1"}}
    fake_tr['protocol'] = transaction['protocol']
    fake_tr['host'] = transaction['host']
    fake_tr['port'] = transaction['port']
    save_input_site(fake_tr)
    save_input_farm(fake_tr)

@hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity3 > Upload machine inputs wec-3 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity2 > Upload machine inputs wec-2 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/machine/wec/complexity1 > Upload machine inputs wec-1 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity3 > Upload machine inputs tec-3 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity2 > Upload machine inputs tec-2 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/machine/tec/complexity1 > Upload machine inputs tec-1 > 201")
@if_not_skipped
def upload_machine(transaction):
    """
    Prepare multipart data
    """
    uri = transaction['request']['uri'].split('/')
    mac = uri[-2].upper()
    cpx = int(uri[-1][-1])

    content_type, body = prepare_multipart_formdata({"file": "file"}, f"machine_{cpx}_{mac}.json")

    # if necessary the precise mime type cane be set, for example like in the next commented line :
    # transaction['request']['headers']['Accept'] = 'application/json'
    transaction['request']['headers']['Accept'] = '*/*'
    transaction['request']['headers']['Content-Type'] = content_type

    transaction['request']['body'] = body


@hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity3 > Upload site inputs wec-3 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity2 > Upload site inputs wec-2 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/site/wec/complexity1 > Upload site inputs wec-1 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity3 > Upload site inputs tec-3 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity2 > Upload site inputs tec-2 > 201")
@hooks.before("/ec/{ecId}/upload/inputs/site/tec/complexity1 > Upload site inputs tec-1 > 201")
@if_not_skipped
def upload_site(transaction):
    """
    Prepare multipart data
    """
    uri = transaction['request']['uri'].split('/')
    mac = uri[-2].upper()
    cpx = int(uri[-1][-1])

    content_type, body = prepare_multipart_formdata({"file": "file"}, f"site_{cpx}_{mac}.json")

    # if necessary the precise mime type cane be set, for example like in the next commented line :
    # transaction['request']['headers']['Accept'] = 'application/json'
    transaction['request']['headers']['Accept'] = '*/*'
    transaction['request']['headers']['Content-Type'] = content_type

    transaction['request']['body'] = body

@hooks.before("/ec/{ecId}/devices/{deviceId} > Delete a device > 200")
# @hooks.before("/ec/{ecId}/devices/{deviceId} > Read device > 200 > application/json")
@hooks.before("/ec/{ecId}/devices > List devices > 200 > application/json")
@hooks.before("/ec/{ecId}/rotors > List rotors > 200 > application/json")
@if_not_skipped
def get_devices(transaction):
    """
    Add input farm to project before query
    """
    post_a_farm(transaction, 1)
    post_devices(transaction, 1)
    r = requests.get(f'{transaction["protocol"]}//{transaction["host"]}:{transaction["port"]}/ec/{1}/devices')
    dev_id = r.json()[0]['id']
    if transaction['request']['uri'][-1] != "s":
        transaction['request']['uri'] = transaction['request']['uri'][:-1] + str(dev_id)
        transaction['fullPath'] = transaction['fullPath'][:-1] + str(dev_id)

@hooks.before("/ec/{ecId}/devices > Create list of devices > 201")
@hooks.before("/ec/{ecId}/farm > Delete the resource farm > 200")
@hooks.before("/ec/{ecId}/farm > Returns the description of the designed farm > 200 > application/json")
@if_not_skipped
def get_farm(transaction):
    """
    Add input farm to project before query
    """
    post_a_farm(transaction, 1)

@hooks.before("/ec/{ecId}/farm > Create the resource farm > 400")
@if_not_skipped
def post_farm(transaction):
    """
    Add input farm to project before query
    """
    requested = json.loads(transaction['request']['body'])
    requested['wrong_key'] = True
    transaction['request']['body'] = json.dumps(requested)

@hooks.before("/ec/{ecId}/inputs/machine/wec/complexity1 > Returns the description of the machine inputs wec-1 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/machine/wec/complexity2 > Returns the description of the machine inputs wec-2 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/machine/wec/complexity3 > Returns the description of the machine inputs wec-3 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/machine/tec/complexity3 > Returns the description of the machine inputs tec-3 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/machine/tec/complexity2 > Returns the description of the machine inputs tec-2 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/machine/tec/complexity1 > Returns the description of the machine inputs tec-1 > 200 > application/json")
@if_not_skipped
def get_input_machine(transaction):
    """
    Add input farm to project before query
    """
    save_input_machine(transaction)

@hooks.before("/ec/{ecId}/inputs/site/wec/complexity3 > Returns the description of the site inputs wec-3 > 200 > application/json")
@if_not_skipped
def debug_get_input_site_wec3(transaction):
    """
    Add input farm to project before query
    """
    tr = transaction
    uri = tr['request']['uri']
    mac = uri.split('/')[-2]
    cpx = uri.split('/')[-1]
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    bbody = {
                "loc_position": [[0,0],[500,500],[0,500]],
                "scatter_diagram": [1],
                "sea_state_directions": [0],
                "sea_state_wave_height": [ 2.5],
                "sea_state_wave_period": [5.3],
                "bathymetry": [[0,0,50]],
                "nogo_areas": [[]],
                "spectral_type": {
                    "model": "Jonswap",
                    "gamma": 3.3,
                    "directional_spreading": 0
                }}
    r = requests.put(f'{protocol}//{host}:{port}{uri}', json=bbody)
    print(r.status_code)
    print(r.json())



@hooks.before("/ec/{ecId}/inputs/site/wec/complexity1 > Returns the description of the site inputs wec-1 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/site/wec/complexity2 > Returns the description of the site inputs wec-2 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/site/tec/complexity3 > Returns the description of the site inputs tec-3 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/site/tec/complexity2 > Returns the description of the site inputs tec-2 > 200 > application/json")
@hooks.before("/ec/{ecId}/inputs/site/tec/complexity1 > Returns the description of the site inputs tec-1 > 200 > application/json")
@if_not_skipped
def get_input_site(transaction):
    """
    Add input farm to project before query
    """
    save_input_site(transaction)

@hooks.before("/ec/{ecId}/inputs/farm > Farm inputs status > 200 > application/json")
@if_not_skipped
def get_input_farm(transaction):
    """
    Add input farm to project before query
    """
    save_input_farm(transaction)

@hooks.before("/ec > Add an Energy Capture (EC) design > 400")
@if_not_skipped
def post_project_400(transaction):
    """
    Remove one of required parameters when creating a project.
    """
    transaction['request']['body'] = transaction['request']['body'].replace(
        ',"type":"WEC"', '')