from dtop_energycapt import business
import pytest


def test_business_assert():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    assert isinstance(ARRAY, business.arraytecstg1.ArrayTECStg1)


def test_business_exceptions():
    with pytest.raises(AssertionError) as excinfo:
        ARRAY = business.ArrayFactory.get_array('TEC', "5")


def test_tecstg1_init():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    assert ARRAY.nb_devices == None


def test_business():
    ARRAY = business.ArrayFactory.get_array('WEC', "3")
    assert ARRAY.stage == "Stage 3"


def test_business_wec2():
    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    assert ARRAY.stage == "Stage 2"


def test_businesstec3():
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    assert ARRAY.stage == "Stage 3"


def test_wecstg1_inputs():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'RM3_NS'
    inputs = {
        'nb_devices': 10,
        'rated_pow_device': 15.,
        'array_layout': [[0,0]],
        'loc_resource': 222.,
        'loc_position': [[5000, 8699], [0, 0], [100, 100]],
        'array_generic_type': 'rectangular',
        'ratedPowerArray': 5000.,
        'device_capture_width_ratio': 0.3,
        'sec_dim_device': 25.,
        'main_dim_device': 5.,
    }
    ARRAY.get_inputs(name, ecID, **inputs)
    assert ARRAY.nb_devices == 10


#def test_tecstg1_output():
#    ARRAY = business.ArrayFactory.get_array('TEC', "1")
#    output1, output2 = ARRAY.send_output()
#    assert "resource-reduction" in output1[0].keys()


def test_wecstg1():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 3
    name = 'RM3_NS'
    inputs = {
        'nb_devices': 10,
        'array_layout': [[0,0]],
        'rated_pow_device': 15.,
        'loc_resource': 222.,
        'array_generic_type': 'rectangular',
        'loc_position': [[5000, 8699], [0, 0], [100, 100]],
        'device_capture_width_ratio': 0.3,
        'sec_dim_device': 25.,
        'main_dim_device': 12.,
    }
    ARRAY.get_inputs(name, ecID, **inputs)

    assert ARRAY.i_converter.max_dim == 25


# def test_wecstg1_output():
#     ARRAY = business.ArrayFactory.get_array('WEC', "1")
#     output = ARRAY.send_output()
#     assert "aep_device" in output


def test_wecstg1_readdig():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    output = ARRAY.read_digrep()
    assert output == "OK"


def test_wecstg1_exceptions():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(ValueError) as excinfo:
        ecID = 5
        name = 'RM3_NS'
        inputs = {
            'nb_devices': -5,
            'rated_pow_device': 15.,
            'loc_resource': 222.,
            'loc_position': [[5000, 5555]],
            'array_generic_type': 'rectangular',
            'device_capture_width': 22,
            'sec_dim_device': 25.,
            'main_dim_device': 12.,
        }
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg2():
    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    assert ARRAY.type == 'WEC'


def test_wecstg3():
    ARRAY = business.ArrayFactory.get_array('WEC', "3")
    assert ARRAY.stage == 'Stage 3'


def test_tecstg2():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    assert ARRAY.type == 'TEC'


def test_tecstg3():
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    assert ARRAY.stage == 'Stage 3'
