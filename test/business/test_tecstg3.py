from dtop_energycapt import business
import pytest
import numpy as np

@pytest.fixture
def in_feat():
    ## Turbines positions
    bathy = -30.0
    z = bathy / 2.0  # hub height/depth
    diam = 10
    array_layout = [[0, 0], [100, 50], [0, 100]]

    ## Turbines features
    cut_in = 0.0  # cut-in speed
    cut_out = 10.0  # cut-out speed
    # actual turbine features
    speed = np.arange(0.0, 10.0, 0.2)

    CT = np.ones(speed.shape) * 0.76
    Ct = [speed.tolist(), CT.tolist()]  # thrust curve

    CP = np.ones(speed.shape) * 0.3
    Cp = [speed.tolist(), CP.tolist()]  # Power curve
    feat = {}

    feat = {}
    feat['OA'] = [270.0]  # orientation angle (deg.), turbines face the West
    feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
    feat['Cp'] = Cp[:]  # Power curve
    feat['Ct'] = Ct[:]  # thrust curve
    feat['Diam'] = diam  # Diam = rotor diameter (m)
    feat['cutIO'] = [cut_in, cut_out]
    feat[
        'floating'] = False  # so hub height will be considered from the seabottom upwards
    feat['2way'] = False  # turbines work in both direction.
    feat['Rating'] = 5e6  # turbines work in both direction.
    feat['hub_height'] = 20
    feat['rotor_diameter'] = 10
    return feat

@pytest.fixture
def in_site():
    bathy = -30.0
    xmax = 840
    ymax = 350
    lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

    x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
    y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
    X, Y = np.meshgrid(x, y)
    nx = len(x)
    ny = len(y)
    BR = 1.0  # blockage ratio

    umax = 0.0  # maximum velocity in the X direction
    vmax = 5.0  # maximum velocity in the Y direction
    sshmax = 0.0  # maximum sea surface elevation
    timax = 0.1
    bathy = -30.0

    U = np.ones(X.shape) * umax
    V = np.ones(X.shape) * vmax
    SSH = np.ones(X.shape) * sshmax
    TI = np.ones(X.shape) * timax
    BATHY = np.ones(X.shape) * bathy
    PLE = 1.0 * np.ones(X.shape)
    # bed roughness coefficient here
    manning = np.ones(X.shape) * 0.001
    site = {}
    site['TI'] = [TI.tolist()]
    site['PLE'] = PLE.tolist()
    site['geophy'] = manning.tolist()
    site['X'] = x.tolist()  # save only 1D array as structured grid assumed
    site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
    site['U'] = [U.tolist()]
    site['V'] = [V.tolist()]
    site['SSH'] = [SSH.tolist()]
    site['bathy'] = BATHY.tolist()
    site['BR'] = BR
    site['lease'] = lease.tolist()
    site['probability'] = [1.0]
    return site

@pytest.fixture
def in_arr():
    array_layout = [[0, 0], [100, 50], [0, 100]]
    inputs = {
        'nb_devices': 3,
        'opt_method': 'cmaes',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'rectangular',
        'ratedPowerArray': 5000,
        'distance_min': (10, 10),
        'nogo_areas': None,
        'array_layout': array_layout,
        'layout_type': 'verification'
        } 
    return inputs

def test_tecstg3_main1(in_site, in_feat, in_arr):
    ecID = 5
    name = "test_project"
    inputs = in_arr
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)
    inputs['nb_devices'] = 1
    inputs['array_layout'] = [[0,0]]
    inputs['floating'] = True
    inputs['2way'] = True
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.verify_farm_layout(debug=True)
    inputs['HAS'] = 0.0
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.verify_farm_layout(debug=True)


def test_tecstg3_main2(in_site, in_feat, in_arr):
    ecID = 5
    name = "test_project"
    inputs = in_arr    
    inputs.update(in_site)
    inputs.update(in_feat)
    inputs.pop('array_layout')
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg3_main3(in_site, in_feat, in_arr):   
    ecID = 5
    name = "test_project"
    inputs = in_arr
    inputs.pop('opt_method')
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.verify_farm_layout(debug=True)

def test_tecstg3_main4(in_site, in_feat, in_arr):     
    ecID = 5
    name = "test_project"
    inputs = in_arr
    inputs['opt_method'] = None
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.i_site_conditions.boundaryPad = 10
    ARRAY.compress_lease_area()
    ARRAY.verify_farm_layout(debug=True)

def test_tecstg3_inputs1(in_site, in_feat, in_arr):
    ecID = 5
    name = "test_project"
    inputs = in_arr
    inputs.pop('nb_devices')
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg3_inputs2(in_site, in_feat, in_arr):
    ecID = 5
    name = "test_project"
    inputs = in_arr 
    inputs.pop('opt_method')  
    inputs['array_generic_type'] = 'full'
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg3_inputs3(in_site, in_feat, in_arr):
    ecID = 5
    name = "test_project"
    inputs = in_arr 
    inputs.pop('opt_method')  
    inputs['array_generic_type'] = 'staggered'    
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg3_inputs4(in_site, in_feat, in_arr):      
    ecID = 5
    name = "test_project"
    inputs = in_arr
    inputs.update(in_site)
    inputs.update(in_feat)
    inputs.pop('BR')
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(IOError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg3_inputs5(in_site, in_feat, in_arr):
    ecID = 5
    name = "test_project"
    inputs = in_arr
    inputs.update(in_site)
    inputs.update(in_feat)
    inputs.pop('Diam')
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(IOError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg3_inputs6(in_site, in_feat, in_arr):    
    ecID = 5
    name = [1,1]
    inputs = in_arr
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg3_inputs7(in_site, in_feat, in_arr):   
    ecID = 'test'
    name = 'test'
    inputs = in_arr
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg3_inputs8(in_site, in_feat, in_arr):     
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs['nb_devices'] = 'test'
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg3_inputs9(in_site, in_feat, in_arr):     
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs['nb_devices'] = -30
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)        


def test_tecstg3_inputs10(in_site, in_feat, in_arr):     
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs.pop('array_layout')
    inputs.pop('optimisation_threshold')
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)        

def test_tecstg3_inputs11(in_site, in_feat, in_arr):
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs['layout_type'] = 'optimization'
    inputs.pop('opt_method')
    inputs['optimisation_threshold'] = 'test'
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)  

def test_tecstg3_inputs12(in_site, in_feat, in_arr):
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs['layout_type'] = 'optimization'
    inputs.pop('opt_method')
    inputs['optimisation_threshold'] = -1.8
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)  

def test_tecstg3_inputs13(in_site, in_feat, in_arr):     
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs['layout_type'] = 'optimization'    
    inputs.pop('opt_method')
    inputs['array_generic_type'] = 12.0
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)  


def test_tecstg3_inputs14(in_site, in_feat, in_arr):    
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs.pop('opt_method')
    inputs['array_generic_type'] = 'staggered'
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)  
    ARRAY.compress_lease_area()


def test_tecstg3_converter1(in_site, in_feat, in_arr):      
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs.pop('opt_method')
    inputs['array_generic_type'] = 'staggered'
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)  

def test_tecstg3_array1(in_site, in_feat, in_arr):     
    speed = np.arange(0.0, 10.0, 0.2)
    CT = np.ones(speed.shape) * 0.76
    Ct = [speed.tolist(), CT.tolist()]  # thrust curve
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs.pop('opt_method')
    inputs['array_generic_type'] = 'staggered'
    inputs['nb_devices'] = 1
    inputs['array_layout'] = [[0, 0]]
    inputs.update(in_site)
    inputs.update(in_feat)
    inputs['Ct'] = Ct
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)  
    # diff_angle = ARRAY.PerfMetrics.__diff_rads(3, [1,2,3])


def test_tecstg3_1turbine(in_site, in_feat, in_arr):
    ecID = 9
    name = 'test'
    inputs = in_arr
    inputs.pop('opt_method')
    inputs['array_generic_type'] = 'staggered'
    inputs['nb_devices'] = 1
    inputs['array_layout'] = [[0, 0]]
    inputs.update(in_site)
    inputs.update(in_feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs) 
    cut_in = 0.0  # cut-in speed
    cut_out = 10.0  # cut-out speed
    inputs['cutIO'] = [cut_out, cut_out]
    ARRAY.get_inputs(name, ecID, **inputs) 
    ARRAY.verify_farm_layout(debug=True)
    inputs['cutIO'] = [cut_in, cut_in]
    ARRAY.get_inputs(name, ecID, **inputs) 
    ARRAY.verify_farm_layout(debug=True)
    inputs['cutIO'] = [cut_in, cut_out]
    ARRAY.get_inputs(name, ecID, **inputs) 
    ARRAY.verify_farm_layout(debug=True)
    inputs['Rating'] = 0.1
    ARRAY.get_inputs(name, ecID, **inputs) 
    ARRAY.verify_farm_layout(debug=True)

def test_sendoutput(in_site, in_feat, in_arr):
    ecID = 9
    name = 'test'
    inputs = in_arr   
    inputs.update(in_site)
    inputs.update(in_feat) 
    inputs['rotor_diameter'] = 10
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs) 
    ARRAY.verify_farm_layout(debug=True)
    results1, results2 = ARRAY.send_output()
    # print(100*'+-+-+-')
    # print(results1, results2)
    # print(100*'+-+-+-')
