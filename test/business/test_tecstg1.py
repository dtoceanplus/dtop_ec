from dtop_energycapt import business
import pytest




def test_tecstg1_init():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    assert ARRAY.nb_devices == None


def test_tecstg1_inputs():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs_tec = {
        'nb_devices': 10,
        'layout_type': 'optimization',
        'opt_method': 'monte_carlo',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'rectangular',
        'rated_pow_device': 40000.,
        'loc_resource': 25.0,
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'cp': 0.3,
        'sec_dim_device': 55.,
        'main_dim_device': 10.,
    }

    ID = 9
    pj_name = "_tec_test_project"
    ARRAY.get_inputs(pj_name, ID, **inputs_tec)
    assert ARRAY.nb_devices == 10


def test_tecstg1_exceptions():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(ValueError) as excinfo:
        ecID = 4
        name = 'RM3_NS'
        inputs = {
            'nb_devices': 5,
            'layout_type': 'optimization',
            'rated_pow_device': -15.,
            'loc_resource': 222.0,
            'loc_position': [[5000, 5555]],
            'array_layout': [[0,0]],
            'array_generic_type': 'rectangular',
            'ratedPowerArray': 5000.,
            'char_length': 22,
            'sec_dim_device': 25.,
            'main_dim_device': 12.,
        }
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg1_exceptions_nbdev():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(ValueError) as excinfo:
        ecID = 1
        name = 'RM3_NS'
        inputs = {
            'nb_devices': -5,
            'rated_pow_device': 15.,
            'array_layout': [[0,0]],
            'loc_resource': 222.0,
            'loc_position': [[5000, 5555]],
            'char_length': 22,
            'sec_dim_device': 25.,
            'main_dim_device': 12.,
        }
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg1_readdig(capsys):
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    output = ARRAY.read_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Reading the digital representation\n'

def test_tecstg1_writedig(capsys):
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    output = ARRAY.write_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Writing the digital representation\n'

def test_tecstg1_main1():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs = {  'nb_devices': 10, 
             'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
             'layout_type': 'verification',
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    ARRAY.calc_powprod()

def test_tecstg1_main2():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    with pytest.raises(Warning) as excinfo:
        ARRAY.layout = None
        ARRAY.verify_farm_layout()


def test_tecstg1_inputs1():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    assert ARRAY.layout == None


def test_tecstg1_inputs2():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    assert ARRAY.optfunname == 'ArrayTECStg1_cmaes'

def test_tecstg1_inputs3():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    assert ARRAY._normalisation_point == 10

# def test_tecstg1_inputs4():
#     # Checking array_generic_type is a str
#     ARRAY = business.ArrayFactory.get_array('TEC', "1")
#     with pytest.raises(TypeError) as excinfo:
#         ecID = 1
#         name = 'RM3_NS'
#         inputs = {  'nb_devices': 10, 
#             #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
#         #     'optimisation_threshold':,
#             'opt_method': 'cmaes',
#             'optimisation_threshold': 0.8,
#             'array_generic_type': 500.4,            
#             } 
#         site = {
#         'loc_resource': 64000.0,  # this is defined as power/m2
#         'loc_position': [[0,0],[500,500],[0,500]],
#         }
#         converter = { 
#                 'cp': 0.3,
#                 'main_dim_device': 10.,
#                 'sec_dim_device': 5.,
#                 'rated_pow_device': 300.
#         }
#         inputs.update(site)
#         inputs.update(converter)
#         ARRAY.get_inputs(name, ecID, **inputs)


# def test_tecstg1_inputs5():
#     # Checking optimisation_threshold is a float
#     ARRAY = business.ArrayFactory.get_array('TEC', "1")
#     with pytest.raises(TypeError) as excinfo:
#         ecID = 1
#         name = 'RM3_NS'
#         inputs = {  'nb_devices': 10, 
#             #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
#         #     'optimisation_threshold':,
#             'opt_method': 'cmaes',
#             'optimisation_threshold': 'blah',
#             'array_generic_type': 500.4,            
#             } 
#         site = {
#         'loc_resource': 64000.0,  # this is defined as power/m2
#         'loc_position': [[0,0],[500,500],[0,500]],
#         }
#         converter = { 
#                 'cp': 0.3,
#                 'main_dim_device': 10.,
#                 'sec_dim_device': 5.,
#                 'rated_pow_device': 300.
#         }
#         inputs.update(site)
#         inputs.update(converter)
#         ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_inputs6():
    # Checking nb_devices is an int
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1
        name = 'RM3_NS'
        inputs = {  'nb_devices': 'nb_devices', 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_inputs7():
    # Checking the name is a string
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1
        name = 5
        inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_inputs8():
    # Checking the ecID is an int
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 45.93
        name = 'test'
        inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
        site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
        converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg1_inputs9():
    # Checking the max number of devices is ok
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    assert ARRAY.max_num_devices == 15



def test_tecstg1_inputs10():
    # Checking the ecID is assigned
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    assert ARRAY.ecID == 5

def test_tecstg1_inputs11():
    # Checking the error raised when nb_devices is not given
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(RuntimeError) as excinfo:
        ecID = 45.93
        name = 'test'
        inputs = {  
            # 'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
        site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
        converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_inputs12():
    # Checking the that TypeError is raised when array_generic_type is not a 
    # string
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 45.93
        name = 'test'
        inputs = {  
             'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 5,            
            } 
        site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
        converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg1_converter1():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    assert ARRAY.i_converter.min_distance == (2*10,5*10)

    
def test_tecstg1_converter2(capsys):
    # Checking the printInput function of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.i_converter.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of Converter\n'


def test_tecstg1_converter3():
    # Checking the cp value
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.,
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    assert ARRAY.i_converter.cp == 0.3



def test_tecstg1_converter4():
    # Checking the value of rated_pow_device
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 'test'
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_converter5():
    # Checking the value of cp
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 1.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(IOError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_converter6():
    # Checking the value of cp
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 'test',
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_converter7(capsys):
    # Checking the value of main dimension
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 'test',
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        captured = capsys.readouterr()
        assert captured.out == 'Main dimension of the device must be a float value.\n'


def test_tecstg1_converter8():
    # Checking the value of main dimension
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': -20.,
                'sec_dim_device': 5,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_converter9():
    # Checking the value of sec dimension
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 5.,
                'sec_dim_device': 'This is not a float',
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_converter10():
    # Checking the value of sec dimension
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': -5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg1_converter11():
    # Checking the value of max dimension
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    assert ARRAY.i_converter.max_dim == 20.

def test_tecstg1_converter12(capsys):
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,'layout_type': 'optimization',
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                # 'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        captured = capsys.readouterr()
        assert captured.out == 'Secondary dimension of the device input missing!\n'


def test_tecstg1_converter13(capsys):
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                # 'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        captured = capsys.readouterr()
        assert captured.out == 'Main dimension of the device input missing!\n'


def test_tecstg1_converter14(capsys):
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                # 'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        captured = capsys.readouterr()
        assert captured.out == 'Rated power of the device input missing!\n'

def test_tecstg1_converter15(capsys):
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': -15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        captured = capsys.readouterr()
        assert captured.out == 'The rated power of the device must be a positive non null\
                    value.\n'


def test_tecstg1_site1():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': -64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
 
def test_tecstg1_site2():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            # 'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(IOError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)



def test_tecstg1_site3(capsys):
    # Checking the printInput function of the site_condtions
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.i_site_conditions.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of SiteConditions\n'


def test_tecstg1_site4():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
            #  'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(IOError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_site5():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 'test',  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_site6():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold'
            'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': 1.0#[[0,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_site7():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization',
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [['test',0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_site8():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
                'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': None,
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': ['test',[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_tecstg1_site9():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[-10,0],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_tecstg1_site10():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[10],[500,500],[0,500]],
                }
    converter = { 
                'cp': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)