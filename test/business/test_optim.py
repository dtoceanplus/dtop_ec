from dtop_energycapt import business
import pytest
import sys, os
import numpy as np

device = {
    'rotor diameter': 10,
    'hub height': 20,
    'floating': False,
    'cut_IO': [1, 10],
    'cp': 0.3,
    'ct': 1,
    'alpha': 0.07,
    'rated_pow_device': 5e6,
}

layout = np.array([[2.39964301, 493.2435497], [2.39964301, 280.02992788],
                [19.69090866, 386.63673879], [36.98217431, 493.2435497],
                [2.39964301, 66.81630606], [19.69090866, 173.42311697],
                [36.98217431, 280.02992788], [54.27343996, 386.63673879],
                [71.5647056, 493.2435497], [36.98217431, 66.81630606],
                [54.27343996, 173.42311697], [71.5647056, 280.02992788],
                [88.85597125, 386.63673879], [106.1472369, 493.2435497],
                [88.85597125, 173.42311697], [106.1472369, 280.02992788],
                [123.43850255, 386.63673879], [140.72976819, 493.2435497],
                [123.43850255, 173.42311697], [140.72976819, 280.02992788],
                [158.02103384, 386.63673879], [175.31229949, 493.2435497],
                [158.02103384, 173.42311697], [175.31229949, 280.02992788],
                [192.60356514, 386.63673879], [209.89483079, 493.2435497],
                [209.89483079, 280.02992788], [227.18609643, 386.63673879],
                [244.47736208, 493.2435497], [244.47736208, 280.02992788],
                [261.76862773, 386.63673879], [279.05989338, 493.2435497],
                [279.05989338, 280.02992788], [296.35115903, 386.63673879],
                [313.64242467, 493.2435497], [330.93369032, 386.63673879],
                [348.22495597, 493.2435497], [365.51622162, 386.63673879],
                [382.80748727, 493.2435497], [417.39001856, 493.2435497],
                [451.97254986, 493.2435497]])
velocity = ((0, 5), )

name = 'testes'
ecID = 4
body = {
    'layout_type': 'optimization',
    'bathymetry': 30.,
    'device': device,
    'main_dim_device': 10,
    'sec_dim_device': 20,
    'loc_resource': 10,
    # 'layout': layout.tolist(),
    'velocity_field': velocity,
    'nb_devices': 42,
    'array_generic_type': 'staggered',
    'loc_position': [[0, 0], [500, 500], [0, 500]],
    'rated_pow_device': 5e6,
    'rotor_diameter': 10,
    'hub_height': 20,
    'floating': False,
    'cut_IO': [1, 10],
    'cp': 0.3,
    'ct': 1,
    'alpha': 0.07,
    'optimisation_threshold': 0.8,
    'opt_method': 'meta_model',
}


def test_optim1(capsys):

    arr = business.ArrayFactory.get_array('TEC', "2")
    
    arr.get_inputs(name, ecID, **body)

    arr.optimize_farm_layout()
    # farm, devices = arr.send_output()
    captured = capsys.readouterr()
    assert 'ArrayTECStg2_meta_model' in captured.out

    arr.generate_array(10, 10, 100, 100, 45/180*3.14, -45/180*3.14)
    # arr.show_layout()
    farm, devices = arr.send_output()


def test_optim2():

    arr = business.ArrayFactory.get_array('TEC', "1")
    body['opt_method'] = 'monte_carlo'

    arr.get_inputs(name, ecID, **body)

    arr.optimize_array()
    farm, devices = arr.send_output()

def test_optim3():

    arr = business.ArrayFactory.get_array('TEC', "1")
    body['opt_method'] = 'brute_force'

    arr.get_inputs(name, ecID, **body)

    arr.optimize_array()
    farm, devices = arr.send_output()

    body['optimisation_threshold'] = 1.0
    arr.get_inputs(name, ecID, **body)

    arr.optimize_array()


def test_optim4():
    arr = business.ArrayFactory.get_array('TEC', "1")
    body['opt_method'] = 'cmaes'
    arr.get_inputs(name, ecID, **body)
    arr.optimize_array()
    farm, devices = arr.send_output()

    body['optimisation_threshold'] = 0.8
    arr.get_inputs(name, ecID, **body)
    arr.optimize_array()
    farm, devices = arr.send_output()

# @pytest.fixture
def test_optim_MM(capsys):
    arr = business.ArrayFactory.get_array('TEC', "1")
    body['opt_method'] = 'meta_model'
    
    arr.get_inputs(name, ecID, **body)
    arr.optimize_array()

