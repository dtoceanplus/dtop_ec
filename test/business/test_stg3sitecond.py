from dtop_energycapt import business
import pytest
import sys, os
import numpy as np

def test_stg3_sitecond(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'
    site = s_wec3
    site['nogo_areas'] = 'test'

    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    ARRAY = business.ArrayFactory.get_array('WEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs) 


def test_tecstg3_converter1():
    xmax = 840
    ymax = 350
    lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

    x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
    y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
    X, Y = np.meshgrid(x, y)
    nx = len(x)
    ny = len(y)
    BR = 1.0  # blockage ratio

    umax = 0.0  # maximum velocity in the X direction
    vmax = 5.0  # maximum velocity in the Y direction
    sshmax = 0.0  # maximum sea surface elevation
    timax = 0.1
    bathy = -30.0

    U = np.ones(X.shape) * umax
    V = np.ones(X.shape) * vmax
    SSH = np.ones(X.shape) * sshmax
    TI = np.ones(X.shape) * timax
    BATHY = np.ones(X.shape) * bathy
    PLE = 1.0 * np.ones(X.shape)
    # bed roughness coefficient here
    manning = np.ones(X.shape) * 0.001

    ## Turbines positions
    z = bathy / 2.0  # hub height/depth
    diam = 10
    array_layout = [[0, 0], [100, 50], [0, 100]]

    ## Turbines features
    cut_in = 0.0  # cut-in speed
    cut_out = 10.0  # cut-out speed
    # actual turbine features
    speed = np.arange(0.0, 10.0, 0.2)

    CT = np.ones(speed.shape) * 0.76
    Ct = [speed.tolist(), CT.tolist()]  # thrust curve

    CP = np.ones(speed.shape) * 0.3
    Cp = [speed.tolist(), CP.tolist()]  # Power curve
    feat = {}

    feat = {}
    feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
    feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
    feat['Cp'] = Cp[:]  # Power curve
    feat['Ct'] = Ct[:]  # thrust curve
    feat['Diam'] = diam  # Diam = rotor diameter (m)
    feat['cutIO'] = [cut_in, cut_out]
    feat[
        'floating'] = False  # so hub height will be considered from the seabottom upwards
    feat['2way'] = False  # turbines work in both direction.
    feat['Rating'] = 5e6  # turbines work in both direction.
    feat['hub_height'] = 20

    ecID = 9
    name = 'test'
    inputs = {
        'nb_devices': 30,
        # 'opt_method': 'cmaes',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'staggered',
        'ratedPowerArray': 5000,
        'distance_min': (10, 10),
        'nogo_areas': None,
        'array_layout': array_layout
    }
    site = {}
    site['TI'] = [TI.tolist()]
    site['PLE'] = PLE.tolist()
    site['geophy'] = manning.tolist()
    site['X'] = x.tolist()  # save only 1D array as structured grid assumed
    site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
    site['U'] = [U.tolist()]
    site['V'] = [V.tolist()]
    site['SSH'] = [SSH.tolist()]
    site['bathy'] = BATHY.tolist()
    site['BR'] = BR
    site['lease'] = lease.tolist()
    site['probability'] = [1.0]
    site['boundary_padding'] = 5.

    inputs['rotor_diameter'] = 10.0
    
    inputs.update(site)
    inputs.update(feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs) 
    with pytest.raises(IOError) as excinfo:
        ARRAY.i_site_conditions.irrotational_flow()



def test_tecstg3_sitecond():
    xmax = 840
    ymax = 350
    lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

    x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
    y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
    X, Y = np.meshgrid(x, y)
    nx = len(x)
    ny = len(y)
    BR = 1.0  # blockage ratio

    umax = 0.0  # maximum velocity in the X direction
    vmax = 5.0  # maximum velocity in the Y direction
    sshmax = 0.0  # maximum sea surface elevation
    timax = 0.1
    bathy = -30.0

    U = np.ones(X.shape) * umax
    V = np.ones(X.shape) * vmax
    SSH = np.ones(X.shape) * sshmax
    TI = np.ones(X.shape) * timax
    BATHY = np.ones(X.shape) * bathy
    PLE = 1.0 * np.ones(X.shape)
    # bed roughness coefficient here
    manning = np.ones(X.shape) * 0.001

    ## Turbines positions
    z = bathy / 2.0  # hub height/depth
    diam = 10
    array_layout = [[0, 0], [100, 50], [0, 100]]

    ## Turbines features
    cut_in = 0.0  # cut-in speed
    cut_out = 10.0  # cut-out speed
    # actual turbine features
    speed = np.arange(0.0, 10.0, 0.2)

    CT = np.ones(speed.shape) * 0.76
    Ct = [speed.tolist(), CT.tolist()]  # thrust curve

    CP = np.ones(speed.shape) * 0.3
    Cp = [speed.tolist(), CP.tolist()]  # Power curve
    feat = {}

    feat = {}
    feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
    feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
    feat['Cp'] = Cp[:]  # Power curve
    feat['Ct'] = Ct[:]  # thrust curve
    feat['Diam'] = diam  # Diam = rotor diameter (m)
    feat['cutIO'] = [cut_in, cut_out]
    feat[
        'floating'] = False  # so hub height will be considered from the seabottom upwards
    feat['2way'] = False  # turbines work in both direction.
    feat['Rating'] = 5e6  # turbines work in both direction.
    feat['hub_height'] = 20

    ecID = 9
    name = 'test'
    inputs = {
        'nb_devices': 30,
        # 'opt_method': 'cmaes',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'staggered',
        'ratedPowerArray': 5000,
        'distance_min': (10, 10),
        'nogo_areas': None,
        'array_layout': array_layout
    }
    site = {}
    site['TI'] = [TI.tolist()]
    site['PLE'] = PLE.tolist()
    site['geophy'] = manning.tolist()
    site['X'] = x.tolist()  # save only 1D array as structured grid assumed
    site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
    site['U'] = [U.tolist()]
    site['V'] = [V.tolist()]
    site['SSH'] = [SSH.tolist()]
    site['bathy'] = BATHY.tolist()
    site['BR'] = BR
    site['lease'] = lease.tolist()
    site['probability'] = [1.0]
    site['boundary_padding'] = 5.

    inputs['rotor_diameter'] = 10.0

    inputs.update(site)
    inputs.update(feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs) 
    ARRAY.i_site_conditions.getmain_angle_t()