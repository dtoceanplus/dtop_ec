from dtop_energycapt.business.stg3utils.utils.set_wdirs_multibody import set_wdirs_multibody, anglewrap
import pytest
import sys, os
import numpy as np


def test_set_wdirs():
    mA = 50. * np.pi / 180 
    yA = 100 * np.pi / 180 
    B = np.linspace(0, 360, 8, endpoint=False) 
    B *= np.pi / 180 

    a = set_wdirs_multibody(B, 0, mA, np.pi, debug=True)
    
    for s in [28]: 
        a = set_wdirs_multibody(B, s, mA, yA, debug=True) 
    a = set_wdirs_multibody(B, s, mA, 0, debug=True)
    
    anglewrap(0, conversion=None)
    anglewrap(0, conversion='r2d')
    with pytest.raises(ValueError) as excinfo: 
        anglewrap(0, conversion='wrong_key')

    with pytest.raises(IOError) as excinfo: 
        set_wdirs_multibody(np.array([0,10,10,0]), s, mA, yA, debug=False)
    
    s = -10
    set_wdirs_multibody(B, s, mA, yA, debug=False)