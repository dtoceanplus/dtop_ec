from dtop_energycapt import business
import pytest
import sys, os
import numpy as np
general_inputs_debug = {    
                'layout_type': 'verification',
                'orientation_angle': [0],
                'nb_devices': 10,
                'opt_method': 'monte_carlo',
                'optimisation_threshold': 0.8,
                'optimisation_options': { 'type': 1,
                                                'grid': 'rectangular'
                                        },
                'array_generic_type': 'staggered',
                'ratedPowerArray': 5000,
                'distance_min': (10, 10),
                'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
                'array_layout': [[9.18003837, 253.5209363], [30.17825547, 413.14573036],
                                [30.17825547, 93.89614225], [51.17647258, 253.5209363],
                                [72.17468969, 413.14573036], [72.17468969, 93.89614225],
                                [93.17290679, 253.5209363], [114.1711239, 413.14573036]]
            }
s_wec3_debug = {
        'sea_state_directions':
        np.array([1, 2, 2.3], np.float).tolist(),
        'sea_state_wave_height':
        np.array([2.3, 5.6], np.float).tolist(),
        'sea_state_wave_period':
        np.array([4.3, 8.6, 9.6], np.float).tolist(),
        'water_depth':
        25.6,
        'scat_diagram':
        np.array([[[0.04, 0.06, 0.05], [0.05, 0.04, 0.06]],
                [[0.04, 0.06, 0.15], [0.05, 0.05, 0.05]],
                [[0.04, 0.06, 0.08], [0.02, 0.07, 0.03]]], np.float).tolist(),
        'bathymetry':
        np.array([[25., 25., 25.], [25., 25., 25.]], np.float).tolist(),
        'spectral_type': ('Jonswap', 3.3, 0.0),
        'main_direction':
        np.array([250, 350], np.float).tolist(),
        'lease':
        np.array([[-100, -100], [50, -150], [100, -100], [150, 0], [0, 50]],
                np.float).tolist(),
        'electrical_connection_point':
        np.array([350, 250], np.float).tolist(),
        'nogo_areas':
        np.array([[-100, -100], [50, -150], [100, -100], [150, 0], [0, 50]],
                np.float).tolist(),
    }

def test_wecstg3_main0(general_inputs, s_wec3):
    project_object = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    project_object.get_inputs(name, ecID, **inputs)

    project_object.generate_array(10, 10, 100, 100, 45/180*3.14, -45/180*3.14)
    
    coord = np.array([[0,0]])
    project_object.calc_powprod()

    farm, devices = project_object.send_output()


def test_wecstg3_main1(general_inputs, s_wec3):
    project_object = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'
    inputs['perf_method'] = 'var_bath'
    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    project_object.get_inputs(name, ecID, **inputs)
    with pytest.raises(Warning) as excinfo:
        project_object.verify_farm_layout()


def test_wecstg3_main2(general_inputs, s_wec3):
    project_object = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['perf_method'] = 'const_bath'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    project_object.get_inputs(name, ecID, **inputs)
    project_object.generate_array(10, 10, 100, 100, 45/180*3.14, -45/180*3.14)
    project_object.calc_powprod()

def test_wecstg3_main3(s_wec3):
    project_object = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {  'nb_devices': 10,
                'array_layout': [[0,0]],
            'opt_method': None,
            'optimisation_threshold': 0.8,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            }

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    project_object.get_inputs(name, ecID, **inputs)
    project_object.generate_array(10, 10, 100, 100, 45/180*3.14, -45/180*3.14)
    project_object.maxNumbDevices = 15
    project_object.param_conditioning([10, 10, 100, 100])


def test_wecstg3_input1(s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {  
            #'nb_devices': 10,
            'opt_method': 'monte_carlo',
            'optimisation_threshold': 0.8,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            }

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_input2(general_inputs, s_wec3):
    """test error in in project name"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = 3
    inputs = general_inputs
    inputs['array_generic_type'] = 'full'
    inputs['layout_type'] = 'optimization'
    inputs['perf_method'] = 'perf_method'  

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input3(general_inputs, s_wec3):
    """test error in miss ste state direciton in site"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = 3
    inputs = general_inputs

    site = s_wec3
    site.pop('sea_state_directions', None)    
    
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(IOError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_input4(general_inputs, s_wec3):
    """test error in converter missing yaw angle"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = 3
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                #     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(IOError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input5(general_inputs, s_wec3):
    """test error in in project name"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = 3
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'
    inputs['nb_devices'] = -10  

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input6(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = "test"
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'
    inputs['array_generic_type'] = 'full'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input7(general_inputs, s_wec3):
    """test error in number devices"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'
    inputs['nb_devices'] = 'test'  

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_input8(general_inputs, s_wec3):
    """test error in number devices"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'
    inputs['nb_devices'] = -10  

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input9(general_inputs, s_wec3):
    """test error in array_generic_type"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'
    inputs['array_generic_type'] = 15    

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input10(general_inputs, s_wec3):
    """test error in optimization threshold"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'
    inputs['optimisation_threshold'] = "tesre"

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_input11(s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {  
            'nb_devices': 10,
            'opt_method': 'monte_carlo',
            'optimisation_threshold': -0.5,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            'array_generic_type': 'rectangular',
            }

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_input12(s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {  
            'nb_devices': 10,
            'opt_method': 'monte_carlo',
        #     'optimisation_threshold': -0.5,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            'array_generic_type': 'rectangular',
            }

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter1(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = {'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    arr.get_inputs(name, ecID, **inputs)
    arr.i_converter.printInput()
#     captured = capsys.readouterr()
#     assert captured.out == '\nPrint all arguments of this instance of Converter\n'


def test_wecstg3_converter2(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'
    site = s_wec3
    site['boundary_padding'] = 5.

    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': 'test',
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_converter3(s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {  
            'nb_devices': 10,
            'opt_method': 'monte_carlo',
            'optimisation_threshold': 0.5,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            'array_generic_type': 'rectangular',
            }

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [34, 32, 56],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter4(s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {  
            'nb_devices': 10,
            'opt_method': 'monte_carlo',
            'optimisation_threshold': 0.5,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            'array_generic_type': 'rectangular',
            }

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [34, 34],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter5(general_inputs, s_wec3):
    """test wrong folder for machine model"""
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'thisIsMyFile'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter7(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': "test",
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter8(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': 1.7,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter9(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': "test",#(10, 10),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter10(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10,12),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)

def test_wecstg3_converter11(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 'test'),
                    'rated_power_device': 300.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_converter12(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 'test',
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_converter13(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': -10.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        arr.get_inputs(name, ecID, **inputs)


def test_wecstg3_converter14(general_inputs, s_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['array_generic_type'] = 'rectangular'
    inputs['layout_type'] = 'optimization'

    site = s_wec3
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                     'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 10.,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(site)
    inputs.update(converter)
    arr.get_inputs(name, ecID, **inputs)
    arr.i_converter.checkPowerConsistency(100)

def test_wecstg3_converter15(general_inputs, s_wec3, m_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'
    converter = m_wec3
    site = s_wec3

    inputs.update(site)
    inputs.update(converter)
    arr.get_inputs(name, ecID, **inputs)
    with pytest.raises(ValueError) as excinfo:
        arr.i_converter.checkPowerConsistency(5)

def test_wecstg3_converter16(general_inputs, s_wec3, m_wec3):
    arr = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'
    converter = m_wec3
    site = s_wec3
    inputs.update(site)
    inputs.update(converter)
    arr.get_inputs(name, ecID, **inputs)
    with pytest.raises(TypeError) as excinfo:
        arr.i_converter.checkPowerConsistency('5')

def test_wecstg3_main101(general_inputs, s_wec3, m_wec3):
    project_object = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = general_inputs
    inputs['layout_type'] = 'optimization'
    converter = m_wec3
    site = s_wec3

    coord = np.array([[0,0]])
    project_object.calc_powprod()
