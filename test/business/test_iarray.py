from dtop_energycapt import business
from shapely.geometry import MultiPoint, MultiPolygon, Polygon, shape
import numpy as np
import pytest


def test_tecstg1_inputs():
    ARRAY = business.ArrayFactory.get_array('TEC', "1")
    inputs_tec = {
        'layout_type': 'optimization',
        'nb_devices': 10,
        'opt_method': 'monte_carlo',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'rectangular',
        'rated_pow_device': 40000.,
        'loc_resource': 25.0,
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'cp': 0.3,
        'sec_dim_device': 10.,
        'main_dim_device': 10.,
    }

    ID = 9
    pj_name = "_tec_test_project"
    ARRAY.get_inputs(pj_name, ID, **inputs_tec)
    assert ARRAY.nb_devices == 10
    ARRAY.optimCostFun([100,100,90,0])

    ARRAY._min_q_factor = 10
    ARRAY.optimCostFun([100,100,90,0])
    LAYOUT = ARRAY.layout
    ARRAY.layout = None
    ARRAY.optimCostFun([100,100,90,0])

    ARRAY.layout = LAYOUT
    ARRAY._min_q_factor = 0.5
    ARRAY.nogo_areas_bathymetry = Polygon([[0,0],[1,0],[1,1],[0,1]])
    ARRAY.optimCostFun([100,100,90,0])
    ARRAY.optimCostFun([10,10,0,0])
    
    with pytest.raises(TypeError) as excinfo:
        ARRAY.scale_param(np.array([5,5]))
    
    ARRAY.par_a = np.eye(4)
    ARRAY.par_a[-1,-1] = -1.
    ARRAY.par_b = np.zeros(4)
    ARRAY.scale_param(np.array([5,5,0,0]))

    ARRAY.par_a = ARRAY.par_a[:, :2]
    ARRAY.par_b[-1] = -np.pi/2
    
    ARRAY.estimate_start_point()
    ARRAY.opt_dim = 4
    ARRAY.estimate_start_point()
    ARRAY._normalisation_point = 10000.
    ARRAY.estimate_start_point()