from dtop_energycapt.business.arraytecstg3.modules.uncertainty import Uncertaintity


from dtop_energycapt.business.arraytecstg3.modules.vertical_velocity_profile import vvpw, vvpw_soulsby
from dtop_energycapt.business.arraytecstg3.modules.streamline import Streamlines
from dtop_energycapt.business.arraytecstg3.submodel.ParametricWake import Wake
from dtop_energycapt.business.arraytecstg3.create_wake_db import create_dummy_wake_db, create_wake_db
from shapely.geometry import Polygon

import pytest
import sys, os
import numpy as np


def test_uncertainty():
    U = Uncertaintity()


def test_vvpw_soulsby():
    vvpw_soulsby(3., 4., 5., 3., 2.5, debug=True)
    with pytest.raises(ArithmeticError) as excinfo:
       vvpw(np.nan, np.nan, np.nan, np.nan, np.nan, np.nan)


def test_streamlines():
    xmax = 840
    ymax = 350
    x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
    y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
    X, Y = np.meshgrid(x, y)
    umax = 0.0  # maximum velocity in the X direction
    vmax = 5.0  # maximum velocity in the Y direction
    U = np.ones(X.shape) * umax
    V = np.ones(X.shape) * vmax
    U[3,6] = 0.0
    V[3,6] = 0.0
    site = {}
    site['X'] = x  # save only 1D array as structured grid assumed
    site['Y'] = y  # save only 1D array as structured grid assumed
    site['U'] = U
    site['V'] = V
    array_layout = np.array([[0, 0], [100, 50], [0, 100]])
    st = Streamlines(site, array_layout,3)
    # st.plot(3) all the instalnce of plots have been removed from the code

    bathy = -30.0
    z = bathy / 2.0  # hub height/depth
    diam = 10
    array_layout = [[0, 0], [100, 50], [0, 100]]
    # array_layout = [[0, 0]]
    BR = 1.0  # blockage ratio
    timax = 0.1
    TI = np.ones(X.shape) * timax
    ## Turbines features
    cut_in = 0.0  # cut-in speed
    cut_out = 10.0  # cut-out speed
    # actual turbine features
    speed = np.arange(0.0, 10.0, 0.2)

    CT = np.ones(speed.shape) * 0.76
    Ct = [speed.tolist(), CT.tolist()]  # thrust curve

    CP = np.ones(speed.shape) * 0.3
    Cp = [speed.tolist(), CP.tolist()]  # Power curve
    feat = {}
    feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
    feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
    feat['Cp'] = Cp[:]  # Power curve
    feat['Ct'] = Ct[:]  # thrust curve
    feat['Diam'] = diam  # Diam = rotor diameter (m)
    feat['cutIO'] = [cut_in, cut_out]
    feat['TIH'] = 1.0
    feat['RY'] = 180.    
    feat[
    'floating'] = False  # so hub height will be considered from the seabottom upwards
    feat['2way'] = False  # turbines work in both direction.
    feat['Rating'] = 5e6  # turbines work in both direction.

    df = create_dummy_wake_db()
    wake1 = Wake(df, feat, -1.0)
    feat['RY'] = -15.0/np.pi
    wake1 = Wake(df, feat, -1.0)
    feat['RY'] = 15.0/np.pi
    wake1 = Wake(df, feat, BR)
    u, v, tke = wake1.read_at_point(10, 5, 0.9, 0.5, debug=False)
    u, v, tke = wake1.read_at_point(10, 5, 2.9, 0.95, debug=False)
    lease = np.asarray([[1.0, 1.0], [1.0, ymax], [xmax, ymax], [xmax, 1.0]])
    nogo_polygons = Polygon(lease)
    x = np.linspace(1, xmax, int(xmax / 10) + 1)  # dx = 10 m
    y = np.linspace(1, ymax, int(ymax / 10) + 1)  # dy = 10 m
    umax = 4.0  # maximum velocity in the X direction
    vmax = 5.0  # maximum velocity in the Y direction
    U = np.ones(X.shape) * umax
    V = np.ones(X.shape) * vmax
    site['U'] = U
    site['V'] = V
    site['X'] = x  # save only 1D array as structured grid assumed
    site['Y'] = y  # save only 1D array as structured grid assumed
    st = Streamlines(site, np.array([[0, 0], [100, 50], [0, 100]]),3)
    # wake1class = WakeShape([U, V], st.streamlines[-1], wake1, lease)

    df = create_wake_db() 
    wake1 = Wake(df, feat, -1.0)  