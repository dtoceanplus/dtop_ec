from dtop_energycapt import business
import numpy as np
import pytest

def test_site_condition():
    obj = business.stg3utils.site_conditions.site_conditions.SiteConditions(boundary_padding=10)

    x = np.linspace(0,10,20)
    y = np.linspace(0,10,20)
    obj.X, obj.Y = x, y

    obj.U = np.array([1,1,1.])
    obj.V = np.array([0,0,0.])

    
    with pytest.raises(IOError) as excinfo:
        obj.irrotational_flow()

    obj.U = np.random.rand(20,20,2)
    obj.V = np.random.rand(20,20,2)
    with pytest.raises(IOError) as excinfo:
        obj.irrotational_flow()

    obj.U = np.random.rand(2,20,20)
    obj.V = np.random.rand(2,20,20)
    with pytest.raises(IOError) as excinfo:
        obj.irrotational_flow()


    