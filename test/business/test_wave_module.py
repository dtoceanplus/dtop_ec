from dtop_energycapt import business
import pytest
import numpy as np
import os


def test_scatterdiagram_threshold():
    p = np.random.rand(9,2)

    with pytest.raises(IOError) as excinfo:
        business.arraywecstg3.perf_metrics.utils.StrDyn.scatterdiagram_threshold(p)
    
    p = np.random.rand(9,2,1)
    with pytest.raises(IOError) as excinfo:
        business.arraywecstg3.perf_metrics.utils.StrDyn.scatterdiagram_threshold(p)

    p /= p.sum()
    business.arraywecstg3.perf_metrics.utils.StrDyn.scatterdiagram_threshold(p, threshold=0.1)


def test_energy_balance(s_wec3):
    project_object = business.ArrayFactory.get_array('WEC', "3")
    ecID = 5
    name = "test_project"
    inputs = {
            'layout_type': 'optimization',
            'nb_devices': 10,
            'opt_method': 'monte_carlo',
            'optimisation_threshold': 0.8,
            'optimisation_options': { 'type': 1,
                                            'grid': 'rectangular'
                                    },
            'ratedPowerArray': 5000,
            'distance_min': (10, 10),
            'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
            'orientation_angle': [0]
            }

    
    converter = { 'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                    'yaw_angle': 0.2,
                    'floating': True,
                    'install_depth': [10, 35],
                    'minimum_interdistance': (10, 10),
                    'rated_power_device': 300,
                    'data_folder': os.path.join(os.getcwd(),'test'),
            }
    inputs.update(s_wec3)
    inputs.update(converter)
    project_object.get_inputs(name, ecID, **inputs)

    project_object.generate_array(10, 10, 100, 100, 45/180*3.14, -45/180*3.14)

    pow_x_dev_x_seastate = np.ones((1,18))
    coord = np.array([[0,0],[100,100]])
    project_object.PerfMetrics.EnergyBalance(pow_x_dev_x_seastate, coord)
    
    project_object.PerfMetrics.iconverter.convention = "W"
    DM = project_object.PerfMetrics.energy(np.array([[0,0]]))

             