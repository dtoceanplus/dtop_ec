from dtop_energycapt.business.arraywecstg3.perf_metrics.utils.spec_class import wave_spec
from dtop_energycapt.business.stg3utils.utils.watwaves import Dispersion_E, len2, Newton, WNumber, WriteVariableinFile
import pytest
import sys, os
import numpy as np


def test_spec_class():
    Nf = 10 
    fstr = 0 
    fstp = 0.5 
    f = np.linspace(fstr,fstp,Nf) 
    df = f[1]-f[0] 
    fp = 1/9. 
    Hs = 1. 
    with pytest.raises(IOError) as excinfo:
        Wave_sp = wave_spec(f,fp,Hs, t=None) 
        
    Wave_sp = wave_spec(f,fp,Hs) 
    Jirr = 500.*Hs**2*(1./fp) 
    Jreg = 1025*9.81**2/(32*np.pi)*Hs**2*(1/fp) 
    Wave_sp.spec_type = "Regular" 
    Wave_sp.add_spectrum() 
    ind = f>0 
    J = 1025.*9.81**2/(4.*np.pi)*sum(Wave_sp.specs[-1][2][ind,0]/f[ind])*df 

    Wave_sp.spec_type = "Bretschneider_Mitsuyasu"
    Wave_sp.add_spectrum()
    J = 1025.*9.81**2/(4.*np.pi)*sum(Wave_sp.specs[-1][2][ind,0]/f[ind])*df

    
    Wave_sp.spec_type = "Modified_Bretschneider_Mitsuyasu"
    Wave_sp.add_spectrum()
    J = 1025.*9.81**2/(4.*np.pi)*sum(Wave_sp.specs[-1][2][ind,0]/f[ind])*df


    Wave_sp.spec_type = "Pierson_Moskowitz"
    Wave_sp.add_spectrum()
    J = 1025.*9.81**2/(4.*np.pi)*sum(Wave_sp.specs[-1][2][ind,0]/f[ind])*df
    Wave_sp.spec_type = "Jonswap"
    Wave_sp.add_spectrum()
    J = 1025.*9.81**2/(4.*np.pi)*sum(Wave_sp.specs[-1][2][ind,0]/f[ind])*df
    #print(Wave_sp.specs_type_list[-1])
    #print(J)
    #print('*'*50)
    Wave_sp.spec_type = "pscSwell"
    Wave_sp.add_spectrum()
    Wave_sp.spec_type = "Regular"
    Wave_sp.add_spectrum()
    # Wave_sp.show() all the instalnce of plots have been removed from the code
    Wave_sp.rm_spectrum(index='all')
    nt = 110
    Wave_sp.t = np.linspace(-np.pi,np.pi,nt)
    Wave_sp.spec_type = "Jonswap"
    Wave_sp.add_spectrum()
    s_vec = np.linspace(1,16,5)
    for s in s_vec:
        Wave_sp.s = s
        Wave_sp.add_spectrum()
        # Wave_sp.show(index=[-1])

    Wave_sp123 = wave_spec(f,fp,Hs,np.linspace(-np.pi,np.pi,nt), 0., 0., 'Regular') 
    Wave_sp123.rm_spectrum(0)

    Wave_sp.spec_type = "pscTMA"
    with pytest.raises(ValueError) as excinfo:
       Wave_sp.add_spectrum()
    

def test_disp_e():
    nt = 110
    f, df = Dispersion_E(6., [3., 8.])

def test_wn():
    
    wp = 13.5
    wd = 35.4
    L = WNumber(wp, wd)

def test_len2():
    p = lambda x: x**3 - x**2 - 1
    with pytest.raises(TypeError) as excinfo:
       len2(p)    

def test_write_variable():

    fn = 'testing.txt'
    nt = 1
    var = np.linspace(-np.pi,np.pi,nt)
    WriteVariableinFile(var, fn)
    nt = 4
    var = np.linspace(-np.pi,np.pi,nt)
    WriteVariableinFile(var, fn)
    x = np.zeros((2,3,4),dtype=np.int16)
    WriteVariableinFile(x, fn)    
    x = np.full((2,2),7)
    WriteVariableinFile(x, fn)    
    x = np.zeros((2,3,4,7),dtype=np.int16)
    WriteVariableinFile(x, fn)    
