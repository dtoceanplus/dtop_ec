from  dtop_energycapt.business.arraywecstg3.perf_metrics.utils.hdf5_interface import *
import pytest
import sys, os
import numpy as np


def test_create_empty_project():
    create_empty_project('..', 'test')
    data = {'x': 'astring',
            'y': np.arange(10),
            'd': {'z': np.ones((2,3)),
                  'b': b'bytestring'}}
    filename = 'test.h5'
    save_dict_to_hdf5(data, filename)
    dd = load_dict_from_hdf5(filename)
    data = load_key_from_hdf5(filename, 'x')