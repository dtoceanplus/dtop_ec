from dtop_energycapt import business
import pytest




def test_wecstg1_init():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    assert ARRAY.nb_devices == None


def test_wecstg1_inputs():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    inputs_wec = {
        'nb_devices': 10,
        'array_layout': [[0,0]],
        # 'opt_method': 'monte_carlo',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'rectangular',
        'rated_pow_device': 40000.,
        'loc_resource': 25.0,
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'device_capture_width_ratio': 0.3,
        'sec_dim_device': 55.,
        'main_dim_device': 10.,
        'arch_type': 'terminator',
    }

    ID = 9
    pj_name = "_wec_test_project"
    ARRAY.get_inputs(pj_name, ID, **inputs_wec)
    assert ARRAY.nb_devices == 10


def test_wecstg1_exceptions():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(ValueError) as excinfo:
        ecID = 4
        name = 'RM3_NS'
        inputs = {
            'nb_devices': 5,
            'rated_pow_device': -15.,
            'loc_resource': 222.0,
            'loc_position': [[5000, 5555]],
            'array_generic_type': 'rectangular',
            'ratedPowerArray': 5000.,
            'device_capture_width_ratio': 0.3,
            'sec_dim_device': 25.,
            'main_dim_device': 12.,
        }
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_exceptions_nbdev():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(ValueError) as excinfo:
        ecID = 1
        name = 'RM3_NS'
        inputs = {
            'nb_devices': -5,
            'rated_pow_device': 15.,
            'opt_method': None,
            'loc_resource': 222.0,
            'loc_position': [[5000, 5555]],
            'device_capture_width': 0.3,
            'sec_dim_device': 25.,
            'main_dim_device': 12.,
        }
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_readdig(capsys):
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    output = ARRAY.read_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Reading the digital representation\n'

def test_wecstg1_writedig(capsys):
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    output = ARRAY.write_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Writing the digital representation\n'

@pytest.mark.skip(reason='Unknown')
def test_wecstg1_main1():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    inputs = {  'nb_devices': 10, 
             'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    ARRAY.verify_farm_layout()

def test_wecstg1_main2():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
            'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    with pytest.raises(Warning) as excinfo:
        ARRAY.verify_farm_layout()


def test_wecstg1_inputs1():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    inputs = {  'nb_devices': 10, 
                # 'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    assert ARRAY.layout == None


def test_wecstg1_inputs2():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }

    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs('test', 1, **inputs)
    assert ARRAY.optfunname == 'ArrayWECStg1_cmaes'

def test_wecstg1_inputs3():
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    inputs = {  #'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,            
            } 
    site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
    converter = { 
                'device_capture_width': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 2, **inputs)

def test_wecstg1_inputs4():
    # Checking array_generic_type is a str
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1
        name = 1
        inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,
            'array_generic_type': 'staggered',            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'device_capture_width': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_inputs5():
    # Checking array_generic_type is a str
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1.98
        name = 'test'
        inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 0.8,
            'array_generic_type': 'staggered',            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'device_capture_width': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_inputs6():
    # Checking optimisation_threshold is a float
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1
        name = 'RM3_NS'
        inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 98.9,
            'array_generic_type': [1,9],            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_inputs7():
    # Checking optimisation_threshold is a float
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1
        name = 'RM3_NS'
        inputs = {  'nb_devices': 'nb', 
             'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            # 'opt_method': 'cmaes',
            'optimisation_threshold': 98.9,
            'array_generic_type': 'rectangular',            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_inputs8():
    # Checking optimisation_threshold is a float
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    with pytest.raises(TypeError) as excinfo:
        ecID = 1
        name = 'RM3_NS'
        inputs = {  'nb_devices': 10, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 'string',
            'array_generic_type': 'rectangular',            
            } 
        site = {
        'loc_resource': 64000.0,  # this is defined as power/m2
        'loc_position': [[0,0],[500,500],[0,500]],
        }
        converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
        inputs.update(site)
        inputs.update(converter)
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter1():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                # 'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_converter2():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                # 'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter3():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                # 'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter4():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                # 'sec_dim_device': 5.,
                'rated_pow_device': 300.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

    
def test_wecstg1_converter5(capsys):
    # Checking the printInput function of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.i_converter.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of Converter\n'

def test_wecstg1_converter6():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 'test',
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter7():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': -50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_converter8():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 'test',
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_converter9():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': -0.4,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter10():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': -10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter11():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 10.,
                'sec_dim_device': -5.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter12():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 'test',
                'sec_dim_device': 5.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    print('testing the type for the main dim')
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_converter13():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 5.,
                'sec_dim_device': 'dhjkahsdkj',
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    print('testing typeerror for secondary dimension')
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_converter14():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 5.,
                'sec_dim_device': 2.,
                'rated_pow_device': 50.,
                'arch_type': 'dummy',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_converter15():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 5.,
                'sec_dim_device': 2.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        ARRAY.i_converter.checkPowerConsistency('test')

def test_wecstg1_converter16():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 5.,
                'sec_dim_device': 2.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.i_converter.checkPowerConsistency(60.)

def test_wecstg1_converter17():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.4,
                'main_dim_device': 5.,
                'sec_dim_device': 2.,
                'rated_pow_device': 50.,
                'arch_type': 'terminator',
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)
        ARRAY.i_converter.checkPowerConsistency(10.)



def test_wecstg1_site1():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
        'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': -64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_site2():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'layout_type': 'optimization', 
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            # 'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_site3():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15,
                'layout_type': 'optimization',  
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
            #  'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_site4():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
                'layout_type': 'optimization', 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 'gsdkjfsdie',  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_site5(capsys):
    # Checking the printInput function of the site conditions
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15,
                'layout_type': 'optimization',  
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.0,  # this is defined as power/m2
             'loc_position': [[0,0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 10.,
                'sec_dim_device': 5.,
                'rated_pow_device': 300.
        }
    inputs.update(site)
    inputs.update(converter)
    ARRAY.get_inputs(name, ecID, **inputs)
    ARRAY.i_site_conditions.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of SiteConditions\n'


def test_wecstg1_site6():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
                'layout_type': 'optimization', 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.,  # this is defined as power/m2
             'loc_position': [['test',0],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)



def test_wecstg1_site7():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
                'layout_type': 'optimization', 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.,  # this is defined as power/m2
             'loc_position': 'test',
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_site8():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15,
                'layout_type': 'optimization', 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.,  # this is defined as power/m2
             'loc_position': ['test',[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)


def test_wecstg1_site9():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.,  # this is defined as power/m2
             'loc_position': [[-30,10],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

def test_wecstg1_site10():
    # Checking the missing inputs print
    ARRAY = business.ArrayFactory.get_array('WEC', "1")
    ecID = 5
    name = 'test'
    inputs = {  'nb_devices': 15, 
            #  'array_layout': [[0,0],[100,0],[100,100],[0,100]], 
        #     'optimisation_threshold':,
            'opt_method': 'cmaes',
            'optimisation_threshold': 75.8,
            'array_generic_type': 'blah',            
            } 
    site = {
            'loc_resource': 64000.,  # this is defined as power/m2
             'loc_position': [[10],[500,500],[0,500]],
                }
    converter = { 
                'device_capture_width_ratio': 0.3,
                'main_dim_device': 20.,
                'sec_dim_device': 5.,
                'rated_pow_device': 15.
        }
    inputs.update(site)
    inputs.update(converter)
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **inputs)

