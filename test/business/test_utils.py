from dtop_energycapt import business
import numpy as np
import pytest

def test_closest_point():
    business.arraytecstg3.utils.misc.closest_point([0,0],[[0,1],[10,10]])
    l1 = business.arraytecstg3.utils.misc.line([0,1],[10,10])
    l2 = business.arraytecstg3.utils.misc.line([0,1],[0,-10])
    business.arraytecstg3.utils.misc.intersection(l1, l2)

    business.arraytecstg3.utils.misc.intersection(l1, l1)
    business.arraytecstg3.utils.misc.pi2pi(-100)


def test_transec_surf():
    ecID = 5
    name = "test_project"

    array_layout = [[0, 0], [100, 50], [0, 100]]
    inputs = {
        'nb_devices': 3,
        'opt_method': 'cmaes',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'rectangular',
        'ratedPowerArray': 5000,
        'distance_min': (10, 10),
        'nogo_areas': None,
        'array_layout': array_layout
        } 
    bathy = -30.0
    xmax = 840
    ymax = 350
    lease = np.asarray([[0.0, 0.0], [0.0, ymax], [xmax, ymax], [xmax, 0.0]])

    x = np.linspace(0, xmax, int(xmax / 10) + 1)  # dx = 10 m
    y = np.linspace(0, ymax, int(ymax / 10) + 1)  # dy = 10 m
    X, Y = np.meshgrid(x, y)
    nx = len(x)
    ny = len(y)
    BR = 1.0  # blockage ratio

    umax = 5.0  # maximum velocity in the X direction
    vmax = 0.0  # maximum velocity in the Y direction
    sshmax = 0.0  # maximum sea surface elevation
    timax = 0.1
    bathy = -30.0

    U = np.ones(X.shape) * umax
    V = np.ones(X.shape) * vmax
    SSH = np.ones(X.shape) * sshmax
    TI = np.ones(X.shape) * timax
    BATHY = np.ones(X.shape) * bathy
    PLE = 1.0 * np.ones(X.shape)
    # bed roughness coefficient here
    manning = np.ones(X.shape) * 0.001
    site = {}
    site['TI'] = [TI.tolist()]
    site['PLE'] = PLE.tolist()
    site['geophy'] = manning.tolist()
    site['X'] = x.tolist()  # save only 1D array as structured grid assumed
    site['Y'] = y.tolist()  # save only 1D array as structured grid assumed
    site['U'] = [U.tolist()]
    site['V'] = [V.tolist()]
    site['SSH'] = [SSH.tolist()]
    site['bathy'] = BATHY.tolist()
    site['BR'] = BR
    site['lease'] = lease.tolist()
    site['probability'] = [1.0]

    ## Turbines positions
    bathy = -30.0
    z = bathy / 2.0  # hub height/depth
    diam = 5
    array_layout = [[0, 0], [100, 50], [0, 100], [300,70]]

    ## Turbines features
    cut_in = 0.0  # cut-in speed
    cut_out = 10.0  # cut-out speed
    # actual turbine features
    speed = np.arange(0.0, 10.0, 0.2)

    CT = np.ones(speed.shape) * 0.76
    Ct = [speed.tolist(), CT.tolist()]  # thrust curve

    CP = np.ones(speed.shape) * 0.3
    Cp = [speed.tolist(), CP.tolist()]  # Power curve
    feat = {}

    feat = {}
    feat['OA'] = 270.0  # orientation angle (deg.), turbines face the West
    feat['HAS'] = 180.0  # heading angle span (deg.), max = 180 deg. = full yaw
    feat['Cp'] = Cp[:]  # Power curve
    feat['Ct'] = Ct[:]  # thrust curve
    feat['Diam'] = diam  # Diam = rotor diameter (m)
    feat['cutIO'] = [cut_in, cut_out]
    feat[
        'floating'] = False  # so hub height will be considered from the seabottom upwards
    feat['2way'] = False  # turbines work in both direction.
    feat['Rating'] = 5e6  # turbines work in both direction.
    feat['hub_height'] = 20
    
    inputs['rotor_diameter'] = 10

    inputs.update(site)
    inputs.update(feat)
    ARRAY = business.ArrayFactory.get_array('TEC', "3")
    ARRAY.get_inputs(name, ecID, **inputs)
    inputs['nb_devices'] = 1
    inputs['array_layout'] = [[0,0]]
    inputs['floating'] = True
    inputs['2way'] = True
    ARRAY.get_inputs(name, ecID, **inputs)

    layout = array_layout
    n_bodies = len(layout)
    z = 15
    coords = {}

    for ii in range(n_bodies):
        coords[f'turbine{ii}'] = {}
        coords[f'turbine{ii}']['position'] = np.asarray(
            (layout[ii][0], layout[ii][1], -z))
    features = {}
    for key in coords.keys():
        features[key] = feat



    hydro = business.arraytecstg3.ArrayTECstg3.Hydro(ARRAY.i_site_conditions, 0)
    array = business.arraytecstg3.array.Array(hydro, coords, features)

    business.arraytecstg3.utils.misc.transec_surf(hydro, array)

    single_coords = {} 
    single_coords['turbine0'] = coords['turbine0']
    array = business.arraytecstg3.array.Array(hydro, single_coords, features)

    business.arraytecstg3.utils.misc.transec_surf(hydro, array)

    layout = [[-100, 0], [100, 100], [300, -1000]]
    n_bodies = len(layout)
    coords = {}

    for ii in range(n_bodies):
        coords[f'turbine{ii}'] = {}
        coords[f'turbine{ii}']['position'] = np.asarray(
            (layout[ii][0], layout[ii][1], -z))
    features = {}
    for key in coords.keys():
        features[key] = feat
    array = business.arraytecstg3.array.Array(hydro, coords, features)
    business.arraytecstg3.utils.misc.transec_surf(hydro, array)