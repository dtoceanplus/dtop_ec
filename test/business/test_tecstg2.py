from dtop_energycapt import business
import pytest
import numpy as np

layout = np.array([[2.39964301, 493.2435497], [2.39964301, 280.02992788],
                   [19.69090866, 386.63673879], [36.98217431, 493.2435497],
                   [2.39964301, 66.81630606], [19.69090866, 173.42311697],
                   [36.98217431, 280.02992788], [54.27343996, 386.63673879],
                   [71.5647056, 493.2435497], [36.98217431, 66.81630606],
                   [54.27343996, 173.42311697], [71.5647056, 280.02992788],
                   [88.85597125, 386.63673879], [106.1472369, 493.2435497],
                   [88.85597125, 173.42311697], [106.1472369, 280.02992788],
                   [123.43850255, 386.63673879], [140.72976819, 493.2435497],
                   [123.43850255, 173.42311697], [140.72976819, 280.02992788],
                   [158.02103384, 386.63673879], [175.31229949, 493.2435497],
                   [158.02103384, 173.42311697], [175.31229949, 280.02992788],
                   [192.60356514, 386.63673879], [209.89483079, 493.2435497],
                   [209.89483079, 280.02992788], [227.18609643, 386.63673879],
                   [244.47736208, 493.2435497], [244.47736208, 280.02992788],
                   [261.76862773, 386.63673879], [279.05989338, 493.2435497],
                   [279.05989338, 280.02992788], [296.35115903, 386.63673879],
                   [313.64242467, 493.2435497], [330.93369032, 386.63673879],
                   [348.22495597, 493.2435497], [365.51622162, 386.63673879],
                   [382.80748727, 493.2435497], [417.39001856, 493.2435497],
                   [451.97254986, 493.2435497]])

def test_tecstg2_init():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    assert ARRAY.nb_devices == None


def test_tecstg2_inputs():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': None,
        }


    ID = 9
    pj_name = "_tec_test_project"
    ARRAY.get_inputs(pj_name, ID, **body)
    assert ARRAY.nb_devices == 42

    body['floating'] = True
    ID = 9
    pj_name = "_tec_test_project"
    ARRAY.get_inputs(pj_name, ID, **body)
    assert ARRAY.nb_devices == 42

def test_tecstg2_exceptions():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': -5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        }

    ecID = 9
    name = "_tec_test_project"
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_exceptions_nbdev():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': -42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        }


    ecID = 9
    name = "_tec_test_project"
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_readdig(capsys):
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    output = ARRAY.read_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Reading the digital representation\n'

def test_tecstg2_writedig(capsys):
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    output = ARRAY.write_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Writing the digital representation\n'

def test_tecstg2_main1():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        }


    ecID = 9
    name = "_tec_test_project"
    ARRAY.get_inputs(name, ecID, **body)
    ARRAY.calc_powprod()

def test_tecstg2_main2():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        # 'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        'layout_type': 'optimization'
        }


    ecID = 9
    name = "_tec_test_project"
    ARRAY.get_inputs(name, ecID, **body)
    with pytest.raises(Warning) as excinfo:
        ARRAY.verify_farm_layout()

def test_tecstg2_inputs1():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        # 'array_layout': layout.tolist(),
        'velocity_field': velocity,
        # 'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        }


    ecID = 9
    name = "_tec_test_project"
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_inputs2():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.9,
        }


    ecID = 9
    name = "_tec_test_project"
    ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_inputs3():
    """test id not int"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.9,
        }


    ecID = 'test'
    name = "_tec_test_project"
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_inputs4():
    """test name not string"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'opt_method': None,
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.9,
        }


    ecID = 1
    name = 3
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_inputs5():
    """test nb_devices not int"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 'test',
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.9,
        }


    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_inputs6():
    """test nb_devices nagative int"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': -40,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.9,
        }


    ecID = 1
    name = 'test'
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_inputs7():
    """test optimisation_threshold not float"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 'test',
        'layout_type': 'optimization'
        }


    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_inputs8():
    """test rated power array not float"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    device = {
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
            }

    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'device': device,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 'test',
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_inputs9():
    """test array_generic_type not correct string"""
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 8,
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        'layout_type': 'optimization'
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter1():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': [[0,0]],
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        # 'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(KeyError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter2():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        # 'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(KeyError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter3():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        # 'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter4():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        # 'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter5():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        # 'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter6():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 'test',
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        # 'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_converter7():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': -4e3,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        # 'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter8():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        # 'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_converter9(capsys):
    # Checking the printInput function of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    ARRAY.get_inputs(name, ecID, **body)
    ARRAY.i_converter.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of Converter\n'

def test_tecstg2_converter10():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)
        max_nb, status, err_str = ARRAY.i_converter.checkPowerConsistency(10.)

def test_tecstg2_converter11():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)
        max_nb, status, err_str = ARRAY.i_converter.checkPowerConsistency('test')

def test_tecstg2_converter12():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 10,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    ARRAY.get_inputs(name, ecID, **body)
    max_nb, status, err_str = ARRAY.i_converter.checkPowerConsistency(20e3)


def test_tecstg2_converter14():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    ARRAY.get_inputs(name, ecID, **body)
    ARRAY.i_converter.__str__()

def test_tecstg2_farm1():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    layout = np.array([[2.39964301], [2.39964301],
                   [19.69090866], [36.98217431],
                   [2.39964301], [19.69090866],
                   [451.97254986]])
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)
        ARRAY.PerfMetrics.rotate_layout()

def test_tecstg2_farm2():
    # Checking the minimum distance of the converter
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    layout = np.array([[2.39964301, 2.39964301, 2.39964301],
                   [451.97254986, 65.455, 2344.5857]])
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    ARRAY.get_inputs(name, ecID, **body)
    ARRAY.PerfMetrics.rotate_layout()


def test_tecstg2_site1():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        # 'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_site2():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        # 'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_site3(capsys):
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'array_layout': layout.tolist(),
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    ARRAY.get_inputs(name, ecID, **body)
    ARRAY.i_site_conditions.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of SiteConditions\n'

def test_tecstg2_site4():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'velocity_field': velocity,
        'nb_devices': 40,
        'array_layout': [[0,0]],
        'array_generic_type': 'rectangular',
        'loc_position': 'test',
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_site5():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'velocity_field': velocity,
        'array_layout': [[0,0]],
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [[1.],[1.,2.],[3.,4.]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)


def test_tecstg2_site6():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'velocity_field': velocity,
        'array_layout': [[0,0]],
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [['test', 1.],[1.,2.],[3.,4.]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)

# THIS TEST HAVE BEEN COMMNETED OUT BECAUSE THE 
# RELATED ERROR HAS BEEN REMOVED
# def test_tecstg2_site7():
#     ARRAY = business.ArrayFactory.get_array('TEC', "2")
#     velocity = ((0, 5), )
#     body = {'bathymetry': 30.,
#         'velocity_field': velocity,
#         'array_layout': [[0,0]],
#         'nb_devices': 40,
#         'array_generic_type': 'rectangular',
#         'loc_position': [[-1., 1.],[.1,.2],[3.,4.]],
#         'rated_pow_device': 5e3,
#         'rotor_diameter': 5,
#         'hub_height': 20,
#         # 'floating': False,
#         'cut_IO': [1, 10],
#         'cp': 0.3,
#         'ct': 1,
#         'alpha': 0.07,
#         # 'opt_method': 'cmaes',
#         'ratedPowerArray': 50e3,
#         'optimisation_threshold': 0.7,
#         }

#     ecID = 1
#     name = 'test'
#     with pytest.raises(ValueError) as excinfo:
#         ARRAY.get_inputs(name, ecID, **body)

def test_tecstg2_site8():
    ARRAY = business.ArrayFactory.get_array('TEC', "2")
    velocity = ((0, 5), )
    body = {'bathymetry': 30.,
        'velocity_field': velocity,
        'array_layout': [[0,0]],
        'nb_devices': 40,
        'array_generic_type': 'rectangular',
        'loc_position': [1.,[.1,.2],[3.,4.]],
        'rated_pow_device': 5e3,
        'rotor_diameter': 5,
        'hub_height': 20,
        # 'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        # 'opt_method': 'cmaes',
        'ratedPowerArray': 50e3,
        'optimisation_threshold': 0.7,
        }

    ecID = 1
    name = 'test'
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs(name, ecID, **body)