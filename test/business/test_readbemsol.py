from dtop_energycapt.business.arraywecstg3.converter.utils.read_bem_solution import *
import pytest
import sys, os
import numpy as np


def test_read_freq():
    cwd = os.getcwd()
    print(cwd)
    df = os.path.join(os.getcwd(),'test')
    print(df)
    f = read_freq(df)
    dir = read_directions(df)


def test_nodata():
    df = '..'
    with pytest.raises(ValueError) as excinfo:
       f = read_freq(df)


# if __name__ == "__main__":
#     test_read_freq()
