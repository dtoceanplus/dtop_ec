# from Visualise_polygons import * 
from shapely.geometry import Point 
from dtop_energycapt.business.stg3utils.utils.bathymetry_utility import get_unfeasible_regions, check_bathymetry_format
import pytest
import sys, os
import numpy as np


def test_bathy():
    z_bounds = [0, np.inf] 
    xyz = np.array([[ 426909.288416,	6306895.895129,  -29.2377],
    [ 427300.354027,	6329157.835841,  -39.4126],
    [ 445919.483171,	6362275.839637, -74.3213],
    [454904.179989,	6333638.380668, -26.376]])
    # test the bathymetry function 
    mp, unf = get_unfeasible_regions(xyz, z_bounds, debug=True) 
    xyz = xyz.T
    mp, unf = get_unfeasible_regions(xyz, z_bounds, debug=True) 
    z_bounds = [0, 39] 
    mp, unf = get_unfeasible_regions(xyz, z_bounds, debug=True) 
    check_bathymetry_format(xyz, z_bounds)

def test_bathy1():
    z_bounds = [0, np.inf] 
    xyz = [[ 426909.288416,	6306895.895129,  -29.2377],
    [ 427300.354027,	6329157.835841,  -39.4126],
    [ 445919.483171,	6362275.839637, -74.3213],
    [454904.179989,	6333638.380668, -26.376]]
    with pytest.raises(IOError) as excinfo:
        check_bathymetry_format(xyz, z_bounds)

def test_bathy2():
    z_bounds = [0, np.inf] 
    xyz = np.array([[ 	6306895.895129,  -29.2377],
    [ 	6329157.835841,  -39.4126],
    [ 	6362275.839637, -74.3213],
    [	6333638.380668, -26.376]])
    with pytest.raises(IOError) as excinfo:
        check_bathymetry_format(xyz, z_bounds)

def test_bathymetry():
    check_bathymetry_format(np.random.rand(3), [0,10])

