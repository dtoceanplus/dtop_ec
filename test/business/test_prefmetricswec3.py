
from dtop_energycapt.business.arraywecstg3.perf_metrics.utils.StrDyn import *
from dtop_energycapt.business.arraytecstg3.utils.interpolation import volume_under_plane
import json
import numpy as np
from dtop_energycapt.business.arraywecstg3.perf_metrics.utils.spectrum import jonswap
from dtop_energycapt.business.arraywecstg3.perf_metrics.utils.power import *
import pytest

def test_power():
    NBo=1 
    B= np.array([0,180.])/180.*np.pi 
    Hs = np.array([0.5,1,1.5,2.]) 
    Tp = np.array([3.5,4.5,5.5,6.5,7.5,8.5,9.5]) 
    wdir = np.linspace(0,360,30,endpoint=False)/180.*np.pi 
    period = np.linspace(2,15,50) 
    spectrum = (jonswap, (3.3,)) 
    M = np.eye(3) 
    Madd = np.array([np.eye(3)]*50) 
    Cpto = np.array([[[np.eye(3)]*2]*4]*7) 
    Crad = np.array([np.eye(3)]*50) 
    Khst = np.eye(3) + Cpto*0.
    Fex = np.ones((50,30,3)) 
    Kfit = np.array([[[np.eye(3)]*2]*4]*7) 
    Cfit = np.array([[[np.eye(3)]*2]*4]*7) 
    convention = "N"
    spreading_weights = [1.0]

    power_irregular(NBo, B, Hs, Tp, spectrum, 1/period, wdir, Fex, Madd,
                    Crad, M, Khst[:,:,0], Cpto[:,:,0], Kfit[:,:,0], Kfit[:,:,0], convention, spreading_weights)

    with pytest.raises(ValueError) as excinfo:
        power_irregular(NBo, B, Hs, Tp, spectrum, 1/period, wdir, Fex, Madd,
                    Crad, M, Khst, Cpto[0,:], Kfit, Kfit, convention, spreading_weights)

    with pytest.raises(IndexError) as excinfo:
        wdir = np.linspace(90, 360, 1, endpoint=False)/180.*np.pi 
        power_irregular(NBo, B, Hs, Tp, spectrum, 1/period, wdir, Fex, Madd,
                    Crad, M, Khst, Cpto, Kfit, Kfit, convention, spreading_weights)



def test_strdyn():
    NBo=1 
    B= np.array([0,180.])/180.*np.pi 
    Hs = np.array([0.5,1,1.5,2.]) 
    Tp = np.array([3.5,4.5,5.5,6.5,7.5,8.5,9.5]) 
    wdir = np.linspace(0,360,30,endpoint=False)/180.*np.pi 
    period = np.linspace(2,15,50) 
    ScatDiag = (np.ones((7,4,2))/(7*4*2),('Jonswap',3.3,0)) 
    M = np.eye(3) 
    Madd = np.array([np.eye(3)]*50) 
    Cpto = np.array([[[np.eye(3)]*2]*4]*7) 
    Crad = np.array([np.eye(3)]*50) 
    Khyd = np.eye(3) 
    Fex = np.ones((50,30,3)) 
    Kfit = np.array([[[np.eye(3)]*2]*4]*7) 
    Cfit = np.array([[[np.eye(3)]*2]*4]*7) 
    Pyr, P_dev = energyProduction(NBo, B, Hs, Tp, wdir, period, ScatDiag, 
                                    M, Madd, Cpto, Crad, Cpto, Khyd, Fex, 
                                    Kfit, Cfit, RatedPower=-1) 

    ScatDiag = (np.ones((7,4,2))/(7*4*2),('Jonswap',3.3,10)) 
    Pyr, P_dev = energyProduction(NBo, B, Hs, Tp, wdir, period, ScatDiag, 
                                    M, Madd, Cpto, Crad, Cpto, Khyd, Fex, 
                                    Kfit, Cfit) 

def test_vol_under_plane():

    a = np.array([1, 2, 3])
    b = np.array([2, 3, 4])
    c = np.array([[1, 3, 5], [1, 3, 5], [1, 3, 5]])
    volume_under_plane(a,b,c, debug=True)
