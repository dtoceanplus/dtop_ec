from dtop_energycapt import business
import pytest
import numpy as np


def test_wecstg1_obj1(inputs, site_wec2, device_wec2):
    
    inputs.pop('array_layout')
    inputs['nb_devices'] = 3
    inputs.pop('array_generic_type')
    inputs.pop('ratedPowerArray')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_obj2(inputs, site_wec2, device_wec2):
    
    inputs['layout_type'] = 'verification'
    inputs['nb_devices'] = 3

    inputs.update(site_wec2)
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    ARRAY.get_inputs('test', 1, **inputs)
    ARRAY.verify_farm_layout()

def test_wecstg2_readdig(capsys):
    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    output = ARRAY.read_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Reading the digital representation\n'

def test_wecstg2_writedig(capsys):
    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    output = ARRAY.write_digrep()
    captured = capsys.readouterr()
    assert captured.out == 'Writing the digital representation\n'


def test_wecstg1_inputs1(inputs, site_wec2, device_wec2):
    
    inputs.pop('nb_devices')
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_inputs2(inputs, site_wec2,device_wec2):
    
    inputs['nb_devices'] = 3
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs([3,5], 1, **inputs)

def test_wecstg1_inputs3(inputs, site_wec2,device_wec2):
    
    inputs['nb_devices'] = 3
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 'ID1', **inputs)

@pytest.mark.skip(reason='Unknown')
def test_wecstg1_inputs4(inputs, site_wec2, device_wec2):
    
    inputs['layout_type'] = 'verification'
    inputs['nb_devices'] = 3
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_inputs5(inputs, site_wec2, device_wec2):
    
    device_wec2['sec_dim_device'] = 12

    inputs['nb_devices'] = 'test'
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_inputs6(inputs, site_wec2, device_wec2):

    inputs['nb_devices'] = -30
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_inputs7(inputs, site_wec2, device_wec2):

    inputs['array_layout'] = 2.0
    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_converter1(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('device_capture_width_ratio')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_converter2(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('tp_capture_width')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_converter3(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('tp_capture_width')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_converter4(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('device_capture_width_ratio')
    device_wec2.pop('main_dim_device')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_converter5(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('sec_dim_device')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter6(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('rated_pow_device')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter7(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2.pop('main_dim_device')

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter8(capsys, inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['sec_dim_device'] = 12

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)


    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    ARRAY.get_inputs('name', 1, **inputs)
    ARRAY.i_converter.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of Converter\n'

def test_wecstg1_converter9(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['rated_pow_device'] = 'test'

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter10(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['rated_pow_device'] = -50e6

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter11(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['device_capture_width_ratio'] = 'test'

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter13(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['rated_pow_device'] = 50e3
    device_wec2['main_dim_device'] = 'test'

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter14(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['rated_pow_device'] = 50e3
    device_wec2['main_dim_device'] = -10

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter15(inputs, site_wec2, device_wec2):
    device_wec2['arch_type'] = 'absorber'

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter16(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['rated_pow_device'] = 50e3
    device_wec2['sec_dim_device'] = 'test'

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_converter17(inputs, site_wec2, device_wec2):
    device_wec2.pop('arch_type')
    device_wec2['rated_pow_device'] = 50e3
    device_wec2['sec_dim_device'] = -10

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_converter18(inputs, site_wec2, device_wec2):
    device_wec2['rated_pow_device'] = 50

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)
        ARRAY.i_converter.checkPowerConsistency('test')

def test_wecstg1_converter19(inputs, site_wec2, device_wec2):
    device_wec2['rated_pow_device'] = 50

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)


    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)
        ARRAY.i_converter.checkPowerConsistency(10.)

def test_wecstg1_converter20(inputs, site_wec2, device_wec2):
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)


    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    ARRAY.get_inputs('test', 1, **inputs)
    max_nb, sta, errstr = ARRAY.i_converter.checkPowerConsistency(150.)

def test_wecstg1_site1(inputs, site_wec2, device_wec2):
    site_wec2.pop('scatter_diagram')
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)


    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(RuntimeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)


def test_wecstg1_site2(inputs, site_wec2, device_wec2):
    site_wec2.pop('sea_state_wave_height')
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site3(inputs, site_wec2, device_wec2):
    site_wec2.pop('sea_state_wave_period')
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site4(inputs, site_wec2, device_wec2):
    site_wec2.pop('loc_position')
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site5(capsys,inputs, site_wec2, device_wec2):
    
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    ARRAY.get_inputs('test', 1, **inputs)
    ARRAY.i_site_conditions.printInput()
    captured = capsys.readouterr()
    assert captured.out == 'Print all arguments of this instance of SiteConditions\n'


def test_wecstg1_site6(inputs, site_wec2, device_wec2):
    site_wec2['scatter_diagram'] = 'test'
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site7(inputs, site_wec2, device_wec2):
    site_wec2['scatter_diagram'] = np.array([0.2, 0.5, -0.3]).tolist()
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site8(inputs, site_wec2, device_wec2):
    site_wec2['sea_state_wave_height'] = np.array([-3]).tolist()
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site9(inputs, site_wec2, device_wec2):
    site_wec2['sea_state_wave_height'] = 'test'
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site10(inputs, site_wec2, device_wec2):
    site_wec2['sea_state_wave_period'] = 'test'
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site11(inputs, site_wec2, device_wec2):
    site_wec2['sea_state_wave_period'] = np.array([4, -5]).tolist()
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site12(inputs, site_wec2, device_wec2):
    site_wec2['loc_position'] = 'test'
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site14(inputs, site_wec2, device_wec2):
    site_wec2['loc_position'] = [[0], [200, 0], [100, 100]]
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(ValueError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site15(inputs, site_wec2, device_wec2):
    site_wec2['loc_position'] = ['test', [200, 0], [100, 100]]
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)

    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)

def test_wecstg1_site16(inputs, site_wec2, device_wec2):
    site_wec2['loc_position'] = [[0,'test'], [200, 0], [100, 100]]
    device_wec2['rated_pow_device'] = 50.

    inputs.update(site_wec2)
    
    inputs.update(device_wec2)
    inputs['opt_method'] =  None
    ARRAY = business.ArrayFactory.get_array('WEC', "2")
    with pytest.raises(TypeError) as excinfo:
        ARRAY.get_inputs('test', 1, **inputs)
