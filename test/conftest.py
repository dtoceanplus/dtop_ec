import pytest
from dtop_energycapt.service import create_app
from dtop_energycapt.service.db import init_db
import numpy as np
import os

@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': 'sqlite://',
        'SERVER_NAME': 'localhost.dev'
    })
    with app.app_context():
        init_db()
    yield app

@pytest.fixture
def client(app):
    return app.test_client()
    
@pytest.fixture
def runner(app):
    return app.test_cli_runner()

@pytest.fixture
def tec2():
    return {
        'main_dim_device': 10,
        'sec_dim_device': 20,
        'rotor diameter': 10,
        'hub height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'rated_pow_device': 5e6,
    }

@pytest.fixture
def body(tec2):

    return  {
        'device': tec2,
        'main_dim_device': 10,
        'sec_dim_device': 20,
        # 'layout': layout.tolist(),
        'velocity_field': (1,0),
        'loc_resource': 10,
        'nb_devices': 42,
        'array_generic_type': 'staggered',
        'loc_position': [[0, 0], [500, 500], [0, 500]],
        'rated_pow_device': 5e6,
        'rotor_diameter': 10,
        'hub_height': 20,
        'floating': False,
        'cut_IO': [1, 10],
        'cp': 0.3,
        'ct': 1,
        'alpha': 0.07,
        'optimisation_threshold': 0.8,
        'opt_method': 'meta_model',
    }

@pytest.fixture
def device_wec2():
    return {
            'device_capture_width_ratio': np.array([0.3, 0.4]).tolist(),
            'tp_capture_width': np.array([4, 5]).tolist(),
            'hs_capture_width': np.array([1, 1]).tolist(),
            'wave_angle_capture_width':  np.array([0, 0]).tolist(),
            'main_dim_device': 10,
            'sec_dim_device': 10,
            'rated_pow_device': 300,
            'arch_type': 'point_absorber'
            }

@pytest.fixture
def site_wec2(layout):
    return {
        'scatter_diagram': np.array([0.5, 0.4]).tolist(),
        'sea_state_wave_height': np.array([3, 3]).tolist(),
        "sea_state_directions": [0,0],
        'sea_state_wave_period': np.array([4, 5]).tolist(),
        'loc_position': [[0, 0], [200, 0], [100, 100]],
            }
@pytest.fixture
def inputs(layout):
    return {
        'nb_devices': 30,
        'array_layout': layout.tolist(),
        'opt_method': 'cmaes',
        'optimisation_threshold': 0.8,
        'array_generic_type': 'rectangular',
        'ratedPowerArray': 50e6,
            }

@pytest.fixture
def layout():
    return  np.array([[9.18003837, 253.5209363], [30.17825547, 413.14573036],
                   [30.17825547, 93.89614225], [51.17647258, 253.5209363],
                   [72.17468969, 413.14573036], [72.17468969, 93.89614225],
                   [93.17290679, 253.5209363], [114.1711239, 413.14573036]])

@pytest.fixture
def m_wec3():
    return {    'position_vector': np.array([1,2,2.3,5.], np.float).tolist(),
                'yaw_angle': 0.2,
                'floating': True,
                'install_depth': [10, 35],
                'minimum_interdistance': (10, 10),
                'rated_power_device': 300,
                'data_folder': os.path.join(os.getcwd(),'test'),
            }

@pytest.fixture
def general_inputs():
    return {    
                'layout_type': 'verification',
                'orientation_angle': [0],
                'nb_devices': 10,
                'opt_method': 'monte_carlo',
                'optimisation_threshold': 0.8,
                'optimisation_options': { 'type': 1,
                                                'grid': 'rectangular'
                                        },
                'array_generic_type': 'staggered',
                'ratedPowerArray': 5000,
                'distance_min': (10, 10),
                'nogo_areas': [[-100,-100],[50,-150],[100,-100],[150,0],[0,50]],
                'array_layout': [[9.18003837, 253.5209363], [30.17825547, 413.14573036],
                                [30.17825547, 93.89614225], [51.17647258, 253.5209363],
                                [72.17468969, 413.14573036], [72.17468969, 93.89614225],
                                [93.17290679, 253.5209363], [114.1711239, 413.14573036]]
            }

@pytest.fixture
def s_wec3():
    return {
        'sea_state_directions':
        [1.0, 2.0, 2.3, 1.0, 2.0, 2.3, 1.0, 2.0, 2.3, 1.0, 2.0, 2.3, 1.0, 2.0, 2.3, 1.0, 2.0, 2.3],
        'sea_state_wave_height':
        [2.3, 2.3, 2.3, 5.6, 5.6, 5.6, 2.3, 2.3, 2.3, 5.6, 5.6, 5.6, 2.3, 2.3, 2.3, 5.6, 5.6, 5.6],
        'sea_state_wave_period':
        [4.3, 4.3, 4.3, 4.3, 4.3, 4.3, 8.6, 8.6, 8.6, 8.6, 8.6, 8.6, 9.6, 9.6, 9.6, 9.6, 9.6, 9.6],
        'water_depth':
        25.6,
        'scat_diagram':
        [0.04, 0.06, 0.05, 0.05, 0.04, 0.06, 0.04, 0.06, 0.15, 0.05, 0.05, 0.05, 0.04, 0.06, 0.08, 0.02, 0.07, 0.03],
        'bathymetry':
        [[0,0,25]],
        'spectral_type': ('Jonswap', 3.3, 0.0),
        'main_direction':
        np.array([250, 350], np.float).tolist(),
        'lease':
        np.array([[-100, -100], [50, -150], [100, -100], [150, 0], [0, 50]],
                np.float).tolist(),
        'electrical_connection_point':
        np.array([350, 250], np.float).tolist(),
        'nogo_areas':
        np.array([[-100, -100], [50, -150], [100, -100], [150, 0], [0, 50]],
                np.float).tolist(),
    }