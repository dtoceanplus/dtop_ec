import DashboardPage from './dashboard.page';

class ProjectPage {

    constructor() {
        this.dashboard = new DashboardPage();
    }

    createProject() {
        this.dashboard.visit()
        this.dashboard.createProject()
        this.dashboard.openProject()
    }


    removeProject() {
        this.dashboard.visit()
        this.dashboard.removeProject()
    }

    openInputsMenu() {
        cy.get('[data-cy="toggleMenu"]').click()
    }

    openLinkFarm() {
        cy.get('[data-cy="linkFarm"]').click()
    }

    openLinkSite() {
        cy.get('[data-cy="linkSite"]').click()
    }

    openLinkMachine() {
        cy.get('[data-cy="linkMachine"]').click()
    }

    change_layout_type(type) {
        cy.get('[data-cy="layout_type"]').click()
        cy.get('.el-select-dropdown').contains(type).click()
        return this;
    }

    fill_array_generic_type(type) {
        cy.get('[data-cy="array_generic_type"]').click()
        cy.get('.el-select-dropdown').contains(type).click()
        return this;
    }

    change_opt_method(eq) {
        cy.get('[data-cy="opt_method"]').click()
        cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
        return this;
    }

    fill_nb_devices(value) {
        const field = cy.get(`[data-cy="nb_devices"]`);
        // field.dblclick();
        cy.get('.el-input-number__increase').dblclick();
        return this;
    }

    fill_array_generic_type(value) {
        const field = cy.get(`[data-cy="array_generic_type"]`);
        field.clear();
        field.type(value);
        return this;
    }

    fill_array_layout() {
        cy.get('#cell1-0').click();
        cy.get('.editable-field > input').type('{rightarrow}');
        cy.get('.editable-field > input').type('100');
        cy.get('body').type('{rightarrow}');
        cy.get('.editable-field > input').type('100');
        return this;
    }

    fill_notes(value) {
        const field = cy.get(`[data-cy="notes"]`);
        field.clear();
        field.type(value);
        return this;
    }

    change_site_model(eq) {
        cy.get('[data-cy="site_model"]').click()
        cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
        return this;
    }

    fill_site_desc(value) {
        const field = cy.get(`[data-cy="site_desc"]`).find('textarea');
        field.clear();
        field.type(value);
        return this;
    }

    change_machine_model(eq) {
        cy.get('[data-cy="machine_model"]').click()
        cy.get('.el-select-dropdown').find('.el-select-dropdown__item').eq(eq).click()
        return this;
    }

    fill_machine_desc(value) {
        const field = cy.get(`[data-cy="machine_desc"]`).find('textarea');
        field.type(value);
        return this;
    }

    submit(btn) {
        const button = cy.get('[data-cy=submit_form]');
        button.click();
    }

}
export default ProjectPage;
