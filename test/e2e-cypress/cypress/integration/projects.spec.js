import ProjectPage from '../pages/project.page';

describe("project", () => {
    const project = new ProjectPage();
    before(() => {
        project.createProject()
    })

    after(() => {
        project.removeProject()
    })

    it('Farm', () => {
        project.submit('farmSubmit')
        // project.fill_nb_devices(3)
        //     .fill_array_layout()
        //     .fill_notes('notes')
        //     .submit('farmSubmit')
    })

    it.skip('Farm Optimization', () => {
        project.openLinkFarm()
        project.fill_nb_devices(3)
            .change_layout_type('Optimization')
            .fill_array_generic_type('Array Type')
            .fill_notes('notes')
            .submit('farmSubmit')
    })

    it.skip('Site simplified', () => {
        project.openLinkSite()
        project.change_site_model(0)
            .fill_site_desc('desc site')
            .submit('siteSubmit')
    })

    it.skip('Site', () => {
        project.openLinkSite()
        project.change_site_model(0)
            .fill_site_desc('desc site')
            .submit('siteSubmit')
        project.openLinkMachine()
        project.openLinkSite()
        cy.contains('desc site')
    })
    
    it.skip('Machine simplified', () => {
        project.openLinkMachine()
        project.change_machine_model(0)
            .fill_machine_desc('desc machine')
            .submit('machineSubmit')
    })

    it.skip('Machine', () => {
        project.openLinkMachine()
        project.change_machine_model(0)
            .fill_machine_desc('desc machine')
            .submit('machineSubmit')
        project.openLinkSite()
        project.openLinkMachine()
        cy.contains('desc machine')
    })

    it.skip('Reload Page', () => {
        project.openLinkMachine()
        cy.reload()
    })

    it.skip('Input string in field: Target number of drivers', () => {
        project.openLinkFarm()
        project.fill_nb_devices('String')
            .fill_array_layout('0,0,100,101')
            .fill_notes('notes')
            .submit('farmSubmit')
    })
})
