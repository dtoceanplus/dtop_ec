import DashboardPage from '../pages/dashboard.page';
describe("dashboard", () => {
    const dashboard = new DashboardPage();

    it('Create EC project', () => {
        dashboard.visit();
        dashboard.createProject();
    })

    it('Edit EC project', () => {
        dashboard.editProject()
    })

    it('Open EC project', () => {
        dashboard.visit();
        dashboard.openProject()
    })

    it('Remove EC project', () => {
        dashboard.visit();
        dashboard.removeProject()
    })
})