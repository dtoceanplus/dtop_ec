import os

module_name = 'machine'
basePath = "/Users/ff/Documents/WORK/PROJECTS/2018_DTOceanPlus/Coding/api-coop/dtop-mc/0.0.1/schemas/examples"
# machine
# data = [{"url": "/mc/1/single_machine_hydrodynamic/wec/complexity3", "json_file": "results_wec_cpx3.json"},
#         {"url": "/mc/1/general", "json_file": "general.json"},
#         {"url": "/mc/1/dimensions", "json_file": "dimensions.json"},
#         {"url": "/mc/1/model/tec/complexity1", "json_file": "modelling_tec_cpx1.json"},
#         {"url": "/mc/1/model/tec/complexity2", "json_file": "modelling_tec_cpx2.json"},
#         {"url": "/mc/1/model/tec/complexity3", "json_file": "modelling_tec_cpx3.json"},
#         {"url": "/mc/1/model/wec/complexity1", "json_file": "modelling_wec_cpx1.json"},
#         {"url": "/mc/1/model/wec/complexity2", "json_file": "modelling_wec_cpx2.json"},
#         {"url": "/mc/1/model/wec/complexity3", "json_file": "modelling_wec_cpx3.json"}]

# site
data = [{"url": "/sc/1/farm", "json_file": "results_wec_cpx3.json"}]

def create_base_request(d):
    baseline = ["  - predicates:",
        "    - equals:",
        "        method: GET",
        f"        path: {d['url']}",
        "    responses:",
        "    - is:",
        "        statusCode: 200",
        "        body:"]
    with open(os.path.join(basePath, d['json_file']), 'r') as fid:
        lines = fid.readlines()

    with open(f'{module_name}.yaml', 'a') as fid:
        for l in baseline:
            fid.write(f'{l}\n')
        for el in lines:
            if el == "{\n" or el == "}\n":
                continue
            el = el.replace('"', '')
            el = el.replace(',\n', '\n')
            fid.write(f'      {el}')
        
        fid.write('\n')
        
for dat in data:
    create_base_request(dat)