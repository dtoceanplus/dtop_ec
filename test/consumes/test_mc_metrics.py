import atexit

import requests
from pact import Consumer, Like, Provider, Term

from wec3_data import *

pact = Consumer('ec').has_pact_with(Provider('mc'), port=1234)
pact.start_service()
atexit.register(pact.stop_service)


def test_mc_metrics_general(client):

    pact.given('mc 1 exists and it has general') \
        .upon_receiving('a request for general') \
        .with_request('GET', Term(r'/mc/\d+/general', '/mc/1/general')) \
        .will_respond_with(200, body=Like({'rated_capacity': 1.11, 
                            'floating': True, 
                            'max_installation_water_depth': -10,
                            'min_installation_water_depth': -100,
                            'min_interdistance_x': 100,
                            'min_interdistance_y': 50}))

    with pact:
        response = requests.get(f'{pact.uri}/mc/1/general')
        assert response.json()['rated_capacity'] == 1.11
        assert response.json()['floating'] == True
        assert response.json()['max_installation_water_depth'] == -10
        assert response.json()['min_installation_water_depth'] == -100
        assert response.json()['min_interdistance_x'] == 100
        assert response.json()['min_interdistance_y'] == 50


def test_mc_metrics_dimensions(client):

    pact.given('mc 1 exists and it has dimensions') \
        .upon_receiving('a request for dimensions') \
        .with_request('GET', Term(r'/mc/\d+/dimensions', '/mc/1/dimensions')) \
        .will_respond_with(200, body=Like({'characteristic_dimension': 10.5,
                            'hub_heigth': 15,
                            }))

    with pact:
        response = requests.get(f'{pact.uri}/mc/1/dimensions')
        assert response.json()['characteristic_dimension'] == 10.5
        assert response.json()['hub_heigth'] == 15

def test_mc_metrics_model_wec1(client):

    pact.given('mc 1 exists and it has model wec1') \
        .upon_receiving('a request for model wec1') \
        .with_request('GET', Term(r'/mc/\d+/model/wec/complexity1', '/mc/1/model/wec/complexity1')) \
        .will_respond_with(200, body=Like({'capture_width_ratio': 0.3, 'machine_archetype': 'point_absorber'}))

    with pact:
        response = requests.get(f'{pact.uri}/mc/1/model/wec/complexity1')
        assert response.json()['capture_width_ratio'] == 0.3
        assert response.json()['machine_archetype'] == 'point_absorber'

# def test_mc_metrics_model_wec2(client):

#     pact.given('mc 1 exists and has model wec2') \
#         .upon_receiving('a request for model wec2') \
#         .with_request('GET', Term(r'/mc/\d+/model/wec/complexity2', '/mc/1/model/wec/complexity2')) \
#         .will_respond_with(200, body=Like({"capture_width_ratio": [[[0.37]]], 
#                             "hs_capture_width": [1], 
#                             "tp_capture_width": [4.5], 
#                             "wave_angle_capture_width": [0], 
#                             "machine_archetype": 'point_absorber', 
#                             "pto_damping": 1000000}))

#     with pact:
#         response = requests.get(f'{pact.uri}/mc/1/model/wec/complexity2')
#         assert response.json()['capture_width_ratio'] == [[[0.37]]]
#         assert response.json()['hs_capture_width'] == [1]
#         assert response.json()['tp_capture_width'] == [4.5]
#         assert response.json()['wave_angle_capture_width'] == [0]
#         assert response.json()['machine_archetype'] == 'point_absorber'
#         assert response.json()['pto_damping'] == 1000000

# def test_mc_metrics_model_wec3(client):

#     pact.given('mc 1 exists and has model wec3') \
#         .upon_receiving('a request for model wec3') \
#         .with_request('GET', Term(r'/mc/\d+/model/wec/complexity3', '/mc/1/model/wec/complexity3')) \
#         .will_respond_with(200, body=Like(WEC3_MODEL_INPUTS))

#     with pact:
#         response = requests.get(f'{pact.uri}/mc/1/model/wec/complexity3')
#         assert response.json() == WEC3_MODEL_INPUTS

# def test_mc_metrics_model_tec1(client):

#     pact.given('mc 1 exists and has model tec1') \
#         .upon_receiving('a request for model tec1') \
#         .with_request('GET', Term(r'/mc/\d+/model/tec/complexity1', '/mc/1/model/tec/complexity1')) \
#         .will_respond_with(200, body=Like({'cp': 0.3, 'number_rotor': 1}))

#     with pact:
#         response = requests.get(f'{pact.uri}/mc/1/model/tec/complexity1')
#         assert response.json()['cp'] == 0.3
#         assert response.json()['number_rotor'] == 1

# def test_mc_metrics_model_tec2(client):

#     pact.given('mc 1 exists and has model tec2') \
#         .upon_receiving('a request for model tec2') \
#         .with_request('GET', Term(r'/mc/\d+/model/tec/complexity2', '/mc/1/model/tec/complexity2')) \
#         .will_respond_with(200, body=Like({"cp": 0.37, 
#                             "ct": 0.43, 
#                             "cut_in_velocity": 0.5, 
#                             "cut_out_velocity": 3, 
#                             "number_rotor": 2, 
#                             "rotor_interdistance": 10}))

#     with pact:
#         response = requests.get(f'{pact.uri}/mc/1/model/tec/complexity2')
#         assert response.json()['cp'] == 0.37
#         assert response.json()['ct'] == 0.43
#         assert response.json()['cut_in_velocity'] == 0.5
#         assert response.json()['cut_out_velocity'] == 3
#         assert response.json()['number_rotor'] == 2
#         assert response.json()['rotor_interdistance'] == 10

# def test_mc_metrics_model_tec3(client):

#     pact.given('mc 1 exists and has model tec3') \
#         .upon_receiving('a request for model tec3') \
#         .with_request('GET', Term(r'/mc/\d+/model/tec/complexity3', '/mc/1/model/tec/complexity3')) \
#         .will_respond_with(200, body=Like({"cp": [0.37,0.37], 
#                             "ct": [0.43,0.43], 
#                             "cp_ct_velocity": [0,1],
#                             "cut_in_velocity": 0.5, 
#                             "cut_out_velocity": 3, 
#                             "number_rotor": 2, 
#                             "rotor_interdistance": 10}))

#     with pact:
#         response = requests.get(f'{pact.uri}/mc/1/model/tec/complexity3')
#         assert response.json()['cp'] == [0.37,0.37]
#         assert response.json()['ct'] == [0.43,0.43]
#         assert response.json()['cp_ct_velocity'] == [0,1]
#         assert response.json()['cut_in_velocity'] == 0.5
#         assert response.json()['cut_out_velocity'] == 3
#         assert response.json()['number_rotor'] == 2
#         assert response.json()['rotor_interdistance'] == 10

