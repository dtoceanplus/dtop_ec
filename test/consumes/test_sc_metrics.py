import atexit

import requests
from pact import Consumer, Like, Provider, Term

pact = Consumer('ec').has_pact_with(Provider('sc'), port=1235)
pact.start_service()
atexit.register(pact.stop_service)


def test_sc_metrics_leasearea(client):
    """testing the site fro all the cases: lease area::all"""
    pact.given('sc 1 exists and it has farm info') \
        .upon_receiving('a request for farm info') \
        .with_request('GET', Term(r'/sc/\d+/farm/info', '/sc/1/farm/info')) \
        .will_respond_with(200, body=Like({'latitude': [0], 'longitude': [0]}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/farm/info')
        assert response.json()['latitude'] == [0]
        assert response.json()['longitude'] == [0]


def test_sc_metrics_tec3(client):
    """testing the site data for the tidal case at complexity 3: velocity, TI, SSH and Manning fields::tec3"""
    pact.given('sc 1 exists and it has farm scenarii matrices currents') \
        .upon_receiving('a request for farm scenarii matrices currents') \
        .with_request('GET', Term(r'/sc/\d+/farm/scenarii_matrices/currents', '/sc/1/farm/scenarii_matrices/currents')) \
        .will_respond_with(200, body=Like({
                            'x': [0], 
                            'y': [0],
                            'U': [[[1]]],
                            'V': [[[-2]]],
                            'id': [0],
                            'p': [1],
                            'SSH': [[[0.5]]],
                            'TI': [[[0.3]]]}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/farm/scenarii_matrices/currents')
        assert response.json()['x'] == [0]
        assert response.json()['y'] == [0]
        assert response.json()['U'] == [[[1]]]
        assert response.json()['V'] == [[[-2]]]
        assert response.json()['id'] == [0]
        assert response.json()['p'] == [1]
        assert response.json()['SSH'] == [[[0.5]]]
        assert response.json()['TI'] == [[[0.3]]]


def test_sc_metrics_bathymetry_point(client):
    """testing the site data bathymetry at a specific point:tec2"""
    pact.given('sc 1 exists and it has point direct_values bathymetry') \
        .upon_receiving('a request for point direct_values bathymetry') \
        .with_request('GET', Term(r'/sc/\d+/point/direct_values/bathymetry', '/sc/1/point/direct_values/bathymetry')) \
        .will_respond_with(200, body=Like({'value': 100}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/point/direct_values/bathymetry')
        assert response.json()['value'] == 100


def test_sc_metrics_bathymetry(client):
    """testing the site data bathymetry on the lease area:tec3,wec3"""
    pact.given('sc 1 exists and it has farm direct_values bathymetry') \
        .upon_receiving('a request for farm direct_values bathymetry') \
        .with_request('GET', Term(r'/sc/\d+/farm/direct_values/bathymetry', '/sc/1/farm/direct_values/bathymetry')) \
        .will_respond_with(200, body=Like({'value': [100],'latitude': [0],'longitude': [0]}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/farm/direct_values/bathymetry')
        assert response.json()['value'] == [100]
        assert response.json()['latitude'] == [0]
        assert response.json()['longitude'] == [0]


def test_sc_metrics_roughness_length(client):
    """testing the site data roughness length on the lease area:tec3"""
    pact.given('sc 1 exists and it has farm direct_values roughness length ') \
        .upon_receiving('a request for farm direct_values roughness length ') \
        .with_request('GET', Term(r'/sc/\d+/farm/direct_values/roughness_length', '/sc/1/farm/direct_values/roughness_length')) \
        .will_respond_with(200, body=Like({'value': [0.25],'latitude': [0],'longitude': [0]}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/farm/direct_values/roughness_length')
        assert response.json()['value'] == [0.25]
        assert response.json()['latitude'] == [0]
        assert response.json()['longitude'] == [0]

def test_sc_metrics_wave_basic(client):
    """testing the site data wave energy flux, gamma and spreading:wec1, wec3"""
    pact.given('sc 1 exists and it has point statistic waves basic') \
        .upon_receiving('a request for point statistic waves basic') \
        .with_request('GET', Term(r'/sc/\d+/point/statistics/waves/Basic', '/sc/1/point/statistics/waves/Basic')) \
        .will_respond_with(200, body=Like({'CgE': {'mean': 0.298}, 'gamma': {'mean': 3.3}, 'spr': {'mean': 59}}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/point/statistics/waves/Basic')
        assert response.json()['CgE']['mean'] == 0.298
        assert response.json()['gamma']['mean'] == 3.3
        assert response.json()['spr']['mean'] == 59

def test_sc_metrics_tidal_flux(client):
    """testing the site data tidal energy flux:tec1"""
    pact.given('sc 1 exists and it has point statistic currents basic mag') \
        .upon_receiving('a request for point statistic currents basic mag') \
        .with_request('GET', Term(r'/sc/\d+/point/statistics/currents/Basic/mag', '/sc/1/point/statistics/currents/Basic/mag')) \
        .will_respond_with(200, body=Like({'mean': 0.298}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/point/statistics/currents/Basic/mag')
        assert response.json()['mean'] == 0.298

def test_sc_metrics_tidal_velocity(client):
    """testing the site data tidal energy flux:tec2"""
    pact.given('sc 1 exists and it has point statistic currents EJPD mag_theta') \
        .upon_receiving('a request for point statistic currents EJPD mag_theta') \
        .with_request('GET', Term(r'/sc/\d+/point/statistics/currents/EJPD/mag_theta', '/sc/1/point/statistics/currents/EJPD/mag_theta')) \
        .will_respond_with(200, body=Like({'bins1': [1.298], 'bins2': [0], 'pdf': [[1]]}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/point/statistics/currents/EJPD/mag_theta')
        assert response.json()['bins1'] == [1.298]
        assert response.json()['bins2'] == [0]
        assert response.json()['pdf'] == [[1]]

def test_sc_metrics_wave_EJPD(client):
    """testing the site data wave scatter diagram:wec3, wec2"""
    pact.given('sc 1 exists and it has point statistic waves EJPD3v') \
        .upon_receiving('a request for point statistic waves EJPD3v') \
        .with_request('GET', Term(r'/sc/\d+/point/statistics/waves/EJPD3v/hs_tp_dp_flattened', '/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_flattened')) \
        .will_respond_with(200, body=Like({'bins1': ["0-0.5"], 'bins2': ["1-2"], 'bins3': ["0-90"], 'pdf': [1], 'id': [0]}))

    with pact:
        response = requests.get(f'{pact.uri}/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_flattened')
        assert response.json()['bins1'] == ["0-0.5"]
        assert response.json()['bins2'] == ["1-2"]
        assert response.json()['bins3'] == ["0-90"]
        assert response.json()['id'] == [0]
        assert response.json()['pdf'] == [1]
