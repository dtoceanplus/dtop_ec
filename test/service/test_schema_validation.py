# from cerberus import Validator
# import numpy as np

# class CustomValidator(Validator):
#     def _validate_array(self, array, field, value):
#         """ Test the oddity of a value.

#         The rule's arguments are validated against this schema:
#         """
#         try:
#             ## validate the schema first
#             depth = array.get('depth', None)
#             shape = array.get('shape', None)
#             constraint = array.get('constraint', None)
#             if depth is None:
#                 raise KeyError('Schema definition error: missing array key "depth"')
#             if shape is None:
#                 raise KeyError('Schema definition error: missing array key "shape"')
#             if constraint is None:
#                 raise KeyError('Schema definition error: missing array key "constraint"')
            
#             if not len(shape) == depth:
#                 raise ValueError(f'Schema definition error: Shape key must have length {depth}')
#             if not len(constraint) == depth:
#                 raise ValueError(f'Schema definition error: Constraint key must have length {depth}')

#             value_shape = np.shape(value)
#             if not len(value_shape) == depth:
#                 self._error(field, f"Must have depth of {depth}")
#             else:
#                 for i in range(depth):
#                     if not eval(f'{value_shape[i]}{constraint[i]}{shape[i]}'):
#                         self._error(field, f"Must have shape at index [{i}]: {value_shape[i]}{constraint[i]}{shape[i]}")

#         except Exception as e:
#             self._error(field, f"Unexpected error: {str(e)}")
#         return True  

# class SiteSchemaValidator():
#     tec1 = {"loc_resource": {'type': 'number', 'required': True},
#             "loc_position": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [3, 2],
#                                        'constraint': ['>=', '==']}, 'required': True}}
#     tec2 = {"bathymetry": {'type': 'number', 'required': True},
#             "loc_position": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [3, 2],
#                                        'constraint': ['>=', '==']}, 'required': True},
#             "velocity_field": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [2],
#                                        'constraint': ['==']}, 'required': True}}
#     tec3 = {"loc_position": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [3, 2],
#                                        'constraint': ['>=', '==']}, 'required': True},
#             "probability":  {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             "TI":  {'array': {'type': 'number',
#                                        'depth': 3,
#                                        'shape': [1,1,1],
#                                        'constraint': ['>=','>=','>=']}, 'required': True},
#             "U":  {'array': {'type': 'number',
#                                        'depth': 3,
#                                        'shape': [1,1,1],
#                                        'constraint': ['>=','>=','>=']}, 'required': True},
#             "V":  {'array': {'type': 'number',
#                                        'depth': 3,
#                                        'shape': [1,1,1],
#                                        'constraint': ['>=','>=','>=']}, 'required': True},
#             "SSH":  {'array': {'type': 'number',
#                                        'depth': 3,
#                                        'shape': [1,1,1],
#                                        'constraint': ['>=','>=','>=']}, 'required': True},
#             "power_law_exponent":  {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [1,1],
#                                        'constraint': ['>=','>=']}, 'required': True},
#             "soil_characteristic":  {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [1,1],
#                                        'constraint': ['>=','>=']}, 'required': True},
#             "bathymetry":  {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [1,1],
#                                        'constraint': ['>=','>=']}, 'required': True},
#             "X":  {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [2],
#                                        'constraint': ['>=']}, 'required': True},
#             "Y":  {'array': {'type': 'number',
#                              'depth': 1,
#                              'shape': [2],
#                              'constraint': ['>=']}, 'required': True},
#             "blockage_ratio":  {'type': 'number', 'required': True},
#             }
#     wec1 = {"loc_resource": {'type': 'number', 'required': True},
#             "loc_position": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [3, 2],
#                                        'constraint': ['>=', '==']}, 'required': True}}
#     wec2 = {"loc_position": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [3, 2],
#                                        'constraint': ['>=', '==']}, 'required': True},
#             "scatter_diagram": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             "hs": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             "tp": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True}}
#     wec3 = {"loc_position": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [3, 2],
#                                        'constraint': ['>=', '==']}, 'required': True},
#             "scatter_diagram": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             "sea_state_wave_height": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             "sea_state_wave_period": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             "sea_state_directions": {'array': {'type': 'number',
#                                        'depth': 1,
#                                        'shape': [1],
#                                        'constraint': ['>=']}, 'required': True},
#             'bathymetry': {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [1, 1],
#                                        'constraint': ['>=', '==']}, 'required': True},                      
#             "nogo_areas": {'array': {'type': 'number',
#                                        'depth': 2,
#                                        'shape': [0, 0],
#                                        'constraint': ['>=', '>=']}, 'required': True},
#             "spectral_type": {'type': 'dict', 'schema': {'model': {'type': 'string'},
#                                                          'gamma': {'type': 'number'},
#                                                          'directional_spreading': {'type': 'number'}}}                  
                                       
#             }
#     def __init__(self, machine, complexity, data):
#         schema = getattr(self, f"{machine.lower()}{complexity}")
#         v = CustomValidator(schema)
        
#         self.valid = v.validate(data)
#         self.errors = v.errors

#         if not self.valid:
#             raise Validation 
#         if machine.lower() == 'tec' and complexity == 3:
#             schema['PLE'] = schema.pop('power_law_exponent')
#             schema['BR'] = schema.pop('blockage_ratio')
#             schema['bathy'] = schema.pop('bathymetry')
#             schema['lease'] = schema.pop('loc_position')
#             schema['geophy'] = schema.pop('soil_characteristic')
#         elif machine.lower() == 'wec' and complexity == 3:
#             spec_type = schema.pop('spectral_type')
#             schema["spectral_type"] = (spec_type['model'], spec_type['gamma'], 
#                                     spec_type['directional_spreading'])
#             schema['scat_diagram'] = schema.pop('scatter_diagram')
#             schema['lease'] = schema.pop('loc_position')
#             schema['water_depth'] = np.array(schema.get('bathymetry')).mean()
#         self.schema = schema
    
#     @staticmethod
#     def serialize(machine, complexity, schema):
#         if machine.lower() == 'tec' and complexity == 3:
#             schema['power_law_exponent'] = schema.pop('PLE')
#             schema['blockage_ratio'] = schema.pop('BR')
#             schema['bathymetry'] = schema.pop('bathy')
#             schema['loc_position'] = schema.pop('lease')
#             schema['soil_characteristic'] = schema.pop('geophy')
#         elif machine.lower() == 'wec' and complexity == 3:
#             spec_type = schema.pop('spectral_type')
#             schema["spectral_type"] = {'model': spec_type[0],
#                                        'gamma': spec_type[1],
#                                        'directional_spreading': spec_type[2]}
#             schema['scatter_diagram'] = schema.pop('scat_diagram')
#             schema['loc_position'] = schema.pop('lease')
#             schema.pop('water_depth')
#         return schema



# site = SiteSchemaValidator('TEC', 1, {'loc_resource': 10, 'loc_position': [[0,0],[0,10],[10,10]]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('TEC', 1, {'loc_resource': 10, 'loc_position': [[0,0],[0,10]]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('TEC', 2, {'loc_position': [[0,0],[0,10],[10,10]], 'bathymetry': 10, 'velocity_field': [0,1]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('TEC', 3, {'loc_position': [[0,0],[0,10],[10,10]], 'bathymetry': [[10]], 'blockage_ratio': 1,
#         'TI': [[[0.3]]], 'U': [[[1]]], 'V': [[[0]]], 'X': [0,10], 'Y': [0,10], 'SSH': [[[1]]], 'probability': [1], 'soil_characteristic': [[10]],
#         'power_law_exponent': [[0.7]]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('WEC', 1, {'loc_resource': 10, 'loc_position': [[0,0],[0,10],[10,10]]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('WEC', 1, {'loc_resource': 10, 'loc_position': [[0,0],[0,10]]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('WEC', 2, {'loc_position': [[0,0],[0,10],[10,10]], 'hs': [0.5], 'tp': [4.5], 'scatter_diagram': [1.0]})
# print(site.valid)
# print(site.errors)

# site = SiteSchemaValidator('WEC', 3, {'loc_position': [[0,0],[0,10],[10,10]], 'bathymetry': [[10]],
#         'sea_state_wave_height': [0.5], 'sea_state_wave_period': [4.5], 'sea_state_directions': [4.5],
#         'scatter_diagram': [1.0], 'nogo_areas': [[]], 'spectral_type': {'model': 'JONSWAP', 'gamma': 3.3, 'directional_spreading': 0}})
# print(site.valid)
# print(site.errors)