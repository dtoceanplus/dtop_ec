# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest
import json

from dtop_energycapt import service

from utils_data import *

project_body = {
    "title": "RM3 North Sea",
    "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
    "complexity": 1,
    "type": "TEC",
    "date": "2019-11-14",
    "status": 80,
    "tags": "RM3-NS",
}

farm_inputs = {
    "nb_devices": 2,
    "layout_type": "verification",
    "array_layout": "[[0,0],[100,100]]",
    "optimisation_threshold": None,
    "opt_method": None,
    "array_generic_type": None,
    "notes": "pytest farm inputs note"
}

site_inputs = {
    "site_url": "/sc/1/site_conditions"
}

machine_inputs = {
    "machine_url": "/mc/1/hydrodynamic"
}

farm_body = {
    "name": "",
    "number-devices": 1,
    "layout": 1,
    "q-factor": 0.98,
    "aep": 100001,
    "captured-power": 100002,
    "captured-power-per-condition": {"siteConditionID": [0], "capturedPower": [0]},
    "resource-reduction": 0.95,
}


@pytest.fixture
def app():
    settings_override = {
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": "sqlite://",  #  https://docs.sqlalchemy.org/en/13/core/engines.html#sqlite
    }
    app = service.create_app(settings_override)
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()

    yield client

def test_projects(client, init_db):
    response = client.get('/ec')
    
    if response.status_code == 404:
        response = client.post('/ec', json=project_body)
        assert response.status_code == 201
        response = client.get('/ec')
    projects = json.loads(response.data)

    project = projects[0]
    assert type(project) is dict
    assert 'type' in project.keys()   
        

# def test_core(client):
#     response = client.get('/routes')
#     assert b'ec' in response.data


@pytest.fixture(scope="function")
def init_db(app):
    from dtop_energycapt.service import db

    app_ctx = app.app_context()
    app_ctx.push()
    db.init_db()

    yield db

def test_farm_post_project_created(client, init_db):
    response = client.get('/ec')
    if response.status_code == 404:
        response = client.post('/ec', json=project_body)
        response = client.get('/ec')

    assert response.status_code == 200

    # the project has been created if it wasnt
    projects = response.get_json()

    response = client.post('/ec', json=project_body)
    assert response.status_code == 201

    #  A reference to the created resource is expected (see https://restfulapi.net/http-methods/#post)
    assert 'ID' in response.get_json()
    ecId = response.get_json()['ID']

    response = client.get(f'/ec/{ecId}')
    project = response.get_json()
    assert 'type' in project.keys()  #  A one more project is created
    assert project.get('farm') is None  #  The newly created project does not have 'farm'

    response = client.get(f'/ec/{ecId}/farm')
    assert response.status_code == 404

    response = client.get(f'/ec/{ecId}/results')
    assert response.status_code == 404  # cannot fetch the input data

    # add project inputs to the database
    response = client.get(f'/ec/{ecId}/inputs/farm')
    assert response.status_code == 404

    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    assert response.status_code == 201
    response = client.put(f'/ec/{-1}/inputs/farm', json=farm_inputs)

    response = client.get(f'/ec/{ecId}/inputs/farm')
    assert response.status_code == 200
    response = client.get(f'/ec/{-1}/inputs/farm')
    assert response.status_code == 404

    response = client.get(f'/ec/{ecId}/inputs/site/wec/complexity1')
    response = client.get(f'/ec/{-1}/inputs/site/wec/complexity1')

    r = client.put(f'/ec/{ecId}/inputs/site/wec/complexity1', json=body_input_site['tec']['complexity1'])
    r = client.put(f'/ec/{-1}/inputs/site/wec/complexity1', json=body_input_site['tec']['complexity1'])

    response = client.get(f'/ec/{ecId}/inputs/machine/wec/complexity1')
    response = client.get(f'/ec/{-1}/inputs/machine/wec/complexity1')
    r = client.put(f'/ec/{ecId}/inputs/machine/wec/complexity1', json=body_input_machine['tec']['complexity1'])
    r = client.put(f'/ec/{-1}/inputs/machine/wec/complexity1', json=body_input_machine['tec']['complexity1'])

    # TODO: check why the sc and mc cannot be fetched
    # response = client.get(f'/ec/{ecId}/results')
    # print(response.get_json())
    # assert response.status_code == 200  # cannot fetch the input data
    # #  A referens to the created resource is expected (see https://restfulapi.net/http-methods/#post)
    # # assert 'ID' in response.get_json()

    # response = client.get('/ec/1/farm')
    # assert response.status_code == 200

    # assert 'layout' in response.get_json()


@pytest.fixture(scope="function")
def fill_db(init_db):
    init_db.fill_db()

    yield init_db


@pytest.mark.skip(reason="'../../../tests/service/data.sql' file does not exists")
def test_farm_get(client, fill_db):
    response = client.get('/ec/1/farm')
    assert b'layout' in response.data
