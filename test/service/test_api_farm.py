# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest
import json

from dtop_energycapt import service

from utils_data import *

@pytest.fixture(scope="function")
def init_db(app):
    from dtop_energycapt.service import db

    app_ctx = app.app_context()
    app_ctx.push()
    db.init_db()

    yield db

@pytest.fixture(scope="function")
def fill_db(init_db):
    init_db.fill_db()

    yield init_db

project_body = {
    "title": "RM3 North Sea",
    "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
    "complexity": 1,
    "type": "TEC",
    "date": "2019-11-14",
    "status": 80,
    "tags": "RM3-NS",
}


project_body1 = {
    "title": "RM3 Atlantic",
    "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
    "complexity": 1,
    "type": "TEC",
    "date": "2020-04-14",
    "status": 80,
    "tags": "RM3-At",
}


farm_01 = {
    "name": "",
     'number_devices': 3, 
     'q_factor': 1.0,
    "layout": 1,
      'aep': 39893601913.35437, 
      'captured_power': 4554064.145360088,
       'captured_power_per_condition': {'siteConditionID': [0], 'capturedPower': [4554064.145360088]},
        'resource_reduction': [0.017086628850431995]}


farm_inputs = {
    "nb_devices": 2,
    "layout_type": "verification",
    "array_layout": "[[0,0],[100,100]]",
    "optimisation_threshold": None,
    "opt_method": None,
    "array_generic_type": None,
    "notes": "pytest farm inputs note"
}

site_inputs = {
    "site_url": "/sc/1/site_conditions"
}

machine_inputs = {
    "machine_url": "/mc/1/hydrodynamic"
}

farm_body = {
  "layout": {
    "deviceID": [0],
    "easting": [0],
    "northing": [0]
  },
  "number_devices": 1,
  "q_factor": 0.98,
  "aep": 100001,
  "captured_power": 100002,
  "captured_power_per_condition": {
    "siteConditionID": [0,1],
    "capturedPower": [10001,10001]
  },
  "resource_reduction": 0.97
}


def test_farm_output(client, init_db):
    response = client.get('/ec')
    projects = response.get_json()
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.get(f'/ec/{ecId}/farm')
    assert response.status_code == 404
    response = client.post(f'/ec/{ecId}/farm', json=farm_body)
    assert response.status_code == 201
    response = client.put(f'/ec/{ecId}/farm', json=farm_body)
    assert response.status_code == 200
    


