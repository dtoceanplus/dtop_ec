# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest
import json

from dtop_energycapt import service

from utils_data import *

@pytest.fixture
def project_data():
    return {
        "title": "RM3 North Sea",
        "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
        "complexity": 1,
        "type": "TEC",
        "date": "2019-11-14",
        "status": 80,
        "tags": "RM3-NS",
    }

# site_inputs = {"url": "http://sc/sc",  "entityID": 1}
site_inputs = {"url": "http://sc.dto.test",  "entityID": 1}
machine_inputs = {"url": "http://mc.dto.test",  "entityID": 1}


def test_fetch_site_data_tec_1(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/site/fetch', json=site_inputs)

    assert 'loc_position' in response.get_json()  # general data assert
    assert 'loc_resource' in response.get_json()  # tec1 specific data

def test_fetch_site_data_tec_2(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['complexity'] = 2
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/site/fetch', json=site_inputs)

    assert 'loc_position' in response.get_json()  # general data assert
    assert 'bathymetry' in response.get_json()  # tec2 specific data
    assert 'velocity_field' in response.get_json()  # tec2 specific data

def test_fetch_site_data_tec_3(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['complexity'] = 3
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/site/fetch', json=site_inputs)

    assert 'lease' in response.get_json()  # general data assert
    assert 'bathy' in response.get_json()  # tec2 specific data

def test_fetch_site_data_wec_1(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['type'] = 'WEC'
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/site/fetch', json=site_inputs)
    assert 'loc_position' in response.get_json()  # general data assert
    assert 'loc_resource' in response.get_json()  # wec1 specific data

def test_fetch_site_data_wec_2(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['type'] = 'WEC'
        project_data['complexity'] = 2
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/site/fetch', json=site_inputs)
    assert 'loc_position' in response.get_json()  # general data assert
    assert 'scatter_diagram' in response.get_json()  # wec2 specific data

def test_fetch_site_data_wec_3(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['type'] = 'WEC'
        project_data['complexity'] = 3
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/site/fetch', json=site_inputs)
    assert 'lease' in response.get_json()  # general data assert
    assert 'scat_diagram' in response.get_json()  # wec2 specific data
    assert 'spectral_type' in response.get_json()  # wec2 specific data
    

def test_fetch_machine_data_tec_1(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/machine/fetch', json=machine_inputs)
    assert 'rated_pow_device' in response.get_json()  # general data assert


def test_fetch_machine_data_tec_2(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['complexity'] = 2
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/machine/fetch', json=machine_inputs)
    assert 'rated_pow_device' in response.get_json()  # general data assert

def test_fetch_machine_data_tec_3(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['complexity'] = 3
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/machine/fetch', json=machine_inputs)
    assert 'rated_pow_device' in response.get_json()  # general data assert

def test_fetch_machine_data_wec_1(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['type'] = 'WEC'
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/machine/fetch', json=machine_inputs)
    assert 'rated_pow_device' in response.get_json()  # general data assert


def test_fetch_machine_data_wec_2(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['type'] = 'WEC'
        project_data['complexity'] = 2
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/machine/fetch', json=machine_inputs)
    assert 'rated_pow_device' in response.get_json()  # general data assert

def test_fetch_machine_data_wec_3(client, project_data):
    response = client.get('/ec')
    if response.status_code == 404:
        project_data['type'] = 'WEC'
        project_data['complexity'] = 3
        response = client.post('/ec', json=project_data)
        response = client.get('/ec')
    assert 'id' in response.get_json()[0]
    ecId = response.get_json()[0]['id']
    response = client.post(f'/ec/{ecId}/inputs/machine/fetch', json=machine_inputs)
    assert 'rated_power_device' in response.get_json()  # general data assert

