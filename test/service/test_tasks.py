# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

from dtop_energycapt import service
from dtop_energycapt import business

# device = {
#     'main_dim_device': 10,
#     'sec_dim_device': 20,
#     'rotor diameter': 10,
#     'hub height': 20,
#     'floating': False,
#     'cut_IO': [1, 10],
#     'cp': 0.3,
#     'ct': 1,
#     'alpha': 0.07,
#     'rated_pow_device': 5e6,
# }

velocity = ((0, 5), )

name = 'testes'
ecID = 4
# body = {
#     'device': tec2,
#     'main_dim_device': 10,
#     'sec_dim_device': 20,
#     # 'layout': layout.tolist(),
#     'velocity_field': velocity,
#     'loc_resource': 10,
#     'nb_devices': 42,
#     'array_generic_type': 'staggered',
#     'loc_position': [[0, 0], [500, 500], [0, 500]],
#     'rated_pow_device': 5e6,
#     'rotor_diameter': 10,
#     'hub_height': 20,
#     'floating': False,
#     'cut_IO': [1, 10],
#     'cp': 0.3,
#     'ct': 1,
#     'alpha': 0.07,
#     'optimisation_threshold': 0.8,
#     'opt_method': 'meta_model',
# }

def test_optim3(body):
    body['velocity_field'] = velocity
    arr = business.ArrayFactory.get_array('TEC', "1")
    body['opt_method'] = 'brute_force'
    body['layout_type'] = 'optimization'

    arr.get_inputs(name, ecID, **body)

    from pickle import dumps, loads
    loads(dumps(arr.verify_farm_layout))


def test_task(client):

    response = client.get('/ec/tasks/10')
    response = client.post(f'/ec', json=project_local)
    response = client.put(f'/ec/1/inputs/farm', json=inputs_farm_local)
    response = client.put(f'/ec/1/inputs/site/tec/complexity1', json=inputs_site_local)
    response = client.put(f'/ec/1/inputs/machine/tec/complexity1', json=inputs_machine_local)

    response = client.post(f'/ec/tasks', json={"id": 1})

    assert response.json == {'data': {'task_id': 1}, 'status': 'success'}

    

project_local = {
    "title": "RM3 North Sea test",
    "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
    "complexity": 1,
    "type": "TEC",
    "date": "2019-11-14",
    "status": 80,
    "tags": "RM3-NS"
}

inputs_farm_local = {
    "nb_devices": 1,
    "layout_type": "",
    "opt_method": "",
    "optimisation_threshold": None,
    "array_layout": [[0,0]],
    "notes": "",
    "array_generic_type": ""
}

inputs_site_local = {
    "loc_resource": 3,
    "loc_position": [[0, 0],[500,0],[500,500],[0,500]]
}

inputs_machine_local = {
  "cp": 0.3,
  "main_dim_device": 10,
  "sec_dim_device": 15,
  "rated_pow_device": 500,
  "number_rotor": 1
}