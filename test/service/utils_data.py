body = {
    "title": "Test array",
    "desc":
    "Simple project to test potential of Wave Energy Converter in the North Sea",
    "complexity": 1,
    "type": "WEC",
    "date": "27-12-2019",
    "status": 20,
    "tags": "wave"
}

body_input_farm = {
    "orientation_angle": 0,
    "minimum_interdistance": [
        10,
        10
    ],
    "nb_devices": 2,
    "layout_type": "verification",
    "opt_method": "",
    "optimisation_threshold": None,
    "array_layout": [[0,0],[100,100]],
    "notes": "",
    "array_generic_type": ""
}

body_input_machine= {
    "wec": {
        "complexity1": {
                        "device_capture_width_ratio": 0.3,
                        "main_dim_device": 10.0,
                        "sec_dim_device": 10.0,
                        "rated_pow_device": 300.0,
                        "machine_archetype": "point_absorber"
                        },
        "complexity2": {
                        "device_capture_width_ratio": [[[0.3,0.4]]],
                        "tp_capture_width": [4,5],
                        "hs_capture_width": [3],
                        "wave_angle_capture_width": [0],
                        "main_dim_device": 10,
                        "sec_dim_device": 10,
                        "rated_pow_device": 300,
                        "machine_archetype": "point_absorber"
                        },
        "complexity3": {
                        "pto_damping": [[[[[1e6]]]]],
                        "mooring_stiffness": [[[[[2e6]]]]],
                        "additional_damping": [[[[[1e6]]]]],
                        "additional_stiffness": [[[[[1e6]]]]],
                        "fitting_damping": [[[[[1e6]]]]],
                        "fitting_stiffness": [[[[[1e6]]]]],
                        "mass_matrix": [[1e6]],
                        "added_mass": [[[0]]],
                        "radiation_damping": [[[5e6]]],
                        "excitation_force_real": [[[1e3]]],
                        "excitation_force_imag": [[[2e3]]],
                        "hydrosatic_stiffness": [[1e6]],
                        "diffraction_transfer_matrix_real": [[[1e3]]],
                        "diffraction_transfer_matrix_imag": [[[1e3]]],
                        "force_transfer_matrix_real": [[[1e3]]],
                        "force_transfer_matrix_imag": [[[1e3]]],
                        "radiation_amplitude_coefficients_real": [[[1e3]]],
                        "radiation_amplitude_coefficients_imag": [[[1e3]]],
                        "inscribing_cylinder_radius": 30,
                        "modes": [[1e3]],
                        "order": [0],
                        "truncation_order": [[0]],
                        "heading_angle_span": 30,
                        "water_depth": 50,
                        "minimum_interdistance": [10,10],
                        "installation_depth": [0, 100],
                        "directions": [0],
                        "frequency": [1,2,3],
                        "dir_pm": [0],
                        "hs_pm": [0.5, 1.5, 2.5],
                        "tp_pm": [2, 3, 4],
                        "scatter_diagram": [[[1.0]]],
                        "power_matrix": [[[1.0]]],
                        "rated_power_device": 300.0
                        },
    },
    "tec": {
        "complexity1": {
                        "cp": 0.3,
                        "main_dim_device": 10,
                        "sec_dim_device": 15,
                        "rated_pow_device": 500
                        },
        "complexity2": {
                        "rotor_diameter": 10,
                        "hub_height": 20,
                        "floating": True,
                        "cut_in_velocity": 1,
                        "cut_out_velocity": 10,
                        "cp": 0.3,
                        "ct": 1,
                        "rated_pow_device": 500,
                        "number_rotor": 1,
                        "rotor_interdistance": 0
                        },
        "complexity3": {
                        "rotor_diameter": 10,
                        "hub_height": 20,
                        "floating": True,
                        "cut_in_velocity": 2,
                        "cut_out_velocity": 5,
                        "cp": [0.3,0.8],
                        "ct": [0.3,0.3],
                        "cp_ct_velocity": [2,5],
                        "rated_pow_device": 500,
                        "number_rotor": 1,
                        "rotor_interdistance": 0,
                        "heading_angle_span": 0,
                        "bidirectional": True
                        }
    }
}

body_input_site= {
    "wec": {
        "complexity1": {"loc_resource": 64000,
                        "loc_position": [[0,0],[500,500],[0,500]]},
        "complexity2": {
                        "loc_position": [[0,0],[500,500],[0,500]],
                        "scatter_diagram": [[1]],
                        "hs": [2.5],
                        "tp": [5.3]},
        "complexity3": {
                        "loc_position": [[0,0],[500,500],[0,500]],
                        "scatter_diagram": [[[1]]],
                        "sea_state_directions": [0],
                        "sea_state_wave_height": [ 2.5],
                        "sea_state_wave_period": [5.3],
                        "bathymetry": [[50]],
                        "nogo_areas": [[]],
                        "spectral_type": {
                            "model": "Jonswap",
                            "gamma": 3.3,
                            "directional_spreading": 0
                        }},
    },
    "tec": {
        "complexity1": {"loc_resource": 64000,
                        "loc_position": [[0,0],[500,500],[0,500]]},
        "complexity2": {"velocity_field": [0,3],
                        "bathymetry": 30,
                        "loc_position": [[0,0],[500,500],[0,500]]},
        "complexity3": {
                        "loc_position": [[0,0],[500,500],[0,500]],
                        "TI": [[[0.3]]],
                        "U": [[[3.0]]],
                        "V": [[[0.0]]],
                        "SSH": [[[10.0]]],
                        "probability": [1],
                        "X": [[0]],
                        "Y": [[0]],
                        "bathymetry": [[50]],
                        "loc_position": [[0,0]],
                        "blockage_ratio": 1,
                        "soil_characteristic": [[0.01]],
                        "power_law_exponent": [[0.15]]}
    }
}

body_farm = {
    "number_devices": 2,
    "q_factor": 0.98,
    "aep": 100001,
    "captured_power": 100002,
    "captured_power_per_condition": {
        "siteConditionID": [0],
        "capturedPower": [10001]
    },
    "resource_reduction": 0.97
}

body_device = {
    "northing": 0,
    "easting": 0,
    "q_factor": 0,
    "aep": 0,
    "captured_power": 0,
    "captured_power_per_condition": {
        "siteConditionID": [0],
        "capturedPower": [0]
    }
}

body_inputs = {
    "nb_devices": 0,
    "layout_type": "",
    "opt_method": "",
    "optimisation_threshold": 0,
    "array_layout": "[0,0]",
    "notes": "",
    "array_generic_type": ""
}

