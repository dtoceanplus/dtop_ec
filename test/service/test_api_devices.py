# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest
import json

from dtop_energycapt import service

from utils_data import *

@pytest.fixture(scope="function")
def init_db(app):
    from dtop_energycapt.service import db

    app_ctx = app.app_context()
    app_ctx.push()
    db.init_db()

    yield db

@pytest.fixture(scope="function")
def fill_db(init_db):
    init_db.fill_db()

    yield init_db

project_body = {
    "title": "RM3 North Sea",
    "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
    "complexity": 1,
    "type": "TEC",
    "date": "2019-11-14",
    "status": 80,
    "tags": "RM3-NS",
}


project_body1 = {
    "title": "RM3 Atlantic",
    "desc": "A EC design for project {projectId}, with a Machine RM3 and at the site Norht Sea",
    "complexity": 1,
    "type": "TEC",
    "date": "2020-04-14",
    "status": 80,
    "tags": "RM3-At",
}


farm_01 = {
    "name": "",
     'number_devices': 3, 
     'q_factor': 1.0,
    "layout": 1,
      'aep': 39893601913.35437, 
      'captured_power': 4554064.145360088,
       'captured_power_per_condition': {'siteConditionID': [0], 'capturedPower': [4554064.145360088]},
        'resource_reduction': [0.017086628850431995]}


devices_01 = [ {'easting': 0.0, 
                'northing': 0.0, 
                'q_factor': 1.0, 
                'aep': 13297867304.451456, 
                'captured_power': 1518021.3817866961, 
                'captured_power_per_condition': {'siteConditionID': [0], 'capturedPower': [1518021.3817866961]}
                },
                 {'easting': 100.0, 
                 'northing': 50.0, 
                 'q_factor': 1.0, 
                 'aep': 13297867304.451456, 
                 'captured_power': 1518021.3817866961, 
                 'captured_power_per_condition': {'siteConditionID': [0], 'capturedPower': [1518021.3817866961]}
                 },
                  {'easting': 0.0, 
                  'northing': 100.0, 
                  'q_factor': 1.0, 
                  'aep': 13297867304.451456, 
                  'captured_power': 1518021.3817866961, 
                  'captured_power_per_condition': {'siteConditionID': [0], 'capturedPower': [1518021.3817866961]}
                  }
                  ]


farm_inputs = {
    "nb_devices": 2,
    "layout_type": "verification",
    "array_layout": "[[0,0],[100,100]]",
    "optimisation_threshold": None,
    "opt_method": None,
    "array_generic_type": None,
    "notes": "pytest farm inputs note"
}

site_inputs = {
    "site_url": "/sc/1/site_conditions"
}

machine_inputs = {
    "machine_url": "/mc/1/hydrodynamic"
}

farm_body = {
    "name": "",
    "number-devices": 1,
    "layout": 1,
    "q-factor": 0.98,
    "aep": 100001,
    "captured-power": 100002,
    "captured-power-per-condition": {"siteConditionID": [0], "capturedPower": [0]},
    "resource-reduction": 0.95,
}


device_body1 = {
    "easting": 3000.,
    "northing":  4500., 
    "q-factor": 0.98,
    "aep": 1001,
    "captured-power": 1002,
    "captured-power-per-condition": {"siteConditionID": [1], "capturedPower": [10]},
}


device_body2 = {
    "easting": 3010.,
    "northing":  4550., 
    # "q-factor": 0.98,
    # "aep": 100001,
    # "captured-power": 100002,
    # "captured-power-per-condition": {"siteConditionID": [0], "capturedPower": [0]},
    
}

device_inputs = {
    'project' : 1,
    'farm' : 1,
    "easting": 3000.,
    "northing":  4500., 
    "q-factor": 0.98,
    "aep": 1001,
    "captured-power": 1002,
    "captured-power-per-condition": {"siteConditionID": [1], "capturedPower": [10]},    

}

@pytest.mark.skip(reason="'../../../tests/service/data.sql' file does not exists")
def test_farm_get(client, fill_db):
    response = client.get('/ec/1/farm')
    assert b'layout' in response.data


def test_projects2(client, init_db):
    response = client.get('/ec')
    projects = response.get_json()  
    response = client.post('/ec')
    response = client.post('/ec', json=project_body)
    response = client.get('/ec')
    projects = response.get_json() 
    response = client.post('/ec', json=project_body1)
    ecId = response.get_json()['ID']  
    project_body1['date'] = '2020-04-18'
    response = client.put(f'/ec/{ecId}')
    response = client.put(f'/ec/{ecId}', json=project_body1)
    response = client.get('/ec')
    projects = response.get_json() 

    r = client.get(f'/ec/{ecId+10}')

    r = client.delete(f'/ec/{ecId+10}')
    r = client.delete(f'/ec/{ecId}')

    assert response.status_code == 200



def test_farms1(client, init_db):
    response = client.get('/ec')
    projects = response.get_json()
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.get(f'/ec/{ecId}/devices')
    assert response.status_code == 409
    r = client.get(f'/ec/{ecId}/results')
    assert r.status_code == 404
    response = client.post(f'/ec/{ecId}/farm', json=farm_inputs)
    assert response.status_code == 400
    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    response = client.get(f'/ec/{ecId+1}/farm')
    assert response.status_code == 404
    response = client.get(f'/ec/{ecId}/farm')
    farm_1 = response.get_json()
    farm_1['number_devices'] = 3
    r = client.put(f'/ec/{ecId}/farm', json=farm_1)
    farm_1['number_devices'] = None
    r = client.put(f'/ec/{ecId}/farm', json=farm_1)

    r = client.post(f'/stage1')

    r = client.delete(f'/ec/{ecId}/farm')
    assert r.status_code == 200 
    r = client.delete(f'/ec/{ecId}/farm')
    assert r.status_code == 404 

    r = client.put(f'/ec/{ecId}/farm', json=farm_1)    
    assert r.status_code == 409

    r = client.get(f'/ec/{ecId}/results')
    assert r.status_code == 404


def test_farms2(client, init_db):

    response = client.get('/ec')
    projects = response.get_json()
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    response = client.get(f'/ec/{ecId}/farm')
    farm_1 = response.get_json()
    response = client.get(f'/ec/{ecId}/results')



def test_device1(client, init_db):
    response = client.get('/ec')
    projects = response.get_json()
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.get(f'/ec/{ecId}')
    response = client.post(f'/ec/{ecId}/farm', json=farm_inputs)
    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    # print(response.get_json())
    response = client.get(f'/ec/{ecId}/farm')
    farm_1 = response.get_json()
    response = client.get(f'/ec/{ecId}')
    farm_1['number_devices'] = 1
    response = client.put(f'/ec/{ecId}', json=farm_1)
    # print(response.get_json())
    r = client.post(f'/ec/{ecId}/devices')
    r = client.post(f'/ec/{ecId}/devices', json=device_body1)
    r = client.post(f'/ec/{ecId}/devices', json=device_body2)
    response = client.get(f'/ec/{ecId}')
    r = client.get(f'/ec/{ecId}/devices')
    r = client.get(f'/ec/{ecId}')
    # devices = response.get_json()['devices']
    # devices['date'] = '2020-04-13'
    # r = client.put(f'/ec/{ecId}/devices', json=devices)
    # print(r.get_json())
    r = client.get(f'/ec/{ecId+10}/devices')
    # print(r.get_json())
    r = client.delete(f'/ec/{ecId}')

def test_devices2(client, init_db):
    response = client.get('/ec')    
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    response = client.get(f'/ec/{ecId}/farm')
    response = client.post(f'/ec/{ecId}/farm', json=farm_body)
    response = client.get(f'/ec/{ecId}/devices')
    response = client.post(f'/ec/{ecId}/devices', json=device_inputs)
    response = client.get(f'/ec/{ecId}/devices')

    response = client.post(f'/ec/<id>/upload/inputs/site/tec/complexity1', json=site_inputs)
    r = client.put(f'/ec/{ecId}/inputs/site/wec/complexity1', json=body_input_site['tec']['complexity1'])
    response = client.get(f'/ec/{ecId}/results')
    r = client.delete(f'/ec/{ecId}')

def test_devices3(client, init_db):
    response = client.get('/ec')    
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.post(f'/ec/{ecId}/devices', json=devices_01)
    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    response = client.post(f'/ec/{ecId}/farm', json=farm_01)
    response = client.put(f'/ec/{ecId}/farm', json=farm_01)
    response = client.get(f'/ec/{ecId}/farm')
    farm_02 = response.get_json()
    farm_02['aep'] = 545.
    response = client.put(f'/ec/{ecId}/farm', json=farm_02)

    response = client.get(f'/ec/{ecId}/devices')
    response = client.post(f'/ec/{ecId}/devices', json=devices_01)
    response = client.get(f'/ec/{ecId}/devices')
    r = response.get_json()
    devId = r[-1]['id']
    response = client.get(f'/ec/{ecId}/devices/{devId+10}')
    response = client.get(f'/ec/{ecId}/devices/{devId}')
    response = client.get(f'/ec/{ecId}/results')
    response = client.delete(f'/ec/{ecId}/devices/{devId+10}')
    response = client.delete(f'/ec/{ecId}/devices/{devId}')

    response = client.get(f'/ec/{ecId+100}/results')
    response = client.post(f'/ec/{ecId}/upload/inputs/site/tec/complexity1', json=site_inputs)
    # response = client.get(f'/ec/{ecId}/print')
    # response = client.get(f'/print')
    r = client.delete(f'/ec/{ecId}')

# @pytest.mark.skip(reason="the final names of the entry point has not been defined yet")
def test_machine_assembly(client, init_db):
    response = client.get('/ec')    
    response = client.post('/ec', json=project_body)
    ecId = response.get_json()['ID']
    response = client.put(f'/ec/{ecId}/inputs/farm', json=farm_inputs)
    response = client.post(f'/ec/{ecId}/farm', json=farm_body)
    response = client.post(f'/ec/{ecId}/devices', json=device_inputs)

    machine_body = body_input_machine['tec']['complexity2']
    r = client.put(f'/ec/{ecId}/machine/tec/complexity2', json=machine_body)

    response = client.get(f'/ec/{ecId}/devices')
    print(f'Devices Output: {response.get_json()}')
    assert response.status_code == 200

    response = client.get(f'/ec/{ecId}/rotors')
    print(f'Rotors Output: {response.get_json()}')
    assert response.status_code == 200

    machine_body['number_rotor'] = 2
    r = client.put(f'/ec/{ecId}/inputs/machine/tec/complexity2', json=machine_body)
    assert response.status_code == 200
    assert len(response.get_json()) == 1
    r = client.delete(f'/ec/{ecId}')

        

