import pytest

from dtop_energycapt.service.db import *
from dtop_energycapt.service.schemas import *
from dtop_energycapt.service.models import *


def test_init_db_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_init_db():
        Recorder.called = True

    monkeypatch.setattr('dtop_energycapt.service.db.init_db', fake_init_db)
    result = runner.invoke(args=['init-db'])
    assert 'Initialized' in result.output
    assert Recorder.called

# def test_close_db_command(runner, monkeypatch):
#     class Recorder(object):
#         called = False

#     def fake_init_db():
#         Recorder.called = True

#     monkeypatch.setattr('dtop_energycapt.service.db.close_db', fake_init_db)
#     result = runner.invoke()
   
# def test_load_json_params(app):
#     with app.app_context():
#         get_db()

