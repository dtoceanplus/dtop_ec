import json

import redis
from flask.cli import FlaskGroup
from rq import Connection, Queue, Worker
from rq.registry import FinishedJobRegistry

from dtop_energycapt.service import create_app, models
from dtop_energycapt.service.models import Task, db

import logging

# # manual config
import os
from logging.handlers import RotatingFileHandler
if not os.path.isdir('./logs'):
    os.mkdir('./logs')

from dtop_energycapt.service.logsetup import LOGGING_CONFIG
import logging.config
logging.config.dictConfig(LOGGING_CONFIG)
LOG = logging.getLogger()

LOG.warning('Started calculation worker')

app = create_app()
cli = FlaskGroup(create_app=create_app)
redis_url = app.config["REDIS_URL"]
redis_connection = redis.from_url(redis_url)


@cli.command("run_worker")
def run_worker():
    with Connection(redis_connection):
        worker = Worker(app.config["QUEUES"])
        worker.work()


if __name__ == "__main__":
    cli()
