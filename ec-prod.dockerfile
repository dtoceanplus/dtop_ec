FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt .
COPY setup.py .
COPY manage.py .

COPY src/instance/dtop_tidal_wakes_db_red.msgpack ./wake_database/
COPY src/dtop_energycapt/ ./src/dtop_energycapt/

RUN apt update && \
    apt install --yes --no-install-recommends fort77 gfortran && \
    pip install --requirement requirements.txt && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes fort77 gfortran

ENV FLASK_APP dtop_energycapt.service

EXPOSE 5000
ENV DATABASE_URL sqlite://///db/energycapt.db