# EnergyCapture Overview

Energy Capture Module of the DTOceanPlus project.
Estimation of array interaction for Wave and Tidal Converter Arrays
Update this Francesco!

# Set Up and Start the module using with Docker-Compose
Install [Docker]
[Docker]: https://www.docker.com/products/docker-desktop

> NOTE: make sure to have the .dockerignore file at the project root prior to continue, otherwise we copy the node_modules to the container

Open a terminal or CMD windows and enter the following command:
```bash
cd PROJECT_ROOT
docker-compose -f docker-compose.yml up -d nginx ec mc sc client worker redis
```
PROJECT_ROOT needs to be replaced with the correct path.

The client page can be found at 0.0.0.0 

To run the backend and frontend it is required to build the respective images.
This can be done in two way:

## With make
### backend
```bash
make --file ci.makefile backend-image
```

### client
```bash
make --file ci.makefile frontend-image
```

## Without make
### backend
```bash
docker build --tag dtop:ec-backend --file backend.dockerfile .
```

### client
```bash
cd src/dtop_energycapt/frontend
docker build --tag dtop:ec-frontend --file ci.dockerfile .
```

The frontend behaviour might be unstable since the testing is not completed.

To stop the services enter:
```bash
docker-compose down
```
in the terminal.

### Clean Docker
> NOTE: if any problem is encountered, first try to clean the docker images and volumes 
```bash
docker system prune --volumes
docker container prune
docker image prune -a
docker volume prune
docker network prune
```
The steps above are extracted from [docker-clean]
[docker-clean]: https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/

___
## Using the module
Check that the server is up by navigating to the following address [ec-module](http://127.0.0.1:5000/ec). You should see the following message:
```
{
  "message": "No project found", 
  "status": "error"
}
``` 
If the server cannot be found please go back to the previous section.

In stand alone the module can be run as follow:
1. create a project with the correct machine type and complexity level
2. post the farm inputs
3. upload the json file relative to the site
4. upload the json file relative to the machine
5. submit a task for the backgrond calculation 
6. inspect the results

Refer to the EC API for the data structure.
The site and machine json file should have the following structure:
```json
{
    "complexity": 1,
    "type": "TEC",
    "data": {
        "loc_resource": 3,
        "loc_position": [[0,0],[500,500],[0,500]]
    },
    "url": ""
}
```
The data field contains the actual schema for the resource.
In the example above the site input for a tidal machine at complexity 1 is given.


# Testing

See [test](test) folder.

The [pytest](https://docs.pytest.org/en/latest/) framework makes it easy to write tests.

Install pytest:
```bash
pip install pytest
```

Run tests:
```bash
python -m pytest
```

## Dredd
Build the docker container with a specifc tag
```bash
docker build --tag dredd_ec .
```

Run the built container using the given tag
```bash
docker run dredd_ec
```

## Coverage

Coverage reports about code which should be tested.

Python has tool [Coverage.py](https://coverage.readthedocs.io/)
and PyTest has plugin for it [pytest-cov](https://pytest-cov.readthedocs.io/).

Install pytest-cov plugin
```bash
pip install pytest-cov
```

Run pytest with coverage:
```bash
python -m pytest --cov=dtop_energycapt
```

# Languages

Python has tool [Babel](http://babel.pocoo.org/) that assist in internationalizing and localizing
and Flask has plugin for it [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

See [babel.cfg](babel.cfg) file.

Extracting Text to Translate:
```bash
pybabel extract .
```
It will create `message.pot` file with translations.
This file should not be stored in repository.

Generating a Language Catalog:
```bash
pybabel init -i messages.pot -d src/dtop_energycapt/service/translations -l fr
```
It will create [src/dtop_energycapt/service/translations/fr/LC_MESSAGES/messages.po](src/service/gui/translations/fr/LC_MESSAGES/messages.po) text file.

Use tool [poEdit](https://www.poedit.net/) for catalog (.po file) editing.

Compile Catalog:
```bash
pybabel compile -d src/dtop_energycapt/service/translations
```
It will create `src/dtop_energycapt/service/translations/fr/LC_MESSAGES/messages.mo` binary file.
This file is used by [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

Tools:
- [poEdit](https://www.poedit.net/)

Documentation:
- [I18n and L10n](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xiii-i18n-and-l10n)
- [Babel](http://babel.pocoo.org/)
- [Flask-Babel](https://pythonhosted.org/Flask-Babel/)


# Service API documentation

For service API description there is [OpenAPI Specification](https://openapis.org).
See [openapi.json](src/dtop_energycapt/service/static/openapi.json).

For API documentation page there is [ReDoc](https://github.com/Rebilly/ReDoc) tool.
It is OpenAPI/Swagger-generated API Reference Documentation.
See [templates/api/index.html](src/dtop_energycapt/service/templates/api/index.html) and
[api/foo/__init__.py](src/dtop_energycapt/service/api/foo/__init__.py).

For generating OpenAPI files from code comments there is tool [oas](https://openap.is).

Install oas with [Node.js npm](https://nodejs.org/):
```bash
npm install oas -g
```
Init template:
```bash
oas init
```
See [swagger.yaml](swagger.yaml).

Generate OpenAPI description:
```bash
oas generate > src/dtop_energycapt/service/static/openapi.json
```
See [openapi.json](src/dtop_energycapt/service/static/openapi.json).

Documentation:
- [ReDoc](https://github.com/Rebilly/ReDoc)
- [OpenAPI Specification by Swagger](https://swagger.io/specification/)
- [oas](https://openap.is)
- [OpenAPI Tools](https://openapi.tools)


# GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file tells the GitLab runner what to do.

GitLab offers a CI (Continuous Integration) service.
CI is for build and check each modification in repository.

Documentation:
- [Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)
- [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README.md)


# Start Service

Install `python-dotenv` for taking service default configuration from `.flaskenv`:

```DOS .bat
pip install python-dotenv
```

It is recommended to create the file `.flaskenv` at the highest folder level
The `.flaskenv` file should contain

```DOS .bat
FLASK_APP=dtop_energycapt.service
FLASK_ENV=development
FLASK_RUN_PORT=5000
```

Without `.flaskenv` you should set environment variables for `flask`:

```DOS .bat
set FLASK_APP=dtop_energycapt.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```
and issue the following command
```DOS .bat
export FLASK_APP=src.dtop_energycapt.service
```

Initialize database:
```DOS .bat
flask init-db
```
Database will be created in `instance/dtop_main.sqlite`.

Start the service:
```DOS .bat
flask run
```

Now you can open in browser this URL: http://localhost:5000/.

## General remarks

**Note** that on Windows use [bash] shell to run the discussed commands

All *local* module tests can be run via a single command

```shell
make
```

To perform some preliminary check on how this module will be tested *externally*, at GitLab CI, use the following command

```shell
make --file ci.makefile
```

**Note** that testing procedures generate some artifacts. These artifacts are kept for performance reasons; to make iterative development & testing cycle seamless. To make 100% sure that the module will work properly everywhere it is suggested to call `make` utility with `clean` rule before changes are pushed into the repository. For example

```shell
make --file ci.makefile clean
```

## E2E Frontend Testing

End-to-End (E2E) testing is one of final valuable indicators of a module progress and a quality.
However, this can be quite time consuming and depending on a stable configuration.

For achievement of good balance between unit tests and E2E tests, it is desirable to provide a sufficient amount of unit tests.

Use the next command for running tests
```bash
make --file e2e.makefile frontend-e2e-test
```

To get more technical details, please consider reading the dedicated [wiki page](https://gitlab.com/opencascade/dtocean/wiki/-/blob/master/dtoceanplus/E2E-tests-via-Cypress.md)


## Frontend unit test current coverage

```bash
make --file e2e.makefile fe-utest-current
```

## Frontend unit test fixed coverage

```bash
make --file e2e.makefile fe-utest-fixed
```
