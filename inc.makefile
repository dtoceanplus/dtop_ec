MODULE_NICKNAME := ec
LOCAL_IMAGE_PREFIX := dtop:$(MODULE_NICKNAME)

module-nickname:
	@echo $(MODULE_NICKNAME)


define image_names
	$(eval IMAGE := $(firstword $(subst -, ,$(1))))
	$(eval LOCAL_NAME := $(LOCAL_IMAGE_PREFIX)-$(IMAGE))
	$(eval REMOTE_NAME := $(CI_REGISTRY_IMAGE):$(MODULE_NICKNAME)-$(IMAGE))
endef


define image_push
	$(call image_names,$(1))
	docker tag ${LOCAL_NAME} $(REMOTE_NAME)
	docker push $(REMOTE_NAME)
endef


define image_pull
	$(call image_names,$(1))
	docker pull $(REMOTE_NAME)
	docker tag $(REMOTE_NAME) $(LOCAL_NAME)
endef
