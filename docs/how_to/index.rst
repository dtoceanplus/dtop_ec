.. _ec-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Energy Capture module. 
These guides are intended for users who have previously completed all the :ref:`Energy Capture tutorials <ec-tutorials>` and have a good knowledge of the features and workings of the Energy Capture module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

#. :ref:`ec-how-to-example`

.. toctree::
   :maxdepth: 1
   :hidden:

   example
