.. _ec-business-logic:

Business Logic
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

Tidal Energy Converter
----------------------------------

.. automodule:: dtop_energycapt.business.iarray.IArray
   :members:
   :undoc-members:
   :show-inheritance:
