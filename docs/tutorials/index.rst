.. _ec-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Energy Capture tool.
They are intended for those who are new to the Energy Capture tool.

.. Note:: The creation and management of projects, studies and entities is out of the scope of the EC tool documentation. Please refer to the general DTOcean+ documentation.

**Using the Energy Capture tool**

The workflow to use and complete an Energy Capture analysis is divided in two tutorials:

#. :ref:`Enter Site and Machine inputs <ec-site-machine-tutorial>`
#. :ref:`Enter Layout and Calculation <ec-layout-tutorial>`

The tutorials are sequential so it is suggested to follow them in the correct order.

The inputs are divided in three tabs:
    #. Site - collects the information about the specific site selected for the deployment
    #. Machine - collects the information about the machine dimensions and working principle as isolated unit
    #. Layout - collects the information on how the machine should be placed in the lease area.

While the Site and Machine workflow will change slightly for the standalone or integrated mode, the specification of the Layout parameter is not affected.

The base machine used in this tutorials is the reference model 1 (RM1) from Sandia reference model project (https://energy.sandia.gov/programs/renewable-energy/water-power/projects/reference-model-project-rmp/)

The RM1 is bottom fixed double rotor tidal turbine, with a hub height of 45m, and a rotor diameter of 10m. The rotors are 20 m apart in the horizontal plane.

The tutorial will be run at low complexity, since the data requirement for the stand alone case is minimal and easy to follow.
For the high complexity case the data requirement is specified in the How-to-Guides.

.. toctree::
   :maxdepth: 1
   :hidden:

   ec-site-machine-tutorial
   ec-layout-tutorial



   
