.. _ec-site-machine-tutorial: 

Site and Machine Inputs
========================

The action required to complete this section are different for the standalone or integrated mode.
The two cases will be presented below.

Enter inputs - Integrated Case
-------------------------------
In integrated mode, the user is asked to run the Site and Machine characterization tools in advance. Therefore, the data required by the EC tools, 
is available on the specific server and ready to be collected.

For both Site and Machine the user is only asked to click ``Fetch Site`` or ``Fetch Machine`` in the respective tab.
This action will trigger a communication with either Site or Machine characterization tools servers.
After the successfull completion of the request, a short summary of the data is presented at the bottom of the page.
Further informations about the data can be found in the specific module.

After completion of both Site and Machine tabs, the progress bar will be set at 36%

Enter inputs - Standalone Case
-------------------------------

For the stanalone case, the user is asked to prepare two seprated JSON files, to be uploaded in the EC tools.
Both Site and Machine tabs will present a ``Drag&Drop`` area and an ``Upload`` button. 
Once the data is correctly uploaded and saved in the EC tool internal storage system, the summary will be shown at the bottom of the page.

For the case of the RM1 machine at complexity 1 (low) the JSON files must be formatted as follow:

``site_RM1.json``

.. code-block::

    {
        "complexity": 1,
        "type": "TEC",
        "data": {
            "loc_resource": 1.5058,
            "loc_position": [
                [
                    533682.00, 5233701.00
                ],
                [
                    532815.97, 5234201.00
                ],
                [
                    534615.97, 5237318.70
                ],
                [
                    535482.00, 5236818.70
                ]
            ]
        },
        "url": "site_1_TEC.json"
    }

``machine_RM1.json``

.. code-block::

   {
        "general": {
            "connector_type": "wet",
            "constant_power_factor": 0.98,
            "floating": false,
            "machine_cost": 1960000,
            "materials": [
                {
                    "material_name": "undefined",
                    "material_quantity": 219370
                }
            ],
            "max_installation_water_depth": -45,
            "min_installation_water_depth": -67.5,
            "min_interdistance_x": 50,
            "min_interdistance_y": 50,
            "preferred_fundation_type": "pile",
            "rated_capacity": 1100,
            "rated_voltage": 11000
        },
        "dimensions": {
            "beam_wet_area": 1,
            "characteristic_dimension": 20,
            "draft": 0,
            "dry_frontal_area": 0,
            "dry_profile": null,
            "footprint_radius": 20,
            "height": 30,
            "hub_height": 30,
            "length": 3.5,
            "mass": 219370,
            "submerged_volume": 433,
            "wet_area": 165,
            "wet_frontal_area": 165,
            "wet_profile": null,
            "width": 3.5
        },
        "model": {
            "cp": 0.55,
            "number_rotor": 2
        },
        "hydro": {}
    }