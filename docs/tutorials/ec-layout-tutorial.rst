.. _ec-layout-tutorial: 

Layout and Calculation Results
===============================

Enter Layout Inputs
--------------------

After the Site and Machine data are given in their respective tabs, the user can enter the Layout definition.
In this tutorial the focus is given to the analysis of a predefined array layout. 
Further info about optimization are given in the How-to-Guide.

The user should first select ``User Specified`` in the ``Farm Layout Type`` multiple choice.

After the user can define the device positioning and orientation using the table on the right hand side of the page.
The table can be operated in two modes.

    #. Use the ``Add New Device`` button to introduce a new device in the list, the data for each device can be edit directly in the table.
    #. Upload the layout from an excel file using the ``Upload XLS``

The excel file must be formatted as follow, please not that the easting and northig coordinates are given in local UTM (m) and the orientation angle is in degrees:

.. Note:: The system will not recognize data given with ``,`` as decimal separator. E.g. 533811,6516 is NOT a valid format, while 533811.6516 is CORRECT. Change your excel configuration if required.

.. list-table:: Layout Excel Format ``layout_rm1.xls``
   :widths: 50 25 25 
   :header-rows: 1
   :width: 100 %

   * - easting
     - northing
     - orientation
   * - 533811.6516
     - 5254315.113
     - 0
   * - 533705.8036
     - 5234381.605
     - 0
   * - 533599.9556
     - 5234448.097
     - 0
   * - 533494.1077
     - 5234514.59
     - 0
   * - 533388.2597	
     - 5234581.082
     - 0
   * - 533282.4118	
     - 5234647.574
     - 0
   * - 533599.7023	
     - 5234918.27
     - 0
   * - 533705.5503	
     - 5234851.778
     - 0
   * - 533811.3983	
     - 5234785.286
     - 0
   * - 533917.2462	
     - 5234718.794
     - 0


To delete a table row or to delete the full table it is possible to use the ``Bin Icon`` or ``Delete All Devices`` respectively.

The data in the table is rendered on the layout plot, to help the user to select the correct device position.

Once the layout is finalized, click the ``Submit`` button to save the data.
If some of the devices are outside the lease area, a warnign is issued, while if no device is inside the lease area or 
if some device have duplicated position an error massage is raised and the data is NOT saved to the EC internal storage system.

.. Warning:: The data is not saved to the Ec internal storage until the user click the ``Submit`` button.

Estimate Array Performance 
--------------------------

After the data is correctly saved to the EC internal storage, the ``Calculate`` button is activated.

To run the analysis, click on the ``Calculate`` button and follow the given instruction.
If succesfull, the operation will redirect to the output page.

The page contains a calculation progression bar, to give the user a basic indication of the calculation status, there is also a ``Log``
button in the output page and in the top navigation bar, with more informations about the actual calculation progress and potential error or warning.

When the calculation is finished the results are rendered in the Output page.
