.. _ec-home:

Energy Capture
========================

Introduction 
------------

The Energy Capture (EC) module will: 
   * Compute the energy production of a farm of either Tidal or Wave energy converters, given the machine and site characteristic at the different level of complexity of the project. 
   * Indicate the machines layout that maximizes the energy production of the farm from an hydrodynamic point of view; excluding energy transformation, energy deliver efficiencies and downtimes.
   * Verify a given machines layouts, designed by the user and return its energy and power production.
   * Estimate parameters that have to be used by other module of the tool to design the different sub-part of the overall project. 
   * Present the results to the user for easy analysis.
The EC modules’ objective are applied to all three levels of complexity proposed in the DTOceanPlus suite of tools. 
These levels (low/mid/high - 1/2/3) corresponds roughly to the maturity of the project and have a direct relation to the complexity of the underpinned model.
This fact has two major implications for the user.
   #. The input data requirement is kept to minimum at the low complexity, assuming the user to have little knowledge yet on the technology and the possible site.
   #. At high complexity the user has a deep knowledge of the machine and the site, and it is assumed to be able to provide a congruent set of inputs.

At low complexity the results accuracy is low as a direct consequence of the fewer information provided by the user.
On the contrary, at high complexity, the expected results accuracy is mid/high.
The accuracy fallow the simple software rule: garbage-in-garbage-out, therefore the results are a direct consequence of the inputs provided.

Structure
---------

This module's documentation is divided into four main sections:

- :ref:`ec-tutorials` to give step-by-step instructions on using the Energy Capture tool for new users. 

- :ref:`ec-how-to` that show how to achieve specific outcomes using the Energy Capture tool. 

- A section on :ref:`background, theory and calculation methods <ec-explanation>` that describes how Energy Capture works and aims to give confidence in the tools. 

- The :ref:`API reference <ec-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The EC Module produces outputs in the following areas: 
   #. Energy Production: EC estimates the gross energy production of the array and the individual machines. The energy is estimated before the introduction of cable and generator efficiencies or downtime of the systems. 
   #. Hydrodynamic and Farm Efficiency: EC estimates the power conversion efficiency of the machines as well as the farm efficiency (q-factor). The latter is estimated as the average power production of the farm over the power produced by an equal number of standalone machines. 
   #. Farm Layout: EC estimates the “best” placement of the machines within the given lease area. In this context the best layout is defined based on the maximization of the annual energy production, with the given constraints in the minimum q-factor and the minimum distance between devices.
   #. Machine motion and load: EC will estimate the response amplitude operator for the machine displacement and load within the array. These are sequentially used by other modules for the design of mooring system and electrical cables or for the estimation of the system reliability.
The module will work at three levels of complexity, namely early/mid/late stage. In the documentation the terms early/mid/late, low,/mid/high and 1/2/3 will refer to the same meaning.
The three levels will have an increasing requirement in terms of input data but also a decreasing range of output accuracy. 
At the current stage of development, the tool generates the same output structure for all levels of complexity.


Workflow for using the Energy Capture module
---------------------------------------------
The workflow for using the Energy Capture module can be summarised in three steps
1) provide inputs, 2) perform a design, and 3) view the results, as shown below. 

.. image:: figs/workflow_ec.jpg
   :alt: Diagram showing high level process for EC: inputs, design, outputs

Overview of Energy Capture data requirements
---------------------------------------------

This section summarises the types of input data required to run the Energy Capture module. Full details and data specifications are given in the how to guide on preparing data.
The required and optional inputs to run the module are summarised in the tables below.
Note that in integrated mode, the required inputs will all come from other modules except for the farm definition inputs.

Required inputs for the Energy Capture module
----------------------------------------------

+-----------------+-----------------------------+--------------------------------------+
| Section         | Low complexity              | Full complexity                      |
+=================+=============================+======================================+
| Site            | Average energy flux or      | Bathymetry data, joint probability   |
|                 |                             |                                      |
| characteristics | average velocity and        | of occurence for wave or tidal       |
|                 |                             |                                      |
|                 | lease area information      |                                      |
+-----------------+-----------------------------+--------------------------------------+
| Device          | Device rated power (kW)     | Device rated power (kW)              |
| characteristics |                             |                                      |
|                 | Device characteristic       | Device characteristic                |
|                 | dimension                   | dimension                            |
|                 |                             |                                      |
|                 | Device efficiency (cp/cwr)  | Single device model                  |
|                 |                             |                                      |
|                 |                             | Array interaction model (Wave Only)  |
+-----------------+-----------------------------+--------------------------------------+
| Farm            | Number of devices           | Number of devices                    |
| definition      |                             |                                      |
|                 | Array layout (m)            | Array layout (m)                     |
|                 |                             |                                      |
|                 | Optimization Parameters     | Optimization Parameters              |
+-----------------+-----------------------------+--------------------------------------+







