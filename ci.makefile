makeCI_REGISTRY_PASSWORD ?= 8c91565f-928c-4116-b9ef-a6eea38d5e21
CI_REGISTRY_USER ?= dtoceanplus2integration
CI_REGISTRY ?= docker.io


REPO_NAME = ci
REGISTRY_PORT = 5500
CI_REGISTRY_IMAGE ?= localhost:$(REGISTRY_PORT)/$(REPO_NAME)
MODULE_NICKNAME = $(shell make module-nickname)

DTOP_BASIC_AUTH=Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE=

all: registry-start \
	dind-push dind-pull \
	backend-build backend-push \
	frontend-push cypress-push frontend-e2e-test frontend-utest-current frontend-utest-fixed
	
include inc.makefile

clean: registry-remove
	export CI_REGISTRY_IMAGE=$(CI_REGISTRY_IMAGE) && \
	docker rmi --force $(shell docker images --filter "reference=${CI_REGISTRY_IMAGE}*" --quiet) | true
	git clean -fxd
	docker system prune --force

registry-start: registry-remove
	docker run --detach --publish $(REGISTRY_PORT):5000 --restart always --name registry registry:2

registry-remove:
	docker container stop registry | true
	docker container rm --volumes registry | true

dind-image:
	$(call image_names,$(@))
	docker build --tag ${LOCAL_NAME} \
	--file dind.dockerfile .

dind-push: dind-image
	$(call image_push,$(@))

dind-pull:
	$(call image_pull,$(@))

backend-image:
	$(call image_names,$(@))
	docker build --tag ${LOCAL_NAME} \
	--file backend.dockerfile .

backend-push: backend-image
	$(call image_push,$(@))

backend-pull:
	$(call image_pull,$(@))

frontend-image:
	$(call image_names,$(@))
	cd src/dtop_energycapt/frontend && \
	docker build --tag ${LOCAL_NAME} \
	--file client.dockerfile .

frontend-push: frontend-image
	$(call image_push,$(@))

frontend-pull:
	$(call image_pull,$(@))

frontend-utest-current: frontend-pull
	$(call image_names,$(@))
	docker run --rm \
	--volume $(PWD)/src/dtop_energycapt/frontend/src:/app/src \
	--volume $(PWD)/src/dtop_energycapt/frontend/tests:/app/tests \
	--workdir /app \
	${LOCAL_NAME} \
	make fe-utest-current

frontend-utest-fixed: frontend-pull
	$(call image_names,$(@))
	docker run --rm \
	--volume $(PWD)/src/dtop_energycapt/frontend/src:/app/src \
	--volume $(PWD)/src/dtop_energycapt/frontend/tests:/app/tests \
	--workdir /app \
	${LOCAL_NAME} \
	make fe-utest-fixed

cypress-image:
	$(call image_names,$(@))
	docker build --tag ${LOCAL_NAME} \
	--file ./test/e2e-cypress/Dockerfile \
	./test/e2e-cypress

cypress-push: cypress-image
	$(call image_push,$(@))

cypress-pull:
	$(call image_pull,$(@))

frontend-e2e-test:
	$(call image_pull,cypress)
	$(call image_pull,backend)
	$(call image_pull,frontend)
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --remove-orphans --abort-on-container-exit --exit-code-from=e2e-cypress nginx ec client mc.dto.test sc.dto.test e2e-cypress

## !
## Set the required module nickname
MODULE_SHORT_NAME=ec

## !
## Set the required docker image TAG
MODULE_TAG=v1.1.6

## !
## Update registry image url
CI_REGISTRY_IMAGE?= registry.gitlab.com/dtoceanplus/dtop_ec
                                        
login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	# Set your GitLab "user name" to <name> and "password" to <pass>	
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	# Set your GitLab "user name" to <name> and "password" to <pass>	
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	  --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
	  --file ./src/dtop_energycapt/frontend/frontend-prod.dockerfile \
	  ./src/dtop_energycapt
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images
